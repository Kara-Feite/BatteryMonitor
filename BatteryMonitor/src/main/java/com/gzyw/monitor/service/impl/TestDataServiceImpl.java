package com.gzyw.monitor.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.ArrayUtil;
import com.gzyw.monitor.common.util.DataUtil;
import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.common.util.JfreeUtil;
import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.AlarmDao;
import com.gzyw.monitor.dao.BatteryDao;
import com.gzyw.monitor.dao.LimitDao;
import com.gzyw.monitor.dao.TestDataDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;
import com.gzyw.monitor.entity.Battery;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.Limit;
import com.gzyw.monitor.entity.TestData;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.TestDataService;

/**
 * 
 * @ClassName: TestDataServiceImpl
 * @Description: 测试数据服务层接口实现类
 * @author LW
 * @date 2018年6月28日 下午4:44:08
 */
@Service
public class TestDataServiceImpl implements TestDataService {
	
	/**
	 * 告警门限
	 */
	public static Map<Integer, Map<Integer,Limit>> mapLimit = new HashMap<Integer, Map<Integer,Limit>>();
	
	/**
	 * 电池持久层接口
	 */
	@Autowired
	private BatteryDao batteryDao;
	
	/**
	 * 测试数据持久层接口
	 */
	@Autowired
	private TestDataDao testDataDao;
	
	@Autowired
	private LimitDao limitDao;

	@Autowired
	private TreeDao treeDao;
	
	@Autowired
	private AlarmDao alarmDao;

	@Override
	public void saveData(String voltage, String inter, String tempera, Group group, Date date) {
		if (StringUtil.isEmpty(voltage) || StringUtil.isEmpty(inter) || StringUtil.isEmpty(tempera)) {
			return;
		}
		// 进行crc校验并获取对应数组格式数据
		// 电压数据
		String[] voltages = DataUtil.toDataArr(voltage);
		if (ArrayUtil.isEmpty(voltages)) {
			return;
		}
		// 电阻数据
		String[] inters = DataUtil.toDataArr(inter);
		if (ArrayUtil.isEmpty(inters)) {
			return;
		}
		// 温度数据
		String[] temperas = DataUtil.toDataArr(tempera);
		if (ArrayUtil.isEmpty(temperas)) {
			return;
		}
		Integer groupId = group.getGroupId();
		List<Battery> listBattery = batteryDao.listBatteryByGroup(groupId);
		int len = NumberUtil.hexToDec(voltages[2]);
		TestData data = null;
		Battery battery = null;
		Map<Integer,Limit> map = null;
		if (!mapLimit.containsKey(groupId) || mapLimit.get(groupId) == null) {
			Map<Integer, Limit> map2 = new HashMap<Integer, Limit>();
			List<Limit> listLimit = limitDao.listLimitByGroup(groupId);
			for (Limit limit : listLimit) {
				String type = limit.getType();
				Integer key = 1;
				if ("单体内阻/mΩ".equals(type)) {
					key = 2;
				} else if ("单体温度/℃".equals(type)) {
					key = 3;
				}
				map2.put(key, limit);
			}
			mapLimit.put(groupId, map2);
		}
		map = mapLimit.get(groupId);
		double voltageVal = 0;
		double interVal = 0;
		// 循环添加数据
		for (int i = 3,j = 3; i < len + 3; i += 2,j++) {
			try {
				data = new TestData();
				battery = listBattery.get(i - j);
				voltageVal = NumberUtil.hexToDouble(voltages[i] + voltages[i + 1]);
				data.setVoltage(voltageVal);
				
				interVal = NumberUtil.hexToDouble(inters[i] + inters[i + 1]);
				data.setInter(interVal);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			double temperaVal = NumberUtil.hexToDouble(temperas[i] + temperas[i + 1], 2);
			data.setTempera(temperaVal);
			saveAlarmByVal(map, group, battery, voltageVal, interVal, temperaVal);
			data.setBatteryName(battery.getName());
			data.setGroup(group);
			data.setBattery(battery);
			data.setTestTime(date);
			data.setType(0);
			testDataDao.save(data);
		}
	}
	
	/**
	 * 
	 * @Title: saveAlarmByVal
	 * @Description: 添加警告
	 * @param map 告警门限集合
	 * @param group 电池组
	 * @param battery 电池
	 * @param voltageVal 电压值
	 * @param interVal 内阻值
	 * @param temperaVal 温度值
	 */
	private void saveAlarmByVal(Map<Integer,Limit> map, Group group, Battery battery, double voltageVal, double interVal, double temperaVal) {
		Limit limitVoltage = map.get(1);
		Limit limitInter = map.get(2);
		Limit limitTemper = map.get(3);
		Date date = new Date();
		Integer groupId = group.getGroupId();
		Tree tree = treeDao.get(groupId);
		Alarm alarm = new Alarm();
		alarm.setGroup(group);
		alarm.setAlarmTime(date);
		alarm.setName(tree.getName() + ">" + battery.getName());
		if (limitVoltage != null) {
			alarm.setType("单体电压/V");
			doSaveAlarm(limitVoltage, voltageVal, alarm, tree, battery);
		}
		if (limitInter != null) {
			alarm.setType("单体内阻/mΩ");
			doSaveAlarm(limitVoltage, voltageVal, alarm, tree, battery);
		}
		if (limitTemper != null) {
			alarm.setType("单体温度/℃");
			doSaveAlarm(limitVoltage, voltageVal, alarm, tree, battery);
		}
	}

	/**
	 * 
	 * @Title: getAlarmByLimit
	 * @Description: 根据告警门限设置并获取告警
	 * @param limit
	 * @param val
	 * @return
	 */
	private Alarm getAlarmByLimit(Limit limit, Integer groupId, double val, Alarm alarm) {
		Double highWarn = limit.getHighWarn();
		Double lowWarn = limit.getLowWarn();
		Double highPlan = limit.getHighPlan();
		Double lowPlan = limit.getLowPlan();
		if (highWarn != null && val > highWarn) {
			alarm.setDetails(val + "高于设定的告警值");
			alarm.setMold("过高告警");
			alarm.setState("告警");
		} else if (highPlan != null && val > highPlan) {
			alarm.setDetails(val + "高于设定的预警值");
			alarm.setMold("过高预警");
			alarm.setState("预警");
		} else if (lowWarn != null && val < lowWarn) {
			alarm.setDetails(val + "低于设定的告警值");
			alarm.setMold("过低告警");
			alarm.setState("告警");
		} else if (lowPlan != null && val < lowPlan) {
			alarm.setDetails(val + "低于设定的预警值");
			alarm.setMold("过低预警");
			alarm.setState("预警");
		}
		if (alarm.getMold() == null) {
			return null;
		}
		Alarm a = alarmDao.getAlarmByExists(alarm, groupId);
		return a == null ? alarm : null;
	}

	@Override
	public GraphDto saveTestData(String voltage, String inter, String tempera, Group group, Date date) {
		TestData data = null;
		Battery battery = null;
		GraphDto graphDto = new GraphDto();
		List<Battery> listBattery = batteryDao.listBatteryByGroup(group.getGroupId());
		List<TestData> listTestData = new ArrayList<TestData>();
		double sumVal = 0;
		double maxVal = 0;
		double minVal = 0;
		if (voltage != null) {
			String[] voltages = DataUtil.toDataArr(voltage);
			int len = NumberUtil.hexToDec(voltages[2]);
			for (int i = 3,j = 3; i < len + 3; i += 2,j++) {
				data = new TestData();
				battery = listBattery.get(i - j);
				double val = NumberUtil.hexToDouble(voltages[i] + voltages[i + 1]);
				data.setInter(0.0);
				data.setTempera(0.0);
				data.setType(1);
				data.setVoltage(val);
				data.setBatteryName(battery.getName());
				data.setGroup(group);
				data.setBattery(battery);
				data.setTestTime(date);
				testDataDao.save(data);
				data.setInter(null);
				data.setTempera(null);
				listTestData.add(data);
				sumVal += val;
				if (val > maxVal) {
					maxVal = val;
				}
				if (val < minVal) {
					minVal = val;
				}
			}
		}
		if (inter != null) {
			String[] inters = DataUtil.toDataArr(inter);
			int len = NumberUtil.hexToDec(inters[2]);
			for (int i = 3,j = 3; i < len + 3; i += 2,j++) {
				data = new TestData();
				battery = listBattery.get(i - j);
				double val = NumberUtil.hexToDouble(inters[i] + inters[i + 1]);
				data.setVoltage(0.0);
				data.setTempera(0.0);
				data.setType(1);
				data.setInter(val);
				data.setBatteryName(battery.getName());
				data.setGroup(group);
				data.setBattery(battery);
				data.setTestTime(date);
				testDataDao.save(data);
				data.setVoltage(null);
				data.setTempera(null);
				listTestData.add(data);
				sumVal += val;
				if (val > maxVal) {
					maxVal = val;
				}
				if (val < minVal) {
					minVal = val;
				}
			}
		}
		if (tempera != null) {
			String[] temperas = DataUtil.toDataArr(tempera);
			int len = NumberUtil.hexToDec(temperas[2]);
			for (int i = 3,j = 3; i < len + 3; i += 2,j++) {
				data = new TestData();
				battery = listBattery.get(i - j);
				double val = NumberUtil.hexToDouble(temperas[i] + temperas[i + 1]);
				data.setVoltage(0.0);
				data.setInter(0.0);
				data.setType(1);
				data.setTempera(val);
				data.setBatteryName(battery.getName());
				data.setGroup(group);
				data.setBattery(battery);
				data.setTestTime(date);
				testDataDao.save(data);
				data.setVoltage(null);
				data.setInter(null);
				listTestData.add(data);
				sumVal += val;
				if (val > maxVal) {
					maxVal = val;
				}
				if (val < minVal) {
					minVal = val;
				}
			}
		}
		int num = listBattery.size();
		double avgVal = sumVal/num;
		graphDto.setAvgVal(avgVal);
		graphDto.setBatteryId(listBattery.get(0).getId());
		graphDto.setGroupId(group.getGroupId());
		graphDto.setListData(listTestData);
		graphDto.setMaxVal(maxVal);
		graphDto.setMinVal(minVal);
		graphDto.setNum(num);
		graphDto.setTime(DateUtil.formatDateTime(date));
		return graphDto;
	}

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, TempDto temp) {
		Long count = testDataDao.countTestData(temp);
		List<TestData> listTestData = testDataDao.listTestDataByPage(page, rows, sort, order, temp);
		return new EUDataGridDto(count, listTestData);
	}

	@Override
	public ResultDto delTestDataByBat(String ids) {
		String[] arr = ids.split(",");
		for (String id : arr) {
			TestData testData = testDataDao.get(id);
			if (testData != null) {
				testDataDao.delete(testData);
			}
		}
		return ResultDto.ok();
	}

	@Override
	public MaxMinDto getMaxMinDto(TempDto temp) {
		return testDataDao.getMaxMinDto(temp);
	}

	@Override
	public GraphDto getGraphDto(TempDto temp) {
		GraphDto graphDto = new GraphDto();
		Integer groupId = temp.getTreeId();
		boolean flag = false;
		if (StringUtil.isEmpty(temp.getType())) {
			temp.setType("单体电压/V");
		}
		if (temp.getVal() == null) {
			Limit limit = limitDao.getLimitByTypeAndGroup(temp.getType(), groupId);
			if (limit != null) {
				graphDto.setLimitVal(limit.getVal());
			} else {
				flag = true;
			}
		} else {
			graphDto.setLimitVal(temp.getVal());
		}
		String type = temp.getType();
		graphDto.setGroupId(groupId);
		graphDto.setType(type);
		graphDto.setTime(temp.getStartTime());
		String unit = type.substring(type.indexOf("/"), type.length());
		graphDto.setUnit(unit );
		String name = "";
		if (type.equals("单体电压/V")) {
			name = "电压";
			temp.setType("voltage");
			if (flag) {
				graphDto.setLimitVal(2.5);
			}
		} else if (type.equals("单体内阻/mΩ")) {
			name = "内阻";
			temp.setType("inter");
			if (flag) {
				graphDto.setLimitVal(0.6);
			}
		} else {
			name = "温度";
			temp.setType("tempera");
			if (flag) {
				graphDto.setLimitVal(30.0);
			}
		}
		graphDto.setName(name);
		List<TestData> listData = testDataDao.listTestDataByGroup(temp);
		graphDto.setListData(listData);
		graphDto.setNum(listData.size());
		return graphDto;
	}

	@Override
	public List<TestData> listTestDataByTime(TempDto temp) {
		return testDataDao.listTestDataByTime(temp);
	}
	
	@Override
	public List<TestData> listTestDataBetTime(TempDto temp) {
		String endTime = temp.getEndTime();
		temp.setStartTime(temp.getStartTime() + " 00:00:00");
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);
		String[] split = dateStr.split(" ");
		if (split[0].equals(endTime)) {
			temp.setEndTime(dateStr);
		} else {
			temp.setEndTime(endTime + " 00:00:00");
		}
		return testDataDao.listTestDataBetTime(temp);
	}

	@Override
	public GraphDto getGraphDtoByBat(TempDto temp) {
		GraphDto graphDto = new GraphDto();
		Integer groupId = temp.getTreeId();
		boolean flag = false;
		if (StringUtil.isEmpty(temp.getType())) {
			temp.setType("单体电压/V");
		}
		if (temp.getVal() == null) {
			Limit limit = limitDao.getLimitByTypeAndGroup(temp.getType(), groupId);
			if (limit != null) {
				graphDto.setLimitVal(limit.getVal());
			} else {
				flag = true;
			}
		} else {
			graphDto.setLimitVal(temp.getVal());
		}
		String type = temp.getType();
		graphDto.setGroupId(groupId);
		graphDto.setType(type);
		graphDto.setTime(temp.getStartTime());
		String unit = type.substring(type.indexOf("/"), type.length());
		graphDto.setUnit(unit );
		String name = "";
		if (type.equals("单体电压/V")) {
			name = "电压";
			temp.setType("voltage");
			if (flag) {
				graphDto.setLimitVal(2.5);
			}
		} else if (type.equals("单体内阻/mΩ")) {
			name = "内阻";
			temp.setType("inter");
			if (flag) {
				graphDto.setLimitVal(0.6);
			}
		} else {
			name = "温度";
			temp.setType("tempera");
			if (flag) {
				graphDto.setLimitVal(30.0);
			}
		}
		graphDto.setName(name);
		List<TestData> listData = testDataDao.listTestDataByBat(temp);
		Collections.reverse(listData);
		graphDto.setListData(listData);
		graphDto.setNum(listData.size());
		return graphDto;
	}

	@Override
	public List<TestData> listTestDataByExport(TempDto temp) {
		return testDataDao.listTestDataByExport(temp);
	}

	@Override
	public void delTestDataByGroup(Integer groupId) {
		testDataDao.delTestDataByGroup(groupId);
	}

	@Override
	public List<TestData> listTestDataByGroup(TempDto temp) {
		return testDataDao.listTestDataByGroup(temp);
	}

	@Override
	public byte[] getChartByGroup(TempDto temp, String name) {
		List<TestData> listTestData = testDataDao.listTestDataByGroup(temp);
		if ("voltage".equals(temp.getType())) {
			temp.setType("单体电压/V");
		} else {
			temp.setType("单体内阻/mΩ");
		}
		Limit limit = limitDao.getLimitByTypeAndGroup(temp.getType(), temp.getTreeId());
		Double limitVal = null;
		if (limit != null) {
			limitVal = limit.getVal();
		}
		JFreeChart chart = JfreeUtil.createXYBarChart(listTestData, limitVal, name);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ChartUtilities.writeChartAsJPEG(bos, chart, 1000, 800);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();
	}

	@Override
	public byte[] getChartByBat(TempDto temp, String name) {
		List<TestData> listTestData = testDataDao.listTestDataByBat(temp);
		JFreeChart chart = JfreeUtil.createLineChart(listTestData, name);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ChartUtilities.writeChartAsJPEG(bos, chart, 1000, 500);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();
	}
	
	/**
	 * 
	 * @Title: updateAlarmByNum
	 * @Description: 更新警告数量
	 * @param oldNum
	 * @param newNum
	 * @param id
	 * @param isWarn
	 */
	private void updateAlarmByNum(int oldNum, int newNum, int id, boolean isWarn) {
		Tree tree = treeDao.getParentTreeById(id);
		if (tree != null) {
			if (isWarn) {
				tree.setWarn(tree.getWarn() - oldNum + newNum);
			} else {
				tree.setPlan(tree.getPlan() - oldNum + newNum);
			}
			treeDao.update(tree);
			updateAlarmByNum(oldNum, newNum, tree.getId(), isWarn);
		}
	}
	
	/**
	 * 
	 * @Title: doSaveAlarm
	 * @Description: 执行添加告警操作
	 * @param limitVoltage
	 * @param voltageVal
	 * @param alarm
	 * @param tree
	 * @param battery
	 */
	private void doSaveAlarm(Limit limitVoltage, double voltageVal, Alarm alarm, Tree tree, Battery battery) {
		int groupId = tree.getId();
		Alarm a = getAlarmByLimit(limitVoltage, groupId, voltageVal, alarm);
		if (a != null) {
			boolean isWarn = alarm.getState().equals("告警");
			int oldNum = isWarn ? tree.getWarn() : tree.getPlan();
			int newNum = oldNum + 1;
			updateAlarmByNum(oldNum, newNum, groupId, true);
			if (isWarn) {
				tree.setWarn(newNum);
				battery.setWarn(newNum);
			} else {
				tree.setPlan(newNum);
				battery.setPlan(newNum);
			}
			treeDao.update(tree);
			batteryDao.update(battery);
			alarmDao.save(a);
		}
	}
	
}
