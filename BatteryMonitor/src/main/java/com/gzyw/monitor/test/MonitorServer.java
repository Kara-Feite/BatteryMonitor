package com.gzyw.monitor.test;

import com.gzyw.monitor.test.handler.ServerHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

/**
 * 
 * @ClassName: MonitorServer
 * @Description: 定时监测服务器
 * @author LW
 * @date 2018年6月25日 下午3:38:45
 */
public class MonitorServer {
	
	public void start(final int port) throws InterruptedException {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workGroup = new NioEventLoopGroup();
		
		ServerBootstrap bootStrap = new ServerBootstrap();
		bootStrap.group(bossGroup, workGroup)
			.channel(NioServerSocketChannel.class)
			.option(ChannelOption.SO_BACKLOG, 1024)
			.childOption(ChannelOption.SO_KEEPALIVE, true)
			.childHandler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline pipeline = ch.pipeline();
					pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4,0,4));
					pipeline.addLast(new LengthFieldPrepender(4));
					pipeline.addLast(new ServerHandler());
				}
				
			});
		ChannelFuture f = bootStrap.bind(port).sync();
		System.out.println("服务已打开，端口号为" + port);
		f.channel().closeFuture().sync();
	}
	
	public static void main(String[] args) {
		try {
			new MonitorServer().start(8080);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}