package com.gzyw.monitor.dao.impl;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.dao.GroupLimitDao;
import com.gzyw.monitor.entity.GroupLimit;

/**
 * 
 * @ClassName: GroupLimitDaoImpl
 * @Description: 组限制持久层接口实现类
 * @author LW
 * @date 2018年6月29日 上午11:03:59
 */
@Repository
public class GroupLimitDaoImpl extends BaseDaoImpl<GroupLimit> implements GroupLimitDao {

}
