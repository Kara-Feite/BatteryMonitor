package com.gzyw.monitor.annotation;
 
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @ClassName: ExcelColumn
 * @Description: Excel列标注
 * @author LW
 * @date 2018年8月7日 下午2:07:33
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelColumn{
	
	/**
	 * 
	 * @Title: row
	 * @Description: 相对于handerName的行
	 * @return
	 */
	int row();
	
	/**
	 * 
	 * @Title: column
	 * @Description: 所在列序号
	 * @return
	 */
	int column();
	
	/**
	 * 
	 * @Title: value
	 * @Description: 值
	 * @return
	 */
	String value();
	
}
