package com.gzyw.monitor.service;

import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ExcelLimitDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.GroupDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: GroupService
 * @Description: 电池组服务层
 * @author LW
 * @date 2018年6月14日 下午4:27:31
 */
public interface GroupService {

	/**
	 * 
	 * @Title: getGroupById
	 * @Description: 根据id获取电池组对象
	 * @param groupId
	 * @return
	 */
	Group getGroupById(Integer groupId);

	/**
	 * 
	 * @Title: getGroupDtoById
	 * @Description: 根据id获取电池组包装类
	 * @param id
	 * @return
	 */
	GroupDto getGroupDtoById(Integer id);

	/**
	 * 
	 * @Title: saveGroup
	 * @Description: 添加电池组
	 * @param groupDto
	 * @return
	 */
	ResultDto saveGroup(GroupDto groupDto);

	/**
	 * 
	 * @Title: updateGroup
	 * @Description: 更新电池组信息
	 * @param groupDto
	 * @return
	 */
	ResultDto updateGroup(GroupDto groupDto);

	/**
	 * 
	 * @Title: listGroupByAll
	 * @Description: 获取所有电池组
	 * @return
	 */
	List<Group> listGroupByAll();

	/**
	 * 
	 * @Title: updateIsRun
	 * @Description: 更新电池组运行状态
	 * @param ids
	 * @param isRun
	 * @return
	 */
	ResultDto updateIsRun(int[] ids, Boolean isRun);

	/**
	 * 
	 * @Title: listGroupByRun
	 * @Description: 获取运行的电池组
	 * @return
	 */
	List<Group> listGroupByRun();

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取电池组分页数据
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Tree tree);

	/**
	 * 
	 * @Title: getTestGraphDto
	 * @Description: 获取测试结果
	 * @param temp
	 * @return
	 */
	GraphDto getTestGraphDto(TempDto temp);

	/**
	 * 
	 * @Title: getSystemInfo
	 * @Description: 获取信息信息
	 * @param temp
	 * @return
	 */
	GroupInfo getSystemInfo(TempDto temp);

	/**
	 * 
	 * @Title: getExcelLimitDto
	 * @Description: 获取Excel门限设置数据
	 * @param treeId
	 * @return
	 */
	ExcelLimitDto getExcelLimitDto(Integer treeId);

}
