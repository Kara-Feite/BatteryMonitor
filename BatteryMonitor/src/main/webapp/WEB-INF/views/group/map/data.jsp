<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>测试数据管理</title>	
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<script>
$(function(){
	$('#node_lists').datagrid({
		url:'${ctx}/test/data/group/datagrid?treeId=${groupId}',
		pagination:true,
		pageSize:20,
		pageList:[10,15,20,25,30],
		pageNumber : 1,
		sortName : 'id',
		sortOrder : 'ASC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[{
				field:'batteryName',
				title:'电 池 名',
				align:'center',
				width:50,
				sortable:true
			},{
				field:'voltage',
				title:'电 压(V)',
				align:'center',
				width:50,
				sortable:true
			},{
				field:'inter',
				title:'内 阻(mΩ)',
				align:'center',
				width:50,
				sortable:true
			},{
				field:'tempera',
				title:'温度(°C)',
				align:'center',
				width:50,
				sortable:true			
			},{
				field:'testTime',
				title:'测试时间',
				align:'center',
				width:70,
				sortable:true,
				formatter : formatDateTime
			}]],
		onDblClickRow : clickRow
	});
});
function test() {
	window.parent.location.href="${ctx}/group/test?id=${groupId}";
}
function query(){
	$('#node_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function exportExcel(){
	var exportForm = document.createElement("form"),
		startTime = $('#search_form').find('[name = startTime]').val(),
		endTime = $('#search_form').find('[name = endTime]').val(),
		treeId = '${groupId}';
	exportForm.id = "exportForm"; 
	exportForm.name = "exportForm"; 
	document.body.appendChild(exportForm); 
	var input1 = document.createElement("input"),
		input2 = document.createElement("input"),
		input3 = document.createElement("input");
	
  	input1.type = "text";
  	input1.name = "startTime";
  	input1.value = startTime;
  	
  	input2.type = "text";
  	input2.name = "endTime";
  	input2.value = endTime;
  	
  	input3.type = "text";
  	input3.name = "treeId";
  	input3.value = treeId;
  	
  	exportForm.appendChild(input1);
  	exportForm.appendChild(input2);
  	exportForm.appendChild(input3);
  	exportForm.method = "POST"; 
  	exportForm.action = "${ctx}/group/export/excel/group?dd=" + new Date(); 
  	exportForm.submit(); 
  	document.body.removeChild(exportForm);
}
function checkExecl(){
	 $('#search_form').form('submit',{
	 	url:'${ctx}/test/data/groupExecl'
	 });
}
function clickRow(index, row) {
	window.parent.location.href="${ctx}/battery/frame?id=" + row.battery.id;
}
</script>
<body>	
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north'" style="height:34px;">
			<form id="search_form">
				<input type="hidden" name="groupId" value="${groupId}">
				<table width="100%" bgcolor="#f5fafe">  		   																														   
					<tr height="25">
						<td id="denglu">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>开始时间：</strong>
							<input name="startTime" autocomplete='off' size="20" editable="false"
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
							&nbsp;&nbsp;
							<strong>结束时间：</strong>
							<input name="endTime" autocomplete='off' size="20" editable="false"
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);" style="width:70px">查  询</a>						
							&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="exportExcel();" href="javascript:void(0);" style="width:70px">导  出</a>
							&nbsp;&nbsp; 
							<a class="easyui-linkbutton" data-options="iconCls:'icon-filter'" 
								onclick="test();" href="javascript:void(0);" style="width:70px">测  试</a>
						</td>					
					</tr>					
				</table>		
			</form>
		</div>
		<!-- ------------------------------------------ -->
		<div data-options="region:'center'">
			<table id="node_lists"></table>
		</div>
	</div>
</body>
</html>