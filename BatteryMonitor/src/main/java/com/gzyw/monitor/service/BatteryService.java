package com.gzyw.monitor.service;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Battery;

/**
 * 
 * @ClassName: BatteryService
 * @Description: 电池服务层
 * @author LW
 * @date 2018年6月14日 下午4:35:11
 */
public interface BatteryService {

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取电池分页数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param battery 电池
	 * @param groupId 电池组id
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Battery battery, Integer groupId);

	/**
	 * 
	 * @Title: getBatteryById
	 * @Description: 根据id获取电池
	 * @param id
	 * @return
	 */
	Battery getBatteryById(Integer id);

	/**
	 * 
	 * @Title: deleteBattery
	 * @Description: 删除电池
	 * @param ids
	 * @return
	 */
	ResultDto deleteBattery(int[] ids);

}
