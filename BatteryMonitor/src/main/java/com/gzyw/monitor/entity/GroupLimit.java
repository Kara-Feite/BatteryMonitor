package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @ClassName: GroupLimit
 * @Description: 电池组限制
 * @author LW
 * @date 2018年6月4日 上午11:11:36
 */
@Entity
@Table(name = "bm_group_limit")
public class GroupLimit implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 8613673366095624925L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电池节数
	 */
	@Column(name = "battery_num", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer batteryNum;

	/**
	 * 电池组电压高越限
	 */
	@Column(name = "high_voltage", nullable = false)
	private Double highVoltage;

	/**
	 * 电池组电压低越限
	 */
	@Column(name = "low_voltage", nullable = false)
	private Double lowVoltage;

	/**
	 * 电池组放电过流门限
	 */
	@Column(name = "discharge", nullable = false)
	private Double discharge;

	/**
	 * 开路电压门限
	 */
	@Column(name = "open_circuit", nullable = false)
	private Double openCircuit;

	/**
	 * 内阻高越限
	 */
	@Column(name = "high_inter", nullable = false)
	private Double highInter;

	/**
	 * 均充电流判定门限
	 */
	@Column(name = "charge_judge", nullable = false)
	private Double chargeJudge;

	/**
	 * 均充电流判定门限
	 */
	@Column(name = "open_circuit_judge", nullable = false)
	private Double openCircuitJudge;

	/**
	 * 环境温度高越限
	 */
	@Column(name = "high_tempera", nullable = false)
	private Double highTempera;

	/**
	 * 环境温度低越限
	 */
	@Column(name = "low_tempera", nullable = false)
	private Double lowTempera;

	/**
	 * 电压均衡始能
	 */
	@Column(name = "voltage_equaliza", nullable = false)
	private Double voltageEqualiza;

	/**
	 * 本机通信地址号
	 */
	@Column(name = "socket_addr", length = 32, nullable = false)
	private String socketAddr;

	/**
	 * 本机通信波特率
	 */
	@Column(name = "baud_rate", nullable = false)
	private Double baudRate;

	/**
	 * 电池组
	 */
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;

	/**
	 * 单体限制
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "mon_limit_id")
	private MonomerLimit monomerLimit;

	/**
	 * 内阻测试时间间隔
	 */
	@Column(name = "inter_test_interval", nullable = false)
	private Integer interTestInterval;

	/**
	 * 历史存储时间间隔
	 */
	@Column(name = "storage_interval", nullable = false)
	private Integer storageInterval;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * @Title: GroupLimit
	 * @Description: GroupLimit构造函数
	 */
	public GroupLimit() {}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getBatteryNum
	 * @Description: 获取batteryNum
	 * @return: Integer batteryNum
	 */
	public Integer getBatteryNum() {
		return batteryNum;
	}

	/**
	 * @Title: setBatteryNum
	 * @Description: 设置batteryNum
	 */
	public void setBatteryNum(Integer batteryNum) {
		this.batteryNum = batteryNum;
	}

	/**
	 * @Title: getHighVoltage
	 * @Description: 获取highVoltage
	 * @return: Double highVoltage
	 */
	public Double getHighVoltage() {
		return highVoltage;
	}

	/**
	 * @Title: setHighVoltage
	 * @Description: 设置highVoltage
	 */
	public void setHighVoltage(Double highVoltage) {
		this.highVoltage = highVoltage;
	}

	/**
	 * @Title: getLowVoltage
	 * @Description: 获取lowVoltage
	 * @return: Double lowVoltage
	 */
	public Double getLowVoltage() {
		return lowVoltage;
	}

	/**
	 * @Title: setLowVoltage
	 * @Description: 设置lowVoltage
	 */
	public void setLowVoltage(Double lowVoltage) {
		this.lowVoltage = lowVoltage;
	}

	/**
	 * @Title: getDischarge
	 * @Description: 获取discharge
	 * @return: Double discharge
	 */
	public Double getDischarge() {
		return discharge;
	}

	/**
	 * @Title: setDischarge
	 * @Description: 设置discharge
	 */
	public void setDischarge(Double discharge) {
		this.discharge = discharge;
	}

	/**
	 * @Title: getOpenCircuit
	 * @Description: 获取openCircuit
	 * @return: Double openCircuit
	 */
	public Double getOpenCircuit() {
		return openCircuit;
	}

	/**
	 * @Title: setOpenCircuit
	 * @Description: 设置openCircuit
	 */
	public void setOpenCircuit(Double openCircuit) {
		this.openCircuit = openCircuit;
	}

	/**
	 * @Title: getHighInter
	 * @Description: 获取highInter
	 * @return: Double highInter
	 */
	public Double getHighInter() {
		return highInter;
	}

	/**
	 * @Title: setHighInter
	 * @Description: 设置highInter
	 */
	public void setHighInter(Double highInter) {
		this.highInter = highInter;
	}

	/**
	 * @Title: getChargeJudge
	 * @Description: 获取chargeJudge
	 * @return: Double chargeJudge
	 */
	public Double getChargeJudge() {
		return chargeJudge;
	}

	/**
	 * @Title: setChargeJudge
	 * @Description: 设置chargeJudge
	 */
	public void setChargeJudge(Double chargeJudge) {
		this.chargeJudge = chargeJudge;
	}

	/**
	 * @Title: getOpenCircuitJudge
	 * @Description: 获取openCircuitJudge
	 * @return: Double openCircuitJudge
	 */
	public Double getOpenCircuitJudge() {
		return openCircuitJudge;
	}

	/**
	 * @Title: setOpenCircuitJudge
	 * @Description: 设置openCircuitJudge
	 */
	public void setOpenCircuitJudge(Double openCircuitJudge) {
		this.openCircuitJudge = openCircuitJudge;
	}

	/**
	 * @Title: getHighTempera
	 * @Description: 获取highTempera
	 * @return: Double highTempera
	 */
	public Double getHighTempera() {
		return highTempera;
	}

	/**
	 * @Title: setHighTempera
	 * @Description: 设置highTempera
	 */
	public void setHighTempera(Double highTempera) {
		this.highTempera = highTempera;
	}

	/**
	 * @Title: getLowTempera
	 * @Description: 获取lowTempera
	 * @return: Double lowTempera
	 */
	public Double getLowTempera() {
		return lowTempera;
	}

	/**
	 * @Title: setLowTempera
	 * @Description: 设置lowTempera
	 */
	public void setLowTempera(Double lowTempera) {
		this.lowTempera = lowTempera;
	}

	/**
	 * @Title: getVoltageEqualiza
	 * @Description: 获取voltageEqualiza
	 * @return: Double voltageEqualiza
	 */
	public Double getVoltageEqualiza() {
		return voltageEqualiza;
	}

	/**
	 * @Title: setVoltageEqualiza
	 * @Description: 设置voltageEqualiza
	 */
	public void setVoltageEqualiza(Double voltageEqualiza) {
		this.voltageEqualiza = voltageEqualiza;
	}

	/**
	 * @Title: getSocketAddr
	 * @Description: 获取socketAddr
	 * @return: String socketAddr
	 */
	public String getSocketAddr() {
		return socketAddr;
	}

	/**
	 * @Title: setSocketAddr
	 * @Description: 设置socketAddr
	 */
	public void setSocketAddr(String socketAddr) {
		this.socketAddr = socketAddr;
	}

	/**
	 * @Title: getBaudRate
	 * @Description: 获取baudRate
	 * @return: Double baudRate
	 */
	public Double getBaudRate() {
		return baudRate;
	}

	/**
	 * @Title: setBaudRate
	 * @Description: 设置baudRate
	 */
	public void setBaudRate(Double baudRate) {
		this.baudRate = baudRate;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getMonomerLimit
	 * @Description: 获取monomerLimit
	 * @return: MonomerLimit monomerLimit
	 */
	public MonomerLimit getMonomerLimit() {
		return monomerLimit;
	}

	/**
	 * @Title: setMonomerLimit
	 * @Description: 设置monomerLimit
	 */
	public void setMonomerLimit(MonomerLimit monomerLimit) {
		this.monomerLimit = monomerLimit;
	}

	/**
	 * @Title: getInterTestInterval
	 * @Description: 获取interTestInterval
	 * @return: Integer interTestInterval
	 */
	public Integer getInterTestInterval() {
		return interTestInterval;
	}

	/**
	 * @Title: setInterTestInterval
	 * @Description: 设置interTestInterval
	 */
	public void setInterTestInterval(Integer interTestInterval) {
		this.interTestInterval = interTestInterval;
	}

	/**
	 * @Title: getStorageInterval
	 * @Description: 获取storageInterval
	 * @return: Integer storageInterval
	 */
	public Integer getStorageInterval() {
		return storageInterval;
	}

	/**
	 * @Title: setStorageInterval
	 * @Description: 设置storageInterval
	 */
	public void setStorageInterval(Integer storageInterval) {
		this.storageInterval = storageInterval;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GroupLimit [id=" + id + ", batteryNum=" + batteryNum + ", highVoltage=" + highVoltage + ", lowVoltage="
				+ lowVoltage + ", discharge=" + discharge + ", openCircuit=" + openCircuit + ", highInter=" + highInter
				+ ", chargeJudge=" + chargeJudge + ", openCircuitJudge=" + openCircuitJudge + ", highTempera="
				+ highTempera + ", lowTempera=" + lowTempera + ", voltageEqualiza=" + voltageEqualiza + ", socketAddr="
				+ socketAddr + ", baudRate=" + baudRate + ", group=" + group + ", monomerLimit=" + monomerLimit
				+ ", interTestInterval=" + interTestInterval + ", storageInterval=" + storageInterval + ", createTime="
				+ createTime + "]";
	}

}
