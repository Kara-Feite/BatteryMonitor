package com.gzyw.monitor.dto;

import java.io.Serializable;
import java.util.List;

import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: GraphDto
 * @Description: 统计图数据包装类
 * @author LW
 * @date 2018年7月13日 下午1:57:11
 */
public class GraphDto implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -4443400945625429077L;

	/**
	 * 电池组id
	 */
	private Integer groupId;
	
	/**
	 * 电池id
	 */
	private Integer batteryId;
	
	/**
	 * 测试类型
	 */
	private String type;
	
	/**
	 * 电池组电池数
	 */
	private Integer num;
	
	/**
	 * 测试别称(电压、电阻、温度)
	 */
	private String name;
	
	/**
	 * 单位
	 */
	private String unit;
	
	/**
	 * 测试数据
	 */
	private List<TestData> listData;
	
	/**
	 * 警告参考值
	 */
	private Double limitVal;
	
	/**
	 * 最大值
	 */
	private Double maxVal;
	
	/**
	 * 最小值
	 */
	private Double minVal;
	
	/**
	 * 平均值
	 */
	private Double avgVal;
	
	/**
	 * 测试时间
	 */
	private String time;

	/**
	 * @Title: getGroupId
	 * @Description: 获取groupId
	 * @return: Integer groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}

	/**
	 * @Title: setGroupId
	 * @Description: 设置groupId
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	/**
	 * @Title: getBatteryId
	 * @Description: 获取batteryId
	 * @return: Integer batteryId
	 */
	public Integer getBatteryId() {
		return batteryId;
	}

	/**
	 * @Title: setBatteryId
	 * @Description: 设置batteryId
	 */
	public void setBatteryId(Integer batteryId) {
		this.batteryId = batteryId;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getNum
	 * @Description: 获取num
	 * @return: Integer num
	 */
	public Integer getNum() {
		return num;
	}

	/**
	 * @Title: setNum
	 * @Description: 设置num
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getListData
	 * @Description: 获取listData
	 * @return: List<TestData> listData
	 */
	public List<TestData> getListData() {
		return listData;
	}

	/**
	 * @Title: setListData
	 * @Description: 设置listData
	 */
	public void setListData(List<TestData> listData) {
		this.listData = listData;
	}

	/**
	 * @Title: getLimitVal
	 * @Description: 获取limitVal
	 * @return: Double limitVal
	 */
	public Double getLimitVal() {
		return limitVal;
	}

	/**
	 * @Title: setLimitVal
	 * @Description: 设置limitVal
	 */
	public void setLimitVal(Double limitVal) {
		this.limitVal = limitVal;
	}

	/**
	 * @Title: getMaxVal
	 * @Description: 获取maxVal
	 * @return: Double maxVal
	 */
	public Double getMaxVal() {
		return maxVal;
	}

	/**
	 * @Title: setMaxVal
	 * @Description: 设置maxVal
	 */
	public void setMaxVal(Double maxVal) {
		this.maxVal = maxVal;
	}

	/**
	 * @Title: getMinVal
	 * @Description: 获取minVal
	 * @return: Double minVal
	 */
	public Double getMinVal() {
		return minVal;
	}

	/**
	 * @Title: setMinVal
	 * @Description: 设置minVal
	 */
	public void setMinVal(Double minVal) {
		this.minVal = minVal;
	}

	/**
	 * @Title: getAvgVal
	 * @Description: 获取avgVal
	 * @return: Double avgVal
	 */
	public Double getAvgVal() {
		return avgVal;
	}

	/**
	 * @Title: setAvgVal
	 * @Description: 设置avgVal
	 */
	public void setAvgVal(Double avgVal) {
		this.avgVal = avgVal;
	}

	/**
	 * @Title: getTime
	 * @Description: 获取time
	 * @return: String time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @Title: setTime
	 * @Description: 设置time
	 */
	public void setTime(String time) {
		this.time = time;
	}
	
	/**
	 * @Title: getUnit
	 * @Description: 获取unit
	 * @return: String unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @Title: setUnit
	 * @Description: 设置unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "GraphDto [groupId=" + groupId + ", batteryId=" + batteryId + ", type=" + type + ", num=" + num
				+ ", name=" + name + ", unit=" + unit + ", listData=" + listData + ", limitVal=" + limitVal
				+ ", maxVal=" + maxVal + ", minVal=" + minVal + ", avgVal=" + avgVal + ", time=" + time + "]";
	}

}
