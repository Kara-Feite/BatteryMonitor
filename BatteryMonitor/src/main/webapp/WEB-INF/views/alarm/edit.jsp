<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>处理告警</title>
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js' ></script>
<style>
select,input{width:220px;}
a{width: 80px;}
.but{width: 140px;}
</style>
<script>
function update(){
	$('#myForm').form('submit',{
		url : '${ctx}/alarm/update',
		onSubmit:function(param){
			var theForm = document.myForm;
			if(!theForm.dealTime.value){
				alert("请输入处理日期");
				theForm.dealTime.focus();
				return false;
			}
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				window.location.href = '${ctx}/alarm/content?treeId=${treeId}';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="alarm" enctype="multipart/form-data">
		<div class="easyui-panel" title="处理：${alarm.name} 告警" style="padding:30px 60px;">
			<sf:hidden path="id"/>	
			<div style="margin-bottom:20px">
				<sf:select path="dealer" items="${listUser}" class="easyui-combobox" 
					 data-options="label:'处理人员：',required:true,editable:false"
					 itemLabel="userName" itemValue="userName"/><br><br>
				<sf:select path="state" class="easyui-combobox" 
					 data-options="label:'处理状态：',required:true,editable:false">  
					     <sf:option label="已处理" value = "true"/>
				         <sf:option label="未处理" value = "false"/>  		
				</sf:select><br><br>
				处理日期：&nbsp;&nbsp;&nbsp;&nbsp;
				 <sf:input path="dealTime" editable="false" class="but" autocomplete='off'
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/><br><br>     										
				<sf:input path="remark" class="easyui-textbox" 
				 		data-options="label:'备&nbsp;&nbsp;&nbsp;&nbsp; 注:'"/><br><br>
				<a href="javascript:update()" class="easyui-linkbutton" 
	           		data-options="iconCls:'icon-save'">提 交</a>
	         		&nbsp;&nbsp;&nbsp;&nbsp;
	       		<a href="${ctx}/alarm/content?treeId=${treeId}" class="easyui-linkbutton" 
	       			data-options="iconCls:'icon-reload'">返 回</a>
			</div>
		</div>	   		
	</sf:form>
</body>
</html>