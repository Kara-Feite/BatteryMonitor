package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.LogDao;
import com.gzyw.monitor.entity.Log;

/**
 * 
 * @ClassName: LogDaoImpl
 * @Description: 日志持久层实现类
 * @author LW
 * @date 2018年7月9日 下午3:00:14
 */
@Repository
public class LogDaoImpl extends BaseDaoImpl<Log> implements LogDao {

	@Override
	public long countLog(Log log, String userName) {
		String hql = "SELECT COUNT(*) FROM Log WHERE 1 = 1";
		hql = setHQLParam(hql, null, null, log, userName);
		return count(hql);
	}

	@Override
	public List<Log> listLogByPage(Integer page, Integer rows, String sort, String order, Log log, String userName) {
		String hql = "FROM Log WHERE 1 = 1";
		hql = setHQLParam(hql, sort, order, log, userName);
		return queryByPage(hql, page, rows);
	}
	
	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置HQL参数
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, Log log, String userName) {
		StringBuilder sb = new StringBuilder(hql);
		String type = log.getType();
		if (StringUtil.isNotEmpty(type)) {
			sb.append(" AND type LIKE '%" + type + "%'");
		}
		if (StringUtil.isNotEmpty(userName)) {
			sb.append(" AND user_id IN (SELECT id FROM User WHERE nickName LIKE '%" + userName + "%')");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY " + sort + " " + order);
		}
		return sb.toString();
	}

}
