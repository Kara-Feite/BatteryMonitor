package com.gzyw.monitor.service;

import java.io.Serializable;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Limit;

/**
 * 
 * @ClassName: LimitService
 * @Description: 警告门限服务层
 * @author LW
 * @date 2018年7月11日 下午3:42:43
 */
public interface LimitService {

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取门限分页数据
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Limit limit, Integer groupId);

	/**
	 * 
	 * @Title: getLimitById
	 * @Description: 根据id获取门限警告
	 * @param id
	 */
	Limit getLimitById(Serializable id);

	/**
	 * 
	 * @Title: updateLimit
	 * @Description: 更新门限警告
	 * @param limit
	 * @return
	 */
	ResultDto updateLimit(Limit limit);

	/**
	 * 
	 * @Title: saveLimit
	 * @Description: 添加门限
	 * @param limit
	 * @return
	 */
	ResultDto saveLimit(Limit limit);

	/**
	 * 
	 * @Title: getLimitByTypeAndGroup
	 * @Description: 根据类型和电池组获取门限
	 * @param type
	 * @param groupId
	 * @return
	 */
	Limit getLimitByTypeAndGroup(String type, Integer groupId);

	/**
	 * 
	 * @Title: delLimitByBat
	 * @Description: 删除门限
	 * @param ids
	 * @return
	 */
	ResultDto delLimitByBat(int[] ids);

}
