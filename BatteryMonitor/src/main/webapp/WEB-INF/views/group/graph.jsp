<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<script src='${ctxStatic}/js/jquery-1.8.3.min.js'></script>
<script src="${ctxStatic}/js/echarts/echarts.min.js"></script>
	<c:if test="${empty graphError}">
		<div id='view' style="width:800px;height:345px;margin-left:100px;"></div>
	</c:if>
	<c:if test="${!empty graphError}">
		<h2>${graphError}</h2>
	</c:if>
	<c:set var="name" value="${graphDto.name}"></c:set>
	<c:set var="limitVal" value="${graphDto.limitVal}"></c:set>
<script>
	function getGroupNum(num) {
		var arr = [];
		if (typeof num !== 'number') {
			num = parseInt(num + "====" + (typeof num));
		}
		for (var i = 1; i <= num; i++) {
			arr.push(i);
		}
		return arr;
	}
	var myChart = echarts.init(document.getElementById('view'));
       // 指定图表的配置项和数据
       var option = {
           title : {
               text: '${graphDto.type}'
           },
           tooltip : {
               trigger: 'axis',
               formatter: '{b0}<br />${name} : {c0}${graphDto.unit}'
           },
           legend: {
               selectedMode : false,
               data:[
                   /*icon : 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'*/
                   {
                       name : '未超出${name}警告线'
                   }
               ]
           },
           toolbox: {
               show : true,
               orient: 'horizontal',
               showTitle: true,
               feature : {
                   dataView:{
                       show: true,
                       readOnly: true,
                       optionToContent: function(opt) {
                           var colName = "序号";
                           var typeName = "${name}值";
                           var dataview = opt.toolbox[0].feature.dataView;  //获取dataview
                           var table = '<div style="position:absolute;top: 5px;left: 0;right: 0;line-height: 1.4em;text-align:center;font-size:14px;"></div>';
                           table += getTable(opt, colName, typeName);
                           return table;
                       }
                   },
                   magicType : {show: true, type: ['line', 'bar']},
                   restore : {show: true},
                   saveAsImage : {show: true}
               }
           },
           /*['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3']*/
           color : ['#2284c7'],
           calculable : true,
           xAxis : [
               {
                   gridIndex : 0,
                   type : 'category',
                   data : getGroupNum('${graphDto.num}')
               }
           ],
           yAxis : [
               {
                   type : 'value'
               }
           ],
           series : [
               {
                   name:'未超出${name}警告线',
                   type:'bar',
                   data:[
                   	<c:forEach items="${graphDto.listData}" var="data">
                   		<c:set var="val" value="${data.voltage}${data.inter}${data.tempera}"></c:set>
                   		{
                   			name : '${data.batteryName}',
                   			value : '${val}'
                   			<c:if test="${val >= limitVal}">
                    			,
	                            symbolSize : 10,
	                            itemStyle:{color : '#c23531'}
                   			</c:if>
                   		},
                   	</c:forEach>
                   ],
                   markPoint : {
                       data : [
                           {type : 'max', name: '最大值', itemStyle:{color : '#c23531'}},
                           {type : 'min', name: '最小值', itemStyle:{color : '#2284c7'}}
                       ]
                   },
                   markLine: {
                       itemStyle: {
                           normal: {lineStyle: {type: 'dashed'},label: {show: true}}
                       },
                       data: [
                           {
                               name: '平均线',
                               // 支持 'average', 'min', 'max'
                               type: 'average'
                           },
                           {
                               itemStyle:{color : '#c23531'},
                               name : '${name}警告门限',
                               yAxis: '${limitVal}'
                           }
                       ]
                   }
               }
           ]
       };
		// 获取数据表格	        
       function getTable(opt,colName,typeName){
           var axisData = opt.xAxis[0].data;//获取图形的data数组
           var series = opt.series;//获取series
           var num = 0;//记录序号
           var sum = new Array();//获取合计数组（根据对应的系数生成相应数量的sum）
           var len = series.length;
           for(var i=0; i<len; i++){
               sum[i] = 0;
           }
           var table = '<table class="bordered"><thead><tr>'
               + '<th>'+colName+'</th>'
               + '<th>'+typeName+'</th>';
           table += '</tr></thead><tbody>';
           for (var i = 0, l = axisData.length; i < l; i++) {
               num += 1;
               table += '<tr>'
                   + '<td>电池_' + axisData[i] + '</td>';
               for(var j=0; j<len;j++){
                   if(series[j].data[i]){
                       var val = parseFloat(series[j].data[i].value).toFixed(3);
                       table += '<td>' + val + '</td>';
                   }else{
                       table += '<td>' + 0.0 + '</td>';
                   }

               }
               table += '</tr>';
           }
           table += '</tr></tbody></table>';
           return table;
       }
       myChart.setOption(option);
</script>
