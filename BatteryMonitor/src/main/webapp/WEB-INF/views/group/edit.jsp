<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>编辑电池组信息</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>	
#denglu *{vertical-align:middle;}
select,input{width:220px;}
a{width:80px;}
.textbox-label{
    width: 82px;
}
</style>
<script>
function edit(){
	<permission:notPermission name="${empty groupDto.group.groupId ? 'tree:add' : 'tree:update'}">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	$('#myForm').form('submit',{
		url : '${ctx}/group/${empty groupDto.group.groupId ? "save" : "update"}',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				<c:if test="${!empty groupDto.group.groupId}">
					window.parent.parent.tree.updateNode(result.data);
				</c:if>
				<c:if test="${empty groupDto.group.groupId}">
					window.parent.parent.tree.addNodes([result.data]);
				</c:if>
				window.location.href = '${ctx}/group/content?groupId=${groupDto.tree.parentId}';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
   <sf:form name="myForm" id='myForm' method="post" modelAttribute="groupDto" enctype="multipart/form-data">  
     <sf:hidden path="group.groupId"/>
     <sf:hidden path="group.norm.id"/>
     <sf:hidden path="tree.parentId"/>
     <sf:hidden path="group.createTime"/>
     <div class="easyui-panel" title="操作：编辑电池组信息" style="padding:30px 60px;">
		<div style="margin-bottom:20px;clear:both;">
			<div style='float:left;'>
				<sf:input path="tree.name" class="easyui-textbox" 
		 			data-options="label:'电池组名称:',required:true"/><br><br>
		 		<sf:input path="group.address" class="easyui-textbox" 
		 			data-options="label:'电池组标识:',required:true"/><br><br>
		 		<sf:input path="tree.number" class="easyui-numberbox" 
		 			data-options="label:'电池数:',required:true"/><br><br>
		 		<sf:input path="group.interval" class="easyui-textbox" 
		 			data-options="label:'测试间隔:',required:true"/><br><br>
		 		<sf:select path="group.unit" class="easyui-combobox" 
					 data-options="label:'间隔单位：',required:true,editable:false">  
				     <sf:option label="秒" value = "1"/>
			         <sf:option label="分钟" value = "2"/>
			         <sf:option label="小时" value = "3"/>
			         <sf:option label="天" value = "4"/>
			         <sf:option label="固定小时" value = "5"/>
				</sf:select><br><br>
				<sf:select path="group.isRun" class="easyui-combobox" 
					 data-options="label:'运行状态：',required:true,editable:false">  
				     <sf:option label="开启" value = "true"/>
			         <sf:option label="停止" value = "false"/>
				</sf:select><br><br>
				<sf:input path="tree.remark" class="easyui-textbox" 
		 			data-options="label:'备 注:'"/><br><br>
				<p style='position:relative;left:155px;'>
		           	<a href="javascript:void(0)" class="easyui-linkbutton" 
		           		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a> 
		           	&nbsp;&nbsp;&nbsp;&nbsp;
		        	<a href="${ctx}/group/content?groupId=${groupDto.tree.parentId}" class="easyui-linkbutton" 
		        		data-options="iconCls:'icon-reload'">返 回</a>
	        	</p>
			</div>
			<div style='margin-left:50px;float:left;'>
			 	<sf:select path="group.norm.maker" class="easyui-combobox"
			 	 	data-options="label:'制 造 商:',required:true">  
				    <c:forEach items = "${listMaker}" var='maker'>
				 		<sf:option value="${maker}"/>
				 	</c:forEach>
				 </sf:select><br><br>
			 	<sf:select path="group.norm.model" class="easyui-combobox" 
			 	 	data-options="label:'型 &nbsp;&nbsp;&nbsp;&nbsp;号:',required:true">
				    <c:forEach items = "${listModel}" var='model'>
				 		<sf:option value="${model}"/>
				 	</c:forEach>
				 </sf:select><br><br>
			 	<sf:select path="group.norm.type" class="easyui-combobox" 
			 	 	data-options="label:'电池类型:',required:true">  
				    <c:forEach items = "${listType}" var='type'>
				 		<sf:option value="${type}"/>
				 	</c:forEach>
				 </sf:select><br><br>
			 	<sf:input path="group.norm.voltage" class="easyui-numberbox" 
			 		data-options="label:'电池电压/V:',required:true"/><br><br>
			 	<sf:input path="group.norm.inter" class="easyui-textbox" 
			 		data-options="label:'参考内阻/mΩ:',required:true"/><br><br>
				<sf:input path="group.norm.volume" class="easyui-textbox" 
			 		data-options="label:'电池容量/AH:',required:true"/><br><br>
		 	</div>
 		</div>
	</div>
</sf:form>
</body>
</html>