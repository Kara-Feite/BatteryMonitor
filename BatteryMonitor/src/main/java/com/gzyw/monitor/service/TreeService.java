package com.gzyw.monitor.service;

import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: TreeService
 * @Description: 树形菜单服务层
 * @author LW
 * @date 2018年6月7日 下午3:36:16
 */
public interface TreeService {

	/**
	 * 
	 * @Title: listTreeByAll
	 * @Description: 获取所有树形节点
	 * @return
	 */
	List<Tree> listTreeByAll();
	
	/**
	 * 
	 * @Title: listTreeByRoot
	 * @Description: 获取根节点集合
	 * @param userId
	 * @return
	 */
	List<Tree> listTreeByRoot(Integer userId);
	
	/**
	 * 
	 * @Title: getDataGridByRoot
	 * @Description: 获取根节点集合的DataGrid
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order	降序或升序
	 * @param tree 基站节点
	 * @return
	 */
	EUDataGridDto getDataGridByRoot(Integer page, Integer rows, String sort, String order, Tree tree, Integer userId);

	/**
	 * 
	 * @Title: getTreeById
	 * @Description: 根据id获取基站树形节点
	 * @param id
	 * @return
	 */
	Tree getTreeById(Integer id);

	/**
	 * 
	 * @Title: saveTreeByRoot
	 * @Description: 添加根节点
	 * @param tree
	 */
	ResultDto saveTreeByRoot(Tree tree);

	/**
	 * 
	 * @Title: updateTreeByRoot
	 * @Description: 更新根节点
	 * @param tree
	 */
	ResultDto updateTreeByRoot(Tree tree);

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取节点分页数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order	降序或升序
	 * @param tree 基站节点
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Tree tree);

	/**
	 * 
	 * @Title: updateTree
	 * @Description: 更新节点
	 * @param tree
	 * @return
	 */
	ResultDto updateTree(Tree tree);

	/**
	 * 
	 * @Title: saveTree
	 * @Description: 添加节点
	 * @param tree
	 * @return
	 */
	ResultDto saveTree(Tree tree);

	/**
	 * 
	 * @Title: updateTreeByState
	 * @Description: 更新节点状态
	 * @param ids
	 * @param state
	 * @return
	 */
	ResultDto updateTreeByState(int[] ids, Boolean state);

	/**
	 * 
	 * @Title: listTreeByPid
	 * @Description: 根据父id获取字节点
	 * @param id
	 * @return
	 */
	List<Tree> listTreeByPid(Integer id);

	/**
	 * 
	 * @Title: listTreeByUserId
	 * @Description: 通过用户id获取基站节点
	 * @param userId
	 * @return
	 */
	List<Tree> listTreeByUserId(Integer userId);

}
