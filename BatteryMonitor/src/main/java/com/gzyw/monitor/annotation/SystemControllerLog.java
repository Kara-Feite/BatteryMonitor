package com.gzyw.monitor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @ClassName: SystemControllerLog
 * @Description: 系统Controller层日志注解
 * @author LW
 * @date 2018年7月9日 下午5:12:13
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemControllerLog {
	
	/**
	 * 
	 * @Title: type
	 * @Description: 类型
	 * @return
	 */
	String type();
	
	/**
	 * 描述业务操作 例:Xxx管理-Xxx执行Xxx操作（用户管理-xxx添加用户）
	 * @Title: description
	 * @Description: 描述、操作
	 * @return
	 */
	String description();
	
}