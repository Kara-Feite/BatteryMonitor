package com.gzyw.monitor.aspect;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NamedThreadLocal;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.entity.Log;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.LogService;

/**
 * 需要配合如下方法使用(在每个Controller都必须定义此方法):
 * 	(@ModelAttribute
 * 	 @LogRequestParam
 * 	 public void requestParam(HttpServletRequest request) {})
 * @ClassName: SystemLogAspect
 * @Description: 系统日志切面类
 * @author LW
 * @date 2018年7月9日 下午2:44:51
 */
@Aspect
@Order(1)
@Component
public class SystemLogAspect {

	private static final Logger logger = LoggerFactory.getLogger(SystemLogAspect.class);

	private static final ThreadLocal<Date> beginTimeThreadLocal = new NamedThreadLocal<Date>("ThreadLocal beginTime");

	private static final ThreadLocal<Log> logThreadLocal = new NamedThreadLocal<Log>("ThreadLocal log");

	private static final ThreadLocal<User> currentUser = new NamedThreadLocal<User>("ThreadLocal user");
	
	private HttpServletRequest request;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private LogService logService;

	/**
	 * 
	 * @Title: controllerAspect
	 * @Description: Controller层切点 注解拦截
	 */
	@Pointcut("@annotation(com.gzyw.monitor.annotation.SystemControllerLog)")
	public void controllerAspect() {}
	
	@Pointcut("@annotation(com.gzyw.monitor.annotation.LogRequestParam)")
	public void LogRequestAspect() {}

	/**
	 * 用于拦截Controller层操作记录，并记录请求
	 * @Title: doBefore
	 * @Description: 前置通知
	 * @param joinPoint
	 * @throws InterruptedException
	 */
	@Before("LogRequestAspect()")
	public void doBefore(JoinPoint joinPoint) throws InterruptedException {
		Date beginTime = new Date();
		// 线程绑定变量（该数据只有当前请求的线程可见）
		beginTimeThreadLocal.set(beginTime);
		Object[] args = joinPoint.getArgs();
		request = (HttpServletRequest) args[0];
		// 这里日志级别为debug
		if (logger.isDebugEnabled()) {
			logger.debug("请求时间: {}  URI: {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(beginTime),
					request.getRequestURI());
		}
		// 读取session中的用户
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		currentUser.set(user);
	}

	/**
	 * 
	 * @Title: doAfter
	 * @Description: 后置通知 用于拦截Controller层记录用户的操作
	 * @param joinPoint
	 */
	@After("controllerAspect()")
	public void doAfter(JoinPoint joinPoint) {
		User user = currentUser.get();
		if (user != null) {
			MethodSignature signature = (MethodSignature) joinPoint.getSignature();
			Method method = signature.getMethod();
			SystemControllerLog controllerLog = method.getAnnotation(SystemControllerLog.class);
			String type = controllerLog.type();
			String description = type + "，用户" + user.getNickName() + ":" + controllerLog.description();
			@SuppressWarnings("unchecked")
			Map<String,String[]> params = request.getParameterMap();
			String ipAddr = request.getRemoteAddr();
			Log log = new Log();
			log.setType(type);
			log.setIpAddr(ipAddr);
			log.setUser(user);
			log.setDescription(description);
			log.setMapToParams(params);
			log.setCreateTime(new Date());
			
			// 1.直接执行保存操作
			// this.logService.saveLog(log);

			// 2.优化:异步保存日志
			// new SaveLogThread(log, logService).start();

			// 3.再优化:通过线程池来执行日志保存
			threadPoolTaskExecutor.execute(new SaveLogThread(log, logService));
			logThreadLocal.set(log);
		}

	}

	/**
	 * 
	 * @Title: doAfterThrowing
	 * @Description: 异常通知记录操作报错日志
	 * @param joinPoint
	 * @param e
	 */
	@AfterThrowing(pointcut = "controllerAspect()", throwing = "e")
	public void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
		Log log = logThreadLocal.get();
		log.setType("error");
		log.setDescription(e.toString());
		new UpdateLogThread(log, logService).start();
	}

	/**
	 * 
	 * @ClassName: SaveLogThread
	 * @Description: 日志保存线程
	 * @author LW
	 * @date 2018年7月9日 下午2:56:35
	 */
	private static class SaveLogThread implements Runnable {
		private Log log;
		private LogService logService;
		public SaveLogThread(Log log, LogService logService) {
			this.log = log;
			this.logService = logService;
		}

		@Override
		public void run() {
			logService.saveLog(log);
		}
	}

	/**
	 * 
	 * @ClassName: UpdateLogThread
	 * @Description: 日志更新线程
	 * @author LW
	 * @date 2018年7月9日 下午2:56:54
	 */
	private static class UpdateLogThread extends Thread {
		private Log log;
		private LogService logService;
		public UpdateLogThread(Log log, LogService logService) {
			super(UpdateLogThread.class.getSimpleName());
			this.log = log;
			this.logService = logService;
		}
		@Override
		public void run() {
			this.logService.updateLog(log);
		}
	}
}