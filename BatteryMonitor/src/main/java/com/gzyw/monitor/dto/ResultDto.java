package com.gzyw.monitor.dto;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @ClassName: ResultUtil
 * @Description: json结果响应工具类
 * @author LW
 * @date 2018年6月6日 下午8:53:45
 */
public class ResultDto {

    /**
     * 定义jackson对象
     */
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 响应业务状态
     */
    private Integer status;

    /**
     * 响应消息
     */
    private String msg;

    /**
     * 响应中的数据
     */
    private Object data;
    
    /**
     * @Title: ResultUtil
     * @Description: ResultUtil构造函数
     */
    public ResultDto() {}
    
    /**
     * @Title: ResultUtil
     * @Description: ResultUtil构造函数
     * @param data
     */
    public ResultDto(Object data) {
        this.status = 200;
        this.msg = "OK";
        this.data = data;
    }
    
    /**
     * @Title: ResultUtil
     * @Description: ResultUtil构造函数
     * @param status
     * @param msg
     * @param data
     */
    public ResultDto(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }
    
    /**
	 * @Title: getStatus
	 * @Description: 获取status
	 * @return: Integer status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @Title: setStatus
	 * @Description: 设置status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @Title: getMsg
	 * @Description: 获取msg
	 * @return: String msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @Title: setMsg
	 * @Description: 设置msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @Title: getData
	 * @Description: 获取data
	 * @return: Object data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @Title: setData
	 * @Description: 设置data
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
     * 
     * @Title: ok
     * @Description: 返回成功信息
     * @return
     */
    public static ResultDto ok() {
        return new ResultDto(null);
    }

    /**
     * 
     * @Title: ok
     * @Description: 返回成功信息
     * @param data
     * @return
     */
    public static ResultDto ok(Object data) {
        return new ResultDto(data);
    }

    /**
     * 
     * @Title: build
     * @Description: 返回指定信息
     * @param status
     * @param msg
     * @return
     */
    public static ResultDto build(Integer status, String msg) {
        return new ResultDto(status, msg, null);
    }
    
    /**
     * status、msg、data
     * @Title: build
     * @Description: 返回指定信息
     * @param status
     * @param msg
     * @param data
     * @return
     */
    public static ResultDto build(Integer status, String msg, Object data) {
        return new ResultDto(status, msg, data);
    }

    /**
     * 
     * @Title: formatToPojo
     * @Description: 将json结果集转化为ResultUtil对象
     * @param jsonData
     * @param clazz
     * @return
     */
    public static ResultDto formatToPojo(String jsonData, Class<?> clazz) {
        try {
            if (clazz == null) {
                return MAPPER.readValue(jsonData, ResultDto.class);
            }
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (clazz != null) {
                if (data.isObject()) {
                    obj = MAPPER.readValue(data.traverse(), clazz);
                } else if (data.isTextual()) {
                    obj = MAPPER.readValue(data.asText(), clazz);
                }
            }
            return build(jsonNode.get("status").intValue(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 没有带任何数据的转换
     * @Title: format
     * @Description: 转换为ResultUtil
     * @param json
     * @return
     */
    public static ResultDto format(String json) {
        try {
            return MAPPER.readValue(json, ResultDto.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 
     * @Title: formatToList
     * @Description: 转换为带集合数据的ResultUtil
     * @param jsonData
     * @param clazz
     * @return
     */
    public static ResultDto formatToList(String jsonData, Class<?> clazz) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isArray() && data.size() > 0) {
                obj = MAPPER.readValue(data.traverse(),
                        MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return build(jsonNode.get("status").intValue(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }

}
