package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.NormDao;
import com.gzyw.monitor.entity.Norm;

/**
 * 
 * @ClassName: NormDaoImpl
 * @Description: 电池规格持久层实现类
 * @author LW
 * @date 2018年6月13日 上午9:36:30
 */
@Repository
public class NormDaoImpl extends BaseDaoImpl<Norm> implements NormDao {

	@Override
	public Long countNorm(Norm norm) {
		String hql = "SELECT COUNT(*) FROM Norm WHERE 1 = 1";
		hql = setHQLParam(hql, null, null, norm);
		return count(hql);
	}

	@Override
	public List<Norm> listNormByPage(Integer page, Integer rows, String sort, String order, Norm norm) {
		String hql = "FROM Norm WHERE 1 = 1";
		hql = setHQLParam(hql, sort, order, norm);
		return queryByPage(hql, page, rows);
	}

	@Override
	public Integer updateState(int id, boolean state) {
		String hql = "UPDATE Norm SET state = ? WHERE id = ?";
		return update(hql, state, id);
	}
	
	@Override
	public List<Norm> listNormByAll() {
		String hql = "FROM Norm";
		return queryForList(hql);
	}

	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置hql参数
	 * @param hql
	 * @param sort
	 * @param order
	 * @param norm
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, Norm norm) {
		String maker = norm.getMaker();
		if (StringUtil.isNotEmpty(maker)) {
			hql += " AND maker like '%" + maker + "%'";
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			hql += " ORDER BY " + sort + " " + order;
		}
		return hql;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> listTypeAll() {
		String hql = "SELECT type FROM Norm";
		return getSession().createQuery(hql).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> listMakerAll() {
		String hql = "SELECT maker FROM Norm";
		return getSession().createQuery(hql).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> listModelAll() {
		String hql = "SELECT model FROM Norm";
		return getSession().createQuery(hql).list();
	}

}
