package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @ClassName: Alarm
 * @Description: 警告
 * @author LW
 * @date 2018年6月19日 上午9:45:22
 */
@Entity
@Table(name = "bm_alarm")
public class Alarm implements Serializable{
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 3988482911284321550L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 警告发生处的名称
	 */
	@Column(name = "name", length = 64, nullable = false)
	private String name;
	
	/**
	 * 警告类型
	 */
	@Column(name = "mold", length = 32, nullable = false)
	private String mold;
	
	/**
	 * 测试类型
	 */
	@Column(name = "type", length = 32, nullable = false)
	private String type;
	
	/**
	 * 详细信息
	 */
	@Column(name = "details", length = 255)
	private String details;
	
	/**
	 * 状态，是否处理
	 */
	@Column(name = "state", length = 16, nullable = false)
	private String state;
	
	/**
	 * 是否处理
	 */
	@Column(name = "is_deal", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isDeal = false;
	
	/**
	 * 处理人员
	 */
	@Column(name = "dealer", length = 64)
	private String dealer;
	
	/**
	 * 警告时间
	 */
	@Column(name = "alarm_time", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date alarmTime;
	
	/**
	 * 处理时间
	 */
	@Column(name = "deal_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date dealTime;
	
	/**
	 * 备注
	 */
	@Column(name = "remark", length = 255)
	private String remark;
	
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;

	/**
	 * @Title: Alarm
	 * @Description: Alarm构造函数
	 */
	public Alarm() {}

	/**
	 * @Title: Alarm
	 * @Description: Alarm构造函数
	 * @param id
	 * @param name
	 * @param mold
	 * @param type
	 * @param details
	 * @param state
	 * @param isDeal
	 * @param dealer
	 * @param alarmTime
	 * @param dealTime
	 * @param remark
	 */
	public Alarm(Integer id, String name, String mold, String type, String details, String state, Boolean isDeal,
			String dealer, Date alarmTime, Date dealTime, String remark) {
		super();
		this.id = id;
		this.name = name;
		this.mold = mold;
		this.type = type;
		this.details = details;
		this.state = state;
		this.isDeal = isDeal;
		this.dealer = dealer;
		this.alarmTime = alarmTime;
		this.dealTime = dealTime;
		this.remark = remark;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @Title: getMold
	 * @Description: 获取mold
	 * @return: String mold
	 */
	public String getMold() {
		return mold;
	}

	/**
	 * @Title: setMold
	 * @Description: 设置mold
	 */
	public void setMold(String mold) {
		this.mold = mold;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getDetails
	 * @Description: 获取details
	 * @return: String details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @Title: setDetails
	 * @Description: 设置details
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: String state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * @Title: getIsDeal
	 * @Description: 获取isDeal
	 * @return: Boolean isDeal
	 */
	public Boolean getIsDeal() {
		return isDeal;
	}

	/**
	 * @Title: setIsDeal
	 * @Description: 设置isDeal
	 */
	public void setIsDeal(Boolean isDeal) {
		this.isDeal = isDeal;
	}

	/**
	 * @Title: getDealer
	 * @Description: 获取dealer
	 * @return: String dealer
	 */
	public String getDealer() {
		return dealer;
	}

	/**
	 * @Title: setDealer
	 * @Description: 设置dealer
	 */
	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	/**
	 * @Title: getAlarmTime
	 * @Description: 获取alarmTime
	 * @return: Date alarmTime
	 */
	public Date getAlarmTime() {
		return alarmTime;
	}

	/**
	 * @Title: setAlarmTime
	 * @Description: 设置alarmTime
	 */
	public void setAlarmTime(Date alarmTime) {
		this.alarmTime = alarmTime;
	}

	/**
	 * @Title: getDealTime
	 * @Description: 获取dealTime
	 * @return: Date dealTime
	 */
	public Date getDealTime() {
		return dealTime;
	}

	/**
	 * @Title: setDealTime
	 * @Description: 设置dealTime
	 */
	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "Alarm [id=" + id + ", name=" + name + ", mold=" + mold + ", type=" + type + ", details=" + details
				+ ", state=" + state + ", isDeal=" + isDeal + ", dealer=" + dealer + ", alarmTime=" + alarmTime
				+ ", dealTime=" + dealTime + ", remark=" + remark + "]";
	}

}
