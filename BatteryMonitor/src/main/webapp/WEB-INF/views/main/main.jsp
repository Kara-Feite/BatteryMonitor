<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<meta name='Keywords' content='蓄电池监测系统'>
<meta name='Description' content='蓄电池监测系统'>
<title>蓄电池监测系统</title>
<link href='${ctxStatic}/css/public.css' rel='stylesheet'>
<link href='${ctxStatic}/css/zTreeStyle/zTreeStyle.css' rel='stylesheet'>
<script src='${ctxStatic}/js/jquery-1.4.4.min.js'></script>
<script src='${ctxStatic}/js/jquery.ztree.all.min.js'></script>
<script src='${ctxStatic}/js/jquery.ztree.exhide.min.js'></script>
<script>
	// 节点资源树
	var tree = {
		menuSetting : {view:{selectedMulti:false}, data:{simpleData:{enable:true}}},
		treeObj : null,
		init : function() {
			var menuZNodes = [
				<c:forEach items="${listTree}" var="tree">
					{
						id: '${tree.id}',
						pId: '${tree.parentId}', 
						name: '${tree.name}',
						url : '${tree.url}' + '${tree.id}',
						target : 'mainFrame',
						isHidden : '${tree.state}' === 'false'
					},
				</c:forEach>
		    ];
			$.fn.zTree.init($('#menu-tree'), this.menuSetting, menuZNodes);
		},
		/* refreshNode : function() {
			$.ajax({
				url : '${ctx}/main/tree',
				type : 'get',
				dataType: 'json',
				success : function(data) {
					var nodes = [];
					for ( var d in data) {
						var node = {};
						node['id'] = data[d]['id'];
						node['pId'] = data[d]['parentId'];
						node['name'] = data[d]['name'];
						node['url'] = data[d]['url'] + data[d]['id'];
						node['target'] = 'mainFrame';
						nodes.push(node);
					}
					$.fn.zTree.init($('#menu-tree'), this.menuSetting, nodes);
				}
			});
		}, */
		updateNode : function(node) {
			var selectedNode = this.treeObj.getSelectedNodes(),
				nodes = this.treeObj.getNodesByParam("id", node['id'], selectedNode[0] ? selectedNode[0] : null);
			if (nodes.length > 0) {
				nodes[0].name = node['name'];
				nodes[0].url = node['url'];
				this.treeObj.updateNode(nodes[0]);
			}
		},
		addNodes : function(data) {
			var selectedNode = this.treeObj.getSelectedNodes(),
				parentNode = selectedNode[0];
				nodes = [];
			for (var d in data) {
				var node = {}, pid = data[d]['parentId'];
				node['id'] = data[d]['id'];
				node['pId'] = pid;
				node['name'] = data[d]['name'];
				node['url'] = data[d]['url'] + data[d]['id'];
				node['target'] = 'mainFrame';
				nodes.push(node);
				if (pid == '0') {
					parentNode = null;
				}
			}
			this.treeObj.addNodes(parentNode ? parentNode : null, nodes);
		},
		removeNodes : function(ids) {
			var nodes = this.getNodesByIds(ids);
			console.log(nodes);
			for (var i = 0, node; node = nodes[i++];) {
				this.treeObj.removeNode(node);
			}
		},
		showNodes : function(ids) {
			var nodes = this.getNodesByIds(ids);
			this.treeObj.showNodes(nodes);
			return nodes.length;
		},
		hideNodes : function(ids) {
			var nodes = this.getNodesByIds(ids);
			this.treeObj.hideNodes(nodes);
		},
		getNodesByIds : function(ids) {
			var selectedNode = this.treeObj.getSelectedNodes(),
				selectedNode = selectedNode[0] ? selectedNode[0] : null;
			var nodes = [];
			for (var i = 0, id; id = ids[i++];) {
				var node = this.treeObj.getNodesByParam("id", id, selectedNode)[0];
				node && nodes.push(node);
			}
			return nodes;
		}
	};
	$(document).ready(function(){
		tree.init();
		tree['treeObj'] = $.fn.zTree.getZTreeObj("menu-tree");
	});
</script>
<style>
	body{overflow-x:hidden;overflow-y:hidden;}
	/* header start */
	#header,#content,#footer{width:100%;overflow:hidden;clear:both;}
	#header{height:99px;background-image:url('${ctxStatic}/images/main/bj5.gif');color:#fff;}
	#header .head-top{height:67px;overflow:hidden;}
	#header .btn-list{margin:45px 70px 0 0;}
	#header .btn-list li{border:1px solid #18518F;width:60px;height:20px;text-align:center;line-height:20px;vertical-align:middle;}
	#header .btn-list li a,#header .head-nav .menu-list li a{display:block;}
	#header .btn-list li a:hover,#header .head-nav .menu-list li a:hover{background-color:#59ACFF;}
	#header .btn-list li img{width:18px;height:18px;}
	#header .btn-list li span{vertical-align:middle;}
	#header .head-nav{height:32px;line-height:32px;overflow:hidden;}
	#header .head-nav .menu-list{margin-top:7px;}
	#header .head-nav .menu-list li{border:1px solid #18518F;width:80px;height:18px;text-align:center;line-height:18px;}
	#header .head-nav .user-info{vertical-align:middle;}
	#header .head-nav .hv-time{margin-right:35px;}
	/* header end */
	
	/* content start */
	#content{height:615px;margin-top:5px;}
	#content .con-left{width:230px;height:97%;margin:0 8px 8px 15px;}
	#content #menu-title{position:relative;left:6px;top:5px;height:19px;line-height:19px;}
	/* content end */
	
	/* footer start */
	#footer{position:fixed;left:0;bottom:0;background-color:rgb(82, 147, 213);height:22px;line-height:22px;text-align:center;color:#fff;}
	/* footer end */
</style>
</head>
<body onload='showTime()'>
	<!-- header start -->
	<div id='header'>
		<div class='head-top clearfix'>
			<h1 id='logo' class='fl'><img src='${ctxStatic}/images/main/logo.jpg' width='442' height='67' alt='优维电子蓄电池监测' title='优维电子蓄电池监测'></h1>
			<ul class='btn-list fr flchild'>
				<li><a href=''><img src='${ctxStatic}/images/icon/main.gif'><span>首页</span></a></li>
				<li><a href='javascript:history.go(-1)'><img src='${ctxStatic}/images/icon/last.gif'><span>后退</span></a></li>
				<li><a href='javascript:history.go()'><img src='${ctxStatic}/images/icon/back.gif'><span>前进</span></a></li>
				<li><a href='${ctx}/user/exit'><img src='${ctxStatic}/images/icon/quit.gif'><span>退出</span></a></li>
			</ul>
		</div>
		<div class='head-nav clearfix'>
			<div class='hv-left fl'>
				<ul class='menu-list flchild fl'>
					<li><a href=''>刷新</a></li>
					<%-- <li><a href='${ctx}/menu/list' target='mainFrame'>基站管理</a></li>
					<li><a href='${ctx}/norm/list' target='mainFrame'>电池规格</a></li>
					<li><a href='${ctx}/user/list' target='mainFrame'>用户管理</a></li>
					<li><a href='${ctx}/role/list' target='mainFrame'>权限管理</a></li>
					<li><a href='${ctx}/log/list' target='mainFrame'>日志管理</a></li> --%>
					<c:forEach items = "${listMenu}" var="menu">
						<li><a href='${ctx}${menu.href}' target='${menu.target}'>${menu.name}</a></li>
					</c:forEach>
					<li><a href='${ctx}/user/password' target='mainFrame'>修改密码</a></li>
				</ul>
				<div class='user-info fl'>
					<span>&nbsp;|||&nbsp;</span>
					<span><img src='${ctxStatic}/images/icon/user.gif' width='16' height='16' alt='用户图标'></span>
					<span>${sessionScope.user.nickName}&nbsp;&nbsp;欢迎您!</span>
				</div>
			</div>
			<div class='hv-time fr'></div>
		</div>
	</div>
	<!-- header end -->
	
	<!-- content start -->
	<div id='content' class='flchild clearfix'>
		<div class='con-left'>
			<div id='menu-title'>
				<img src='${ctxStatic}/images/ztree/base.gif'><span>蓄电池系统管理</span>
			</div>
			<div id='menu-tree' class='ztree'></div>
		</div>
		<div class='con-right'>
			<iframe id='mainFrame' name='mainFrame' src='${ctx}/menu/list' style='overflow:visible;' scrolling='yes' frameborder='no' width='100%' height='625'></iframe>
		</div>
	</div>
	<!-- content end -->
	
	<!-- footer start -->
	<div id='footer'>广州优维电子科技有限公司  Copyright© 2018  All rights reserved. 版本：2018 V2.0</div>
	<!-- footer start -->
	<script>
		function showTime(){
			var date = new Date(),
				year = date.getFullYear(),
				month = date.getMonth() + 1,
				day = date.getDate(),
				hour = date.getHours(),
				minu = date.getMinutes(),
				scond = date.getSeconds(),
				am = null,
				arrWeek = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
				weekday = arrWeek[date.getDay()];
			//console.log(year + "======" + date.getUTCFullYear());
			if(hour > 0 && hour < 10)
				hour = '0' + hour;
			if(scond<10)
				scond = '0' + scond;
			if(minu<10)
				minu = '0' + minu;
			//时间段判断
			if(hour >= 0 && hour < 6)
				am = '凌晨';
			if(hour > 6 && hour <= 11)
				am = '上午';
			if(hour > 11 && hour <= 12)
				am = '中午';
			if(hour > 12 && hour <= 17)
				am = '下午';
			if(hour > 17 && hour <= 19)
				am = '傍晚';
			if(hour > 19 && hour <= 22)
				am = '晚上';
			if(hour > 22 && hour <= 24)
				am = '深夜';
			document.getElementsByClassName('hv-time')[0].innerHTML = '今天是: ' + year + '年' + month + '月' + day + '日&nbsp;&nbsp;&nbsp;' + weekday + ' ' + am + ' ' + hour + '时' + minu + '分' + scond + '秒&nbsp;&nbsp;';
		}
		setInterval(showTime, 1000);
		/* 根据浏览器可视区域的距离动态调整iframe的宽度 */
		function changeWidth() {
			var w = document.documentElement.offsetWidth || document.body.offsetWidth ;
			$('.con-right').css('width',w - 255 + 'px');
		}
		changeWidth();
		/* 根据浏览器可视区域的距离动态调整iframe的高度 */
		function changeHeight(){
		    var h = document.documentElement.clientHeight;
		    $('#content, #mainFrame').css('height', h - 130 + 'px');
		}
		changeHeight();
		// 屏幕宽度调整时触发的事件
		window.onresize = function(){
			changeWidth();
			changeHeight();  
		}
	</script>
</body>
</html>