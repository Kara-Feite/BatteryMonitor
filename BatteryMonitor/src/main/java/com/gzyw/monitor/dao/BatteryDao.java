package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Battery;

/**
 * 
 * @ClassName: BatteryDao
 * @Description: 电池持久层
 * @author LW
 * @date 2018年6月14日 下午4:38:42
 */
public interface BatteryDao extends BaseDao<Battery> {

	/**
	 * 
	 * @Title: countBattery
	 * @Description: 获取电池总数
	 * @param groupId
	 * @return
	 */
	Long countBattery(Battery battery, Integer groupId);

	/**
	 * 
	 * @Title: listBatteryByPage
	 * @Description: 获取电池分页数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param battery 电池
	 * @param groupId 电池组id
	 * @return
	 */
	List<Battery> listBatteryByPage(Integer page, Integer rows, String sort, String order, Battery battery, Integer groupId);
	
	/**
	 * 
	 * @Title: listBatteryByGroup
	 * @Description: 根据电池组获取电池
	 * @param groupId
	 * @return
	 */
	List<Battery> listBatteryByGroup(Integer groupId);

	/**
	 * 
	 * @Title: deleteBatteryByGroup
	 * @Description: 根据电池组id删除电池
	 * @param groupId
	 */
	void deleteBatteryByGroup(Integer groupId);
	
	/**
	 * 
	 * @Title: getBatteryByAlarm
	 * @Description: 根据告警获取电池
	 * @param groupId
	 * @param batteryName
	 * @return
	 */
	Battery getBatteryByAlarm(int groupId, String batteryName);

}
