package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @ClassName: User
 * @Description: 用户表
 * @author LW
 * @date 2018年6月4日 下午2:01:45
 */
@Entity
@Table(name = "bm_user")
public class User implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 1079608442599093358L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 姓名
	 */
	@Column(name = "nick_name", length = 64, nullable = false)
	@NotEmpty(message = "姓名不能为空")
	private String nickName;

	/**
	 * 用户名
	 */
	@Column(name = "user_name", length = 64, nullable = false)
	@NotEmpty(message = "用户名不能为空")
	private String userName;

	/**
	 * 密码
	 */
	@Column(name = "password", length = 127, nullable = false)
	@NotEmpty(message = "密码不能为空")
	private String password;

	/**
	 * 手机号
	 */
	@Column(name = "phone", length = 16, nullable = false)
	@NotEmpty(message = "手机号不能为空")
	private String phone;

	/**
	 * 邮箱
	 */
	@Column(name = "email", length = 64)
	private String email;

	/**
	 * 状态
	 */
	@Column(name = "state", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean state;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 对应角色
	 */
	@ManyToMany(mappedBy = "setUser")
	@JsonIgnore
	private Set<Role> setRole;
	
	/**
	 * 对应日志
	 */
	@OneToMany(mappedBy = "user")
	@JsonIgnore
	private Set<Log> setLog;

	/**
	 * @Title: User
	 * @Description: User构造函数
	 */
	public User() {}

	/**
	 * @Title: User
	 * @Description: User构造函数
	 * @param nickName
	 * @param userName
	 * @param password
	 * @param phone
	 * @param email
	 * @param state
	 * @param createTime
	 * @param updateTime
	 */
	public User(String nickName, String userName, String password, String phone, String email, Boolean state,
			Date createTime, Date updateTime) {
		this.nickName = nickName;
		this.userName = userName;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.state = state;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getNickName
	 * @Description: 获取nickName
	 * @return: String nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @Title: setNickName
	 * @Description: 设置nickName
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @Title: getUserName
	 * @Description: 获取userName
	 * @return: String userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @Title: setUserName
	 * @Description: 设置userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @Title: getPassword
	 * @Description: 获取password
	 * @return: String password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @Title: setPassword
	 * @Description: 设置password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @Title: getPhone
	 * @Description: 获取phone
	 * @return: String phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @Title: setPhone
	 * @Description: 设置phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @Title: getEmail
	 * @Description: 获取email
	 * @return: String email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @Title: setEmail
	 * @Description: 设置email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetRole
	 * @Description: 获取setRole
	 * @return: Set<Role> setRole
	 */
	public Set<Role> getSetRole() {
		return setRole;
	}

	/**
	 * @Title: setSetRole
	 * @Description: 设置setRole
	 */
	public void setSetRole(Set<Role> setRole) {
		this.setRole = setRole;
	}
	
	/**
	 * @Title: getSetLog
	 * @Description: 获取setLog
	 * @return: Set<Log> setLog
	 */
	public Set<Log> getSetLog() {
		return setLog;
	}

	/**
	 * @Title: setSetLog
	 * @Description: 设置setLog
	 */
	public void setSetLog(Set<Log> setLog) {
		this.setLog = setLog;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", nickName=" + nickName + ", userName=" + userName + ", password=" + password
				+ ", phone=" + phone + ", email=" + email + ", state=" + state + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}

}
