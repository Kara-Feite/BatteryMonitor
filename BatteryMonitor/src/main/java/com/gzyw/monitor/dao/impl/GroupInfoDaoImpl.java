package com.gzyw.monitor.dao.impl;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.dao.GroupInfoDao;
import com.gzyw.monitor.entity.GroupInfo;

/**
 * 
 * @ClassName: GroupInfoDaoImpl
 * @Description: 电池组信息持久层接口实现类
 * @author LW
 * @date 2018年6月29日 下午2:31:20
 */
@Repository
public class GroupInfoDaoImpl extends BaseDaoImpl<GroupInfo> implements GroupInfoDao {

	@Override
	public GroupInfo getGroupInfoByNow(Integer id) {
		String hql = "FROM GroupInfo WHERE group_id = ? ORDER BY id DESC";
		return (GroupInfo) uniqueResult(hql, id);
	}
	
}
