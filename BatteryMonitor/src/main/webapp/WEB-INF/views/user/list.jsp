<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>用户列表</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
#denglu *{vertical-align:middle;}
a{width:80px;}
</style>
<script>
$(function(){
	$('#user_lists').datagrid({
		url:'${ctx}/user/datagrid',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		sortName : 'createTime',
		sortOrder : 'ASC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[
			{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'nickName',
		        title:'昵称',
		        width:50,
		        align:'center'
			},{
		        field:'userName',
		        title:'用户名',
		        width:50,
		        align:'center'
			},{
		        field:'phone',
		        title:'手机号',
		        width:50,
		        align:'center'
			},{
		        field:'email',
		        title:'电子邮件',
		        width:50,
		        align:'center',
	        	formatter : formatNull
			},{
				field:'createTime',
				title:'创建时间',
				width:50,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			},{
				field : 'state',
				title : '状 态',
				width: 30,
				align:'center',
				formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">激 活</font>';
					}else{
						return '<font style="color:red">禁 用</font>';
					}
				},
				width : 50
			}
		]],
		toolbar:[				
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',
			{text:'激 活',iconCls:'icon-remove',handler:function(){thaw();}},'-',	
			{text:'禁 用',iconCls:'icon-reload',handler:function(){congeal();}},'-',
			{text:'删 除',iconCls:'icon-cancel',handler:function(){del();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});

//查询
function query(){
	$('#user_lists').datagrid('load',serializeObject($('#search_form').form()));
}

//清空
function clean(){
	$('#user_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}
function add() {
	<permission:notPermission name="user:add">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	window.location.href="${ctx}/user/edit";
}
function update(){
	<permission:notPermission name="user:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#user_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/user/edit?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}

function del(){
	<permission:notPermission name="user:delete">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#user_lists').datagrid('getChecked');
	if(rows.length > 0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/user/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#user_lists').datagrid('load',{});
						$('#user_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}
function congeal(){
	<permission:notPermission name="user:congeal">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#user_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要禁用选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/user/congeal',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#user_lists').datagrid('load',{});
						$('#user_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要禁用的记录'});
	}
}
function thaw(){
	<permission:notPermission name="user:thaw">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#user_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要激活选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/user/thaw',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						//console.info(r);
						$('#user_lists').datagrid('load',{});
						$('#user_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要激活的记录'});
	}
}
function unselect(){
	$('#user_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>
<body>
<div class="easyui-layout" fit="true">
	<div data-options="region:'north',title:'查询条件'" style="height:60px;">
		<form id="search_form">
			<table width="100%" bgcolor="#f5fafe">
				<tr height="25">
					<td id="denglu"><img src="${ctxStatic}/images/yin.gif" width="22">							
						<strong>名 称：</strong><input name="nickName" size="15"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
							onclick="query();" href="javascript:void(0);">查  询</a>						
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
						onclick="clean();" href="javascript:void(0);">清  空</a>
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-add'" 
							target="mainFrame" href="javascript:add();">添 加</a>
					</td>					
				</tr>					
			</table>
		</form>
	</div>
	<!-- ------------------------------------------ -->
	<div data-options="region:'center'">
		<table id="user_lists"></table>
	</div>
</div>
</body>
</html>