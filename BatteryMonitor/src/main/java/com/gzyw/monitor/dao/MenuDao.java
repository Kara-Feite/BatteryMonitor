package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Menu;

/**
 * 
 * @ClassName: MenuDao
 * @Description: 菜单持久层接口
 * @author LW
 * @date 2018年7月4日 上午10:34:24
 */
public interface MenuDao extends BaseDao<Menu> {

	/**
	 * 
	 * @Title: listMenuByUserId
	 * @Description: 根据用户id获取菜单
	 * @param id
	 * @return
	 */
	List<Menu> listMenuByUserId(Integer id, boolean isRoot);

	/**
	 * 
	 * @Title: listMenuByAll
	 * @Description: 获取所有菜单
	 * @return
	 */
	List<Menu> listMenuByAll();

}
