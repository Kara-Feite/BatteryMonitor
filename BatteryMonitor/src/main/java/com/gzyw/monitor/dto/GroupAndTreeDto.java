package com.gzyw.monitor.dto;

import java.util.Date;

/**
 * 
 * @ClassName: GroupTestDto
 * @Description: 电池组测试包装类
 * @author LW
 * @date 2018年7月27日 上午8:55:22
 */
public class GroupAndTreeDto {

	/**
	 * 电池组id
	 */
	private Integer id;
	
	/**
	 * 电池组名称
	 */
	private String groupName;
	
	/**
	 * ip地址
	 */
	private String address;
	
	/**
	 * 电池数
	 */
	private Integer number;
	
	/**
	 * 测试时间间隔
	 */
	private String intervals;
	
	/**
	 * 间隔单位
	 * 1:秒，2:分钟，3:小时，4:天，5:固定小时
	 */
	private Integer unit;
	
	/**
	 * 是否运行
	 */
	private Boolean isRun;
	
	/**
	 * 状态
	 */
	private Boolean state;
	
	/**
	 * 警告
	 */
	private Integer warn;

	/**
	 * 预警
	 */
	private Integer plan;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;
	
	/**
	 * @Title: GroupAndTreeDto
	 * @Description: GroupAndTreeDto构造函数
	 */
	public GroupAndTreeDto() {}

	/**
	 * @Title: GroupAndTreeDto
	 * @Description: GroupAndTreeDto构造函数
	 * @param id
	 * @param groupName
	 * @param address
	 * @param number
	 * @param intervals
	 * @param unit
	 * @param isRun
	 * @param state
	 * @param warn
	 * @param plan
	 * @param remark
	 * @param createTime
	 * @param updateTime
	 */
	public GroupAndTreeDto(Integer id, String groupName, String address, Integer number,
			String intervals, Integer unit, Boolean isRun, Boolean state, Integer warn, Integer plan, String remark,
			Date createTime, Date updateTime) {
		this.id = id;
		this.groupName = groupName;
		this.address = address;
		this.number = number;
		this.intervals = intervals;
		this.unit = unit;
		this.isRun = isRun;
		this.state = state;
		this.warn = warn;
		this.plan = plan;
		this.remark = remark;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getGroupName
	 * @Description: 获取groupName
	 * @return: String groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @Title: setGroupName
	 * @Description: 设置groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @Title: getNumber
	 * @Description: 获取number
	 * @return: Integer number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @Title: setNumber
	 * @Description: 设置number
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @Title: getIntervals
	 * @Description: 获取intervals
	 * @return: String intervals
	 */
	public String getIntervals() {
		return intervals;
	}

	/**
	 * @Title: setIntervals
	 * @Description: 设置intervals
	 */
	public void setIntervals(String intervals) {
		this.intervals = intervals;
	}

	/**
	 * @Title: getUnit
	 * @Description: 获取unit
	 * @return: Integer unit
	 */
	public Integer getUnit() {
		return unit;
	}

	/**
	 * @Title: setUnit
	 * @Description: 设置unit
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	/**
	 * @Title: getIsRun
	 * @Description: 获取isRun
	 * @return: Boolean isRun
	 */
	public Boolean getIsRun() {
		return isRun;
	}

	/**
	 * @Title: setIsRun
	 * @Description: 设置isRun
	 */
	public void setIsRun(Boolean isRun) {
		this.isRun = isRun;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getWarn
	 * @Description: 获取warn
	 * @return: Integer warn
	 */
	public Integer getWarn() {
		return warn;
	}

	/**
	 * @Title: setWarn
	 * @Description: 设置warn
	 */
	public void setWarn(Integer warn) {
		this.warn = warn;
	}

	/**
	 * @Title: getPlan
	 * @Description: 获取plan
	 * @return: Integer plan
	 */
	public Integer getPlan() {
		return plan;
	}

	/**
	 * @Title: setPlan
	 * @Description: 设置plan
	 */
	public void setPlan(Integer plan) {
		this.plan = plan;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "GroupAndTreeDto [id=" + id + ", groupName=" + groupName + ", address=" + address + ", number=" + number
				+ ", intervals=" + intervals + ", unit=" + unit + ", isRun=" + isRun + ", state=" + state + ", warn="
				+ warn + ", plan=" + plan + ", remark=" + remark + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}

}
