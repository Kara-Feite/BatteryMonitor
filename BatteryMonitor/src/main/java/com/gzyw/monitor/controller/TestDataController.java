package com.gzyw.monitor.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.service.TestDataService;

/**
 * 
 * @ClassName: TestDataController
 * @Description: 测试数据控制层
 * @author LW
 * @date 2018年6月29日 下午1:13:58
 */
@Controller
@RequestMapping("/test/data")
public class TestDataController {
	
	/**
	 * 测试数据服务层
	 */
	@Autowired
	private TestDataService testDataService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: showGroup
	 * @Description: 电池组数据页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/group",method=RequestMethod.GET)
	String showGroup(Model model,Integer id) {
		model.addAttribute("groupId", id);
		return "group/map/data";
	}
	
	/**
	 * 
	 * @Title: showBattery
	 * @Description: 电池组数据页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/battery",method=RequestMethod.GET)
	String showBattery(Model model,Integer id) {
		model.addAttribute("batteryId", id);
		return "battery/map/data";
	}
	
	/**
	 * 
	 * @Title: getGroupDatagrid
	 * @Description: 获取测试数据列表
	 * @param page 页码
	 * @param temp 临时类
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或者降序
	 * @return
	 */
	@RequestMapping(value = "/group/datagrid")
	@ResponseBody
	EUDataGridDto getGroupDatagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, TempDto temp,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return testDataService.getDatagrid(page, rows, sort, order, temp);
	}
	
	/**
	 * 
	 * @Title: getBatteryDatagrid
	 * @Description: 获取测试数据列表
	 * @param page 页码
	 * @param temp 临时类
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或者降序
	 * @return
	 */
	@RequestMapping(value = "/battery/datagrid")
	@ResponseBody
	EUDataGridDto getBatteryDatagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, TempDto temp,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return testDataService.getDatagrid(page, rows, sort, order, temp);
	}
	
	/**
	 * 
	 * @Title: delTestDataByBat
	 * @Description: 批量删除测试数据
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/delete")
	ResultDto delTestDataByBat(String ids) {
		if (StringUtil.isEmpty(ids)) {
			return ResultDto.build(500, "请选中要删除的项！");
		}
		try {
			return testDataService.delTestDataByBat(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
}
