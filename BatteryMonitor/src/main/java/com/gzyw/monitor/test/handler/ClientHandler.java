package com.gzyw.monitor.test.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ClientHandler extends ChannelInboundHandlerAdapter  {
	
	private static final Logger LOG = LoggerFactory.getLogger(ClientHandler.class);
	
	//private Channel client;

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		ctx.channel().writeAndFlush("00000000001,0103000000018040a");
		LOG.info("Socket Client: 与服务器建立连接！");
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		System.out.println(msg);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		LOG.info("Socket Client: 与电池组断开连接:" + cause.getMessage());
		cause.printStackTrace();
		ctx.close();
	}
	
	
	
}
