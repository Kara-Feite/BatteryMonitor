package com.gzyw.monitor.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.CRCUtil;
import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.control.MonitorClient;
import com.gzyw.monitor.dao.BatteryDao;
import com.gzyw.monitor.dao.GroupDao;
import com.gzyw.monitor.dao.LimitDao;
import com.gzyw.monitor.dao.RoleDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ExcelLimitDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.GroupAndTreeDto;
import com.gzyw.monitor.dto.GroupDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.Limit;
import com.gzyw.monitor.entity.Norm;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.GroupInfoService;
import com.gzyw.monitor.service.GroupLimitService;
import com.gzyw.monitor.service.GroupService;
import com.gzyw.monitor.service.TestDataService;
import com.gzyw.monitor.task.QuartzManager;

/**
 * 
 * @ClassName: GroupServiceImpl
 * @Description: 电池组服务层接口实现类
 * @author LW
 * @date 2018年6月14日 下午4:28:30
 */
@Service
public class GroupServiceImpl implements GroupService {

	/**
	 * 电池组持久层
	 */
	@Autowired
	private GroupDao groupDao;

	/**
	 * 基站节点持久层
	 */
	@Autowired
	private TreeDao treeDao;
	
	/**
	 * 电池持久层
	 */
	@Autowired
	private BatteryDao batteryDao;

	/**
	 * 角色持久层
	 */
	@Autowired
	private RoleDao roleDao;

	/**
	 * 门限持久层
	 */
	@Autowired
	private LimitDao limitDao;
	
	/**
	 * Quartz定时管理类
	 */
	@Autowired
	private QuartzManager quartzManager;

	/**
	 * 监测客户机
	 */
	@Autowired
	private MonitorClient client;

	/**
	 * 组信息服务层
	 */
	@Autowired
	private GroupInfoService groupInfoService;

	/**
	 * 测试数据服务层
	 */
	@Autowired
	private TestDataService testDataService;

	/**
	 * 组限制服务层
	 */
	@Autowired
	private GroupLimitService groupLimitService;

	@Override
	public Group getGroupById(Integer groupId) {
		return groupDao.get(groupId);
	}

	@Override
	public GroupDto getGroupDtoById(Integer id) {
		Group group = groupDao.get(id);
		Tree tree = treeDao.get(id);
		GroupDto groupDto = new GroupDto();
		if (group != null) {
			groupDto.setGroup(group);
		}
		if (tree != null) {
			groupDto.setTree(tree);
		}
		return groupDto;
	}

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Tree tree) {
		long count = treeDao.countTree(tree);
		List<GroupAndTreeDto> listGroup = groupDao.listGroupByPage(page, rows, sort, order, tree);
		return new EUDataGridDto(count, listGroup);
	}

	@Override
	public ResultDto saveGroup(GroupDto groupDto) {
		Tree tree = groupDto.getTree();
		Group group = groupDto.getGroup();
		if (tree == null || group == null) {
			return ResultDto.build(500, "电池组信息填写有误！");
		}
		if (group.getAddress().length() != 11) {
			return ResultDto.build(500, "电池组标识必须为11位长度的字符！");
		}
		Integer unit = group.getUnit();
		if (unit < 0 || unit > 5) {
			return ResultDto.build(500, "间隔单位格式错误！");
		}
		String interval = group.getInterval();
		if (unit != 5 && Integer.parseInt(interval) >= 60 && Integer.parseInt(interval) > 0) {
			return ResultDto.build(500, "时间间隔格式错误！");
		}
		if (unit == 5) {
			String regex = "[0-9,]+";
			if (!interval.matches(regex)) {
				return ResultDto.build(500, "时间间隔格式错误！");
			}
			String[] arr = interval.split("\\s*[,|，]+\\s*");
			for (String str : arr) {
				if (StringUtil.isEmpty(str)) {
					return ResultDto.build(500, "时间间隔格式错误！");
				}
				int num = Integer.parseInt(str);
				if (num < 0 || num >= 24) {
					return ResultDto.build(500, "时间间隔格式错误！");
				}
			}
			interval = interval.replaceAll("，", ",");
		}
		Date date = new Date();
		tree.setCreateTime(date);
		tree.setUpdateTime(date);
		tree.setUrl("../battery/home?groupId=");
		Integer baseId = treeDao.get(tree.getParentId()).getBaseId();
		tree.setBaseId(baseId);
		try {
			treeDao.save(tree);
			group.setGroupId(tree.getId());
			group.setCreateTime(date);
			group.setUpdateTime(date);
			Norm norm = group.getNorm();
			norm.setBeginTime(date);
			norm.setUpdateTime(date);
			groupDao.save(group);
			groupDao.listGroupByRun();
			Integer groupId = group.getGroupId();
			groupDao.saveBattery(groupId, tree.getNumber());
			String[] ids = { groupId.toString() };
			updateTreeByNum(0, tree.getNumber(), groupId);
			roleDao.saveRoleToTree(1, ids);
			if (group.getIsRun()) {
				quartzManager.addJob(group, tree);
			}
			return ResultDto.ok(tree);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，添加失败");
		}
	}

	/**
	 * 
	 * @Title: updateTreeByNum
	 * @Description: 递归更新电池数量
	 * @param oldNum
	 * @param newNum
	 * @param id
	 */
	private void updateTreeByNum(int oldNum, int newNum, int id) {
		Tree tree = treeDao.getParentTreeById(id);
		if (tree != null) {
			tree.setNumber(tree.getNumber() - oldNum + newNum);
			treeDao.update(tree);
			updateTreeByNum(oldNum, newNum, tree.getId());
		}
	}

	@Override
	public ResultDto updateGroup(GroupDto groupDto) {
		Tree tree = groupDto.getTree();
		Group group = groupDto.getGroup();
		if (tree == null || group == null) {
			return ResultDto.build(500, "电池组信息填写有误！");
		}
		Integer unit = group.getUnit();
		if (unit < 0 || unit > 5) {
			return ResultDto.build(500, "间隔单位格式错误！");
		}
		String interval = group.getInterval();
		if (unit != 5 && Integer.parseInt(interval) >= 60 && Integer.parseInt(interval) > 0) {
			return ResultDto.build(500, "时间间隔格式错误！");
		}
		if (unit == 5) {
			String regex = "[0-9,]+";
			if (!interval.matches(regex)) {
				return ResultDto.build(500, "时间间隔格式错误！");
			}
			String[] arr = interval.split("\\s*[,|，]+\\s*");
			for (String str : arr) {
				if (StringUtil.isEmpty(str)) {
					return ResultDto.build(500, "时间间隔格式错误！");
				}
				int num = Integer.parseInt(str);
				if (num < 0 || num >= 24) {
					return ResultDto.build(500, "时间间隔格式错误！");
				}
			}
			interval = interval.replaceAll("，", ",");
		}
		Date date = new Date();
		tree.setUpdateTime(date);
		tree.setId(group.getGroupId());
		group.setUpdateTime(date);
		try {
			Tree tree2 = treeDao.get(group.getGroupId());
			treeDao.updateByInfo(tree);
			Norm norm = group.getNorm();
			norm.setBeginTime(date);
			norm.setUpdateTime(date);
			groupDao.update(group);
			Integer groupId = tree.getId();
			if (!tree.getNumber().equals(tree2.getNumber())) {
				testDataService.delTestDataByGroup(groupId);
				batteryDao.deleteBatteryByGroup(groupId);
				groupDao.saveBattery(groupId, tree.getNumber());
				updateTreeByNum(tree2.getNumber(), tree.getNumber(), groupId);
			}
			// updateTreeByNum(tree2.getNumber(), tree.getNumber(), tree.getId());
			quartzManager.removeJob(group);
			if (group.getIsRun()) {
				quartzManager.addJob(group, tree2);
			}
			return ResultDto.ok(tree);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，更新失败");
		}
	}

	@Override
	public List<Group> listGroupByAll() {
		return groupDao.listGroupByAll();
	}

	@Override
	public ResultDto updateIsRun(int[] ids, Boolean isRun) {
		if (ids == null) {
			return ResultDto.build(500, "请选择需要更新状态的用户");
		}
		for (int id : ids) {
			Group group = groupDao.get(id);
			if (group != null) {
				groupDao.updateIsRun(id, isRun);
				if (isRun) {
					Tree tree = treeDao.get(id);
					quartzManager.addJob(group, tree);
				} else {
					quartzManager.removeJob(group);
				}
			}
		}
		return ResultDto.build(200, "更改状态成功！");
	}

	@Override
	public List<Group> listGroupByRun() {
		return groupDao.listGroupByRun();
	}

	@Override
	public GraphDto getTestGraphDto(TempDto temp) {
		GraphDto graphDto = null;
		Integer groupId = temp.getId();
		Group group = groupDao.get(groupId);
		Tree tree = treeDao.get(groupId);
		quartzManager.removeJob(group);
		String type = temp.getType();
		Date date = new Date();
		try {
			Integer len = tree.getNumber();
			// 01 03 00 00 00 14 45 c5
			// 01：装置地址 03：功能码 00 00 ：寄存器起始号 00 14：读取长度 45 c5：CRC校验码
			String sysInfo = client.send(group.getAddress(), "0103000000104406"); // 系统信息
			// 单体电压
			if ("voltage".equals(type)) {
				String voltageCode = "010303e8" + NumberUtil.decToHex(len);
				String voltageCrc = CRCUtil.getCrc16(StringUtil.toBytes(voltageCode));
				String voltage = client.send(group.getAddress(), voltageCode + voltageCrc);
				graphDto = testDataService.saveTestData(voltage, null, null, group, date);
				graphDto.setName("电压");
				graphDto.setType("单体电压/V");
				graphDto.setUnit("/V");
			}
			// 单体内阻
			if ("inter".equals(type)) {
				String interCode = "010307d0" + NumberUtil.decToHex(len);
				String interCrc = CRCUtil.getCrc16(StringUtil.toBytes(interCode));
				String inter = client.send(group.getAddress(), interCode + interCrc);
				graphDto = testDataService.saveTestData(null, inter, null, group, date);
				graphDto.setName("内阻");
				graphDto.setType("单体内阻/mΩ");
				graphDto.setUnit("/mΩ");
			}
			// 单体温度
			if ("tempera".equals(type)) {
				String temperaCode = "01030bb8" + NumberUtil.decToHex(len);
				String temperaCrc = CRCUtil.getCrc16(StringUtil.toBytes(temperaCode));
				String tempera = client.send(group.getAddress(), temperaCode + temperaCrc);
				graphDto = testDataService.saveTestData(null, null, tempera, group, date);
				graphDto.setName("温度");
				graphDto.setType("单体温度/℃");
				graphDto.setUnit("/℃");
			}
			// 参数设置
			String param = client.send(group.getAddress(), "01030fa00016C732");
			groupInfoService.saveGroupInfo(sysInfo, group, date);
			groupLimitService.saveLimit(param, group, date);
			graphDto.setLimitVal(temp.getVal());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		quartzManager.addJob(group, tree);
		return graphDto;
	}

	@Override
	public GroupInfo getSystemInfo(TempDto temp) {
		return groupDao.getSystemInfo(temp);
	}

	@Override
	public ExcelLimitDto getExcelLimitDto(Integer treeId) {
		List<Limit> listLimit = limitDao.listLimitByGroup(treeId);
		ExcelLimitDto excelLimitDto = new ExcelLimitDto();
		for (Limit limit : listLimit) {
			String type = limit.getType();
			Double highWarn = limit.getHighWarn();
			Double lowWarn = limit.getLowWarn();
			if ("电池组电压/V".equals(type)) {
				excelLimitDto.setGroupHighVoltage(highWarn);
				excelLimitDto.setGroupLowVoltage(lowWarn);
			} else if ("环境温度/℃".equals(type)) {
				excelLimitDto.setGroupHighTempera(highWarn);
			} else if ("单体电压/V".equals(type)) {
				excelLimitDto.setMonHighVoltage(highWarn);
				excelLimitDto.setMonLowVoltage(lowWarn);
			} else if ("单体内阻/mΩ".equals(type)) {
				excelLimitDto.setMonHighInter(highWarn);
			} else if ("单体温度/℃".equals(type)) {
				excelLimitDto.setMonHighTempera(highWarn);
			}
		}
		return excelLimitDto;
	}

}
