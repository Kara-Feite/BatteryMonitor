package com.gzyw.monitor.service;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;

/**
 * 
 * @ClassName: AlarmService
 * @Description: 警告服务层
 * @author LW
 * @date 2018年6月19日 上午10:50:47
 */
public interface AlarmService {

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param temp 临时对象
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, TempDto temp);

	/**
	 * 
	 * @Title: getAlarmById
	 * @Description: 根据id获取警告
	 * @param id
	 * @return
	 */
	Alarm getAlarmById(Integer id);

	/**
	 * 
	 * @Title: updateAlarm
	 * @Description: 更新警告
	 * @param alarm
	 * @return
	 */
	ResultDto updateAlarm(Alarm alarm);

	/**
	 * 
	 * @Title: delAlarmByBat
	 * @Description: 批量删除警告
	 * @param ids
	 * @return
	 */
	ResultDto delAlarmByBat(int[] ids);

}
