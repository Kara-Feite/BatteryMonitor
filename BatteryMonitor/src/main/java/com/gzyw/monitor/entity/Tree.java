package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DynamicInsert、DynamicUpdate 设置动态添加和动态更新 生成动态的语句,
 * 如果这个字段的值是null就不会被加入到语句中，提高了效率
 * @ClassName: Tree
 * @Description: 基站树形节点
 * @author LW
 * @date 2018年6月4日 下午1:54:39
 */
@Entity
@Table(name = "bm_tree")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Tree implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -2638594560498796421L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 根id
	 */
	@Column(name = "base_id", nullable = false)
	private Integer baseId;

	/**
	 * 父id
	 */
	@Column(name = "parent_id", nullable = false)
	private Integer parentId;

	/**
	 * 名称
	 */
	@Column(name = "name", length = 64, nullable = false)
	@NotEmpty(message = "名称不能为空")
	private String name;

	/**
	 * 链接地址
	 */
	@Column(name = "url", length = 64, nullable = false)
	private String url;

	/**
	 * 电池数量
	 */
	@Column(name = "number", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer number;

	/**
	 * 状态
	 */
	@Column(name = "state", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean state;

	/**
	 * 警告
	 */
	@Column(name = "warn", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer warn;

	/**
	 * 预警
	 */
	@Column(name = "plan", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer plan;

	/**
	 * 备注
	 */
	@Column(name = "remark", length = 255)
	private String remark;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 角色
	 */
	@ManyToMany(mappedBy = "setTree", fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Role> setRole;

	/**
	 * @Title: Tree
	 * @Description: Tree构造函数
	 */
	public Tree() {}

	/**
	 * @Title: Tree
	 * @Description: Tree构造函数
	 * @param baseId
	 * @param parentId
	 * @param name
	 * @param url
	 * @param number
	 * @param state
	 * @param warn
	 * @param plan
	 * @param remark
	 * @param createTime
	 * @param updateTime
	 */
	public Tree(Integer baseId, Integer parentId, String name, String url, Integer number, Boolean state, Integer warn,
			Integer plan, String remark, Date createTime, Date updateTime) {
		this.baseId = baseId;
		this.parentId = parentId;
		this.name = name;
		this.url = url;
		this.number = number;
		this.state = state;
		this.warn = warn;
		this.plan = plan;
		this.remark = remark;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getBaseId
	 * @Description: 获取baseId
	 * @return: Integer baseId
	 */
	public Integer getBaseId() {
		return baseId;
	}

	/**
	 * @Title: setBaseId
	 * @Description: 设置baseId
	 */
	public void setBaseId(Integer baseId) {
		this.baseId = baseId;
	}

	/**
	 * @Title: getParentId
	 * @Description: 获取parentId
	 * @return: Integer parentId
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @Title: setParentId
	 * @Description: 设置parentId
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getUrl
	 * @Description: 获取url
	 * @return: String url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @Title: setUrl
	 * @Description: 设置url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @Title: getNumber
	 * @Description: 获取number
	 * @return: Integer number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @Title: setNumber
	 * @Description: 设置number
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getWarn
	 * @Description: 获取warn
	 * @return: Integer warn
	 */
	public Integer getWarn() {
		return warn;
	}

	/**
	 * @Title: setWarn
	 * @Description: 设置warn
	 */
	public void setWarn(Integer warn) {
		this.warn = warn;
	}

	/**
	 * @Title: getPlan
	 * @Description: 获取plan
	 * @return: Integer plan
	 */
	public Integer getPlan() {
		return plan;
	}

	/**
	 * @Title: setPlan
	 * @Description: 设置plan
	 */
	public void setPlan(Integer plan) {
		this.plan = plan;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetRole
	 * @Description: 获取setRole
	 * @return: Set<Role> setRole
	 */
	public Set<Role> getSetRole() {
		return setRole;
	}

	/**
	 * @Title: setSetRole
	 * @Description: 设置setRole
	 */
	public void setSetRole(Set<Role> setRole) {
		this.setRole = setRole;
	}
	
	/**
	 * 
	 * @return 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baseId == null) ? 0 : baseId.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((plan == null) ? 0 : plan.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((setRole == null) ? 0 : setRole.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((warn == null) ? 0 : warn.hashCode());
		return result;
	}

	/**
	 * 
	 * @param obj
	 * @return 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tree other = (Tree) obj;
		if (baseId == null) {
			if (other.baseId != null)
				return false;
		} else if (!baseId.equals(other.baseId))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (plan == null) {
			if (other.plan != null)
				return false;
		} else if (!plan.equals(other.plan))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (setRole == null) {
			if (other.setRole != null)
				return false;
		} else if (!setRole.equals(other.setRole))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (warn == null) {
			if (other.warn != null)
				return false;
		} else if (!warn.equals(other.warn))
			return false;
		return true;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tree [id=" + id + ", baseId=" + baseId + ", parentId=" + parentId + ", name=" + name + ", url=" + url
				+ ", number=" + number + ", state=" + state + ", warn=" + warn + ", plan=" + plan + ", remark=" + remark
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
