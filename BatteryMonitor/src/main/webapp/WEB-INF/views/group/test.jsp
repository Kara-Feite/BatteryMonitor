<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
select,input{width:220px;}
a{width:70px;}
.textbox-label{width:70px;}
</style>
<script>
function test(){
	myForm = $('#myForm');
	if ($('#myForm').form('enableValidation').form('validate')) {
		var id = myForm.find('[name=id]').val(),
			type = myForm.find('[name=type]').val(),
			val = myForm.find('[name=val]').val();
		testFrame.location.href = '${ctx}/group/test/graph?id=' + id + '&type=' + type + '&val=' + val;
	}
}
</script>
<div class="easyui-panel" title="操作：电池组测试" data-options="width:'100%',height:'99.5%'" style='padding:10px;'>
	<div class="easyui-layout" data-options="fit:true">
	    <div data-options="region:'west',split:false" style="width:250px;padding:100px 0 0 15px">
	    	<sf:form name="myForm" id='myForm' method="get" modelAttribute="temp" enctype="multipart/form-data">
				<sf:hidden path="id"/>
				<sf:select path="type" class="easyui-combobox" 
					 data-options="label:'测试类型：',required:true,editable:false">>
				 	<sf:option label="单体电压/V" value="voltage"/>
		            <sf:option label="单体内阻/mΩ" value="inter"/>
		            <sf:option label="单体温度/℃" value="tempera"/>
				</sf:select>
				<div style='margin:50px 0'>
					<sf:input path="val" class="easyui-numberbox" style='margin:50px 0'
		 			data-options="label:'门 限 值:',required:true,min:0,precision:2"/>
				</div>
				<div style='margin-left: 20px'>
			 		<a href="javascript:void(0)" class="easyui-linkbutton" 
	           		data-options="iconCls:'icon-search'" onclick="test()">测 试</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	           		<a href="${ctx}/group/frame?id=${group.groupId}" class="easyui-linkbutton" 
	           			data-options="iconCls:'icon-reload'">返 回</a>
				</div>
			</sf:form>
	    </div>
	    <div data-options="region:'center'" class='data-right'>
	    	<div id='group-info'>
	    		<div class="easyui-layout" fit="true">
	    			<div data-options="region:'center',title:'电池组信息',border:false">
					    <table id='group_lists' class='easyui-datagrid' iconCls='icon-table' fitColumns='true' data-options="toolbar:groupToolbar,border:false">
						    <thead>
						        <tr>
						            <th field='groupId' width='30' align='center'>id</th>  
					                <th field='groupName' width='50'align='center'>名称</th>   
					                <th field='address' width='50' align='center'>电池组标识</th>
					                <th field='interval' width='30' align='center'>测试间隔</th>
					                <th field='unit' width='30' align='center' formatter='formatUnit'>间隔单位</th>
					                <th field='createTime' width='70' align='center'>创建时间</th>
					                <th field='updateTime' width='70' align='center'>更新时间</th>
					                <th field='isRun' width='30' align='center' formatter='isRun'>是否运行</th>
						        </tr>
						    </thead>
						    <tbody>
					            <tr>
					                <td>${group.groupId}</td>
					                <td>${tree.name}</td>
					                <td>${group.address}</td>
					                <td>${group.interval}</td>
					                <td>${group.unit}</td>
					                <td>${group.createTime}</td>
					                <td>${group.updateTime}</td>
					                <td>${group.isRun}</td>
					            </tr>
					        </tbody>
						</table>
	    			</div>
	    		</div>
	    	</div>
	    	<div id='test-data'>
	    		<div class="easyui-layout" fit="true">
	    			<div data-options="region:'center',title:'测试结果',border:false">
	    				<iframe name='testFrame' src='${ctx}/group/test/graph' style='overflow:visible;' scrolling='no' frameborder='no' width='95%' height='350px'></iframe>
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>
</div>
<style>
	.data-right{padding:10px;}
	.data-right>div{border:1px solid #95B8E7;}
	#group-info{height:108px;margin-bottom:10px;}
	#test-data{height:382px;}
</style>
<script>
function isRun(val, row) {
	if (val === 'true') {
		return '<font style="color:green">已 运 行</font>';
	}else{
		return '<font style="color:red">未 运 行</font>';
	}
}
function formatUnit(val, row) {
	if (val === '1') {
		return '秒';
	} else if (val === '2') {
		return '分钟';
	} else if (val === '3') {
		return '小时';
	} else if (val === '4') {
		return '天';
	} else if (val === '5') {
		return '固定小时';
	}
}
var groupToolbar = [
    {text:'开 始',iconCls:'icon-remove',handler:function(){start();}},'-',
    {text:'停 止',iconCls:'icon-reload',handler:function(){stop();}},'-',
];
function start() {
	$.messager.confirm('确认','你确认要开启监测选择的记录吗？',function(r){
		if(r){
			$.ajax({
				type:'post',
				url:'${ctx}/group/start',
				data:{ids:'${group.groupId}'},
				dataType:'json',
				success:function(r){
					if (r.status === 200) {
						document.getElementsByClassName('datagrid-cell-c1-isRun')[1].innerHTML = '<font style="color:green">已 运 行</font>';
					} else {
						$.messager.alert({title:'提示',msg:r.msg});
					}
				}
			});
		}
	});
}

function stop() {
	$.messager.confirm('确认','你确认要关闭监测选择的记录吗？',function(r){
		if(r){
			$.ajax({
				type:'post',
				url:'${ctx}/group/stop',
				data:{ids:'${group.groupId}'},
				dataType:'json',
				success:function(r){
					if (r.status === 200) {
						document.getElementsByClassName('datagrid-cell-c1-isRun')[1].innerHTML = '<font style="color:red">未 运 行</font>';
					} else {
						$.messager.alert({title:'提示',msg:r.msg});
					}
				}
			});
		}
	});
}

</script>
