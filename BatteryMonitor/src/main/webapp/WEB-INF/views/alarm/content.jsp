<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>站点管理</title>	
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<c:set var="treeId" value="${empty treeId ? tree.id : treeId}"/>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>	
#denglu *{vertical-align:middle;}
select,input{width:160px;}
</style>
<script>
$(function(){
	$('#alarm_lists').datagrid({
		url:'${ctx}/alarm/datagrid?treeId=${treeId}',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		pageNumber : 1,
		sortName : 'alarmTime',
		sortOrder : 'DESC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'name',
		        title:'名 称',
		        width:50,
		        align:'center'
			},{
		        field:'mold',
		        title:'告警类型',
		        width:50,
		        align:'center'
			},{
		        field:'type',
		        title:'测量类型',
		        width:50,
		        align:'center'
			},{
		        field:'details',
		        title:'详情',
		        width:60,
		        align:'center'
			},{
		        field:'alarmTime',
		        title:'告警日期',
		        width:70,
		        align:'center',
		        sortable:true,
		        formatter : formatDateTime
			},{
		        field:'isDeal',
		        title:'状态',
		        width:50,
		        align:'center',
		        formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">已处理</font>';
					}else{
						return '<font style="color:red">未处理</font>';
					}
				}
			},{
				field:'remark',
				title:'备注',
				width:50,
				align:'center',
				formatter : formatNull
			},{
				field:'dealer',
				title:'处理人员',
				width:40,
				align:'center',
				formatter : formatNull
			},{
				field:'dealTime',
				title:'处理日期',
				width:70,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			}]],
		toolbar:[
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',		
			{text:'删 除',iconCls:'icon-remove',handler:function(){del();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});

function query(){
	$('#alarm_lists').datagrid('load',serializeObject($('#search_form').form()));
}

function clean(){
	$('#alarm_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}

function update(){
	<permission:notPermission name="tree:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#alarm_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/alarm/edit?id="+rows[0].id+"&treeId=${treeId}";
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}

function del(){
	<permission:notPermission name="tree:delete">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#alarm_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/alarm/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						console.log(r);
						$('#alarm_lists').datagrid('load',{});
						$('#alarm_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}
function unselect(){
	$('#alarm_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
} 
</script>
<body>	
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north',title:'节点：${tree.name} 告警'" style="height:81px;">
			<form id="search_form">
				<input type="hidden" name="treeId" value="${alarm.group.id}">
				<table cellSpacing="1" cellPadding="1" width="100%" bgColor="#eeeeee" 
			    	style="border:0px solid #8ba7e3" border="0">
				    <tr bgColor="#f5fafe" class="ta_01">
				    	<td width="15%">
				    	 	<strong>告警类型：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">
							<select name="mold"> 
								<option value=""/></option>
								<option value="系统告警"/></option>
						     	<option value="过高告警">过高告警</option>   
					            <option value="过高预警">过高预警</option>  
					            <option value="过低告警">过低告警</option>   
					            <option value="过低预警">过低预警</option>  													           
		        			</select></td>	
				         <td>
				          	<strong>已处理：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">
							<select name="state"> 								
					            <option value=""/></option>  
						     	<option value="true">已处理</option>   
					            <option value="false">未处理</option>    							           
			        		</select></td>
				         <td >
				          	<strong>测试类型：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">
				         	<select name="type">
								<option value=""/></option>  
								<option value="系统告警"/></option>
					            <option value="单体电压/V">单体电压/V</option>   
					            <option value="单体内阻/mΩ">单体内阻/mΩ</option>  
					            <option value="单体温度/℃">单体温度/℃</option> 
				        	</select></td>	
				    </tr>	    	   
				    <tr bgColor="#f5fafe" class="ta_01">
				    	<td>
				          	<strong>开始时间：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">
				         	<input name="startTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/></td>
				          <td>
				          	<strong>结束时间：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">
				         	<input name="endTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/></td>
				         <td colspan="2">			          												
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查  询</a>						
							&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
								onclick="clean();" href="javascript:void(0);">清  空</a>
							<!-- &nbsp;&nbsp; 
							<a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="checkExecl();" href="javascript:void(0);" style="width:130px;">导出Excel表格</a> -->
						</td>					
					</tr>					
				</table>
			</form>
		</div>
		<!-- ----------------数据层------------- -->
		<div data-options="region:'center'">
			<table id="alarm_lists"></table>
		</div>
	</div>
</body>
</html>