package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.JsonUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Menu;
import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToMenu;
import com.gzyw.monitor.entity.RoleToTree;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.MenuService;
import com.gzyw.monitor.service.RoleService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: RoleController
 * @Description: 权限管理控制层
 * @author LW
 * @date 2018年7月5日 下午3:01:35
 */
@Controller
@RequestMapping("/role")
public class RoleController {
	
	/**
	 * 角色服务层
	 */
	@Autowired
	private RoleService roleService;
	
	/**
	 * 节点树服务层
	 */
	@Autowired
	private TreeService treeService;
	
	/**
	 * 菜单服务层
	 */
	@Autowired
	private MenuService menuService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: showList
	 * @Description: 显示列表页面
	 * @return
	 */
	@RequestMapping("/list")
	@Permissions("role:query")
	String showList() {
		return "role/list";
	}
	
	/**
	 * 
	 * @Title: showEdit
	 * @Description: 跳转到编辑页面
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/edit")
	String showEdit(@RequestParam(value = "id", required = false) Integer id, Model model) {
		if (id != null) {
			Role role = roleService.getRoleById(id);
			if (role != null) {
				List<RoleToMenu> listRoleToMenu = roleService.listMenuById(id);
				List<RoleToTree> listRoleToTree = roleService.listTreeById(id);
				model.addAttribute("role", role);
				model.addAttribute("listRoleToMenu", listRoleToMenu);
				model.addAttribute("listRoleToTree", listRoleToTree);
			} else {
				return "redirect:/error";
			}
		} else {
			model.addAttribute("role", new Role());
		}
		List<Tree> listTree = treeService.listTreeByAll();
		model.addAttribute("listTree", listTree);
		List<Menu> listMenu = menuService.listMenuByAll();
		model.addAttribute("listMenu", listMenu);
		return "role/edit";
	}
	
	/**
	 * 
	 * @Title: saveRole
	 * @Description: 添加角色
	 * @param role 角色
	 * @param menuIds 菜单id列表
	 * @param treeIds 基站id列表
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "权限管理", description = "添加角色")
	@Permissions("role:add")
	public ResultDto saveRole(String roleJson, String menuIds, String treeIds, HttpSession session) {
		Role role = JsonUtil.jsonToObject(roleJson, Role.class);
		if (role == null || StringUtil.isEmpty(role.getName())) {
			ResultDto.build(500, "用户名不能为空");
		}
		User user = (User) session.getAttribute("user");
		Integer userId = user.getId();
		role.setCreateBy(userId);
		role.setUpdateBy(userId);
		return roleService.saveRole(role, menuIds, treeIds);
	}
	
	/**
	 * 
	 * @Title: updateRole
	 * @Description: 更新角色
	 * @param roleJson
	 * @param menuIds
	 * @param treeIds
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "权限管理", description = "更新角色")
	@Permissions("role:update")
	public ResultDto updateRole(String roleJson, String menuIds, String treeIds, HttpSession session) {
		Role role = JsonUtil.jsonToObject(roleJson, Role.class);
		if (role == null || StringUtil.isEmpty(role.getName())) {
			ResultDto.build(500, "用户名不能为空");
		}
		User user = (User) session.getAttribute("user");
		role.setUpdateBy(user.getId());
		return roleService.updateRole(role, menuIds, treeIds);
	}
	
	/**
	 * 
	 * @Title: deleteRole
	 * @Description: 删除角色
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemControllerLog(type = "权限管理", description = "删除角色")
	@Permissions("role:delete")
	public ResultDto deleteRole(int[] ids) {
		try {
			return roleService.deleteRole(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: congeal
	 * @Description: 冻结
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/congeal",method=RequestMethod.GET)
	@ResponseBody
	@SystemControllerLog(type = "权限管理", description = "禁用角色")
	@Permissions("role:congeal")
	public ResultDto congeal(int[] ids) {
		try {
			return roleService.updateState(ids, false);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，冻结失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: thaw
	 * @Description: 激活
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/thaw",method=RequestMethod.GET)
	@ResponseBody
	@SystemControllerLog(type = "权限管理", description = "激活角色")
	@Permissions("role:thaw")
	public ResultDto thaw(int[] ids) {
		try {
			return roleService.updateState(ids, true);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，激活失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 返回电池规格列表数据
	 * @param page 页码
	 * @param role 角色
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value="/datagrid",method=RequestMethod.POST)
	@ResponseBody
	public EUDataGridDto getDatagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Role role,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {	
		return roleService.getDatagrid(page, rows, sort, order, role);
	}

}
