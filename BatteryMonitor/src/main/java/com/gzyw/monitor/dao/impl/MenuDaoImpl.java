package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.dao.MenuDao;
import com.gzyw.monitor.entity.Menu;

/**
 * 
 * @ClassName: MenuDaoImpl
 * @Description: 菜单持久层接口实现类
 * @author LW
 * @date 2018年7月4日 上午10:35:29
 */
@Repository
public class MenuDaoImpl extends BaseDaoImpl<Menu> implements MenuDao {

	@Override
	public List<Menu> listMenuByUserId(Integer id, boolean isRoot) {
		StringBuilder sb = new StringBuilder("SELECT bm.* FROM bm_menu bm");
		sb.append(" LEFT JOIN bm_role_menu br ON bm.id = br.menu_id");
		sb.append(" LEFT JOIN bm_role_user bu ON br.role_id = bu.role_id");
		sb.append(" WHERE bu.user_id = :userId");
		if (isRoot) {
			sb.append(" AND bm.parent_id = 0");
		}
		@SuppressWarnings("unchecked")
		NativeQuery<Menu> q = getSession().createNativeQuery(sb.toString()).addEntity(Menu.class);
		return q.setParameter("userId", id).list();
	}

	@Override
	public List<Menu> listMenuByAll() {
		String hql = "FROM Menu WHERE state = true";
		return queryForList(hql);
	}

}
