package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: TreeDaoImpl
 * @Description: 树形节点持久层接口实现类
 * @author LW
 * @date 2018年6月7日 下午3:40:43
 */
@Repository
public class TreeDaoImpl extends BaseDaoImpl<Tree> implements TreeDao {

	@Override
	public List<Tree> listTreeByAll() {
		String hql = "FROM Tree";
		return queryForList(hql);
	}

	@Override
	public List<Tree> listTreeByRoot(Integer userId, Tree tree, String sort, String order) {
		StringBuilder sb = new StringBuilder("SELECT bt.* FROM bm_tree bt");
		sb.append(" LEFT JOIN bm_role_tree rt ON bt.id = rt.tree_id");
		sb.append(" LEFT JOIN bm_role br ON rt.role_id = br.id");
		sb.append(" LEFT JOIN bm_role_user ru ON br.id = ru.role_id");
		sb.append(" WHERE ru.user_id = :userId AND bt.parent_id = 0");
		String name = tree.getName();
		if (StringUtil.isNotEmpty(name)) {
			sb.append(" AND bt.name LIKE '%" + name + "%'");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY bt." + sort + " " + order);
		}
		@SuppressWarnings("unchecked")
		NativeQuery<Tree> q = getSession().createNativeQuery(sb.toString()).addEntity(Tree.class);
		return q.setParameter("userId", userId).list();
	}

	@Override
	public long countTreeByRoot(Integer userId) {
		StringBuilder sb = new StringBuilder("SELECT COUNT(DISTINCT(bt.id)) FROM bm_tree bt");
		sb.append(" LEFT JOIN bm_role_tree rt ON bt.id = rt.tree_id");
		sb.append(" LEFT JOIN bm_role br ON rt.role_id = br.id");
		sb.append(" LEFT JOIN bm_role_user ru ON br.id = ru.role_id");
		sb.append(" WHERE ru.user_id = :userId AND parent_id = 0");
		NativeQuery<?> q = getSession().createNativeQuery(sb.toString());
		Object result = q.setParameter("userId", userId).uniqueResult();
		return Long.valueOf(result.toString());
	}

	@Override
	public List<Tree> listTreeByPage(Integer page, Integer rows, String sort, String order, Tree tree) {
		String hql = "FROM Tree ";
		hql = setParam(hql, tree, sort, order);
		return queryByPage(hql, page, rows);
	}

	@Override
	public void updateTreeByRoot(Tree tree) {
		String hql = "UPDATE Tree SET name = ?, remark = ?, updateTime = ? WHERE id = ? AND parentId = 0";
		update(hql, tree.getName(), tree.getRemark(), tree.getUpdateTime(), tree.getId());
	}

	@Override
	public Integer updateTree(Tree tree) {
		String hql = "UPDATE Tree SET name = ?, remark = ?, url = ?, updateTime = ? WHERE id = ?";
		return update(hql, tree.getName(), tree.getRemark(), tree.getUrl(), tree.getUpdateTime(), tree.getId());
	}

	@Override
	public void updateTreeByState(int id, Boolean state) {
		String hql = "UPDATE Tree SET state = ? WHERE id = ?";
		update(hql, state, id);
	}

	@Override
	public void updateByInfo(Tree tree) {
		String hql = "UPDATE Tree SET name = ?, remark = ?, number = ?, updateTime = ? WHERE id = ?";
		update(hql, tree.getName(), tree.getRemark(), tree.getNumber(), tree.getUpdateTime(), tree.getId());
	}

	@Override
	public List<Tree> listTreeByPid(int id) {
		String hql = "FROM Tree WHERE parentId = ?";
		return queryForList(hql, id);
	}

	@Override
	public List<Tree> listTreeByUserId(Integer userId) {
		StringBuilder sb = new StringBuilder("SELECT bt.* FROM bm_tree bt");
		sb.append(" LEFT JOIN bm_role_tree rt ON bt.id = rt.tree_id");
		sb.append(" LEFT JOIN bm_role br ON rt.role_id = br.id");
		sb.append(" LEFT JOIN bm_role_user ru ON br.id = ru.role_id");
		sb.append(" WHERE ru.user_id = :userId AND bt.state = true");
		@SuppressWarnings("unchecked")
		NativeQuery<Tree> q = getSession().createNativeQuery(sb.toString()).addEntity(Tree.class);
		return q.setParameter("userId", userId).list();
	}

	@Override
	public long countTree(Tree tree) {
		String hql = "SELECT COUNT(*) FROM Tree";
		hql = setParam(hql, tree, null, null);
		return count(hql);
	}
	
	@Override
	public Tree getParentTreeById(int id) {
		Tree tree = get(id);
		return get(tree.getParentId());
	}

	/**
	 * 
	 * @Title: setParam
	 * @Description: 设置HQL参数
	 * @return
	 */
	private String setParam(String hql, Tree tree, String sort, String order) {
		StringBuilder sb = new StringBuilder(hql);
		Integer pid = tree.getParentId();
		sb.append(" WHERE parentId = " + pid);
		String name = tree.getName();
		if (StringUtil.isNotEmpty(name)) {
			sb.append(" AND name LIKE '%" + name + "%'");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY " + sort + " " + order);
		}
		return sb.toString();
	}
	
}
