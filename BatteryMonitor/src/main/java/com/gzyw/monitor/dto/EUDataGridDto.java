package com.gzyw.monitor.dto;

import java.util.List;

/**
 * EeasyUi的DataGrid的数据结构
 * @ClassName: EUDataGridResult
 * @Description: DataGrid数据返回结构
 * @author LW
 * @date 2018年6月8日 上午10:16:21
 */
public class EUDataGridDto {

	/**
	 * 总数
	 */
	private Long total;
	
	/**
	 * 数据
	 */
	private List<?> rows;

	/**
	 * @Title: EUDataGridDto
	 * @Description: EUDataGridDto构造函数
	 */
	public EUDataGridDto() {
		super();
	}

	/**
	 * @Title: EUDataGridDto
	 * @Description: EUDataGridDto构造函数
	 * @param total
	 * @param rows
	 */
	public EUDataGridDto(Long total, List<?> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

	/**
	 * @Title: getTotal
	 * @Description: 获取total
	 * @return: Long total
	 */
	public Long getTotal() {
		return total;
	}

	/**
	 * @Title: setTotal
	 * @Description: 设置total
	 */
	public void setTotal(Long total) {
		this.total = total;
	}

	/**
	 * @Title: getRows
	 * @Description: 获取rows
	 * @return: List<?> rows
	 */
	public List<?> getRows() {
		return rows;
	}

	/**
	 * @Title: setRows
	 * @Description: 设置rows
	 */
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	
	
}
