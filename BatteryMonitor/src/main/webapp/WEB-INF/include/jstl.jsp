<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="permission" uri="/security/permission/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- 
fmt使用--->
格式化浮点型数据：
	<fmt:formatNumber type="number" value="${val}" pattern="0.00" maxFractionDigits="2"/>
格式化时间：
	<fmt:formatDate value="${time}" pattern="yyyy-MM-dd HH:mm:ss"/> 
--%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="ctxStatic" value="${pageContext.request.contextPath}/static" />

