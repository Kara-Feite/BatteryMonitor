package com.gzyw.monitor.common.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

/**
 * 
 * @ClassName: ArrayUtil
 * @Description: 数组工具类
 * @author LW
 * @date 2018年6月14日 上午10:15:48
 */
public class ArrayUtil {

	private transient static final String DEFAULT_JOIN_STR = ",";

	/**
	 * 
	 * @Title: isEmpty
	 * @Description: 判断数组是否为空
	 * @param array
	 * @return
	 */
	public static <T> boolean isEmpty(final T[] arr) {
		return arr == null || arr.length == 0;
	}

	/**
	 * 
	 * @Title: isEmpty
	 * @Description:
	 * @param arr
	 * @return
	 */
	public static <T> boolean isNotEmpty(final T[] arr) {
		return (arr != null && arr.length != 0);
	}

	/**
	 * 
	 * @Title: join
	 * @Description: 以逗号拼接为字符串
	 * @param arr
	 * @return
	 */
	public static final String join(final Object[] arr) {
		return join(arr, DEFAULT_JOIN_STR);
	}

	/**
	 * 
	 * @Title: join
	 * @Description: 以指定字符拼接为字符串
	 * @param arr
	 * @param joinStr
	 * @return
	 */
	public static final String join(final Object[] arr, final String joinStr) {
		if (isEmpty(arr)) {
			return StringUtil.EMPTY;
		}
		final StringBuffer sb = new StringBuffer(String.valueOf(arr[0]));
		for (int i = 1, len = arr.length; i < len; i++) {
			sb.append(StringUtil.isNotEmpty(joinStr) ? joinStr : StringUtil.EMPTY).append(String.valueOf(arr[i]));
		}
		return sb.toString();
	}

	/**
	 * 
	 * @Title: toArray
	 * @Description: 集合转数组
	 * @param collection
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] toArray(final Collection<T> collection, final Class<T> clazz) {
		if (collection == null) {
			return null;
		}
		final T[] arr = (T[]) Array.newInstance(clazz, collection.size());
		return collection.toArray(arr);
	}

	/**
	 * 
	 * @Title: toArray
	 * @Description: 集合转对象数组
	 * @param collection
	 * @return
	 */
	public static final Object[] toArray(final Collection<?> collection) {
		if (collection == null) {
			return null;
		}
		final Object[] arr = (Object[]) Array.newInstance(Object.class, collection.size());
		int i = 0;
		for (Iterator<?> it = collection.iterator(); it.hasNext();) {
			arr[i++] = it.next();
		}
		return arr;
	}

}
