<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<style>	
a:active {color: #cc0000; text-decoration: underline}
a:hover {color: #cc0000; text-decoration: underline;position:relative;left:1px;top:1px}
font{color:#ff0000;}
#denglu *{ vertical-align:middle;}
</style>
<script src="${ctxStatic}/js/jquery-1.4.4.min.js"></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
</head>	
<body >
<sf:form name="myForm" method="get" enctype="multipart/form-data" modelAttribute="groupDto">	
 	<table cellSpacing="1" cellPadding="1" width="100%" bgColor="#eeeeee" 
    	style="border:0px solid #8ba7e3" border="0">
	    <tr>
			<td colSpan="10" id="denglu">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" bgColor="#ffffff">  	
				  	<tr height="25">
				  		<td id="denglu">
				    		<img src="${ctxStatic}/images/yin.gif" width="22">
			 				<strong>节点：${groupDto.tree.name} 查看</strong>
				    	</td>
				    	<td align="right">
				    		&nbsp;|&nbsp;
				    		<a target="groupsFrame"
								href="${ctx}/test/data/group?id=${groupDto.tree.id}">
								<b>表 格</b></a>				    						    										
							&nbsp;|&nbsp;
							<a target="groupsFrame"
								href="${ctx}/group/graph?treeId=${groupDto.tree.id}">
								<b>混合图</b></a>
							&nbsp;|&nbsp;
							<a target="mainFrame"
								href="${ctx}/group/home?groupId=${groupDto.tree.parentId}">
								<b>返 回</b></a>
							&nbsp;|||&nbsp;			    					    	
				    	</td> 	    	    			    	
				  	</tr>  	
				</table>					
			</td>
	    </tr>	  	 	
		<tr bgColor="#f5fafe" class="ta_01">    	
	         <td width="15%">
	          	<strong>制 造 商</strong><font> *</font></td>
	         <td bgColor="#ffffff" width="15%">${groupDto.group.norm.maker}</td>
	         <td width="15%">
	          	<strong>电池电压/V</strong><font> *</font></td>
	         <td bgColor="#ffffff" width="10%"><fmt:formatNumber type="number" value="${groupDto.group.norm.voltage}" pattern="0.000" maxFractionDigits="3"/></td>
	          <td width="15%"><strong>电 池 数</strong><font> *</font></td>
	         <td bgColor="#ffffff" width="10%">${groupDto.tree.number}</td>	            	
	    </tr>	    	   
	     <tr bgColor="#f5fafe" class="ta_01">	    	
	         <td><strong>电池类型</strong><font> *</font></td>
	         <td bgColor="#ffffff">${groupDto.group.norm.type}</td>
	         <td><strong>参考内阻/mΩ</strong><font> *</font></td>
	         <td bgColor="#ffffff"><fmt:formatNumber type="number" value="${groupDto.group.norm.inter}" pattern="0.000" maxFractionDigits="3"/></td>
	         <td><strong>投运日期</strong><font> *</font></td>
	         <td bgColor="#ffffff"><fmt:formatDate value="${groupDto.group.norm.beginTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>	           
	    </tr>
	    <tr bgColor="#f5fafe" class="ta_01">    	
	         <td><strong>&nbsp;型&nbsp;&nbsp;&nbsp;&nbsp;号&nbsp;</strong><font> *</font></td>
	         <td bgColor="#ffffff">${groupDto.group.norm.model}</td>	
	         <td><strong>电池容量/Ah</strong><font> *</font></td>
	         <td bgColor="#ffffff">${groupDto.group.norm.volume}</td>	  
	         <td><strong>&nbsp;备&nbsp;&nbsp;&nbsp;&nbsp;注&nbsp;</strong><font> *</font></td>
	         <td bgColor="#ffffff">${empty groupDto.tree.remark ? '无' : groupDto.tree.remark}</td>       
	    </tr>  
	   <tr bgColor="#f5fafe" class="ta_01">
			<td><strong>最大值(电压/V)</strong></td>			
			<td bgColor="#ffffff">
				${maxMinDto.maxVoltageNo}：<font><fmt:formatNumber type="number" value="${maxMinDto.maxVoltage}" pattern="0.000" maxFractionDigits="3"/></font></td>
			<td><strong>最小值(电压/V)</strong></td>
			<td bgColor="#ffffff">
				${maxMinDto.minVoltageNo}：<font><fmt:formatNumber type="number" value="${maxMinDto.minVoltage}" pattern="0.000" maxFractionDigits="3"/></font></td>																							
			<td><strong>系统电压(电压/V)</strong></td>
			<td bgColor='#ffffff'>
				<font><fmt:formatNumber type="number" value="${groupInfo.goupVoltage}" pattern="0.000" maxFractionDigits="3"/></font></td>
		</tr>
		<tr bgColor="#f5fafe" class="ta_01">			
			<td><strong>最大值(内阻/mΩ)</strong></td>
			<td bgColor="#ffffff">
				${maxMinDto.maxInterNo}：<font><fmt:formatNumber type="number" value="${maxMinDto.maxInter}" pattern="0.000" maxFractionDigits="3"/></font></td>			
			<td><strong>平均电压(电压/V)</strong></td>
			<td bgColor="#ffffff">
				${groupDto.tree.name}：<font><fmt:formatNumber type="number" value="${maxMinDto.avgVoltage}" pattern="0.000" maxFractionDigits="3"/></font></td>																									
			<td><strong>充放电电流(电流/V)</strong></td>
			<td bgColor='#ffffff'>
				<font><fmt:formatNumber type="number" value="${groupInfo.elecCurrent}" pattern="0.000" maxFractionDigits="3"/></font></td>
		</tr>  	  
	</table>	
</sf:form>	
</body>
</html>