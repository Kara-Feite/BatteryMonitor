package com.gzyw.monitor.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.gzyw.monitor.annotation.Permissions;

/**
 * 
 * @ClassName: AuthorizationInterceptor
 * @Description: 权限认证拦截器
 * @author LW
 * @date 2018年7月18日 下午5:08:58
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Permissions permissions = null;
		if (handler instanceof HandlerMethod) {
			HandlerMethod h = (HandlerMethod) handler;
			permissions = h.getMethodAnnotation(Permissions.class);
		}
		// 对没有@Permissions注解的请求放行
		if (permissions == null) {
			return true;
		}
		String permission = permissions.value();
		if (SecurityUtils.getSubject().isPermitted(permission)) {
			return true;
		} else {
			response.sendRedirect("/refuse");
		}
		return false;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

}
