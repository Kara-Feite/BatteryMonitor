package com.gzyw.monitor.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gzyw.monitor.control.MonitorClient;

/**
 * 
 * @ClassName: MyJob
 * @Description: 监测任务
 * @author LW
 * @date 2018年7月25日 下午4:41:32
 */
@Component
public class MyJob implements Job {
	
	@Autowired
	private MonitorClient monitorClient;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		monitorClient.dispatch(context);
	}

}
