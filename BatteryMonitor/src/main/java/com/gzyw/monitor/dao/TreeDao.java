package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: TreeDao
 * @Description: 树形节点持久层
 * @author LW
 * @date 2018年6月7日 下午3:39:08
 */
public interface TreeDao extends BaseDao<Tree> {

	/**
	 * 
	 * @Title: listTreeByAll
	 * @Description: 获取所有树形节点
	 * @return
	 */
	List<Tree> listTreeByAll();

	/**
	 * 
	 * @Title: listTreeByRoot
	 * @Description: 获取所有根节点
	 * @param userId
	 * @return
	 */
	List<Tree> listTreeByRoot(Integer userId, Tree tree, String sort, String order);

	/**
	 * 
	 * @Title: countTreeByRoot
	 * @Description: 获取根节点总数
	 * @return
	 */
	long countTreeByRoot(Integer userId);

	/**
	 * 
	 * @Title: listTreeByPage
	 * @Description: 分页获取根节点数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @patam tree 树形基站节点
	 * @return
	 */
	List<Tree> listTreeByPage(Integer page, Integer rows, String sort, String order, Tree tree);

	/**
	 * 
	 * @Title: updateTreeByRoot
	 * @Description: 更新根节点
	 * @param tree
	 */
	void updateTreeByRoot(Tree tree);

	/**
	 * 
	 * @Title: updateTree
	 * @Description: 更新节点信息
	 * @param tree
	 * @return
	 */
	Integer updateTree(Tree tree);

	/**
	 * 
	 * @Title: updateTreeByState
	 * @Description: 更新节点状态
	 * @param id
	 * @param state
	 */
	void updateTreeByState(int id, Boolean state);

	/**
	 * 
	 * @Title: updateByInfo
	 * @Description: 更新节点
	 * @param tree
	 */
	void updateByInfo(Tree tree);

	/**
	 * 
	 * @Title: listTreeByPid
	 * @Description: 根据父id获取节点集合
	 * @param id
	 * @return
	 */
	List<Tree> listTreeByPid(int id);

	/**
	 * 
	 * @Title: listTreeByUserId
	 * @Description: 根据用户id查询基站节点
	 * @param userId
	 * @return
	 */
	List<Tree> listTreeByUserId(Integer userId);

	/**
	 * 
	 * @Title: countTree
	 * @Description: 统计基站节点
	 * @param tree
	 * @return
	 */
	long countTree(Tree tree);

	/**
	 * 
	 * @Title: getParentTreeById
	 * @Description: 通过id获取父节点
	 * @param id
	 * @return
	 */
	Tree getParentTreeById(int id);

}
