package com.gzyw.monitor.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Log;
import com.gzyw.monitor.service.LogService;

/**
 * 
 * @ClassName: LogController
 * @Description: 日志控制层
 * @author LW
 * @date 2018年7月16日 下午3:30:15
 */
@Controller
@RequestMapping("/log")
public class LogController {
	
	@Autowired
	private LogService logService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}
	
	/**
	 * 
	 * @Title: showList
	 * @Description: 日志列表页面
	 * @return
	 */
	@RequestMapping("/list")
	@Permissions("log:query")
	String showList() {
		return "log/list";
	}
	
	/**
	 * 
	 * @Title: datagrid
	 * @Description: 获取分页数据
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Log log, String userName,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return logService.getDatagrid(page, rows, sort, order, log, userName);
	}
	
	/**
	 * 
	 * @Title: deleteLog
	 * @Description: 删除日志
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@Permissions("log:delete")
	ResultDto deleteLog(int[] ids) {
		try {
			return logService.deleteLog(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
}
