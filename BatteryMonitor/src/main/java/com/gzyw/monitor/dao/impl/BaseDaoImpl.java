package com.gzyw.monitor.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.ArrayUtil;
import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.BaseDao;

/**
 * 
 * @ClassName: BaseDaoImpl
 * @Description: BaseDao接口实现类
 * @author LW
 * @date 2018年6月6日 下午2:31:14
 * @param <T>
 */
@SuppressWarnings("unchecked")
@Repository
public class BaseDaoImpl<T> implements BaseDao<T> {
	
	/**
	 * SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * 
	 * @Title: getSession
	 * @Description: 获取session
	 * @return
	 */
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/**
	 * 获取的泛型对象
	 */
	private Class<T> cls;
	
	/**
	 * 
	 * @Title: getClzz
	 * @Description: 获取泛型的Class
	 * @return
	 */
	private Class<T> getClzz() {
		if (null == cls) {
			cls = (Class<T>)(((ParameterizedType)(this.getClass().getGenericSuperclass())).getActualTypeArguments()[0]);
		}
		return cls;
	}
	
	/**
	 * 
	 * @Title: setParam
	 * @Description: 设置HQL参数
	 * @param params
	 * @param q
	 */
	private void setParam(Query<T> q, Map<String, Object> params) {
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				q.setParameter(key, params.get(key));
			}
		}
	}
	
	/**
	 * 
	 * @Title: setParam
	 * @Description: 设置HQL参数
	 * @param q
	 * @param params
	 */
	private void setParam(Query<T> q, Object... params) {
		int len = params.length;
		if (params != null && len > 0) {
			for (int i = 0; i < len; i++) {
				q.setParameter(i, params[i]);
			}
		}
	}
	
	/**
	 * 格式为name = :name, password = :password
	 * @Title: setParam
	 * @Description: 设置Map的key值hql作为参数
	 * @param coll
	 * @return
	 */
	protected String setParam(Collection<?> coll) {
		if (CollectionUtil.isEmpty(coll)) {
			return StringUtil.EMPTY;
		}
		Object[] arr = ArrayUtil.toArray(coll);
		final StringBuffer sb = new StringBuffer(" " + arr[0] + " = :" + arr[0]);
		for (int i = 1, len = arr.length; i < len; i++) {
			sb.append(", " + arr[i] + " = :" + arr[i] + " ");
		}
		return sb.toString();
	}

	@Override
	public Serializable save(T t) {
		return getSession().save(t);
	}

	@Override
	public void delete(Serializable id) {
		getSession().delete(id);
	}

	@Override
	public void delete(T t) {
		getSession().delete(t);
	}
	
	@Override
	public int delete(String hql, Object... params) {
		return update(hql, params);
	}

	@Override
	public int delete(String hql, Map<String, Object> params) {
		return update(hql, params);
	}

	@Override
	public void update(T t) {
		getSession().update(t);
	}

	@Override
	public void saveOrUpdate(T t) {
		getSession().saveOrUpdate(t);
	}

	@Override
	public T get(Serializable id) {
		return getSession().get(getClzz(), id);
	}
	
	@Override
	public T get(String hql, Object... params) {
		return (T) uniqueResult(hql, params);
	}

	@Override
	public T get(String hql, Map<String, Object> params) {
		return (T) uniqueResult(hql, params);
	}

	@Override
	public List<T> queryForList(String hql, Object... params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.list();
	}

	@Override
	public List<T> queryForList(String hql, Map<String, Object> params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.list();
	}

	@Override
	public List<T> queryByPage(String hql, int pageNo, int pageSize, Object... params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.setFirstResult((pageNo - 1)*pageSize).setMaxResults(pageSize).list();
	}

	@Override
	public List<T> queryByPage(String hql, int pageNo, int pageSize, Map<String, Object> params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.setFirstResult((pageNo - 1)*pageSize).setMaxResults(pageSize).list();

	}

	@Override
	public Long count(String hql, Object... params) {
		Object result = uniqueResult(hql, params);
		return Long.valueOf(result.toString());
	}

	@Override
	public Long count(String hql, Map<String, Object> params) {
		Object result = uniqueResult(hql, params);
		return Long.valueOf(result.toString());
	}

	@Override
	public int update(String hql, Object... params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.executeUpdate();
	}

	@Override
	public int update(String hql, Map<String, Object> params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		return q.executeUpdate();
	}

	@Override
	public Object uniqueResult(String hql, Object... params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		q.setMaxResults(1);
		return q.uniqueResult();
	}

	@Override
	public Object uniqueResult(String hql, Map<String, Object> params) {
		Query<T> q = getSession().createQuery(hql);
		setParam(q, params);
		q.setMaxResults(1);
		return q.uniqueResult();
	}

	@Override
	public Integer getAutoId(String tableName) {
		String sql = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = :table";
		Query<BigInteger> query = getSession().createNativeQuery(sql);
		query.setParameter("table", tableName);
		return Integer.valueOf(query.uniqueResult().toString());
	}

	@Override
	public void executeProcedure(Work work) {
		getSession().doWork(work);
	}
	
}
