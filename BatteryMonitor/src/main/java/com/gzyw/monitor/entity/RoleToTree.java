package com.gzyw.monitor.entity;

import java.io.Serializable;

/**
 * 
 * @ClassName: RoleToTree
 * @Description: 角色基站实体类
 * @author LW
 * @date 2018年7月16日 下午1:12:16
 */
public class RoleToTree implements Serializable {
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 8286816273450765022L;

	/**
	 * 角色id
	 */
	private Integer roleId;
	
	/**
	 * 基站节点id
	 */
	private Integer treeId;
	
	/**
	 * 
	 * @Title: RoleToTree
	 * @Description: RoleToTree构造函数
	 */
	public RoleToTree() {}

	/**
	 * 
	 * @Title: RoleToTree
	 * @Description: RoleToTree构造函数
	 * @param roleId
	 * @param treeId
	 */
	public RoleToTree(Integer roleId, Integer treeId) {
		super();
		this.roleId = roleId;
		this.treeId = treeId;
	}

	/**
	 * @Title: getRoleId
	 * @Description: 获取roleId
	 * @return: Integer roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @Title: setRoleId
	 * @Description: 设置roleId
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @Title: getTreeId
	 * @Description: 获取treeId
	 * @return: Integer treeId
	 */
	public Integer getTreeId() {
		return treeId;
	}

	/**
	 * @Title: setTreeId
	 * @Description: 设置treeId
	 */
	public void setTreeId(Integer treeId) {
		this.treeId = treeId;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "RoleToTree [roleId=" + roleId + ", treeId=" + treeId + "]";
	}

}
