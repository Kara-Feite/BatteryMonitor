<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/include/jstl.jsp" %>

<script src='${ctxStatic}/js/easyui/jquery.min.js'></script>
<script src='${ctxStatic}/js/easyui/jquery.easyui.min.js'></script>
<script src='${ctxStatic}/js/easyui/locale/easyui-lang-zh_CN.js'></script>
<script src='${ctxStatic}/js/easyui/easyloader.js'></script>

<link href='${ctxStatic}/js/easyui/demo/demo.css' rel='stylesheet'>
<link href='${ctxStatic}/js/easyui/themes/default/easyui.css' rel='stylesheet'>
<link href='${ctxStatic}/js/easyui/themes/icon.css' rel='stylesheet'>