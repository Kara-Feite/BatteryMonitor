package com.gzyw.monitor.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @ClassName: RoleDto
 * @Description: 角色包装类
 * @author LW
 * @date 2018年7月8日 下午3:05:24
 */
public class RoleDto implements Serializable {
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 3635590470241145695L;

	/**
	 * id
	 */
	private Integer id;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 状态
	 */
	private Boolean state;

	/**
	 * 创建者
	 */
	private Integer createBy;

	/**
	 * 更新者
	 */
	private Integer updateBy;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;
	
	/**
	 * 创建人
	 */
	private String createName;
	
	/**
	 * 更新人
	 */
	private String updateName;

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getCreateBy
	 * @Description: 获取createBy
	 * @return: Integer createBy
	 */
	public Integer getCreateBy() {
		return createBy;
	}

	/**
	 * @Title: setCreateBy
	 * @Description: 设置createBy
	 */
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	/**
	 * @Title: getUpdateBy
	 * @Description: 获取updateBy
	 * @return: Integer updateBy
	 */
	public Integer getUpdateBy() {
		return updateBy;
	}

	/**
	 * @Title: setUpdateBy
	 * @Description: 设置updateBy
	 */
	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getCreateName
	 * @Description: 获取createName
	 * @return: String createName
	 */
	public String getCreateName() {
		return createName;
	}

	/**
	 * @Title: setCreateName
	 * @Description: 设置createName
	 */
	public void setCreateName(String createName) {
		this.createName = createName;
	}

	/**
	 * @Title: getUpdateName
	 * @Description: 获取updateName
	 * @return: String updateName
	 */
	public String getUpdateName() {
		return updateName;
	}

	/**
	 * @Title: setUpdateName
	 * @Description: 设置updateName
	 */
	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "RoleDto [id=" + id + ", name=" + name + ", state=" + state + ", createBy=" + createBy + ", updateBy="
				+ updateBy + ", remark=" + remark + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", createName=" + createName + ", updateName=" + updateName + "]";
	}
	
}
