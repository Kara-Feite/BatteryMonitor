package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.AlarmDao;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;

/**
 * 
 * @ClassName: AlarmDaoImpl
 * @Description: 警告持久层接口实现类
 * @author LW
 * @date 2018年6月20日 下午1:45:13
 */
@Repository
public class AlarmDaoImpl extends BaseDaoImpl<Alarm> implements AlarmDao {

	@Override
	public Long countAlarm(TempDto temp, String ids) {
		String hql = "SELECT COUNT(*) FROM Alarm";
		hql = setHQLParam(hql, null, null, temp, ids);
		return count(hql);
	}

	@Override
	public List<Alarm> listAlarmByPage(Integer page, Integer rows, String sort, String order, TempDto temp, String ids) {
		String hql = "FROM Alarm";
		hql = setHQLParam(hql, sort, order, temp, ids);
		return queryByPage(hql, page, rows);
	}
	
	@Override
	public Integer updateAlarm(Alarm alarm) {
		String hql = "UPDATE Alarm SET dealer = ?, state = ?, dealTime = ?, remark = ? WHERE id = ?";
		return update(hql, alarm.getDealer(), alarm.getState(), alarm.getDealTime(), alarm.getRemark(), alarm.getId());
	}
	
	@Override
	public Alarm getAlarmByExists(Alarm alarm, Integer groupId) {
		String hql = "FROM Alarm WHERE type = ? AND	group_id = ? AND mold = ? AND isDeal = false AND name = ?";
		return get(hql, alarm.getType(), groupId, alarm.getMold(), alarm.getName());
	}

	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置HQL参数
	 * @param hql HQL语句
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param temp 临时对象
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, TempDto temp, String ids) {
		StringBuilder sb = new StringBuilder(hql);
		sb.append(" WHERE group_id IN (").append(ids).append(")");
		String mold = temp.getMold();
		if (StringUtil.isNotEmpty(mold)) {
			sb.append(" and mold like '%").append(mold).append("%'");
		}
		Boolean state = temp.getState();
		if (state != null) {
			sb.append(" and isDeal = ").append(state);
		}
		String type = temp.getType();
		if (StringUtil.isNotEmpty(type)) {
			sb.append(" and type like '%").append(type).append("%'");
		}
		String startTime = temp.getStartTime();
		String endTime = temp.getEndTime();
		if (startTime != null && endTime != null) {
			sb.append(" and alarmTime between '").append(startTime).append("' and '").append(endTime).append("'");
		}
		if (StringUtil.isNotEmpty(sort)  && StringUtil.isNotEmpty(order)) {
			sb.append(" order by ").append(sort).append(" ").append(order);
		}
		return sb.toString();
	}

}
