package com.gzyw.monitor.control;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.gzyw.monitor.common.util.CRCUtil;
import com.gzyw.monitor.common.util.JsonUtil;
import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.AlarmDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.entity.Alarm;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.GroupInfoService;
import com.gzyw.monitor.service.GroupLimitService;
import com.gzyw.monitor.service.TestDataService;

public class MonitorClient {
	
	private Selector selector;

	private SocketChannel client;

	private Charset charset = Charset.forName("UTF-8");
	
	@Autowired
	private GroupInfoService groupInfoService;
	
	@Autowired
	private TestDataService testDataService;
	
	@Autowired
	private GroupLimitService groupLimitService;
	
	@Autowired
	private TreeDao treeDao;

	@Autowired
	private AlarmDao alarmDao;
	
	public MonitorClient(String ipAddr, int port) throws IOException {
		client = SocketChannel.open(new InetSocketAddress(ipAddr, port));
		client.configureBlocking(false);
		selector = Selector.open();
		client.register(selector, SelectionKey.OP_READ);
	}
	
	public String send(String address, String command) throws IOException {
		client.write(charset.encode(address + "---->" + command));
		while (true) {
			int readyChannels = selector.select();
			if (readyChannels == 0)
				continue;
			Set<SelectionKey> selectionKeys = selector.selectedKeys();
			Iterator<SelectionKey> it = selectionKeys.iterator();
			while (it.hasNext()) {
				SelectionKey key = it.next();
				it.remove();
				if (key.isReadable()) {
					SocketChannel sc = (SocketChannel) key.channel();
					ByteBuffer buff = ByteBuffer.allocate(1024);
					StringBuilder content = new StringBuilder();
					if (sc.read(buff) > 0) {
						buff.flip();
						content.append(charset.decode(buff));
					}
					key.interestOps(SelectionKey.OP_READ);
					String data = content.toString();
					String[] dataArr = StringUtils.split(data, "<----");
					if (ArrayUtils.getLength(dataArr) == 2) {
						data = dataArr[1];
						int limit = buff.limit() - 5 - dataArr[0].length();
						if (limit != Integer.valueOf(dataArr[0])) {
							String receiveData = receiveData();
							data += receiveData;
						}
					}
					if (StringUtil.isNotEmpty(data)) {
						return data;
					}
				}
			}
		}
	}
	
	private String receiveData() throws IOException {
		while (true) {
			int readyChannels = selector.select();
			if (readyChannels == 0)
				continue;
			Set<SelectionKey> selectionKeys = selector.selectedKeys();
			Iterator<SelectionKey> it = selectionKeys.iterator();
			while (it.hasNext()) {
				SelectionKey key = it.next();
				it.remove();
				if (key.isReadable()) {
					SocketChannel sc = (SocketChannel) key.channel();
					ByteBuffer buff = ByteBuffer.allocate(1024);
					StringBuilder content = new StringBuilder();
					if (sc.read(buff) > 0) {
						buff.flip();
						content.append(charset.decode(buff));
						buff.clear();
					}
					key.interestOps(SelectionKey.OP_READ);
					String data = content.toString();
					if (StringUtil.isNotEmpty(data)) {
						return data;
					}
				}
			}
		}
	}
	
	public void dispatch(JobExecutionContext context) {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        String groupJson = dataMap.getString("group");
        String treeJson = dataMap.getString("tree");
        Group group = JsonUtil.jsonToObject(groupJson, Group.class);
        Tree tree = JsonUtil.jsonToObject(treeJson, Tree.class);
        Date date = new Date();
        synchronized(this) {
        	try {
    			Integer len = tree.getNumber();
    			String address = group.getAddress();
    			// 01 03 00 00 00 14 45 c5
    			// 01：装置地址    03：功能码   00 00 ：寄存器起始号   00 14：读取长度   45 c5：CRC校验码
    			String sysInfo = send(address, "0103000000104406"); //系统信息
    			// 单体电压
    			String voltageCode = "010303e8" + NumberUtil.decToHex(len);
    			String voltageCrc = CRCUtil.getCrc16(StringUtil.toBytes(voltageCode));
    			String voltage = send(address, voltageCode + voltageCrc);
    			// 单体内阻
    			String interCode = "010307d0" + NumberUtil.decToHex(len);
    			String interCrc = CRCUtil.getCrc16(StringUtil.toBytes(interCode));
    			String inter = send(address, interCode + interCrc);
    			// 单体温度
    			String temperaCode = "01030bb8" + NumberUtil.decToHex(len);
    			String temperaCrc = CRCUtil.getCrc16(StringUtil.toBytes(temperaCode));
    			String tempera = send(address, temperaCode + temperaCrc);
    			// 参数设置
    			String param = send(address, "01030fa00016C732");
    			if (groupInfoService.saveGroupInfo(sysInfo, group, date)) {
    				testDataService.saveData(voltage, inter, tempera, group, date);
    				groupLimitService.saveLimit(param, group, date);
    			}
    		} catch (Exception e) {
    			Integer groupId = group.getGroupId();
    			Alarm alarm = new Alarm();
    			alarm.setState("告警");
    			alarm.setAlarmTime(date);
    			alarm.setName(tree.getName());
    			alarm.setMold("系统告警");
    			alarm.setType("系统告警");
    			alarm.setDetails("电池组连接出现错误！");
    			alarm.setGroup(group);
    			Alarm a = alarmDao.getAlarmByExists(alarm, groupId);
    			if (a == null) {
    				Tree tree2 = treeDao.get(groupId);
    				Integer oldNum = tree2.getWarn();
    				updateAlarmByNum(oldNum, oldNum + 1, groupId, true);
    				tree2.setWarn(oldNum + 1);
    				treeDao.update(tree2);
    				alarmDao.save(alarm);
    			}
    			e.printStackTrace();
    		}
        }
	}
	
	/**
	 * 
	 * @Title: updateAlarmByNum
	 * @Description: 更新告警
	 * @param oldNum
	 * @param newNum
	 * @param id
	 * @param isWarn
	 */
	private void updateAlarmByNum(int oldNum, int newNum, int id, boolean isWarn) {
		Tree tree = treeDao.getParentTreeById(id);
		if (tree != null) {
			if (isWarn) {
				tree.setWarn(tree.getWarn() - oldNum + newNum);
			} else {
				tree.setPlan(tree.getPlan() - oldNum + newNum);
			}
			treeDao.update(tree);
			updateAlarmByNum(oldNum, newNum, tree.getId(), isWarn);
		}
	}

}
