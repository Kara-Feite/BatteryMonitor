package com.gzyw.monitor.task;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.scheduling.quartz.AdaptableJobFactory;

/**
 * <p>目的：可以让Job的字段可以使用@Autowired注入对象</p>
 * <p>注意：spirng配置文件中schedulerFactoryBean要配置</p>
 * {@code <property name="jobFactory" ref="jobFactory"></property>}
 * @ClassName: JobFactory
 * @Description: 自定义继承AdaptableJobFactory的JobFactory
 * @author LW
 * @date 2018年7月26日 上午8:44:18
 */
public class JobFactory extends AdaptableJobFactory {

	@Autowired
	private AutowireCapableBeanFactory capableBeanFactory;

	@Override
	protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
		// 调用父类的方法
		Object jobInstance = super.createJobInstance(bundle);
		// 进行注入
		capableBeanFactory.autowireBean(jobInstance);
		return jobInstance;
	}

}