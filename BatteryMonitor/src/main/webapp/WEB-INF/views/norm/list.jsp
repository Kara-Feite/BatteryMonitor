<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池规格</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script>
$(function(){
	$('#norm_lists').datagrid({
		url:'${ctx}/norm/datagrid',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns : [ [ {
				field : 'id',
				title : 'ID编号',
				checkbox:true,
				align:'center',
				width : 30
			},{
				field : 'groupName',
				title : '电池组',
				align:'center',
				width : 100
			},{
				field : 'maker',
				title : '制造商',
				align:'center',
				width : 50
			},{
				field : 'model',
				title : '型号',
				align:'center',
				width : 50
			},{
				field : 'type',
				title : '电池类型',
				align:'center',
				width : 50
			},{
				field : 'voltage',
				sortable:true,
				align:'center',
				title : '电压/V',
				width : 50
			},{
				field : 'inter',
				sortable:true,
				align:'center',
				title : '内阻/mΩ',
				width : 50
			},{
				field : 'volume',		
				title : '容量/Ah',
				align:'center',
				sortable:true,
				width : 50
			},{
				field : 'beginTime',
				title : '投运日期',
				align:'center',
				sortable:true,
				width : 70,
				formatter : formatDateTime
			},{
				field : 'updateTime',
				title : '更新日期',
				align:'center',
				sortable:true,
				width : 70,
				formatter : formatDateTime
			}]],
		toolbar:[	
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',		
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});

function query(){
	$('#norm_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function clean(){
	$('#norm_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}
function update(){
	<permission:notPermission name="norm:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#norm_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/norm/edit?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}
function unselect(){
	$('#norm_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>
<body>
<div class="easyui-layout" fit="true" border="false">
	<div data-options="region:'north',title:'查询条件'" style="height:60px;">
		<form id="search_form">
			<table width="100%" bgcolor="#f5fafe">
				<tr height="25">
					<td id="denglu"><img src="${ctxStatic}/images/yin.gif" width="22">							
						<strong>制造商：</strong><input name="maker" size="15"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
							onclick="query();" href="javascript:void(0);">查  询</a>						
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
							onclick="clean();" href="javascript:void(0);">清  空</a>
					</td>					
				</tr>					
			</table>
		</form>
	</div>
	<!-- ------------显示数据------------------- -->
	<div data-options="region:'center'">
		<table id="norm_lists"></table>
	</div>
</div>
</body>
</html>