package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.TestDataDao;
import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: TestDataDaoImpl
 * @Description: 测试数据持久层实现类
 * @author LW
 * @date 2018年6月29日 上午9:52:43
 */
@Repository
public class TestDataDaoImpl extends BaseDaoImpl<TestData> implements TestDataDao {

	@Override
	public Long countTestData(TempDto temp) {
		String hql = "SELECT COUNT(*) FROM TestData";
		hql = setHQLParam(hql, null, null, temp);
		return count(hql);
	}

	@Override
	public List<TestData> listTestDataByPage(Integer page, Integer rows, String sort, String order, TempDto temp) {
		String hql = "FROM TestData";
		hql = setHQLParam(hql, sort, order, temp);
		return queryByPage(hql, page, rows);
	}
	
	/**
	 * 这里根据temp的id和treeId来区分Battery和Group
	 * id为来自battery的请求，treeId为Group
	 * @param temp
	 * @return 
	 * @see com.gzyw.monitor.dao.TestDataDao#getMaxMinDto(com.gzyw.monitor.dto.TempDto)
	 */
	@Override
	public MaxMinDto getMaxMinDto(TempDto temp) {
		StringBuilder sb = new StringBuilder("SELECT td1.maxVoltage maxVoltage, td1.maxVoltageNo maxVoltageNo,");
		sb.append("td2.minVoltage minVoltage, td2.minVoltageNo minVoltageNo,");
		sb.append("td3.maxInter maxInter, td3.maxInterNo maxInterNo,");
		sb.append("td4.minInter minInter, td4.minInterNo minInterNo, td5.avgVoltage avgVoltage FROM ");
		Integer treeId = temp.getTreeId();
		String startTime = temp.getStartTime();
		String endTime = temp.getEndTime();
		boolean isTime = (startTime != null && endTime != null);
		String timeStr = isTime ? " AND test_time BETWEEN '" + startTime + "' AND '" + endTime + "'" : "";
		if (treeId != null) {
			// 最大电压和最大电压序号
			sb.append("(SELECT td.voltage maxVoltage, td.battery_name maxVoltageNo FROM bm_test_data td INNER JOIN (");
			sb.append("SELECT MAX(voltage) maxVoltage, test_time time FROM bm_test_data WHERE test_time = (");
			sb.append("SELECT MAX(test_time) FROM bm_test_data WHERE group_id = " + treeId + " AND type = 0" + timeStr + ")");
			sb.append(" AND group_id = " + treeId + " AND type = 0 GROUP BY test_time) tw");
			sb.append(" ON td.voltage = tw.maxVoltage AND td.test_time = tw.time");
			sb.append(" WHERE group_id =  " + treeId + " ORDER BY id ASC LIMIT 0,1) td1,");
			// 最小电压和最小电压序号
			sb.append("(SELECT td.voltage minVoltage, td.battery_name minVoltageNo FROM bm_test_data td INNER JOIN (");
			sb.append("SELECT MIN(voltage) minVoltage, test_time time FROM bm_test_data WHERE test_time = (");
			sb.append("SELECT MAX(test_time) FROM bm_test_data WHERE group_id = " + treeId + " AND type = 0"  + timeStr + ")");
			sb.append(" AND group_id = " + treeId + " AND type = 0 GROUP BY test_time) tw");
			sb.append(" ON td.voltage = tw.minVoltage AND td.test_time = tw.time");
			sb.append(" WHERE group_id =  " + treeId + " ORDER BY id ASC LIMIT 0,1) td2,");
			// 最大电阻和最大电阻序号
			sb.append("(SELECT td.inter maxInter, td.battery_name maxInterNo FROM bm_test_data td INNER JOIN (");
			sb.append("SELECT Max(inter) maxInter, test_time time FROM bm_test_data WHERE test_time = (");
			sb.append("SELECT MAX(test_time) FROM bm_test_data WHERE group_id = " + treeId + " AND type = 0"  + timeStr + ")");
			sb.append(" AND group_id = " + treeId + " AND type = 0 GROUP BY test_time) tw");
			sb.append(" ON td.inter = tw.maxInter AND td.test_time = tw.time");
			sb.append(" WHERE group_id =  " + treeId + " ORDER BY id ASC LIMIT 0,1) td3,");
			// 最小电阻和最小电阻序号
			sb.append("(SELECT td.inter minInter, td.battery_name minInterNo FROM bm_test_data td INNER JOIN (");
			sb.append("SELECT MIN(inter) minInter, test_time time FROM bm_test_data WHERE test_time = (");
			sb.append("SELECT MAX(test_time) FROM bm_test_data WHERE group_id = " + treeId + " AND type = 0" + timeStr + ")");
			sb.append(" AND group_id = " + treeId + " AND type = 0 GROUP BY test_time) tw");
			sb.append(" ON td.inter = tw.minInter AND td.test_time = tw.time");
			sb.append(" WHERE group_id =  " + treeId + " ORDER BY id ASC LIMIT 0,1) td4,");
			// 平均电压
			sb.append("(SELECT AVG(voltage) avgVoltage FROM bm_test_data ");
			sb.append("WHERE test_time = (SELECT MAX(test_time) FROM bm_test_data ");
			sb.append("WHERE group_id = " + treeId + " AND type = 0" + timeStr + ") AND group_id = " + treeId + " AND type = 0 ) td5");
		} else {
			Integer batteryId = temp.getId();
			sb.append("(SELECT td.voltage maxVoltage, td.battery_name maxVoltageNo FROM bm_test_data td ");
			sb.append("WHERE td.voltage = (SELECT MAX(voltage) FROM bm_test_data ");
			sb.append("WHERE battery_id = " + batteryId + " AND type = 0" + timeStr + ") AND battery_id = " + batteryId + " AND type = 0" + timeStr);
			sb.append(" ORDER BY id ASC LIMIT 0,1) td1,");

			sb.append("(SELECT td.voltage minVoltage, td.battery_name minVoltageNo FROM bm_test_data td ");
			sb.append("WHERE td.voltage = (SELECT MIN(voltage) FROM bm_test_data ");
			sb.append("WHERE battery_id = " + batteryId + " AND type = 0" + timeStr + ") AND battery_id = " + batteryId + " AND type = 0" + timeStr);
			sb.append(" ORDER BY id ASC LIMIT 0,1) td2,");

			sb.append("(SELECT td.inter maxInter, td.battery_name maxInterNo FROM bm_test_data td ");
			sb.append("WHERE td.inter = (SELECT MAX(inter) FROM bm_test_data ");
			sb.append("WHERE battery_id = " + batteryId + " AND type = 0" + timeStr + ") AND battery_id = " + batteryId + " AND type = 0" + timeStr);
			sb.append(" ORDER BY id ASC LIMIT 0,1) td3,");

			sb.append("(SELECT td.inter minInter, td.battery_name minInterNo FROM bm_test_data td ");
			sb.append("WHERE td.inter = (SELECT Min(inter) FROM bm_test_data ");
			sb.append("WHERE battery_id = " + batteryId + " AND type = 0" + timeStr + ") AND battery_id = " + batteryId + " AND type = 0" + timeStr);
			sb.append(" ORDER BY id ASC LIMIT 0,1) td4,");

			sb.append("(SELECT avg(voltage) avgVoltage FROM bm_test_data WHERE battery_id = " + batteryId + " AND type = 0" + timeStr + ") td5");
		}
		@SuppressWarnings("deprecation")
		Object maxMinDto = getSession().createNativeQuery(sb.toString())
				.setResultTransformer(Transformers.aliasToBean(MaxMinDto.class)).uniqueResult();
		return (MaxMinDto) maxMinDto;
	}
	
	@Override
	public List<TestData> listTestDataByGroup(TempDto temp) {
		StringBuilder sb = new StringBuilder("SELECT new com.gzyw.monitor.entity.TestData(batteryName," + temp.getType() + ") FROM TestData WHERE testTime = ");
		String startTime = temp.getStartTime();
		Integer treeId = temp.getTreeId();
		sb.append("(SELECT MAX(testTime) FROM TestData WHERE group_id = ? AND type = 0");
		if (StringUtil.isNotEmpty(startTime)) {
			sb.append(" AND testTime LIKE '" + startTime + "%')");
		} else {
			sb.append(")");
		}
		sb.append(" AND group_id = ? AND type = 0");
		return queryForList(sb.toString(), treeId, treeId);
	}
	
	@Override
	public List<TestData> listTestDataByTime(TempDto temp) {
		String hql = "FROM TestData WHERE group_id = ? AND testTime = ?";
		return queryForList(hql, temp.getTreeId(), DateUtil.parseDate(temp.getStartTime()));
	}
	
	@Override
	public List<TestData> listTestDataBetTime(TempDto temp) {
		String startTime = temp.getStartTime();
		String endTime = temp.getEndTime();
		String hql = "FROM TestData WHERE group_id = ? AND testTime BETWEEN '" + startTime + "' AND '" + endTime + "' GROUP BY testTime ORDER BY testTime ASC";
		return queryForList(hql, temp.getTreeId());
	}

	@Override
	public List<TestData> listTestDataByBat(TempDto temp) {
		StringBuilder sb = new StringBuilder("SELECT new com.gzyw.monitor.entity.TestData(testTime," + temp.getType() + ") FROM TestData");
		String startTime = temp.getStartTime();
		String endTime = temp.getEndTime();
		sb.append(" WHERE battery_id = ? AND type = 0");
		if (StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)) {
			sb.append(" AND testTime BETWEEN '" + startTime + "' AND '" + endTime + "'");
		}
		sb.append(" ORDER BY testTime DESC");
		return queryByPage(sb.toString(), 1, 30, temp.getId());
	}
	
	@Override
	public List<TestData> listTestDataByExport(TempDto temp) {
		String hql = "FROM TestData";
		hql = setHQLParam(hql, null, null, temp);
		return queryForList(hql);
	}
	
	@Override
	public void delTestDataByGroup(Integer groupId) {
		String hql = "DELETE FROM TestData WHERE group_id = ?";
		delete(hql, groupId);
	}

	/**
	 * 这里根据temp的id和treeId来区分Battery和Group
	 * id为来自battery的请求，treeId为Group
	 * @Title: setHQLParam
	 * @Description: 设置hql语句参数
	 * @param hql
	 * @param sort
	 * @param order
	 * @param temp
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, TempDto temp) {
		StringBuilder sb = new StringBuilder(hql);
		String startTime = temp.getStartTime();
		String endTime = temp.getEndTime();
		boolean isTime = (StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime));
		if (temp.getTreeId() != null) {
			sb.append(" WHERE testTime = (SELECT MAX(testTime) FROM TestData WHERE "
					+ "group_id = " + temp.getTreeId() + " AND type = 0 " + (isTime ? "" : ")"));
			if (isTime) {
				sb.append(" AND testTime BETWEEN '" + startTime + "' AND '" + endTime + "')");
			}
			sb.append(" AND group_id = " + temp.getTreeId() + " AND type = 0 ");
		} else {
			sb.append(" WHERE battery_id = " + temp.getId() + " AND type = 0 ");
			if (isTime) {
				sb.append(" AND testTime BETWEEN '" + startTime + "' AND '" + endTime + "'");
			}
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY " + sort + " " + order);
		}
		return sb.toString();
	}

}
