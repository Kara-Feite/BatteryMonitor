package com.gzyw.monitor.dto;

import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: GroupDto
 * @Description: 电池组包装类
 * @author LW
 * @date 2018年6月19日 下午2:32:26
 */
public class GroupDto {
	
	/**
	 * 电池组
	 */
	private Group group;
	
	/**
	 * 电池组对应节点
	 */
	private Tree tree;

	/**
	 * @Title: GroupDto
	 * @Description: GroupDto构造函数
	 */
	public GroupDto() {
		super();
	}

	/**
	 * @Title: GroupDto
	 * @Description: GroupDto构造函数
	 * @param group
	 * @param tree
	 */
	public GroupDto(Group group, Tree tree) {
		super();
		this.group = group;
		this.tree = tree;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getTree
	 * @Description: 获取tree
	 * @return: Tree tree
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * @Title: setTree
	 * @Description: 设置tree
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}

	@Override
	public String toString() {
		return "GroupDto [group=" + group + ", tree=" + tree + "]";
	}
	
}
