package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: MonomerInfo
 * @Description: 单体信息
 * @author LW
 * @date 2018年6月4日 下午1:18:39
 */
@Entity
@Table(name = "bm_monomer_info")
public class MonomerInfo implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 4166485311088064746L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 单体最高电池电压
	 */
	@Column(name = "max_voltage", nullable = false)
	@ExcelColumn(row = 2, column = 0, value = "单体最高电压/V")
	private Double maxVoltage;

	/**
	 * 单体最高电压序号
	 */
	@Column(name = "max_voltage_no", length = 32, nullable = false)
	@ExcelColumn(row = 2, column = 2, value = "单体最高电压编号")
	private String maxVoltageNo;

	/**
	 * 单体最低电池电压
	 */
	@Column(name = "min_voltage", nullable = false)
	@ExcelColumn(row = 2, column = 4, value = "单体最低电压/V")
	private Double minVoltage;

	/**
	 * 单体最低电压序号
	 */
	@Column(name = "min_voltage_no", length = 32, nullable = false)
	@ExcelColumn(row = 2, column = 6, value = "单体最低电压编号")
	private String minVoltageNo;

	/**
	 * 单体最大电池内阻
	 */
	@Column(name = "max_inter", nullable = false)
	@ExcelColumn(row = 3, column = 0, value = "单体最大电池内阻/mΩ")
	private Double maxInter;

	/**
	 * 单体最大内阻序号
	 */
	@Column(name = "max_inter_no", length = 32, nullable = false)
	@ExcelColumn(row = 3, column = 2, value = "单体最大内阻编号")
	private String maxInterNo;

	/**
	 * 单体平均电压
	 */
	@Column(name = "avg_voltage", nullable = false)
	@ExcelColumn(row = 3, column = 4, value = "平均电压/V")
	private Double avgVoltage;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 电池组信息
	 */
	@OneToOne(mappedBy = "monomerInfo")
	private GroupInfo groupInfo;

	/**
	 * @Title: MonomerInfo
	 * @Description: MonomerInfo构造函数
	 */
	public MonomerInfo() {}

	/**
	 * @Title: MonomerInfo
	 * @Description: MonomerInfo构造函数
	 * @param maxVoltage
	 * @param maxVoltageNo
	 * @param minVoltage
	 * @param minVoltageNo
	 * @param maxInter
	 * @param maxInterNo
	 * @param avgVoltage
	 * @param createTime
	 */
	public MonomerInfo(Double maxVoltage, String maxVoltageNo, Double minVoltage, String minVoltageNo,
			Double maxInter, String maxInterNo, Double avgVoltage, Date createTime) {
		this.maxVoltage = maxVoltage;
		this.maxVoltageNo = maxVoltageNo;
		this.minVoltage = minVoltage;
		this.minVoltageNo = minVoltageNo;
		this.maxInter = maxInter;
		this.maxInterNo = maxInterNo;
		this.avgVoltage = avgVoltage;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getMaxVoltage
	 * @Description: 获取maxVoltage
	 * @return: Double maxVoltage
	 */
	public Double getMaxVoltage() {
		return maxVoltage;
	}

	/**
	 * @Title: setMaxVoltage
	 * @Description: 设置maxVoltage
	 */
	public void setMaxVoltage(Double maxVoltage) {
		this.maxVoltage = maxVoltage;
	}

	/**
	 * @Title: getMaxVoltageNo
	 * @Description: 获取maxVoltageNo
	 * @return: Integer maxVoltageNo
	 */
	public String getMaxVoltageNo() {
		return maxVoltageNo;
	}

	/**
	 * @Title: setMaxVoltageNo
	 * @Description: 设置maxVoltageNo
	 */
	public void setMaxVoltageNo(String maxVoltageNo) {
		this.maxVoltageNo = maxVoltageNo;
	}

	/**
	 * @Title: getMinVoltage
	 * @Description: 获取minVoltage
	 * @return: Double minVoltage
	 */
	public Double getMinVoltage() {
		return minVoltage;
	}

	/**
	 * @Title: setMinVoltage
	 * @Description: 设置minVoltage
	 */
	public void setMinVoltage(Double minVoltage) {
		this.minVoltage = minVoltage;
	}

	/**
	 * @Title: getMinVoltageNo
	 * @Description: 获取minVoltageNo
	 * @return: Integer minVoltageNo
	 */
	public String getMinVoltageNo() {
		return minVoltageNo;
	}

	/**
	 * @Title: setMinVoltageNo
	 * @Description: 设置minVoltageNo
	 */
	public void setMinVoltageNo(String minVoltageNo) {
		this.minVoltageNo = minVoltageNo;
	}

	/**
	 * @Title: getMaxInter
	 * @Description: 获取maxInter
	 * @return: Double maxInter
	 */
	public Double getMaxInter() {
		return maxInter;
	}

	/**
	 * @Title: setMaxInter
	 * @Description: 设置maxInter
	 */
	public void setMaxInter(Double maxInter) {
		this.maxInter = maxInter;
	}

	/**
	 * @Title: getMaxInterNo
	 * @Description: 获取maxInterNo
	 * @return: Integer maxInterNo
	 */
	public String getMaxInterNo() {
		return maxInterNo;
	}

	/**
	 * @Title: setMaxInterNo
	 * @Description: 设置maxInterNo
	 */
	public void setMaxInterNo(String maxInterNo) {
		this.maxInterNo = maxInterNo;
	}

	/**
	 * @Title: getAvgVoltage
	 * @Description: 获取avgVoltage
	 * @return: Double avgVoltage
	 */
	public Double getAvgVoltage() {
		return avgVoltage;
	}

	/**
	 * @Title: setAvgVoltage
	 * @Description: 设置avgVoltage
	 */
	public void setAvgVoltage(Double avgVoltage) {
		this.avgVoltage = avgVoltage;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getGroupInfo
	 * @Description: 获取groupInfo
	 * @return: GroupInfo groupInfo
	 */
	public GroupInfo getGroupInfo() {
		return groupInfo;
	}

	/**
	 * @Title: setGroupInfo
	 * @Description: 设置groupInfo
	 */
	public void setGroupInfo(GroupInfo groupInfo) {
		this.groupInfo = groupInfo;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonomerInfo [id=" + id + ", maxVoltage=" + maxVoltage + ", maxVoltageNo=" + maxVoltageNo
				+ ", minVoltage=" + minVoltage + ", minVoltageNo=" + minVoltageNo + ", maxInter=" + maxInter
				+ ", maxInterNo=" + maxInterNo + ", avgVoltage=" + avgVoltage + ", createTime=" + createTime
				+ ", groupInfo=" + groupInfo + "]";
	}

}
