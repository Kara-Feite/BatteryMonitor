<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>编辑电池规格</title>
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<style type="text/css">
select,input{width:220px;}
a{width: 80px;}
.but{
	width:136px;
	margin-left:4px;
	height:22px;
	line-height:22px;
	border-radius:5px;
	border:1px solid #95B8E7;
	text-indent:3px;
	outline:none;
}
</style>
<script>
function edit(){	     	
	$('#myForm').form('submit',{
		url : '${ctx}/norm/${empty norm.id ? "save" : "update"}',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				window.location.href = '${ctx}/norm/list';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
} 
</script> 
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="norm" enctype="multipart/form-data">
		<c:if test="${!empty norm.id }">
			<sf:hidden path="id"/>
		</c:if>
		<div class="easyui-panel" title="操作：编辑电池规格" style="padding:30px 60px;">
			 <div style="margin-bottom:20px">
				<sf:select path="maker" class="easyui-combobox"
			 	 	data-options="label:'制 造 商:',required:true">  
				    <c:forEach items = "${listMaker}" var='maker'>
				 		<sf:option value="${maker}"/>
				 	</c:forEach>
				 </sf:select>
			 	<font color="red"><sf:errors path="maker" cssClass="error"></sf:errors></font><br><br>
			 	<sf:select path="model" class="easyui-combobox" 
			 	 	data-options="label:'型 &nbsp;&nbsp;&nbsp;&nbsp;号:',required:true">
				    <c:forEach items = "${listModel}" var='model'>
				 		<sf:option value="${model}"/>
				 	</c:forEach>
				 </sf:select><br><br>
			 	<sf:select path="type" class="easyui-combobox" 
			 	 	data-options="label:'电池类型:',required:true">  
				    <c:forEach items = "${listType}" var='type'>
				 		<sf:option value="${type}"/>
				 	</c:forEach>
				 </sf:select><br><br>
				<sf:input path="voltage" class="easyui-numberbox" 
			 		data-options="label:'电池电压:',required:true,min:0,precision:2"/><br><br> 
			 	<sf:input path="inter" class="easyui-numberbox" 
			 		data-options="label:'参考内阻:',required:true,min:0,precision:2"/><br><br> 
			 	<sf:input path="volume"  class="easyui-numberbox" 
			 		data-options="label:'电池容量:',required:true,min:0,precision:2"/><br><br>     		 		
			 	投运日期：&nbsp;&nbsp;&nbsp;&nbsp;
			 	<sf:input path="beginTime" editable="false" class="but" autocomplete='off'
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/><br><br>     
            	<a href="javascript:void(0)" class="easyui-linkbutton" 
            		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a>
          		&nbsp;&nbsp;&nbsp;&nbsp;
        		<a href="${ctx}/norm/list" class="easyui-linkbutton" 
        			data-options="iconCls:'icon-reload'">返 回</a>
	 		</div>
		</div>
	</sf:form>
</body>
</html>