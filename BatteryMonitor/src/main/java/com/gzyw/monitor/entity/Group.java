package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @ClassName: Group
 * @Description: 电池组
 * @author LW
 * @date 2018年6月4日 上午10:13:31
 */
@Entity
@Table(name = "bm_group")
public class Group implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -1525317364993972498L;

	/**
	 * id
	 */
	@Id
	@Column(name = "group_id")
	private Integer groupId;

	/**
	 * ip地址
	 */
	/*@Column(name = "ip_addr", length = 32, nullable = false)
	@NotEmpty(message="ip地址不能为空")
	private String ipAddr;*/

	/**
	 * 端口号
	 */
	/*@Column(name = "port", length = 5, nullable = false)
	private Integer port;*/
	
	/**
	 * 测试时间间隔
	 */
	@Column(name = "intervals", nullable = false)
	@NotEmpty(message="测试时间间隔不能为空")
	private String interval;
	
	/**
	 * 间隔单位
	 * 1:秒，2:分钟，3:小时，4:天，5:固定小时
	 */
	@Column(name = "unit", nullable = false)
	private Integer unit;

	/**
	 * 电池规格
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "norm_id")
	private Norm norm;
	
	/**
	 * 是否运行
	 */
	@Column(name = "is_run", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean isRun;
	
	/**
	 * 电池组标识
	 */
	@Column(name = "address", length = 64,nullable = false)
	private String address;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;
	
	/**
	 * 电池组所有的电池
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<Battery> setBattery;
	
	/**
	 * 电池组所有的测试数据
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<TestData> setTestData;

	/**
	 * 电池组信息
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<GroupInfo> setGroupInfo;

	/**
	 * 电池组限制
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<GroupLimit> setGroupLimit;
	
	/**
	 * 电池组发生的警告
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<Alarm> setAlarm;
	
	/**
	 * 电池组警告门限
	 */
	@OneToMany(mappedBy = "group")
	@JsonIgnore
	private Set<Limit> setLimit;

	/**
	 * @Title: Group
	 * @Description: Group构造函数
	 */
	public Group() {}
	
	/**
	 * 
	 * @Title: Group
	 * @Description: Group构造函数
	 * @param groupId
	 * @param interval
	 * @param unit
	 * @param norm
	 * @param isRun
	 * @param address
	 * @param createTime
	 * @param updateTime
	 */
	public Group(Integer groupId, String interval, Integer unit, Norm norm, Boolean isRun, String address,
			Date createTime, Date updateTime) {
		this.groupId = groupId;
		this.interval = interval;
		this.unit = unit;
		this.norm = norm;
		this.isRun = isRun;
		this.address = address;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getGroupId
	 * @Description: 获取groupId
	 * @return: Integer groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}

	/**
	 * @Title: setGroupId
	 * @Description: 设置groupId
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	
	/**
	 * @Title: getInterval
	 * @Description: 获取interval
	 * @return: String interval
	 */
	public String getInterval() {
		return interval;
	}

	/**
	 * @Title: setInterval
	 * @Description: 设置interval
	 */
	public void setInterval(String interval) {
		this.interval = interval;
	}

	/**
	 * @Title: getUnit
	 * @Description: 获取unit
	 * @return: Integer unit
	 */
	public Integer getUnit() {
		return unit;
	}

	/**
	 * @Title: setUnit
	 * @Description: 设置unit
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	/**
	 * @Title: getNorm
	 * @Description: 获取norm
	 * @return: Norm norm
	 */
	public Norm getNorm() {
		return norm;
	}

	/**
	 * @Title: setNorm
	 * @Description: 设置norm
	 */
	public void setNorm(Norm norm) {
		this.norm = norm;
	}

	/**
	 * @Title: getIsRun
	 * @Description: 获取isRun
	 * @return: Boolean isRun
	 */
	public Boolean getIsRun() {
		return isRun;
	}

	/**
	 * @Title: setIsRun
	 * @Description: 设置isRun
	 */
	public void setIsRun(Boolean isRun) {
		this.isRun = isRun;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetBattery
	 * @Description: 获取setBattery
	 * @return: Set<Battery> setBattery
	 */
	public Set<Battery> getSetBattery() {
		return setBattery;
	}

	/**
	 * @Title: setSetBattery
	 * @Description: 设置setBattery
	 */
	public void setSetBattery(Set<Battery> setBattery) {
		this.setBattery = setBattery;
	}

	/**
	 * @Title: getSetTestData
	 * @Description: 获取setTestData
	 * @return: Set<TestData> setTestData
	 */
	public Set<TestData> getSetTestData() {
		return setTestData;
	}

	/**
	 * @Title: setSetTestData
	 * @Description: 设置setTestData
	 */
	public void setSetTestData(Set<TestData> setTestData) {
		this.setTestData = setTestData;
	}

	/**
	 * @Title: getSetGroupInfo
	 * @Description: 获取setGroupInfo
	 * @return: Set<GroupInfo> setGroupInfo
	 */
	public Set<GroupInfo> getSetGroupInfo() {
		return setGroupInfo;
	}

	/**
	 * @Title: setSetGroupInfo
	 * @Description: 设置setGroupInfo
	 */
	public void setSetGroupInfo(Set<GroupInfo> setGroupInfo) {
		this.setGroupInfo = setGroupInfo;
	}

	/**
	 * @Title: getSetGroupLimit
	 * @Description: 获取setGroupLimit
	 * @return: Set<GroupLimit> setGroupLimit
	 */
	public Set<GroupLimit> getSetGroupLimit() {
		return setGroupLimit;
	}

	/**
	 * @Title: setSetGroupLimit
	 * @Description: 设置setGroupLimit
	 */
	public void setSetGroupLimit(Set<GroupLimit> setGroupLimit) {
		this.setGroupLimit = setGroupLimit;
	}

	/**
	 * @Title: getSetAlarm
	 * @Description: 获取setAlarm
	 * @return: Set<Alarm> setAlarm
	 */
	public Set<Alarm> getSetAlarm() {
		return setAlarm;
	}

	/**
	 * @Title: setSetAlarm
	 * @Description: 设置setAlarm
	 */
	public void setSetAlarm(Set<Alarm> setAlarm) {
		this.setAlarm = setAlarm;
	}

	/**
	 * @Title: getSetLimit
	 * @Description: 获取setLimit
	 * @return: Set<Limit> setLimit
	 */
	public Set<Limit> getSetLimit() {
		return setLimit;
	}

	/**
	 * @Title: setSetLimit
	 * @Description: 设置setLimit
	 */
	public void setSetLimit(Set<Limit> setLimit) {
		this.setLimit = setLimit;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Group [groupId=" + groupId + ", interval=" + interval + ", unit=" + unit + ", norm=" + norm + ", isRun="
				+ isRun + ", address=" + address + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
