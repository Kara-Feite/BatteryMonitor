<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池测试数据统计图</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src="${ctxStatic}/js/echarts/echarts.min.js"></script>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<script> 
function query(){
   	document.myForm.action="${ctx}/battery/graph";
  	document.myForm.submit();
}
</script>
</head>
<body>
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north'" style="height:62;">
			<sf:form name="myForm" method="get" modelAttribute="temp" enctype="multipart/form-data">
				<sf:hidden path="id"/>
				<sf:hidden path="treeId"/>
				<table width="100%" bgcolor="#f5fafe" id="denglu">
					<tr>
						<td>
							<strong>开始时间：</strong>
							<sf:input path="startTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>测量类型：</strong>
							<sf:select path="type" style="width:110px;">
							 	<sf:option value="单体电压/V"/>
					            <sf:option value="单体内阻/mΩ"/>
					            <sf:option value="单体温度/℃"/>
							</sf:select>
						</td>
					</tr>
					<tr>
						<td>
							<strong>结束时间：</strong>
							<sf:input path="endTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>参 考 值：</strong>&nbsp;<sf:input path="val" size="14"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查  询</a>
							&nbsp;&nbsp;
							<!-- <a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="checkExecl();" href="javascript:void(0);" style="width:130px;">导出Excel报告</a>	 -->
						</td>
					</tr>
				</table>		
			</sf:form>
		</div>
		<!-- ------------------------------------------ -->
		<div data-options="region:'center'">
			<div id='view' style="width:800px;height:420px;margin:5px 0 0 200px;"></div>
		</div>
	</div>
	<c:set var="name" value="${graphDto.name}"></c:set>
	<c:set var="limitVal" value="${graphDto.limitVal}"></c:set>
	<script>
		function getTestNum(num) {
			var arr = [];
			for (var i = 1; i <= num; i++) {
				arr.push(i);
			}
			return arr;
		}
		var myChart = echarts.init(document.getElementById('view'));
        // 指定图表的配置项和数据
        var option = {
            title : {
                text: '${graphDto.type}'
            },
            tooltip : {
                trigger: 'axis',
                formatter: '{b0}<br />${name} : {c0}${graphDto.unit}'
            },
            animation : false,
            legend: {
                selectedMode : false,
                data:[
                    /*icon : 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'*/
                    {
                        name : '未超出${name}警告线'
                    }
                ]
            },
            toolbox: {
                show : true,
                orient: 'horizontal',
                showTitle: true,
                feature : {
                    saveAsImage : {show: true}
                }
            },
            /*['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3']*/
            color : ['#2284c7'],
            calculable : true,
            xAxis : [
                {
                    gridIndex : 0,
                    type : 'category',
                    data : getTestNum('${graphDto.num}')
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'未超出${name}警告线',
                    type:'line',
                    data:[
                    	<c:forEach items="${graphDto.listData}" var="data">
                    		<c:set var="val" value="${data.voltage}"></c:set>
                    		{
                    			name : '<fmt:formatDate value="${data.testTime}" pattern="yyyy-MM-dd HH:mm:ss"/>',
                    			value : '${val}'
                    			<c:if test="${val >= limitVal}">
	                    			,
		                            symbolSize : 10,
		                            itemStyle:{color : '#c23531'}
                    			</c:if>
                    		},
                    	</c:forEach>
                    ],
                    markPoint : {
                        data : [
                            {type : 'max', name: '最大值', itemStyle:{color : '#c23531'}},
                            {type : 'min', name: '最小值', itemStyle:{color : '#2284c7'}}
                        ]
                    },
                    markLine: {
                        itemStyle: {
                            normal: {lineStyle: {type: 'dashed'},label: {show: true}}
                        },
                        data: [
                            /* {
                                name: '平均线',
                                // 支持 'average', 'min', 'max'
                                type: 'average'
                            }, */
                            {
                                itemStyle:{color : '#c23531'},
                                name : '${name}警告门限',
                                yAxis: '${limitVal}'
                            }
                        ]
                    }
                }
            ]
        };
        myChart.setOption(option);
	</script>
</body>
</html>
