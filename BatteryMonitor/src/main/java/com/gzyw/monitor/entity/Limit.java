package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @ClassName: Limit
 * @Description: 警告门限实体类
 * @author LW
 * @date 2018年7月11日 下午2:45:20
 */
@Entity
@Table(name = "bm_limit")
public class Limit implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 8231894183517960947L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 电池组
	 */
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;
	
	/**
	 * 电池组名称
	 */
	@Column(name = "group_name", length = 32, nullable = false)
	private String groupName;
	
	/**
	 * 测试类型
	 */
	@Column(name = "type", length = 16, nullable = false)
	private String type;
	
	/**
	 * 门限类型
	 */
	@Column(name = "mold", length = 16, nullable = false)
	private String mold;
	
	/**
	 * 参考值
	 */
	@Column(name = "val", nullable = false, columnDefinition = "DOUBLE DEFAULT 0")
	@NotNull(message = "警告高门限不能为空")
	private Double val;
	
	/**
	 * 警告高门限
	 */
	@Column(name = "high_warn", nullable = false, columnDefinition = "DOUBLE DEFAULT 0")
	@NotNull(message = "过高警告门限不能为空")
	private Double highWarn;

	/**
	 * 预警高门限
	 */
	@Column(name = "high_plan", nullable = false, columnDefinition = "DOUBLE DEFAULT 0")
	@NotNull(message = "过高预警门限不能为空")
	private Double highPlan;

	/**
	 * 警告低门限
	 */
	@Column(name = "low_warn", nullable = true, columnDefinition = "DOUBLE DEFAULT 0")
	private Double lowWarn;

	/**
	 * 预警低门限
	 */
	@Column(name = "low_plan", nullable = true, columnDefinition = "DOUBLE DEFAULT 0")
	private Double lowPlan;

	/**
	 * 状态
	 */
	@Column(name = "state", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean state;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 
	 * @Title: Limit
	 * @Description: Limit构造函数
	 */
	public Limit() {}

	/**
	 * 
	 * @Title: Limit
	 * @Description: Limit构造函数
	 * @param id
	 * @param group
	 * @param groupName
	 * @param type
	 * @param mold
	 * @param val
	 * @param highWarn
	 * @param highPlan
	 * @param lowWarn
	 * @param lowPlan
	 * @param state
	 * @param createTime
	 * @param updateTime
	 */
	public Limit(Integer id, Group group, String groupName, String type, String mold, Double val, Double highWarn,
			Double highPlan, Double lowWarn, Double lowPlan, Boolean state, Date createTime, Date updateTime) {
		this.id = id;
		this.group = group;
		this.groupName = groupName;
		this.type = type;
		this.mold = mold;
		this.val = val;
		this.highWarn = highWarn;
		this.highPlan = highPlan;
		this.lowWarn = lowWarn;
		this.lowPlan = lowPlan;
		this.state = state;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getGroupName
	 * @Description: 获取groupName
	 * @return: String groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @Title: setGroupName
	 * @Description: 设置groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getMold
	 * @Description: 获取mold
	 * @return: String mold
	 */
	public String getMold() {
		return mold;
	}

	/**
	 * @Title: setMold
	 * @Description: 设置mold
	 */
	public void setMold(String mold) {
		this.mold = mold;
	}

	/**
	 * @Title: getVal
	 * @Description: 获取val
	 * @return: Double val
	 */
	public Double getVal() {
		return val;
	}

	/**
	 * @Title: setVal
	 * @Description: 设置val
	 */
	public void setVal(Double val) {
		this.val = val;
	}

	/**
	 * @Title: getHighWarn
	 * @Description: 获取highWarn
	 * @return: Double highWarn
	 */
	public Double getHighWarn() {
		return highWarn;
	}

	/**
	 * @Title: setHighWarn
	 * @Description: 设置highWarn
	 */
	public void setHighWarn(Double highWarn) {
		this.highWarn = highWarn;
	}

	/**
	 * @Title: getHighPlan
	 * @Description: 获取highPlan
	 * @return: Double highPlan
	 */
	public Double getHighPlan() {
		return highPlan;
	}

	/**
	 * @Title: setHighPlan
	 * @Description: 设置highPlan
	 */
	public void setHighPlan(Double highPlan) {
		this.highPlan = highPlan;
	}

	/**
	 * @Title: getLowWarn
	 * @Description: 获取lowWarn
	 * @return: Double lowWarn
	 */
	public Double getLowWarn() {
		return lowWarn;
	}

	/**
	 * @Title: setLowWarn
	 * @Description: 设置lowWarn
	 */
	public void setLowWarn(Double lowWarn) {
		this.lowWarn = lowWarn;
	}

	/**
	 * @Title: getLowPlan
	 * @Description: 获取lowPlan
	 * @return: Double lowPlan
	 */
	public Double getLowPlan() {
		return lowPlan;
	}

	/**
	 * @Title: setLowPlan
	 * @Description: 设置lowPlan
	 */
	public void setLowPlan(Double lowPlan) {
		this.lowPlan = lowPlan;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "Limit [id=" + id + ", group=" + group + ", groupName=" + groupName + ", type=" + type + ", mold=" + mold
				+ ", val=" + val + ", highWarn=" + highWarn + ", highPlan=" + highPlan + ", lowWarn=" + lowWarn
				+ ", lowPlan=" + lowPlan + ", state=" + state + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}
	
}
