package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.entity.Menu;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.MenuService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: MainController
 * @Description: 主页面控制层
 * @author LW
 * @date 2018年6月7日 上午9:02:56
 */
@Controller
@RequestMapping("/main")
public class MainController {
	
	@Autowired
	private TreeService treeService;
	
	@Autowired
	private MenuService  menuService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}
	
	/**
	 * 
	 * @Title: showMain
	 * @Description: 主页面
	 * @return
	 */
	@RequestMapping("/index")
	String showMain(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		Integer userId = user.getId();
		List<Tree> listTree = treeService.listTreeByUserId(userId);
		List<Menu> listMenu = menuService.listMenuByUserId(userId, true);
		model.addAttribute("listTree", listTree);
		model.addAttribute("listMenu", listMenu);
		return "main/main";
	}
	
	/**
	 * 
	 * @Title: listTreeByAll
	 * @Description: 节点信息
	 * @param id
	 * @return
	 */
	@RequestMapping("/tree")
	@ResponseBody
	List<Tree> listTreeByAll() {
		return treeService.listTreeByAll();
	}
	
}
