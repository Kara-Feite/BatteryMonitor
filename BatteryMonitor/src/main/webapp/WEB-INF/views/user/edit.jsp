<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>编辑用户</title>
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<style>
select,input{width:220px;}
.input-role{width:15px;}
.role{margin-top:-6px;margin-bottom:8px;vertical-align:middle;}
.role input{width:16px;}
.role span{margin-right:39px;}
.role *{vertical-align:middle;}
a{width: 80px;}
</style>
<script>
function edit(){
	$('#myForm').form('submit',{
		url : '${ctx}/user/${empty user.id ? "save" : "update"}',
		onSubmit:function(param){
			<c:if test="${!empty user.id}">
				param.password = '1';
 			</c:if>
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				alert('${empty user.id ? "添加成功！" : "更新成功！"}');
				window.location.href = '${ctx}/user/list';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="user" enctype="multipart/form-data">
		<sf:hidden path="id"/>
  		<div class="easyui-panel" title="操作：编辑用户信息" style="padding:30px 60px;">
           <div style="margin-bottom:20px">
			 	<sf:input path="nickName" class="easyui-textbox"
			 		data-options="label:'姓&nbsp;&nbsp;&nbsp;&nbsp;名:',required:true"/><br><br>
			 	<sf:input path="userName" class="easyui-textbox" 
			 		data-options="label:'用 户 名:',required:true"/><br><br>
			 	<c:if test="${empty user.id}">
				 	<sf:input path="password"  class="easyui-textbox" 
				 		data-options="label:'密&nbsp;&nbsp;&nbsp;&nbsp;码:',required:true"/><br><br>
			 	</c:if>
			 	<sf:input path="phone" class="easyui-textbox" 
			 		data-options="label:'手 机 号:',required:true"/><br><br>
			 	<sf:input path="email" class="easyui-textbox" 
			 		data-options="label:'E m a i l:',validType:'email'"/><br><br>
			 	<div class='role'>
			 		<span>角&nbsp;&nbsp;&nbsp;&nbsp;色:</span>
				 	<c:forEach items="${listRole}" var="role">				 			
						<span style='margin-right:15px;'>
							<input id="roleId${role.id}" name="listRole" class="required" type="checkbox" 
							<c:if test="${mapUserRole[role.id] == role.id}">checked</c:if>  value="${role.id}"/>
							<label for="roleId${role.id}">${role.name}</label>
						</span>
					</c:forEach>
				</div>
            	<a href="javascript:void(0)" class="easyui-linkbutton" 
            		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a>
          		&nbsp;&nbsp;&nbsp;&nbsp;
        		<a href="${ctx}/user/list" class="easyui-linkbutton"
        			data-options="iconCls:'icon-reload'">返 回</a>
        	</div>
   		</div>
	</sf:form>
</body>
</html>