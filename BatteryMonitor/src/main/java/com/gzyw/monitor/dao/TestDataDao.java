package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: TestDataDao
 * @Description: 测试数据持久层
 * @author LW
 * @date 2018年6月29日 上午9:51:26
 */
public interface TestDataDao extends BaseDao<TestData> {

	/**
	 * 
	 * @Title: countTestData
	 * @Description: 获取对应测试数据总数
	 * @param temp
	 * @return
	 */
	Long countTestData(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataByPage
	 * @Description: 获取测试数据分页信息
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或者降序
	 * @param temp 临时对象
	 * @return
	 */
	List<TestData> listTestDataByPage(Integer page, Integer rows, String sort, String order, TempDto temp);

	/**
	 * 
	 * @Title: getMaxMinDto
	 * @Description: 获取最大值和最小值相关数据
	 * @param temp
	 * @return
	 */
	MaxMinDto getMaxMinDto(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataByGroup
	 * @Description: 根据电池组获取测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByGroup(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataByTime
	 * @Description: 根据时间获取测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByTime(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataBetTime
	 * @Description: 获取时间段内的测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataBetTime(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataByBat
	 * @Description: 根据电池获取测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByBat(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataByExport
	 * @Description: 获取导出电池组测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByExport(TempDto temp);

	/**
	 * 
	 * @Title: delTestDataByGroup
	 * @Description: 删除电池组测试数据
	 * @param groupId
	 */
	void delTestDataByGroup(Integer groupId);
}
