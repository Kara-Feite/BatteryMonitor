package com.gzyw.monitor.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.EncodeUtil;
import com.gzyw.monitor.dao.RoleDao;
import com.gzyw.monitor.dao.UserDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.UserService;

/**
 * 
 * @ClassName: UserServiceImpl
 * @Description: 用户服务层实现类
 * @author LW
 * @date 2018年6月6日 下午8:45:42
 */
@Service
public class UserServiceImpl implements UserService {
	
	/**
	 * 用户持久层接口
	 */
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public User loginUser(String userName, String password) {
		User user = userDao.getUserByName(userName);
		if (user != null && vaildatePassword(password, user.getPassword())) {
			return user;
		}
		return null;
	}
	
	@Override
	public EUDataGridDto getDataGrid(Integer page, Integer rows, String sort, String order, User user) {
		Long count = userDao.countUser();
		List<User> listUser = userDao.listUserByPage(page, rows, sort, order, user);
		return new EUDataGridDto(count, listUser);
	}

	@Override
	public User getUserById(Integer id) {
		return userDao.get(id);
	}
	
	@Override
	public ResultDto saveUser(User user, int[] listRole) {
		User userByName = userDao.getUserByName(user.getUserName());
		if (userByName != null) {
			return ResultDto.build(500, "该用户名已被占用！");
		}
		Date date = new Date();
		user.setCreateTime(date);
		user.setUpdateTime(date);
		user.setPassword(EncodeUtil.encode(user.getPassword()));
		user.setState(true);
		userDao.save(user);
		roleDao.saveRoleToUser(user.getId(), listRole);
		return ResultDto.ok();
	}
	
	@Override
	public ResultDto updateUser(User user, int[] listRole) {
		User userByName = userDao.getUserByName(user.getUserName());
		Integer userId = user.getId();
		if (userByName != null && userId != userByName.getId()) {
			return ResultDto.build(500, "该用户名已被占用！");
		}
		user.setUpdateTime(new Date());
		Integer result = userDao.updateUser(user);
		roleDao.deleteRoleToUser(userId);
		roleDao.saveRoleToUser(userId, listRole);
		if (result != null & result > 0) {
			return ResultDto.ok();
		}
		return ResultDto.build(500, "未更新 成功");
	}
	
	@Override
	public ResultDto delUserByBat(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的用户");
		}
		for (int id : ids) {
			roleDao.deleteRoleToUser(id);
			User user = userDao.get(id);
			if (user != null) {			
				userDao.delete(user);														
			}
		}
		return ResultDto.build(200, "删除用户成功！");
	}
	
	@Override
	public ResultDto updateState(int[] ids, boolean state) {
		if (ids == null) {
			return ResultDto.build(500, "请选择需要更新状态的用户");
		}
		for (int id : ids) {
			User user = userDao.get(id);
			if (user != null) {			
				userDao.updateState(id, state);
			}
		}
		return ResultDto.build(200, "更改状态成功！");
	}
	
	@Override
	public ResultDto changePwd(String oldPwd, String newPwd, Integer userId) {
		User user = userDao.get(userId);
		if (user != null && vaildatePassword(oldPwd, user.getPassword())) {
			newPwd = EncodeUtil.encode(newPwd);
			Integer result = userDao.changePwd(newPwd, userId);
			if (result > 0) {
				return ResultDto.ok();
			}
		} else {
			return ResultDto.build(500, "旧密码不正确");
		}
		return ResultDto.build(500, "修改密码失败");
	}
	
	@Override
	public List<User> listUserByAll() {
		return userDao.listUserByAll();
	}
	
	/**
	 * 
	 * @Title: vaildatePassword
	 * @Description: 验证密码
	 * @param plainPwd
	 * @param encryptPwd
	 * @return
	 */
	private boolean vaildatePassword(String plainPwd, String encryptPwd) {
		return EncodeUtil.decode(plainPwd, encryptPwd);
	}

}
