package com.gzyw.monitor.common.util;
/**
 * 
 * @ClassName: EncodeUtil
 * @Description: PBKDF加解密工具类
 * @author LW
 * @date 2018年3月29日 下午9:06:05
 */
public class EncodeUtil {
	
	/**
	 * 随机盐位数
	 */
	private static final Integer SIZE = 32;
	/**
	 * HASH散列次数
	 */
	private static final Integer HASH = 1024;
	
	/**
	 * 
	 * @Title: encode
	 * @Description: 加密方法
	 * @param plain
	 * @return
	 */
	public static String encode(String plain) {
		//1创建一个随机字节数组
		byte[] salt = EncryptUtil.generateSalt(SIZE);
		//2使用可逆加密算法对随机数进行加密(HEX)
		String encodeHex = EncryptUtil.encodeHex(salt);
		//3将随机数和明文用不可逆加密算法进行加密(SHA1)
		byte[] sha1 = EncryptUtil.sha1(plain.getBytes(), salt, HASH);
		//4将第三步得到的结果进行可逆加密(HEX)
		String ciphertext = EncryptUtil.encodeHex(sha1);
		//5将第二步和第四步的结果进行拼接
		String finalCipher = encodeHex+ciphertext;
		return finalCipher;
	}
	
	/**
	 * 
	 * @Title: decode
	 * @Description: 密码校验方法
	 * @param plain 明文密码
	 * @param cipher 密文密码
	 * @return
	 */
	public static boolean decode(String plain,String cipher) {
		boolean flag = false;
		//截取密文的盐
		String saltHex = cipher.substring(0, SIZE*2);
		//解密盐
		byte[] salt = EncryptUtil.decodeHex(saltHex);
		//将盐和明文进行不可逆加密
		byte[] sha1 = EncryptUtil.sha1(plain.getBytes(), salt, HASH);
		//将不可逆加密后的密文进行可逆加密
		String encodeHex = EncryptUtil.encodeHex(sha1);
		//将可逆加密的盐和可逆加密的密文进行拼接
		String finalCipher = saltHex+encodeHex;
		if (finalCipher.equals(cipher)) {
			flag = true;
		}
		return flag;
	}
	
	public static void main(String[] args) {
		System.out.println(encode("admin"));
	}

}