<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>站点管理</title>	
<c:set var="groupId" value="${groupId}"/>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>	
#denglu *{vertical-align:middle;}
select{width:120px;}
</style>
<script>
$(function(){
	$('#group_lists').datagrid({
		url:'${ctx}/limit/datagrid?groupId=${groupId}',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		pageNumber : 1,
		sortName : 'name',
		sortOrder : 'DESC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'groupName',
		        title:'电池组',
		        width:50,
		        align:'center'
			},{
		        field:'type',
		        title:'测量类型',
		        width:50,
		        align:'center'
			},{
		        field:'mold',
		        title:'门限类型',
		        width:50,
		        align:'center'
			},{
		        field:'val',
		        title:'参考值',
		        width:50,
		        align:'center',
		        sortable:true
			},{
		        field:'highWarn',
		        title:'过高告警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
		        field:'highPlan',
		        title:'过高预警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
				field:'lowWarn',
				title:'过低告警',
				width:50,
				align:'center',
				sortable:true
			},{
				field:'lowPlan',
				title:'过低预警',
				width:50,
				align:'center',
				sortable:true
			},{
		        field:'state',
		        title:'状态',
		        width:50,
		        align:'center',
		        formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">激活</font>';
					}else{
						return '<font style="color:red">禁用</font>';
					}
				}
			}]],
		toolbar:[
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',		
			{text:'删 除',iconCls:'icon-remove',handler:function(){del();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});

function query(){
	$('#group_lists').datagrid('load',serializeObject($('#search_form').form()));
}

function clean(){
	$('#group_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
	$('#search_form').form().find('select').val('');
}
function add() {
	<permission:notPermission name="tree:add">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	window.location.href = "${ctx}/limit/edit?groupId=${groupId}";
}
function update(){
	<permission:notPermission name="tree:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#group_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/limit/edit?id=" + rows[0].id + "&groupId=${groupId}";
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}

function del(){
	var rows=$('#group_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/limit/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#group_lists').datagrid('load',{});
						$('#group_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}
function unselect(){
	$('#group_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
} 
</script>
<body>	
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north',title:'节点：${tree.name} 告警'" style="height:61px;">
			<form id="search_form">
				<input type="hidden" name="groupId" value="${group.id}">
				<table width="100%" bgcolor="#f5fafe">
			    	<tr height="25">
						<td id="denglu"><img src="${ctxStatic}/images/yin.gif" width="22">							
							<strong>名 称：</strong><input name="groupName" size="15"/>
							&nbsp;&nbsp;
							<strong>测试类型：</strong>			        
				         	<select name="type">
								<option value=""/></option>  				     	
					            <option value="单体电压/V">单体电压/V</option>   
					            <option value="单体内阻/mΩ">单体内阻/mΩ</option>  
					            <option value="单体温度/℃">单体温度/℃</option> 
				        	</select>
				        	&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查 询</a>						
							&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
								onclick="clean();" href="javascript:void(0);">清 空</a>
							&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-add'" 
								target="groupFrame" href="javascript:add();">添 加</a>
							<!-- &nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="checkExecl();" href="javascript:void(0);" style="width:130px;">导出Excel表格</a> -->
						</td>					
					</tr>		   			    				
				</table>
			</form>
		</div>
		<!-- ----------------数据层------------- -->
		<div data-options="region:'center'">
			<table id="group_lists"></table>
		</div>
	</div>
</body>
</html>