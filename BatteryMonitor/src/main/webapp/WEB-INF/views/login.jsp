<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>登录</title>
<link href='${ctxStatic}/css/public.css' rel='stylesheet'>
<style>
	body{width:100%;height:100%;background-color:#1F62A9;}
	/* content start */
	#content{position:absolute;margin:auto;left:0;right:0;top:0;bottom:0;overflow:hidden;width:770px;height:450px;background-image:url('${ctxStatic}/images/login/login.jpg');}
	#content .main{position:absolute;top:215px;right:0;width:226px;}
	#content .main li{height:24px;margin-bottom:3px;vertical-align:middle;overflow:hidden;}
	#content .main label{display:inline-block;vertical-align:middle;margin-right:3px;width:84px;height:22px;}
	#content .main li input,#content #btn-submit{vertical-align:middle;width:130px;text-indent:1px;}
	#content #code{width:72px;}
	#content #remeber{width:12.8px;}
	#content #btn-submit{display:inline-block;width:72px;height:20px;border:none;border-radius:5px;margin-left:15px;background-image:url('${ctxStatic}/images/login/btn-login.gif');}
	#content #message{text-align:center;margin-top:20px;color:#b31d1d;font-size:16px;font-weight:bold;}
	/* content end */
</style>
<script src='${ctxStatic}/js/jquery-1.4.4.min.js'></script>
</head>
<body onload='init()' onkeydown='keyLogin()'>
	<!-- content start -->
	<div id='content'>
		<div class='main'>
			<form method='post' name='myForm'>
				<ul>
					<li>
						<label for='userName'><img src='${ctxStatic}/images/login/user.gif' width='84' height='20'></label>
						<input type='text' name='userName' id = 'userName'>
					</li>
					<li>
						<label for='password'><img src='${ctxStatic}/images/login/password.gif' width='84' height='20'></label>
						<input type='password' name='password' id='password'>
					</li>
					<li>
						<label for='code'><img src='${ctxStatic}/images/login/code.jpg' width='84' height='20'></label>
						<input type='text' name='code' id='code'>
						<img src='${ctx}/user/code' id='img-code' alt='验证码' title='点击切换图片'>
					</li>
					<li>
						<label for='remeber'><img src='${ctxStatic}/images/login/remeber.jpg' width='84' height='20'></label>
						<input type='checkbox' name='remeber' id='remeber'>
						<a id='btn-submit' href='javascript:login()'></a>
					</li>
				</ul>
			</form>
			<div id='message'></div>
		</div>
	</div>
	<!-- content end -->
	<script>
		// 初始化记住密码
		(function() {
			var cookie = document.cookie,
				exp1 = new RegExp('userName' + '=.*?(?=;|$)'),
				userCookie = cookie.match(exp1),
				exp2 = new RegExp('password' + '=.*?(?=;|$)'),
				pwdCookie = cookie.match(exp2);
			if (!userCookie || !pwdCookie) {
				return;
			}
			document.getElementById('userName').value = userCookie[0].substring(9);
			document.getElementById('password').value = pwdCookie[0].substring(9);
			document.myForm.remeber.checked = true;
		})();
		// 登录的方法
		function login() {
			var form = document.myForm,
				userName = form.userName.value.trim();
			if(userName === ''){
				alert('请输入用户名');
				form.userName.focus();
				return;
			}
			var password = form.password.value.trim();
			if(password === ''){
				alert('请输入密码');
				form.password.focus();
				return;
			}
			var code = form.code.value.trim();
			if(code == ''){
				alert('请输入验证码');
				form.code.focus();
				return;
			}
			var msg = document.getElementById('message');
			// 验证验证码
			$.ajax({
				'url' : '${ctx}/user/check/code',
				'data' : {'code' : form.code.value.trim()},
				'success' : function(result) {
					if (result.status === 200) {
						var remeber = form.remeber.checked;
						// 成功，验证登录信息
						$.ajax({
							'url' : '${ctx}/user/login',
							'type' : 'post',
							'data' : {'code' : code, 'userName' : userName, 'password' : password, 'remeber' : remeber},
							'dataType' : 'json',
							'beforeSend' : function(xhr) { 
								xhr.withCredentials = true; 
							},
							'success' : function(result) {
								if (result.status === 200) {
									var data = result.data;
									var date = new Date();
									if (data) {
										date.setTime(date.getTime() + 7*24*60*60*1000);
										document.cookie = "userName=" + data.userName + ";expires=" + date.toGMTString();
										document.cookie = "password=" + data.password + ";expires=" + date.toGMTString();
									} else {
						                date.setTime(date.getTime() - 10000); 
						                document.cookie = "userName=a" + ";expires=" + date.toGMTString();
										document.cookie = "password=b" + ";expires=" + date.toGMTString();
									}
									form.password.value = '';
									changeImage(form);
									msg.innerText = '';
									window.location.href = '${ctx}/main/index';
								} else {
									form.password.value = '';
									changeImage(form);
									msg.innerText = result.msg;
								}
							},
							error : function() {
								form.password.value = '';
								changeImage(form);
								msg.innerText = '登录失败'
							}
						});
					} else {
						changeImage(form);
						msg.innerText = result.msg;
					}
				}
			});
		}
		// 初始化焦点位置
		function init(){
			// 如果是记住密码的话，焦点直接为验证码，否则为用户名
			if (document.myForm.remeber.checked) {
				document.all.code.focus();
			} else {
				document.all.userName.focus();
			}
		}
		// 改变验证码的图片
		function changeImage(form){
			var imgCode = document.getElementById('img-code');
			form.code.value = '';
			imgCode.src = '${ctx}/user/code?date=' + new Date();
		}
		// 回车登录
		function keyLogin(e) {
			e = e || window.event;
			if (event.keyCode == 13){
				login();
	        }
		}
	</script>
</body>
</html>