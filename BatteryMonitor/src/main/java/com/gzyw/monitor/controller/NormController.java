package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Norm;
import com.gzyw.monitor.service.NormService;

/**
 * 
 * @ClassName: NormController
 * @Description: 电池规格控制层
 * @author LW
 * @date 2018年6月13日 上午9:25:14
 */
@Controller
@RequestMapping("/norm")
public class NormController {
	
	/**
	 * 电池规格服务层接口
	 */
	@Autowired
	private NormService normService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}
	
	/**
	 * 
	 * @Title: showList
	 * @Description: 跳转到规格列表
	 * @return
	 */
	@RequestMapping("/list")
	@Permissions("norm:query")
	String showList() {
		return "norm/list";
	}
	
	/**
	 * id为空就添加，否则更新
	 * @Title: showEdit
	 * @Description: 跳转到编辑页码
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/edit")
	String showEdit(@RequestParam(value = "id", required = false) Integer id, Model model) {
		if (id != null) {
			Norm norm = normService.getNormById(id);
			if (norm != null) {
				model.addAttribute("norm", norm);
			} else {
				return "redirect:/error";
			}
		} else {
			model.addAttribute("norm", new Norm());
		}
		List<String> listType = normService.listTypeAll();
		List<String> listMaker = normService.listMakerAll();
		List<String> listModel = normService.listModelAll();
		model.addAttribute("listType", listType);
		model.addAttribute("listMaker", listMaker);
		model.addAttribute("listModel", listModel);
		return "norm/edit";
	}
	
	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 返回电池规格列表数据
	 * @param page 页码
	 * @param norm 电池规格
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value="/datagrid",method=RequestMethod.POST)
	@ResponseBody
	EUDataGridDto getDatagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Norm norm,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {	
		return normService.getDatagrid(page, rows, sort, order, norm);
	}
	
	/**
	 * 
	 * @Title: saveNorm
	 * @Description: 添加规格
	 * @param norm
	 * @param br
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	@SystemControllerLog(type = "规格管理", description = "添加规格")
	@Permissions("norm:add")
	public ResultDto saveNorm(@Validated Norm norm , BindingResult br) {
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage() + ",");
			}
			return ResultDto.build(500, sb.substring(0, sb.length() - 1));
		}
		return normService.saveNorm(norm);		
	}
	
	/**
	 * 
	 * @Title: updateNorm
	 * @Description: 更新规格
	 * @param norm
	 * @param br
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	@SystemControllerLog(type = "规格管理", description = "更新规格")
	@Permissions("norm:update")
	public ResultDto updateNorm(@Validated Norm norm , BindingResult br) {
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage() + ",");
			}
			return ResultDto.build(500, sb.substring(0, sb.length() - 1));
		}
		return normService.updateNorm(norm);		
	}
	
	/**
	 * 
	 * @Title: delNormByBat
	 * @Description: 批量删除规格
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	@SystemControllerLog(type = "规格管理", description = "删除规格")
	@Permissions("norm:delete")
	public ResultDto delNormByBat(int[] ids) {
		try {
			return normService.delNormByBat(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}

}
