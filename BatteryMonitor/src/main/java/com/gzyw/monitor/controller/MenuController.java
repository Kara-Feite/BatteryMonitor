package com.gzyw.monitor.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: MenuController
 * @Description: 菜单控制层
 * @author LW
 * @date 2018年6月8日 上午9:59:46
 */
@Controller
@RequestMapping("/menu")
public class MenuController {

	@Autowired
	private TreeService treeService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: listTreeRoot
	 * @Description: 展示所有根节点信息
	 * @return
	 */
	@RequestMapping("/list")
	String listTreeRoot() {
		return "menu/list";
	}

	/**
	 * id为空就添加，否则更新
	 * @Title: showEdit
	 * @Description: 跳转到编辑页码
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/edit")
	String showEdit(@RequestParam(value = "id", required = false) Integer id, Model model) {
		if (id != null) {
			Tree tree = treeService.getTreeById(id);
			if (tree != null) {
				model.addAttribute("tree", tree);
			} else {
				return "redirect:/error";
			}
		} else {
			model.addAttribute("tree", new Tree());
		}
		return "menu/edit";
	}

	/**
	 * 
	 * @Title: saveTreeByRoot
	 * @Description: 添加基站根节点
	 * @param tree
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "添加基站")
	@Permissions("tree:add")
	public ResultDto saveTreeByRoot(Tree tree) {
		if (tree == null || StringUtil.isEmpty(tree.getName())) {
			return ResultDto.build(500, "名称不能为空");
		}
		return treeService.saveTreeByRoot(tree);
	}

	/**
	 * 
	 * @Title: updateTreeByRoot
	 * @Description: 更新根节点
	 * @param tree
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "更新基站")
	@Permissions("tree:update")
	public ResultDto updateTreeByRoot(Tree tree) {
		if (tree == null || StringUtil.isEmpty(tree.getName())) {
			return ResultDto.build(500, "名称不能为空");
		}
		return treeService.updateTreeByRoot(tree);
	}

	/**
	 * 
	 * @Title: getDataGridByRoot
	 * @Description: 根节点列表数据
	 * @return
	 */
	@RequestMapping("/datagrid")
	@ResponseBody
	EUDataGridDto getDataGridByRoot(@RequestParam(value = "page", defaultValue = "1") Integer page, Tree tree,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order, HttpSession session) {
		User user = (User) session.getAttribute("user");
		return treeService.getDataGridByRoot(page, rows, sort, order, tree, user.getId());
	}
	
	/**
	 * 
	 * @Title: congeal
	 * @Description: 冻结
	 * @param ids 选中的id数组
	 * @return
	 */
	@RequestMapping(value = "/congeal")
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "禁用基站")
	@Permissions("tree:congeal")
	public ResultDto congeal(int[] ids) {
		try {
			return treeService.updateTreeByState(ids, Boolean.FALSE);
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，冻结失败！");
		}
	}
	
	/**
	 * 
	 * @Title: thaw
	 * @Description: 解冻
	 * @param ids 选中的id数组
	 * @return
	 */
	@RequestMapping(value = "/thaw")
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "激活基站")
	@Permissions("tree:thaw")
	public ResultDto thaw(int[] ids) {
		try {
			return treeService.updateTreeByState(ids, Boolean.TRUE);
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，解冻失败！");
		}
	}

}
