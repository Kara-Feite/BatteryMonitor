<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池组头部链接</title>
<script src="${ctxStatic}/js/jquery-1.4.4.min.js"></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<link href="${ctxStatic}/css/Font.css" rel="stylesheet">
<style>
.select-top{
	background-color: #59ACFF;
    display: block;
    text-decoration: none;
}
</style>
</head>   
<body> 
	<sf:form method="get" modelAttribute="groupId">
		<table width="100%" border="0" height="25" cellpadding="0" cellspacing="0" 
			background="${ctxStatic}/images/b-info.gif">
			<tr height="22">
				<td>	
					<div class="topnav">
						<ul>
						   <li><a target="groupFrame" class='select-top'
								href="${ctx}/group/content?groupId=${groupId}">
								<font>概 况</font></a></li>
						   <li><a target="groupFrame"
								href="${ctx}/alarm/content?treeId=${groupId}">
								<font>告 警</font></a></li>
							<li><a target="groupFrame"
								href="${ctx}/limit/content?groupId=${groupId}">
								<font>告警门限</font></a></li>					  
				  			<li><a target="groupFrame"
								href="${ctx}/group/report?groupId=${groupId}">
								<font>报 告</font></a></li>
							<li><a target="groupFrame"
								href="${ctx}/group/edit?treeId=${groupId}">
								<font>添加电池组</font></a></li>
						</ul>
					</div>																
	    		</td>						
			</tr>							
	    </table>		
	</sf:form>
	<script>
		$('.topnav li a').click(function() {
			$('.select-top').removeClass('select-top');
			$(this).addClass('select-top');
		});
	</script>
</body>
</html>