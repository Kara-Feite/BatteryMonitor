<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>添加节点</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
	.div-group, .div-group *{vertical-align:middle;}
</style>
<script>
function save(){
	$('#myForm').form('submit',{
		url : '${ctx}/tree/save',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				window.parent.parent.tree.addNodes([result.data]);
				window.location.href = '${ctx}/tree/content?id=${tree.parentId}';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="tree" enctype="multipart/form-data">
		<sf:hidden path="parentId" value="${tree.parentId}"/>
		<div class="easyui-panel" title="操作：添加节点" style="padding:30px 60px;">
			<div style="margin-bottom:20px">			
			 	<sf:input path="name" size="35" class="easyui-textbox" 
			 	data-options="label:'名 称:',required:true"/><br>
			 		<font color="red">${error}</font><br><br>
			 	<sf:input path="remark" size="35" class="easyui-textbox" 
			 		data-options="label:'备 注:'"/><br><br>
			 	<div class='div-group'><label for='isGroup'>是否为电池组节点</label>
			 		<input type="checkbox" id='isGroup' name='isGroup' value='true'></div><br>
            	<a href="javascript:void(0)" class="easyui-linkbutton" style="width:80px"
            		data-options="iconCls:'icon-save'" onclick="save()">提 交</a>
            	<a href="${ctx}/tree/content?id=${tree.parentId}" class="easyui-linkbutton" 
        			data-options="iconCls:'icon-reload'">返 回</a>
	 		</div>
		</div>
	</sf:form>
</body>
</html>