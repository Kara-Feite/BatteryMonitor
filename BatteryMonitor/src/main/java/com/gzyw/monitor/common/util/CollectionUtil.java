package com.gzyw.monitor.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * @ClassName: CollectionUtil
 * @Description: 集合工具类
 * @author LW
 * @date 2018年6月14日 下午4:43:06
 */
public class CollectionUtil {

	/**
	 * 集合默认大小
	 */
	private static final int DEFAULT_SIZE = 1000;

	/**
	 * 
	 * @Title: isEmpty
	 * @Description: 判断集合是否为空
	 * @param coll
	 * @return
	 */
	public static boolean isEmpty(final Collection<?> coll) {
		return coll == null || coll.isEmpty();
	}

	/**
	 * 
	 * @Title: isNotEmpty
	 * @Description: 判断集合不为空
	 * @param coll
	 * @return
	 */
	public static boolean isNotEmpty(final Collection<?> coll) {
		return !isEmpty(coll);
	}

	/**
	 * 返回集合的size越小,此方法性能越高
	 * @Title: fastSplitList
	 * @Description: 拆分List为固定大小的多个集合
	 * @param baseList
	 * @param size
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<List<T>> fastSplitList(List<T> baseList, int size) {
		if (baseList == null || baseList.size() == 0) {
			return null;
		}
		if (size <= 0) {
			size = DEFAULT_SIZE;
		}
		int arrSize = baseList.size() % size == 0 ? baseList.size() / size : baseList.size() / size + 1;
		List<List<T>> resultList = new ArrayList<List<T>>();
		for (int i = 0; i < arrSize; i++) {
			if (arrSize - 1 == i) {
				resultList.add((List<T>) new ArrayList<Object>(baseList.subList(i * size, baseList.size())));
			} else {
				resultList.add((List<T>) new ArrayList<Object>(baseList.subList(i * size, size * (i + 1))));
			}
		}
		return resultList;
	}

	/**
	 * 返回集合的size越小,此方法性能越高
	 * @Title: splitList
	 * @Description: 拆分List为固定大小的多个集合
	 * @param baseList
	 * @param size
	 * @return
	 */
	public static <T> List<List<T>> splitList(List<T> baseList, int size) {
		if (baseList == null || baseList.size() == 0) {
			return null;
		}
		if (size <= 0) {
			size = DEFAULT_SIZE;
		}
		List<List<T>> resultList = new ArrayList<List<T>>();
		for (int i = 0; i < baseList.size(); ++i) {
			if (i % size == 0) {
				List<T> result = new ArrayList<T>();
				resultList.add(result);
			}
			resultList.get(i / size).add(baseList.get(i));
		}
		return resultList;
	}

	/**
	 * 
	 * @Title: asList
	 * @Description: 集合转List
	 * @param coll
	 * @return
	 */
	public static <V> List<V> asList(final java.util.Collection<V> coll) {
		if (coll != null && coll.isEmpty()) {
			return new ArrayList<V>(0);
		}
		final List<V> list = new ArrayList<V>();
		for (final V value : coll) {
			if (value != null) {
				list.add(value);
			}
		}
		return list;
	}

	/**
	 * 
	 * @Title: join
	 * @Description: 指定字符拼接
	 * @param collection
	 * @param joinStr
	 * @return
	 */
	public static final String join(final Collection<?> collection, final String joinStr) {
		if (isEmpty(collection)) {
			return StringUtil.EMPTY;
		}
		if (joinStr == null) {
			throw new IllegalArgumentException("join string is null.");
		}
		Object[] arr = ArrayUtil.toArray(collection);
		return ArrayUtil.join(arr, joinStr);
	}
	
	/**
	 * 
	 * @Title: join
	 * @Description: 默认","拼接
	 * @param collection
	 * @return
	 */
	public static final String join(final Collection<?> collection) {
		if (isEmpty(collection)) {
			return StringUtil.EMPTY;
		}
		Object[] arr = ArrayUtil.toArray(collection);
		return ArrayUtil.join(arr);
	}

}