package com.gzyw.monitor.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.dao.GroupDao;
import com.gzyw.monitor.dao.NormDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.NormDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Norm;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.NormService;

/**
 * 
 * @ClassName: NormServiceImpl
 * @Description: 电池规格服务层实现类
 * @author LW
 * @date 2018年6月13日 上午9:30:44
 */
@Service
public class NormServiceImpl implements NormService {
	
	@Autowired
	private NormDao normDao;
	
	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private TreeDao treeDao;
	
	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Norm norm) {
		Long count = normDao.countNorm(norm);
		List<Norm> listNorm = normDao.listNormByPage(page, rows, sort, order, norm);
		List<NormDto> listNormDto = new ArrayList<NormDto>();
		for (Norm n : listNorm) {
			/*Group group = groupDao.getGroupByNorm(norm.getId());
			Integer groupId = group.getGroupId();*/
			Integer groupId = n.getGroup().getGroupId();
			Tree tree = treeDao.get(groupId);
			Tree parentTree = treeDao.getParentTreeById(groupId);
			NormDto normDto = new NormDto(n, parentTree.getName() + ">" + tree.getName());
			listNormDto.add(normDto);
		}
		return new EUDataGridDto(count, listNormDto);
	}

	@Override
	public ResultDto delNormByBat(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的规格");
		}
		for (int id : ids) {
			Norm norm = normDao.get(id);
			if (norm != null) {			
				normDao.delete(norm);														
			}
		}
		return ResultDto.build(200, "删除规格成功！");
	}

	@Override
	public Norm getNormById(Integer id) {
		return normDao.get(id);
	}

	@Override
	public List<Norm> listNormByAll() {
		return normDao.listNormByAll();
	}

	@Override
	public ResultDto saveNorm(Norm norm) {
		try {
			Date date = new Date();
			norm.setUpdateTime(date);
			normDao.save(norm);
			return ResultDto.ok();
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，添加规格失败，请联系管理员");
		}
	}

	@Override
	public ResultDto updateNorm(Norm norm) {
		try {
			norm.setUpdateTime(new Date());
			normDao.update(norm);
			return ResultDto.ok();
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，更新规格失败，请联系管理员");
		}
	}

	@Override
	public List<String> listTypeAll() {
		List<String> listType = normDao.listTypeAll();
		if (CollectionUtil.isNotEmpty(listType)) {
			listType = new ArrayList<String>(new LinkedHashSet<String>(listType));
		}
		return listType;
	}

	@Override
	public List<String> listMakerAll() {
		List<String> listMaker = normDao.listMakerAll();
		if (CollectionUtil.isNotEmpty(listMaker)) {
			listMaker = new ArrayList<String>(new LinkedHashSet<String>(listMaker));
		}
		return listMaker;
	}

	@Override
	public List<String> listModelAll() {
		List<String> listModel = normDao.listModelAll();
		if (CollectionUtil.isNotEmpty(listModel)) {
			listModel = new ArrayList<String>(new LinkedHashSet<String>(listModel));
		}
		return listModel;
	}

	@Override
	public Norm getNormByGroupId(Integer treeId) {
		return groupDao.get(treeId).getNorm();
	}
	
}
