package com.gzyw.monitor.dao;

import com.gzyw.monitor.entity.GroupLimit;

/**
 * 
 * @ClassName: GroupLimitDao
 * @Description: 组限制持久层（参数设置）
 * @author LW
 * @date 2018年6月29日 上午11:02:19
 */
public interface GroupLimitDao extends BaseDao<GroupLimit> {

}
