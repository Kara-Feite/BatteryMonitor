package com.gzyw.monitor.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.dao.AlarmDao;
import com.gzyw.monitor.dao.BatteryDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;
import com.gzyw.monitor.entity.Battery;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.AlarmService;

/**
 * 
 * @ClassName: AlarmServiceImpl
 * @Description: 警告服务层接口实现类
 * @author LW
 * @date 2018年6月20日 下午2:11:06
 */
@Service
public class AlarmServiceImpl implements AlarmService {
	
	/**
	 * 警告持久层接口
	 */
	@Autowired
	private AlarmDao alarmDao;
	
	@Autowired
	private TreeDao treeDao;
	
	@Autowired
	private BatteryDao batteryDao;

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, TempDto temp) {
		StringBuilder sb = new StringBuilder();
		recurGroupIds(temp.getTreeId(), sb);
		String ids = sb.substring(0, sb.length() - 1).toString();
		Long count = alarmDao.countAlarm(temp, ids);
		List<Alarm> listAlarm = alarmDao.listAlarmByPage(page, rows, sort, order, temp, ids);
		return new EUDataGridDto(count, listAlarm);
	}
	
	/**
	 * 
	 * @Title: recurGroupIds
	 * @Description: 递归获取最后的电池组id
	 * @param pid
	 * @param sb
	 */
	private void recurGroupIds(int pid, StringBuilder sb) {
		List<Tree> listTree = treeDao.listTreeByPid(pid);
		if (CollectionUtil.isEmpty(listTree)) {
			sb.append(pid + ",");
		} else {
			for (Tree tree : listTree) {
				recurGroupIds(tree.getId(), sb);
			}
		}
	}

	@Override
	public Alarm getAlarmById(Integer id) {
		return alarmDao.get(id);
	}

	@Override
	public ResultDto updateAlarm(Alarm alarm) {
		if (alarmDao.updateAlarm(alarm) > 0) {
			if (alarm.getIsDeal()) {
				int groupId = alarm.getGroup().getGroupId();
				Tree tree = treeDao.get(groupId);
				boolean isWarn = alarm.getState().equals("告警");
				int oldNum = isWarn ? tree.getWarn() : tree.getPlan();
				int newNum = oldNum - 1;
				updateAlarmByNum(oldNum, newNum, groupId, isWarn);
				if (isWarn) {
					tree.setWarn(newNum);
				} else {
					tree.setPlan(newNum);
				}
				treeDao.update(tree);
				String[] names = alarm.getName().split(">");
				if (names == null || names.length < 2) {
					return ResultDto.ok();
				}
				Battery battery = batteryDao.getBatteryByAlarm(groupId, names[1]);
				if (isWarn) {
					battery.setWarn(newNum);
				} else {
					battery.setPlan(newNum);
				}
				batteryDao.update(battery);
			}
			return ResultDto.ok();
		}
		return ResultDto.build(500, "更新失败");
	}

	@Override
	public ResultDto delAlarmByBat(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的警告");
		}
		for (int id : ids) {
			Alarm alarm = alarmDao.get(id);
			if (alarm != null) {
				int groupId = alarm.getGroup().getGroupId();
				Tree tree = treeDao.get(groupId);
				boolean isWarn = alarm.getState().equals("告警");
				int oldNum = isWarn ? tree.getWarn() : tree.getPlan();
				int newNum = oldNum - 1;
				updateAlarmByNum(oldNum, newNum, groupId, isWarn);
				if (isWarn) {
					tree.setWarn(newNum);
				} else {
					tree.setPlan(newNum);
				}
				treeDao.update(tree);
				String[] names = alarm.getName().split(">");
				if (names == null || names.length < 2) {
					return ResultDto.ok();
				}
				Battery battery = batteryDao.getBatteryByAlarm(groupId, names[1]);
				if (isWarn) {
					battery.setWarn(newNum);
				} else {
					battery.setPlan(newNum);
				}
				batteryDao.update(battery);
				alarmDao.delete(alarm);
			}
		}
		return ResultDto.build(200, "删除警告成功！");
	}
	
	/**
	 * 
	 * @Title: updateAlarmByNum
	 * @Description: 更新警告数量
	 * @param oldNum
	 * @param newNum
	 * @param id
	 * @param isWarn
	 */
	private void updateAlarmByNum(int oldNum, int newNum, int id, boolean isWarn) {
		Tree tree = treeDao.getParentTreeById(id);
		if (tree != null) {
			if (isWarn) {
				tree.setWarn(tree.getWarn() - oldNum + newNum);
			} else {
				tree.setPlan(tree.getPlan() - oldNum + newNum);
			}
			treeDao.update(tree);
			updateAlarmByNum(oldNum, newNum, tree.getId(), isWarn);
		}
	}
	
}
