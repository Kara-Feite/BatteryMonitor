package com.gzyw.monitor.dto;

import java.util.Date;

import com.gzyw.monitor.annotation.ExcelColumn;
import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: TestDataDto
 * @Description: 测试数据包装类
 * @author LW
 * @date 2018年9月7日 上午10:51:56
 */
public class TestDataDto {
	
	/**
	 * ID
	 */
	private Integer id;
	
	/**
	 * 电池名
	 */
	@ExcelColumn(row = -1, column = 0, value = "电池编号")
	private String batteryName;

	/**
	 * 电压
	 */
	@ExcelColumn(row = -1, column = 1, value = "单体电压/V")
	private Double voltage;

	/**
	 * 电阻
	 */
	@ExcelColumn(row = -1, column = 2, value = "单体内阻/mΩ")
	private Double inter;

	/**
	 * 温度
	 */
	@ExcelColumn(row = -1, column = 3, value = "温度/℃")
	private Double tempera;

	/**
	 * 测试时间
	 */
	@ExcelColumn(row = -1, column = 4, value = "测试时间")
	private Date testTime;

	/**
	 * @Title: TestDataDto
	 * @Description: TestDataDto构造函数
	 * @param batteryName
	 * @param voltage
	 * @param inter
	 * @param tempera
	 * @param testTime
	 */
	public TestDataDto(TestData testData) {
		this.batteryName = testData.getBatteryName();
		this.voltage = testData.getVoltage();
		this.inter = testData.getInter();
		this.tempera = testData.getTempera();
		this.testTime = testData.getTestTime();
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getBatteryName
	 * @Description: 获取batteryName
	 * @return: String batteryName
	 */
	public String getBatteryName() {
		return batteryName;
	}

	/**
	 * @Title: setBatteryName
	 * @Description: 设置batteryName
	 */
	public void setBatteryName(String batteryName) {
		this.batteryName = batteryName;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: Double voltage
	 */
	public Double getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getInter
	 * @Description: 获取inter
	 * @return: Double inter
	 */
	public Double getInter() {
		return inter;
	}

	/**
	 * @Title: setInter
	 * @Description: 设置inter
	 */
	public void setInter(Double inter) {
		this.inter = inter;
	}

	/**
	 * @Title: getTempera
	 * @Description: 获取tempera
	 * @return: Double tempera
	 */
	public Double getTempera() {
		return tempera;
	}

	/**
	 * @Title: setTempera
	 * @Description: 设置tempera
	 */
	public void setTempera(Double tempera) {
		this.tempera = tempera;
	}

	/**
	 * @Title: getTestTime
	 * @Description: 获取testTime
	 * @return: Date testTime
	 */
	public Date getTestTime() {
		return testTime;
	}

	/**
	 * @Title: setTestTime
	 * @Description: 设置testTime
	 */
	public void setTestTime(Date testTime) {
		this.testTime = testTime;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "TestDataDto [id=" + id + ", batteryName=" + batteryName + ", voltage=" + voltage + ", inter=" + inter
				+ ", tempera=" + tempera + ", testTime=" + testTime + "]";
	}
	
}
