package com.gzyw.monitor.common.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: DataUtil
 * @Description: 监测数据处理工具类
 * @author LW
 * @date 2018年6月29日 上午8:49:15
 */
public class DataUtil {

	/**
	 * 进行crc校验和每隔两位拆分数据 原数据
	 * 01031e5d030013098f000f0000000100000001c5340068869f0422011000000000f4b0 拆分后
	 * [01,03,1e,...]
	 * 
	 * @Title: toDataArr
	 * @Description: 拆解数据获得数组
	 * @param data
	 * @return
	 */
	public static String[] toDataArr(String data) {
		data = StringUtil.toTwoSpaceStr(data);
		String[] arr = data.split("\\s+");
		// 01(装置地址) 03（功能码） 28（数据长度）接受数据前三位长度固定不变
		int len = NumberUtil.hexToDec(arr[2]);
		// crc校验，成功保存数据，否则放弃返回
		try {
			String crcCode = CRCUtil.getCrc16(StringUtil.toBytes(StringUtil.trimAll(data).substring(0, len * 2 + 6)));
			if (!arr[len + 3].toLowerCase().equals(crcCode.substring(0, 2).toLowerCase())
					&& !arr[len + 4].toLowerCase().equals(crcCode.substring(2, 4).toLowerCase())) {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arr;
	}

	/**
	 * 
	 * @Title: toDataMap
	 * @Description: 将各种数据转换为Excel对应的数据
	 * @param headerName
	 * @param object
	 * @param map
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static void toDataMap(String headerName, Object object,
			LinkedHashMap<String, List<Map<Integer, ?>>> map) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<? extends Object> clazz = null;
		List<Map<Integer, ?>> listData = new ArrayList<Map<Integer, ?>>();
		HashMap<Integer, Object> mapData = null;
		if (object instanceof Object[]) {
			Object[] objs = (Object[]) object;
			Map<Integer, List<ExcelColumn>> mapRow = new HashMap<>();
			for (Object obj : objs) {
				clazz = obj.getClass();
				Field[] fields = clazz.getDeclaredFields();
				List<ExcelColumn> listColumn = null;
				for (Field field : fields) {
					ExcelColumn excelColumn = field.getAnnotation(ExcelColumn.class);
					if (null != excelColumn) {
						int row = excelColumn.row();
						if (mapRow.containsKey(row)) {
							listColumn = mapRow.get(row);
						} else {
							listColumn = new ArrayList<ExcelColumn>();
						}
						String methodName = "get" + StringUtil.upperFirstCase(field.getName());
						Method method = clazz.getDeclaredMethod(methodName);
						Object val = method.invoke(obj);
						ExcelColumn excel = new ExcelColumn() {
							@Override
							public Class<? extends Annotation> annotationType() {
								return null;
							}
							@Override
							public String value() {
								return val != null ? val.toString() : null;
							}
							@Override
							public int row() {
								return row;
							}
							@Override
							public int column() {
								return excelColumn.column() + 1;
							}
						};
						listColumn.add(excelColumn);
						listColumn.add(excel);
						mapRow.put(row, listColumn);
					}
				}
			}
			for (Entry<Integer, List<ExcelColumn>> row : mapRow.entrySet()) {
				List<ExcelColumn> value = row.getValue();
				mapData = new HashMap<>();
				for (ExcelColumn excelColumn : value) {
					mapData.put(excelColumn.column(), excelColumn.value());
				}
				listData.add(mapData);
			}
		}
		if (object instanceof List) {
			int i = 0;
			List<?> list = (List<?>) object;
			HashMap<Integer, Object> mapHeader = new HashMap<>();
			for (Object obj : list) {
				clazz = obj.getClass();
				Field[] fields = clazz.getDeclaredFields();
				ExcelColumn excelColumn = null;
				mapData = new HashMap<>();
				for (Field field : fields) {
					excelColumn = field.getAnnotation(ExcelColumn.class);
					if (null != excelColumn) {
						int column = excelColumn.column();
						if (i == 0) {
							mapHeader.put(column, excelColumn.value());
						}
						String methodName = "get" + StringUtil.upperFirstCase(field.getName());
						Method method = clazz.getDeclaredMethod(methodName);
						Object val = method.invoke(obj);
						mapData.put(column, val);
					}
				}
				if (i == 0) {
					listData.add(mapHeader);
					i++;
				}
				listData.add(mapData);
			}
		}
		if (object instanceof String) {
			String str = (String) object;
			if (StringUtil.isNotEmpty(str)) {
				str = str.replaceAll(" ", "+");
				String[] strUrl = str.split("base64,");
				byte[] strBuffer = Base64.decodeBase64(strUrl[1]);
				mapData = new HashMap<>();
				mapData.put(3, strBuffer);
				listData.add(mapData);
			}
		}
		if (object instanceof byte[]) {
			byte[] buf = (byte[]) object;
			mapData = new HashMap<>();
			mapData.put(1, buf);
			listData.add(mapData);
		}
		map.put(headerName, listData);
	}

}
