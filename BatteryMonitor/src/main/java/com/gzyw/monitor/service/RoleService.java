package com.gzyw.monitor.service;

import java.io.Serializable;
import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToMenu;
import com.gzyw.monitor.entity.RoleToTree;
import com.gzyw.monitor.entity.RoleToUser;

/**
 * 
 * @ClassName: RoleService
 * @Description: 角色服务层
 * @author LW
 * @date 2018年7月8日 下午2:41:00
 */
public interface RoleService {

	/**
	 * 
	 * @Title: getRoleById
	 * @Description: 通过角色id查询角色
	 * @param id
	 * @return
	 */
	Role getRoleById(Serializable id);

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取角色列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或降序
	 * @param role 角色对象
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Role role);

	/**
	 * 
	 * @Title: saveRole
	 * @Description: 添加角色
	 * @param role
	 * @param menuIds
	 * @param treeIds
	 * @return
	 */
	ResultDto saveRole(Role role, String menuIds, String treeIds);

	/**
	 * 
	 * @Title: listMenuById
	 * @Description: 根据角色获取菜单
	 * @param id
	 * @return
	 */
	List<RoleToMenu> listMenuById(Integer id);
	
	/**
	 * 
	 * @Title: listTreeById
	 * @Description: 根据角色id获取所以对于的基站节点
	 * @param id
	 * @return
	 */
	List<RoleToTree> listTreeById(Integer id);

	/**
	 * 
	 * @Title: updateRole
	 * @Description: 更新角色
	 * @param role
	 * @param menuIds
	 * @param treeIds
	 * @return
	 */
	ResultDto updateRole(Role role, String menuIds, String treeIds);

	/**
	 * 
	 * @Title: deleteRole
	 * @Description: 删除角色
	 * @param ids
	 * @return
	 */
	ResultDto deleteRole(int[] ids);

	/**
	 * 
	 * @Title: updateState
	 * @Description: 更新角色状态，激活或者禁用
	 * @param ids
	 * @param b
	 * @return
	 */
	ResultDto updateState(int[] ids, boolean b);

	/**
	 * 
	 * @Title: listRoleByAll
	 * @Description: 获取所有角色
	 * @return
	 */
	List<Role> listRoleByAll();
	
	/**
	 * 
	 * @Title: listRoleToUser
	 * @Description: 更加用户id获取对应的角色关系
	 * @param userId
	 * @return
	 */
	List<RoleToUser> listRoleToUser(Integer userId);

}
