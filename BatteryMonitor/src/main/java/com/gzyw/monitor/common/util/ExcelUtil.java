package com.gzyw.monitor.common.util;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * 
 * @ClassName: ExcelUtil
 * @Description: Excel 导入/导出
 * @author LW
 * @date 2018年8月7日 下午2:11:54
 */
public class ExcelUtil <T> {
	
	/**
	 * 
	 * @Title: exportHSExcelByColumn
	 * @Description: 导出Excel
	 * @param title 标题
	 * @param map 每段数据，没有标题就填""
	 * @param columnWidths 每列宽度
	 * @param column  总列数
	 */
	@SuppressWarnings("unused")
	public static Workbook exportHSExcel(String title, LinkedHashMap<String, List<Map<Integer, ?>>> map,
			Map<Integer, Integer> columnWidths, int column) {
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(title);
		// 设置默认宽高
		sheet.setDefaultColumnWidth(8);
		sheet.setDefaultRowHeightInPoints((float) 18.6);
		// 设置列样式
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.AUTOMATIC.getIndex());
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		// 设置字体
		Font font = workbook.createFont();
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setFontHeightInPoints((short) 12);
		font.setFontName("宋体");
		style.setFont(font);

		/*
		 * sheet.setColumnWidth(0, 22 * 256); sheet.setDefaultColumnStyle(0, style);
		 */
		// 设置每列宽度
		if (columnWidths != null) {
			for (Entry<Integer, Integer> entry : columnWidths.entrySet()) {
				sheet.setColumnWidth(entry.getKey(), entry.getValue());
			}
		}
		// 设置每列的默认样式
		for (int i = 0; i < column; i++) {
			sheet.setDefaultColumnStyle(i, style);
		}
		// 设置标题
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, column));
		Row rowTitle = sheet.createRow(0);
		Cell cellTitle = rowTitle.createCell(0);
		cellTitle.setCellValue(title);
		cellTitle.setCellStyle(style);
		// 设置表格数据
		int i = 1;
		Row row = null;
		Row rowLine = null;
		Row rowHead = null;
		Cell cell = null;
		Cell cellHead = null;
		// 声明一个画图的顶级管理器
		Drawing<?> patriarch = sheet.createDrawingPatriarch();
		for (Entry<String, List<Map<Integer, ?>>> entry : map.entrySet()) {
			// 每段数据前都空一行
			rowLine = sheet.createRow(i++);
			String key = entry.getKey();
			if (NumberUtil.isNotNumber(key)) {
				sheet.addMergedRegion(new CellRangeAddress(i, i, 0, column - 1));
				rowHead = sheet.createRow(i++);
				cellHead = rowHead.createCell(0);
				cellHead.setCellValue(key);
				cellHead.setCellStyle(style);
			}
			List<Map<Integer, ?>> data = entry.getValue();
			for (Map<Integer, ?> mapData : data) {
				row = sheet.createRow(i++);
				for (Entry<Integer, ?> entry2 : mapData.entrySet()) {
					Integer columnNo = entry2.getKey();
					Object obj = entry2.getValue();
					// 判断是否是图片
					if (obj instanceof byte[]) {
						// 设置行高为360px;
						row.setHeightInPoints(360);
						// 设置图片所在列宽度为860px
						//sheet.setColumnWidth(3, (short) (35.7 * 860));
						byte[] bsValue = (byte[]) obj;
						int j = i - 1;
						sheet.addMergedRegion(new CellRangeAddress(j, j, 0, column - 1));
						ClientAnchor anchor = new HSSFClientAnchor(250, 12, 0, 236, (short) 0, j, (short) (column - 1), j);
						anchor.setAnchorType(AnchorType.MOVE_DONT_RESIZE);
						patriarch.createPicture(anchor, workbook.addPicture(bsValue, HSSFWorkbook.PICTURE_TYPE_PNG));
						continue;
					}
					if (obj instanceof String) {
						String val = obj.toString();
						cell = row.createCell(columnNo);
						if (NumberUtil.isDouble(val)) {
							cell.setCellValue(Double.parseDouble(val));
						} else if (NumberUtil.isInteger(val)) {
							cell.setCellValue(Integer.parseInt(val));
						} else {
							cell.setCellValue(val);
						}
						cell.setCellStyle(style);
						continue;
					}
					if (obj instanceof Double) {
						Double val = (Double) obj;
						cell = row.createCell(columnNo);
						cell.setCellStyle(style);
						cell.setCellValue(val);
						continue;
					}
					if (obj instanceof Integer) {
						Integer val = (Integer) obj;
						cell = row.createCell(columnNo);
						cell.setCellStyle(style);
						cell.setCellValue(val);
						continue;
					}
					if (obj instanceof Date) {
						Date val = (Date) obj;
						String time = DateUtil.formatDateTime(val);
						cell = row.createCell(columnNo);
						cell.setCellStyle(style);
						cell.setCellValue(time);
						continue;
					}
				}
			}
		}
		return workbook;
	}

}
