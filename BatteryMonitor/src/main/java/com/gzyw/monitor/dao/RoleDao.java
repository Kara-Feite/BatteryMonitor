package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToMenu;
import com.gzyw.monitor.entity.RoleToTree;
import com.gzyw.monitor.entity.RoleToUser;

/**
 * 
 * @ClassName: RoleDao
 * @Description: 角色持久层
 * @author LW
 * @date 2018年7月8日 下午2:44:47
 */
public interface RoleDao extends BaseDao<Role> {

	/**
	 * 
	 * @Title: countRole
	 * @Description: 统计角色总数
	 * @param role
	 * @return
	 */
	Long countRole(Role role);

	/**
	 * 
	 * @Title: listRoleByPage
	 * @Description: 角色列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或降序
	 * @param role 角色对象
	 * @return
	 */
	List<Role> listRoleByPage(Integer page, Integer rows, String sort, String order, Role role);

	/**
	 * 
	 * @Title: saveRoleToMenu
	 * @Description: 添加角色菜单授权
	 * @param id
	 * @param ids
	 */
	void saveRoleToMenu(Integer id, String[] ids);

	/**
	 * 
	 * @Title: saveRoleToTree
	 * @Description: 添加角色基站授权
	 * @param id
	 * @param ids
	 */
	void saveRoleToTree(Integer id, String[] ids);

	/**
	 * 
	 * @Title: listMenuById
	 * @Description: 根据角色获取对应菜单
	 * @param id
	 * @return
	 */
	List<RoleToMenu> listMenuById(Integer id);

	/**
	 * 
	 * @Title: listTreeById
	 * @Description: 根据角色获取基站节点
	 * @param id
	 * @return
	 */
	List<RoleToTree> listTreeById(Integer id);
	
	/**
	 * 
	 * @Title: listUserById
	 * @Description: 根据角色获取用户节点
	 * @param id
	 * @return
	 */
	List<RoleToUser> listUserById(Integer id);

	/**
	 * 
	 * @Title: updateRole
	 * @Description: 更新用户
	 * @param role
	 */
	void updateRole(Role role);

	/**
	 * 
	 * @Title: deleteRoleToMenu
	 * @Description: 删除角色菜单对应关系
	 * @param id
	 */
	void deleteRoleToMenu(Integer id);

	/**
	 * 
	 * @Title: deleteRoleToTree
	 * @Description: 删除角色基站对应关系
	 * @param id
	 */
	void deleteRoleToTree(Integer id);
	
	/**
	 * 
	 * @Title: deleteRoleToUser
	 * @Description: 根据用户id, 删除角色用户对应关系
	 * @param userId
	 */
	void deleteRoleToUser(Integer userId);

	/**
	 * 
	 * @Title: listRoleByAll
	 * @Description: 获取所有角色
	 * @return
	 */
	List<Role> listRoleByAll();
	
	/**
	 * 
	 * @Title: listRoleToUser
	 * @Description: 根据用户id获取
	 * @param userId
	 * @return
	 */
	List<RoleToUser> listRoleToUser(Integer userId);

	/**
	 * 
	 * @Title: saveRoleToUser
	 * @Description: 为用户添加的角色
	 * @param userId
	 * @param listRole
	 */
	void saveRoleToUser(Integer userId, int[] listRole);

}
