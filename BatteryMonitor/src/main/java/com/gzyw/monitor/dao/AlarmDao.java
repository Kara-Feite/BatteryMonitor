package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;

/**
 * 
 * @ClassName: AlarmDao
 * @Description: 警告持久层
 * @author LW
 * @date 2018年6月20日 下午1:41:57
 */
public interface AlarmDao extends BaseDao<Alarm> {

	/**
	 * 
	 * @Title: countAlarm
	 * @Description: 获取警告总数
	 * @param temp
	 * @param ids 
	 * @return
	 */
	Long countAlarm(TempDto temp, String ids);

	/**
	 * 
	 * @Title: listAlarmByPage
	 * @Description: 获取警告分页数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param temp 临时对象
	 * @param ids 
	 * @return
	 */
	List<Alarm> listAlarmByPage(Integer page, Integer rows, String sort, String order, TempDto temp, String ids);

	/**
	 * 
	 * @Title: updateAlarm
	 * @Description: 更新警告
	 * @param alarm
	 */
	Integer updateAlarm(Alarm alarm);

	/**
	 * 
	 * @Title: getAlarmByExists
	 * @Description: 获取一个存在没有被处理的警告
	 * @param alarm
	 * @param groupId
	 * @return
	 */
	Alarm getAlarmByExists(Alarm alarm, Integer groupId);

}
