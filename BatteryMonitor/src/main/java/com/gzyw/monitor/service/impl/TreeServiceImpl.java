package com.gzyw.monitor.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.RoleDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: TreeServiceImpl
 * @Description: 树形节点服务层接口实现类
 * @author LW
 * @date 2018年6月7日 下午3:37:34
 */
@Service
public class TreeServiceImpl implements TreeService {
	
	/**
	 * 树形节点持久层接口
	 */
	@Autowired
	private TreeDao treeDao;
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public List<Tree> listTreeByAll() {
		return treeDao.listTreeByAll();
	}

	@Override
	public List<Tree> listTreeByRoot(Integer userId) {
		return treeDao.listTreeByRoot(userId, null, null, null);
	}

	@Override
	public EUDataGridDto getDataGridByRoot(Integer page, Integer rows, String sort, String order, Tree tree, Integer userId) {
		long count = treeDao.countTreeByRoot(userId);
		List<Tree> listTree = treeDao.listTreeByRoot(userId, tree, sort, order);
		listTree = new ArrayList<Tree>(new LinkedHashSet<Tree>(listTree));
		return new EUDataGridDto(count, listTree);
	}
	
	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Tree tree) {
		long count = treeDao.countTree(tree);
		List<Tree> listTree = treeDao.listTreeByPage(page, rows, sort, order, tree);
		return new EUDataGridDto(count, listTree);
	}

	@Override
	public Tree getTreeById(Integer id) {
		return treeDao.get(id);
	}

	@Override
	public ResultDto saveTreeByRoot(Tree tree) {
		Integer baseId = treeDao.getAutoId("bm_tree");
		Date date = new Date();
		tree.setBaseId(baseId);
		tree.setParentId(0);
		tree.setCreateTime(date);
		tree.setUpdateTime(date);
		tree.setUrl("../tree/frame?id=");
		try {
			treeDao.save(tree);
			String[] ids = {tree.getId().toString()};
			roleDao.saveRoleToTree(1, ids);
			return ResultDto.ok(tree);
		} catch (Exception e) {
			return ResultDto.build(500, "出现未知错误，添加失败！");
		}
		
	}

	@Override
	public ResultDto updateTreeByRoot(Tree tree) {
		tree.setUpdateTime(new Date());
		try {
			treeDao.updateTreeByRoot(tree);
			return ResultDto.ok(tree);
		} catch (Exception e) {
			return ResultDto.build(500, "出现未知错误，更新失败！");
		}
	}
	
	@Override
	public ResultDto saveTree(Tree tree) {
		Date date = new Date();
		Integer pid = tree.getParentId();
		Tree treeParent = treeDao.get(pid);
		tree.setBaseId(treeParent.getBaseId());
		tree.setParentId(pid);
		tree.setCreateTime(date);
		tree.setUpdateTime(date);
		if (StringUtil.isEmpty(tree.getUrl())) {
			tree.setUrl("../tree/home?id=");
		}
		try {
			treeDao.save(tree);
			String[] ids = {tree.getId().toString()};
			roleDao.saveRoleToTree(1, ids);
			return ResultDto.ok(tree);
		} catch (Exception e) {
			return ResultDto.build(500, "添加节点失败！");
		}
	}

	@Override
	public ResultDto updateTree(Tree tree) {
		tree.setUpdateTime(new Date());
		if (treeDao.updateTree(tree) > 0) {
			return ResultDto.ok(tree);
		}
		return ResultDto.build(500, "更新节点失败！");
	}

	@Override
	public ResultDto updateTreeByState(int[] ids, Boolean state) {
		if (ids == null || ids.length < 1) {
			return ResultDto.build(500, "请选中要更改的节点");
		}
		for (int id : ids) {
			treeDao.updateTreeByState(id, state);
			updateState(id, state);
		}
		return ResultDto.ok(ids);
	}
	
	@Override
	public List<Tree> listTreeByPid(Integer id) {
		return treeDao.listTreeByPid(id);
	}
	
	@Override
	public List<Tree> listTreeByUserId(Integer userId) {
		List<Tree> listTree = treeDao.listTreeByUserId(userId);
		listTree = new ArrayList<Tree>(new LinkedHashSet<Tree>(listTree));
		return listTree;
	}

	/**
	 * 
	 * @Title: updateState
	 * @Description: 递归更新状态
	 * @param id
	 * @param state
	 * @return
	 */
	private Integer updateState(int id, boolean state) {
		List<Tree> listTree = listTreeByPid(id);
		if (listTree != null) {
			for (Tree tree : listTree) {
				treeDao.updateTreeByState(tree.getId(), state);
				updateState(tree.getId(), state);
			}
		}
		return id;
	}
	
}
