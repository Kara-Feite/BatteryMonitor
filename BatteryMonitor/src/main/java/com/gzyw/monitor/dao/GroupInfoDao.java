package com.gzyw.monitor.dao;

import com.gzyw.monitor.entity.GroupInfo;

/**
 * 
 * @ClassName: GroupInfoDao
 * @Description: 组信息持久层
 * @author LW
 * @date 2018年6月26日 下午4:40:51
 */
public interface GroupInfoDao extends BaseDao<GroupInfo> {

	/**
	 * 
	 * @Title: getGroupInfoByNow
	 * @Description: 获取最新电池组信息
	 * @param id
	 * @return
	 */
	GroupInfo getGroupInfoByNow(Integer id);

}
