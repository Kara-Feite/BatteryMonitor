package com.gzyw.monitor.control;

import java.nio.channels.SocketChannel;

public class Control {

    private SocketChannel server;

    private SocketChannel client;

    public SocketChannel getServer() {
        return server;
    }

    public void setServer(SocketChannel server) {
        this.server = server;
    }

    public SocketChannel getClient() {
        return client;
    }

    public void setClient(SocketChannel client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Control{" +
                "server=" + server +
                ", client=" + client +
                '}';
    }

}
