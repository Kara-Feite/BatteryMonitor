package com.gzyw.monitor.common.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 
 * @ClassName: CodeUtil
 * @Description: 验证码工具类
 * @author LW
 * @date 2018年6月6日 下午4:04:54
 */
public class CodeUtil {
	
	/**
	 * Random随机对象
	 */
	private static final Random RANDOM = new Random();
	
	/**
	 * 
	 * @Title: getCode
	 * @Description: 获取验证码随机数
	 * @return
	 */
	public static String getCode() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			sb.append(RANDOM.nextInt(10));
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @Title: createCodeImage
	 * @Description: 根据随机数生成图片
	 * @param code
	 * @return
	 */
	public static BufferedImage createCodeImage(String code) {
		// 在内存中创建图象
		int width = 55;
		int height = 20;
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		// 获取图形上下文
		Graphics g = image.getGraphics();

		// 设定背景色
		g.setColor(getRandColor(200, 250));
		g.fillRect(0, 0, width, height);

		//设定字体
		g.setFont(new Font("serif", Font.CENTER_BASELINE, 16));

		//画边框
		//g.setColor(new Color(1));
		//g.drawRect(0,0,width-1,height-1);

		// 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
		g.setColor(getRandColor(160, 200));
		for (int i = 0; i < 100; i++) {
			int x = RANDOM.nextInt(width);
			int y = RANDOM.nextInt(height);
			int xl = RANDOM.nextInt(12);
			int yl = RANDOM.nextInt(12);
			g.drawLine(x, y, x + xl, y + yl);
		}
		for (int i = 0; i < 4; i++) {
			String rand = code.substring(i, i + 1);
			// 将认证码显示到图象中
			g.setColor(new Color(20 + RANDOM.nextInt(110), 20 + RANDOM
					.nextInt(110), 20 + RANDOM.nextInt(110)));
			//调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
			g.drawString(rand, 13 * i + 6, 16);
		}
		// 图象生效
		g.dispose();
		return image;
	}

	/**
	 * 
	 * @Title: getRandColor
	 * @Description: 获取随机色
	 * @param fc
	 * @param bc
	 * @return
	 */
	public static Color getRandColor(int fc, int bc) {
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + RANDOM.nextInt(bc - fc);
		int g = fc + RANDOM.nextInt(bc - fc);
		int b = fc + RANDOM.nextInt(bc - fc);
		return new Color(r, g, b);
	}
	
}
