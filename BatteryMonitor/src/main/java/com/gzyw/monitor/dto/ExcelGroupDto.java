package com.gzyw.monitor.dto;

import java.text.DecimalFormat;

import com.gzyw.monitor.annotation.ExcelColumn;
import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.Norm;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: ExcelGroupDto
 * @Description: 电池组excel导出实体类
 * @author LW
 * @date 2018年9月7日 上午9:31:47
 */
public class ExcelGroupDto {
	
	/**
	 * 生产厂家
	 */
	@ExcelColumn(row = 1, column = 0, value = "生产厂家")
	private String maker;
	
	/**
	 * 电池组电压
	 */
	@ExcelColumn(row = 1, column = 2, value = "电池电压/V")
	private Double voltage;
	
	/**
	 * 电池数
	 */
	@ExcelColumn(row = 1, column = 4, value = "电池数")
	private Integer number;
	
	/**
	 * 电池类型
	 */
	@ExcelColumn(row = 2, column = 0, value = "电池类型")
	private String type;
	
	/**
	 * 内阻
	 */
	@ExcelColumn(row = 2, column = 2, value = "参考内阻/mΩ ")
	private Double inter;
	
	/**
	 * 投运时间
	 */
	@ExcelColumn(row = 2, column = 4, value = "投运日期")
	private String beginTime;
	
	/**
	 * 型号
	 */
	@ExcelColumn(row = 3, column = 0, value = "型号")
	private String model;
	
	/**
	 * 容量
	 */
	@ExcelColumn(row = 3, column = 2, value = "电池容量/Ah")
	private Double volume;
	
	/**
	 * 备注
	 */
	@ExcelColumn(row = 3, column = 4, value = "备注")
	private String remark;
	
	/**
	 * 最大电压
	 */
	@ExcelColumn(row = 4, column = 0, value = "最大值(电压/V)")
	private String maxVoltage;
	
	/**
	 * 最小电压
	 */
	@ExcelColumn(row = 4, column = 2, value = "最小值(电压/V)")
	private String minVoltage;
	
	/**
	 * 系统电压
	 */
	@ExcelColumn(row = 4, column = 4, value = "系统电压(电压/V)")
	private Double goupVoltage;
	
	/**
	 * 最大内阻
	 */
	@ExcelColumn(row = 5, column = 0, value = "最大值(内阻/mΩ)")
	private String maxInter;
	
	/**
	 * 平均电压
	 */
	@ExcelColumn(row = 5, column = 2, value = "平均电压(电压/V)")
	private String avgVoltage;
	
	/**
	 * 充放电电流
	 */
	@ExcelColumn(row = 5, column = 4, value = "充放电电流(电流/A)")
	private Double elecCurrent;
	
	/**
	 * 
	 * @Title: ExcelGroupDto
	 * @Description: ExcelGroupDto构造函数
	 * @param groupDto
	 * @param maxMinDto
	 * @param groupInfo
	 */
	public ExcelGroupDto(GroupDto groupDto, MaxMinDto maxMinDto, GroupInfo groupInfo) {
		Norm norm = groupDto.getGroup().getNorm();
		Tree tree = groupDto.getTree();
		DecimalFormat df = new DecimalFormat("#.000");
		this.maker = norm.getMaker();
		this.voltage = norm.getVoltage();
		this.number = tree.getNumber();
		this.type = norm.getType();
		this.inter = norm.getInter();
		this.beginTime = DateUtil.format(norm.getBeginTime(), "yyyy年MM月dd日");
		this.model = norm.getModel();
		this.volume = norm.getVolume();
		this.remark = tree.getRemark();
		if (maxMinDto != null) {
			this.maxVoltage = maxMinDto.getMaxVoltageNo() + " : " + maxMinDto.getMaxVoltage();
			this.minVoltage = maxMinDto.getMinVoltageNo() + " : " + maxMinDto.getMinVoltage();
			this.maxInter = maxMinDto.getMaxInterNo() + " : " + maxMinDto.getMaxInter();
			this.avgVoltage = tree.getName() + " : " + df.format(maxMinDto.getAvgVoltage());
		}
		if (groupInfo != null) {
			this.goupVoltage = groupInfo.getGoupVoltage();
			this.elecCurrent = groupInfo.getElecCurrent();
		}
	}

	/**
	 * @Title: getMaker
	 * @Description: 获取maker
	 * @return: String maker
	 */
	public String getMaker() {
		return maker;
	}

	/**
	 * @Title: setMaker
	 * @Description: 设置maker
	 */
	public void setMaker(String maker) {
		this.maker = maker;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: Double voltage
	 */
	public Double getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getNumber
	 * @Description: 获取number
	 * @return: Integer number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @Title: setNumber
	 * @Description: 设置number
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getInter
	 * @Description: 获取inter
	 * @return: Double inter
	 */
	public Double getInter() {
		return inter;
	}

	/**
	 * @Title: setInter
	 * @Description: 设置inter
	 */
	public void setInter(Double inter) {
		this.inter = inter;
	}

	/**
	 * @Title: getBeginTime
	 * @Description: 获取beginTime
	 * @return: String beginTime
	 */
	public String getBeginTime() {
		return beginTime;
	}

	/**
	 * @Title: setBeginTime
	 * @Description: 设置beginTime
	 */
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * @Title: getModel
	 * @Description: 获取model
	 * @return: String model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @Title: setModel
	 * @Description: 设置model
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @Title: getVolume
	 * @Description: 获取volume
	 * @return: Double volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @Title: setVolume
	 * @Description: 设置volume
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getMaxVoltage
	 * @Description: 获取maxVoltage
	 * @return: String maxVoltage
	 */
	public String getMaxVoltage() {
		return maxVoltage;
	}

	/**
	 * @Title: setMaxVoltage
	 * @Description: 设置maxVoltage
	 */
	public void setMaxVoltage(String maxVoltage) {
		this.maxVoltage = maxVoltage;
	}

	/**
	 * @Title: getMinVoltage
	 * @Description: 获取minVoltage
	 * @return: String minVoltage
	 */
	public String getMinVoltage() {
		return minVoltage;
	}

	/**
	 * @Title: setMinVoltage
	 * @Description: 设置minVoltage
	 */
	public void setMinVoltage(String minVoltage) {
		this.minVoltage = minVoltage;
	}

	/**
	 * @Title: getGoupVoltage
	 * @Description: 获取goupVoltage
	 * @return: Double goupVoltage
	 */
	public Double getGoupVoltage() {
		return goupVoltage;
	}

	/**
	 * @Title: setGoupVoltage
	 * @Description: 设置goupVoltage
	 */
	public void setGoupVoltage(Double goupVoltage) {
		this.goupVoltage = goupVoltage;
	}

	/**
	 * @Title: getMaxInter
	 * @Description: 获取maxInter
	 * @return: String maxInter
	 */
	public String getMaxInter() {
		return maxInter;
	}

	/**
	 * @Title: setMaxInter
	 * @Description: 设置maxInter
	 */
	public void setMaxInter(String maxInter) {
		this.maxInter = maxInter;
	}

	/**
	 * @Title: getAvgVoltage
	 * @Description: 获取avgVoltage
	 * @return: String avgVoltage
	 */
	public String getAvgVoltage() {
		return avgVoltage;
	}

	/**
	 * @Title: setAvgVoltage
	 * @Description: 设置avgVoltage
	 */
	public void setAvgVoltage(String avgVoltage) {
		this.avgVoltage = avgVoltage;
	}

	/**
	 * @Title: getElecCurrent
	 * @Description: 获取elecCurrent
	 * @return: Double elecCurrent
	 */
	public Double getElecCurrent() {
		return elecCurrent;
	}

	/**
	 * @Title: setElecCurrent
	 * @Description: 设置elecCurrent
	 */
	public void setElecCurrent(Double elecCurrent) {
		this.elecCurrent = elecCurrent;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "ExcelGroupDto [maker=" + maker + ", voltage=" + voltage + ", number=" + number + ", type=" + type
				+ ", inter=" + inter + ", beginTime=" + beginTime + ", model=" + model + ", volume=" + volume
				+ ", remark=" + remark + ", maxVoltage=" + maxVoltage + ", minVoltage=" + minVoltage + ", goupVoltage="
				+ goupVoltage + ", maxInter=" + maxInter + ", avgVoltage=" + avgVoltage + ", elecCurrent=" + elecCurrent
				+ "]";
	}

}
