package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @ClassName: Battery
 * @Description: 电池
 * @author LW
 * @date 2018年6月4日 上午9:43:49
 */
@Entity
@Table(name = "bm_battery")
public class Battery implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -4169410700150954685L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电池名称
	 */
	@Column(name = "name", length = 64, nullable = false)
	private String name;

	/**
	 * 警告
	 */
	@Column(name = "warn", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer warn;

	/**
	 * 预警
	 */
	@Column(name = "plan", nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer plan;

	/**
	 * 电池组id
	 */
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 测试数据
	 */
	@OneToMany(mappedBy = "battery")
	@JsonIgnore
	private Set<TestData> setTestData;

	/**
	 * @Title: Battery
	 * @Description: Battery构造函数
	 */
	public Battery() {}

	/**
	 * @Title: Battery
	 * @Description: Battery构造函数
	 * @param id
	 * @param name
	 * @param warn
	 * @param plan
	 * @param createTime
	 * @param updateTime
	 */
	public Battery(Integer id, String name, Integer warn, Integer plan, Date createTime, Date updateTime) {
		this.id = id;
		this.name = name;
		this.warn = warn;
		this.plan = plan;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getWarn
	 * @Description: 获取warn
	 * @return: Integer warn
	 */
	public Integer getWarn() {
		return warn;
	}

	/**
	 * @Title: setWarn
	 * @Description: 设置warn
	 */
	public void setWarn(Integer warn) {
		this.warn = warn;
	}

	/**
	 * @Title: getPlan
	 * @Description: 获取plan
	 * @return: Integer plan
	 */
	public Integer getPlan() {
		return plan;
	}

	/**
	 * @Title: setPlan
	 * @Description: 设置plan
	 */
	public void setPlan(Integer plan) {
		this.plan = plan;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetTestData
	 * @Description: 获取setTestData
	 * @return: Set<TestData> setTestData
	 */
	public Set<TestData> getSetTestData() {
		return setTestData;
	}

	/**
	 * @Title: setSetTestData
	 * @Description: 设置setTestData
	 */
	public void setSetTestData(Set<TestData> setTestData) {
		this.setTestData = setTestData;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Battery [id=" + id + ", name=" + name + ", warn=" + warn + ", plan=" + plan + ", group=" + group
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
