package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: GroupInfo
 * @Description: 电池组信息
 * @author LW
 * @date 2018年6月4日 上午11:01:39
 */
@Entity
@Table(name = "bm_group_info")
public class GroupInfo implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 3661031369292173168L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电池组电压
	 */
	@Column(name = "goup_voltage", nullable = false)
	@ExcelColumn(row = 1, column = 0, value = "电池组电压/V")
	private Double goupVoltage;

	/**
	 * 充放电电流
	 */
	@Column(name = "elec_current", nullable = false)
	@ExcelColumn(row = 1, column = 2, value = "充放电电流/A")
	private Double elecCurrent;

	/**
	 * 总电池节数
	 */
	@Column(name = "count_node", nullable = false)
	@ExcelColumn(row = 1, column = 4, value = "电池总节数")
	private Integer countNode;

	/**
	 * 总充电容量
	 */
	@Column(name = "total_charg_volume", nullable = false)
	@ExcelColumn(row = 4, column = 0, value = "总充电容量/A")
	private Double totalChargVolume;

	/**
	 * 总放电容量
	 */
	@Column(name = "total_discharg_volume", nullable = false)
	@ExcelColumn(row = 4, column = 2, value = "总放电容量/A")
	private Double totalDischargVolume;

	/**
	 * 环境温度
	 */
	@Column(name = "tempera", nullable = false)
	@ExcelColumn(row = 4, column = 4, value = "环境温度/℃")
	private Double tempera;

	/**
	 * 电池组
	 */
	@OneToOne
	@JoinColumn(name = "group_id")
	private Group group;

	/**
	 * 单体信息
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "mon_info_id")
	private MonomerInfo monomerInfo;

	/**
	 * 系统状态
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "sys_status_id")
	@ExcelColumn(row = 4, column = 6, value = "系统状态")
	private SystemStatus systemStatus;

	/**
	 * 单体状态
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "mon_status_id")
	@ExcelColumn(row = 1, column = 6, value = "系统状态")
	private MonomerStatus monomerStatus;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * @Title: GroupInfo
	 * @Description: GroupInfo构造函数
	 */
	public GroupInfo() {}

	/**
	 * @Title: GroupInfo
	 * @Description: GroupInfo构造函数
	 * @param id
	 * @param goupVoltage
	 * @param elecCurrent
	 * @param countNode
	 * @param totalChargVolume
	 * @param totalDischargVolume
	 * @param tempera
	 * @param createTime
	 */
	public GroupInfo(Integer id, Double goupVoltage, Double elecCurrent, Integer countNode, Double totalChargVolume,
			Double totalDischargVolume, Double tempera, Date createTime) {
		this.id = id;
		this.goupVoltage = goupVoltage;
		this.elecCurrent = elecCurrent;
		this.countNode = countNode;
		this.totalChargVolume = totalChargVolume;
		this.totalDischargVolume = totalDischargVolume;
		this.tempera = tempera;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getGoupVoltage
	 * @Description: 获取goupVoltage
	 * @return: Double goupVoltage
	 */
	public Double getGoupVoltage() {
		return goupVoltage;
	}

	/**
	 * @Title: setGoupVoltage
	 * @Description: 设置goupVoltage
	 */
	public void setGoupVoltage(Double goupVoltage) {
		this.goupVoltage = goupVoltage;
	}

	/**
	 * @Title: getElecCurrent
	 * @Description: 获取elecCurrent
	 * @return: Double elecCurrent
	 */
	public Double getElecCurrent() {
		return elecCurrent;
	}

	/**
	 * @Title: setElecCurrent
	 * @Description: 设置elecCurrent
	 */
	public void setElecCurrent(Double elecCurrent) {
		this.elecCurrent = elecCurrent;
	}

	/**
	 * @Title: getCountNode
	 * @Description: 获取countNode
	 * @return: Integer countNode
	 */
	public Integer getCountNode() {
		return countNode;
	}

	/**
	 * @Title: setCountNode
	 * @Description: 设置countNode
	 */
	public void setCountNode(Integer countNode) {
		this.countNode = countNode;
	}

	/**
	 * @Title: getTotalChargVolume
	 * @Description: 获取totalChargVolume
	 * @return: Double totalChargVolume
	 */
	public Double getTotalChargVolume() {
		return totalChargVolume;
	}

	/**
	 * @Title: setTotalChargVolume
	 * @Description: 设置totalChargVolume
	 */
	public void setTotalChargVolume(Double totalChargVolume) {
		this.totalChargVolume = totalChargVolume;
	}

	/**
	 * @Title: getTotalDischargVolume
	 * @Description: 获取totalDischargVolume
	 * @return: Double totalDischargVolume
	 */
	public Double getTotalDischargVolume() {
		return totalDischargVolume;
	}

	/**
	 * @Title: setTotalDischargVolume
	 * @Description: 设置totalDischargVolume
	 */
	public void setTotalDischargVolume(Double totalDischargVolume) {
		this.totalDischargVolume = totalDischargVolume;
	}

	/**
	 * @Title: getTempera
	 * @Description: 获取tempera
	 * @return: Double tempera
	 */
	public Double getTempera() {
		return tempera;
	}

	/**
	 * @Title: setTempera
	 * @Description: 设置tempera
	 */
	public void setTempera(Double tempera) {
		this.tempera = tempera;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getMonomerInfo
	 * @Description: 获取monomerInfo
	 * @return: MonomerInfo monomerInfo
	 */
	public MonomerInfo getMonomerInfo() {
		return monomerInfo;
	}

	/**
	 * @Title: setMonomerInfo
	 * @Description: 设置monomerInfo
	 */
	public void setMonomerInfo(MonomerInfo monomerInfo) {
		this.monomerInfo = monomerInfo;
	}

	/**
	 * @Title: getSystemStatus
	 * @Description: 获取systemStatus
	 * @return: SystemStatus systemStatus
	 */
	public SystemStatus getSystemStatus() {
		return systemStatus;
	}

	/**
	 * @Title: setSystemStatus
	 * @Description: 设置systemStatus
	 */
	public void setSystemStatus(SystemStatus systemStatus) {
		this.systemStatus = systemStatus;
	}

	/**
	 * @Title: getMonomerStatus
	 * @Description: 获取monomerStatus
	 * @return: MonomerStatus monomerStatus
	 */
	public MonomerStatus getMonomerStatus() {
		return monomerStatus;
	}

	/**
	 * @Title: setMonomerStatus
	 * @Description: 设置monomerStatus
	 */
	public void setMonomerStatus(MonomerStatus monomerStatus) {
		this.monomerStatus = monomerStatus;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GroupInfo [id=" + id + ", goupVoltage=" + goupVoltage + ", elecCurrent=" + elecCurrent + ", countNode="
				+ countNode + ", totalChargVolume=" + totalChargVolume + ", totalDischargVolume=" + totalDischargVolume
				+ ", tempera=" + tempera + ", createTime=" + createTime + "]";
	}

}
