package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: TestData
 * @Description: 测试数据
 * @author LW
 * @date 2018年6月4日 下午2:18:59
 */
@Entity
@Table(name = "bm_test_data")
public class TestData implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 4744711076374167070L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电池组
	 */
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;
	
	/**
	 * 电池
	 */
	@ManyToOne
	@JoinColumn(name = "battery_id")
	private Battery battery;
	
	/**
	 * 电池名
	 */
	@Column(name = "battery_name", length = 32, nullable = false)
	@ExcelColumn(row = -1, column = 1, value = "电池编号")
	private String batteryName;
	
	/**
	 * 测试类型，0：后台监测，1：手动测试，默认0
	 */
	@Column(name = "type", length = 2, nullable = false, columnDefinition = "INT DEFAULT 1")
	private Integer type;

	/**
	 * 电压
	 */
	@Column(name = "voltage", nullable = false, columnDefinition = "Double DEFAULT 0.0")
	@ExcelColumn(row = -1, column = 2, value = "单体电压/V")
	private Double voltage;

	/**
	 * 电阻
	 */
	@Column(name = "inter", nullable = false, columnDefinition = "Double DEFAULT 0.0")
	@ExcelColumn(row = -1, column = 3, value = "单体内阻/mΩ")
	private Double inter;

	/**
	 * 温度
	 */
	@Column(name = "tempera", nullable = false, columnDefinition = "Double DEFAULT 0.0")
	@ExcelColumn(row = -1, column = 4, value = "温度/℃")
	private Double tempera;

	/**
	 * 测试时间
	 */
	@Column(name = "test_time", nullable = false)
	private Date testTime;

	/**
	 * @Title: TestData
	 * @Description: TestData构造函数
	 */
	public TestData() {}
	
	/**
	 * @Title: TestData
	 * @Description: TestData构造函数
	 * @param voltage
	 * @param testTime
	 */
	public TestData(Date testTime, Double voltage) {
		this.voltage = voltage;
		this.testTime = testTime;
	}

	/**
	 * 
	 * @Title: TestData
	 * @Description: TestData构造函数
	 * @param batteryName
	 * @param voltage
	 */
	public TestData(String batteryName, Double voltage) {
		this.batteryName = batteryName;
		this.voltage = voltage;
	}

	/**
	 * @Title: TestData
	 * @Description: TestData构造函数
	 * @param batteryName
	 * @param voltage
	 * @param inter
	 * @param tempera
	 * @param testTime
	 */
	public TestData(String batteryName, Double voltage, Double inter, Double tempera, Date testTime) {
		this.batteryName = batteryName;
		this.voltage = voltage;
		this.inter = inter;
		this.tempera = tempera;
		this.testTime = testTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @Title: getBatteryName
	 * @Description: 获取batteryName
	 * @return: String batteryName
	 */
	public String getBatteryName() {
		return batteryName;
	}

	/**
	 * @Title: setBatteryName
	 * @Description: 设置batteryName
	 */
	public void setBatteryName(String batteryName) {
		this.batteryName = batteryName;
	}
	
	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: Integer type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: Double voltage
	 */
	public Double getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getInter
	 * @Description: 获取inter
	 * @return: Double inter
	 */
	public Double getInter() {
		return inter;
	}

	/**
	 * @Title: setInter
	 * @Description: 设置inter
	 */
	public void setInter(Double inter) {
		this.inter = inter;
	}

	/**
	 * @Title: getTempera
	 * @Description: 获取tempera
	 * @return: Double tempera
	 */
	public Double getTempera() {
		return tempera;
	}

	/**
	 * @Title: setTempera
	 * @Description: 设置tempera
	 */
	public void setTempera(Double tempera) {
		this.tempera = tempera;
	}

	/**
	 * @Title: getTestTime
	 * @Description: 获取testTime
	 * @return: Date testTime
	 */
	public Date getTestTime() {
		return testTime;
	}

	/**
	 * @Title: setTestTime
	 * @Description: 设置testTime
	 */
	public void setTestTime(Date testTime) {
		this.testTime = testTime;
	}

	/**
	 * @Title: getBattery
	 * @Description: 获取battery
	 * @return: Battery battery
	 */
	public Battery getBattery() {
		return battery;
	}

	/**
	 * @Title: setBattery
	 * @Description: 设置battery
	 */
	public void setBattery(Battery battery) {
		this.battery = battery;
	}
	
	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "TestData [id=" + id + ", group=" + group + ", battery=" + battery + ", batteryName=" + batteryName
				+ ", type=" + type + ", voltage=" + voltage + ", inter=" + inter + ", tempera=" + tempera
				+ ", testTime=" + testTime + "]";
	}

}
