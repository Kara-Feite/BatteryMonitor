package com.gzyw.monitor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.RoleDao;
import com.gzyw.monitor.dao.UserDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.RoleDto;
import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToMenu;
import com.gzyw.monitor.entity.RoleToTree;
import com.gzyw.monitor.entity.RoleToUser;
import com.gzyw.monitor.security.UserRealm;
import com.gzyw.monitor.service.RoleService;

/**
 * 
 * @ClassName: RoleServiceImpl
 * @Description: 角色服务层接口实现类
 * @author LW
 * @date 2018年7月8日 下午2:42:14
 */
@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private UserRealm userRealm;

	@Override
	public Role getRoleById(Serializable id) {
		return roleDao.get(id);
	}

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Role role) {
		Long count = roleDao.countRole(role);
		List<Role> listRole = roleDao.listRoleByPage(page, rows, sort, order, role);
		List<RoleDto> listRoleDto = new ArrayList<RoleDto>();
		RoleDto roleDto = null;
		for (Role r : listRole) {
			roleDto = new RoleDto();
			roleDto.setId(r.getId());
			roleDto.setName(r.getName());
			roleDto.setRemark(r.getRemark());
			roleDto.setUpdateBy(r.getUpdateBy());
			roleDto.setCreateBy(r.getCreateBy());
			roleDto.setCreateTime(r.getCreateTime());
			roleDto.setUpdateTime(r.getUpdateTime());
			roleDto.setState(r.getState());
			String createName = userDao.get(r.getCreateBy()).getNickName();
			if (r.getCreateBy() == r.getUpdateBy()) {
				roleDto.setUpdateName(createName);
			} else {
				String updateName = userDao.get(r.getUpdateBy()).getNickName();
				roleDto.setUpdateName(updateName);
			}
			roleDto.setCreateName(createName);
			listRoleDto.add(roleDto);
		}
		return new EUDataGridDto(count, listRoleDto);
	}

	@Override
	public ResultDto saveRole(Role role, String menuIds, String treeIds) {
		role.setState(true);
		Date date = new Date();
		role.setCreateTime(date);
		role.setUpdateTime(date);
		try {
			roleDao.save(role);
			if (StringUtil.isNotEmpty(menuIds)) {
				String[] Ids = menuIds.split(",");
				roleDao.saveRoleToMenu(role.getId(), Ids);
			}
			if (StringUtil.isNotEmpty(treeIds)) {
				String[] Ids = treeIds.split(",");
				roleDao.saveRoleToTree(role.getId(), Ids);
			}
			userRealm.clearCachedAuthorizationInfo();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误,请联系管理员！");
		}
		return ResultDto.ok();
	}

	@Override
	public List<RoleToMenu> listMenuById(Integer id) {
		return roleDao.listMenuById(id);
	}

	@Override
	public List<RoleToTree> listTreeById(Integer id) {
		return roleDao.listTreeById(id);
	}

	@Override
	public ResultDto updateRole(Role role, String menuIds, String treeIds) {
		role.setUpdateTime(new Date());
		try {
			roleDao.updateRole(role);
			if (StringUtil.isNotEmpty(menuIds)) {
				String[] ids = menuIds.split(",");
				roleDao.deleteRoleToMenu(role.getId());
				roleDao.saveRoleToMenu(role.getId(), ids);
			}
			if (StringUtil.isNotEmpty(treeIds)) {
				String[] ids = treeIds.split(",");
				roleDao.deleteRoleToTree(role.getId());
				roleDao.saveRoleToTree(role.getId(), ids);
			}
			// 清除权限授权缓存
			userRealm.clearCachedAuthorizationInfo();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误,请联系管理员！");
		}
		return ResultDto.ok();
	}

	@Override
	public ResultDto deleteRole(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的角色");
		}
		for (int id : ids) {
			Role role = roleDao.get(id);
			if (role != null) {
				roleDao.deleteRoleToMenu(id);
				roleDao.deleteRoleToTree(id);
				roleDao.deleteRoleToUser(id);
				roleDao.delete(role);
			}
		}
		// 清除权限授权缓存
		userRealm.clearCachedAuthorizationInfo();
		return ResultDto.build(200, "删除角色成功！");
	}

	@Override
	public ResultDto updateState(int[] ids, boolean state) {
		if (ids == null) {
			return ResultDto.build(500, "请选择需要更新状态的角色");
		}
		for (int id : ids) {
			Role role = roleDao.get(id);
			if (role != null) {
				role.setState(state);
				roleDao.update(role);
			}
		}
		// 清除权限授权缓存
		userRealm.clearCachedAuthorizationInfo();
		return ResultDto.build(200, "更改状态成功！");
	}

	@Override
	public List<Role> listRoleByAll() {
		return roleDao.listRoleByAll();
	}
	
	@Override
	public List<RoleToUser> listRoleToUser(Integer userId) {
		return roleDao.listRoleToUser(userId);
	}
	
}
