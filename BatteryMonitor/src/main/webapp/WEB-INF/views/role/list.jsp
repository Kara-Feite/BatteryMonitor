<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>角色管理</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>	
<script>
$(function(){
	$('#norm_lists').datagrid({
		url:'${ctx}/role/datagrid',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns : [ [ {
				field : 'id',
				title : 'ID编号',
				checkbox:true,
				align:'center',
				width : 30
			},{
				field : 'name',
				title : '名称',
				align:'center',
				width : 60
			},{
				field : 'createName',
				title : '创建人',
				align:'center',
				width : 50
			},{
				field : 'updateName',
				title : '更新人',
				align:'center',
				width : 50
			},{
				field : 'createTime',
				title : '创建时间',
				align:'center',
				sortable:true,
				width : 80,
				formatter : formatDateTime
			},{
				field : 'updateTime',
				title : '更新时间',
				align:'center',
				sortable:true,
				width : 80,
				formatter : formatDateTime
			},{
				field : 'remark',
				title : '备注',
				align:'center',
				width : 80,
				formatter : formatNull
			},{
				field : 'state',
				title : '状 态',
				width:50,
				align:'center',
				formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">正常</font>';
					}else{
						return '<font style="color:red">冻结</font>';
					}
				}
			}]],
		toolbar:[	
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',		
			{text:'冻 结',iconCls:'icon-remove',handler:function(){congeal();}},'-',	
			{text:'解 冻',iconCls:'icon-reload',handler:function(){thaw();}},'-',			
			{text:'删 除',iconCls:'icon-cancel',handler:function(){del();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});
function query(){
	$('#norm_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function clean(){
	$('#norm_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}
function add() {
	<permission:notPermission name="role:add">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	window.location.href="${ctx}/role/edit";
}
function update(){
	<permission:notPermission name="role:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#norm_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/role/edit?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}
function del(){
	<permission:notPermission name="role:delete">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#norm_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/role/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#norm_lists').datagrid('load',{});
						$('#norm_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}
function congeal(){
	<permission:notPermission name="role:congeal">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#norm_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要冻结选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/role/congeal',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#norm_lists').datagrid('load',{});
						$('#norm_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要冻结的记录'});
	}
}
function thaw(){
	<permission:notPermission name="role:thaw">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#norm_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要解冻选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/role/thaw',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#norm_lists').datagrid('load',{});
						$('#norm_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要解冻的记录'});
	}
}
function unselect(){
	$('#norm_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>

<body>
<div class="easyui-layout" fit="true" border="false">
	<div data-options="region:'north',title:'查询条件'" style="height:61px;">
		<form id="search_form">
			<table width="100%" bgcolor="#f5fafe">
				<tr height="25">
					<td id="denglu"><img src="${ctxStatic}/images/yin.gif" width="22">							
						<strong>名称：</strong><input name="maker" size="15"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
							onclick="query();" href="javascript:void(0);">查  询</a>						
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
							onclick="clean();" href="javascript:void(0);">清  空</a>
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-add'" 
							target="mainFrame" href="javascript:add();">添 加</a>
					</td>					
				</tr>					
			</table>
		</form>
	</div>
	<!-- ------------显示数据------------------- -->
	<div data-options="region:'center'">
		<table id="norm_lists"></table>
	</div>
</div>
</body>
</html>