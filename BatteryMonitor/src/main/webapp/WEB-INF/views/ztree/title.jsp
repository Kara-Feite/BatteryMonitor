<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<script src="${ctxStatic}/js/jquery-1.4.4.min.js"></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<link href="${ctxStatic}/css/Font.css" rel="stylesheet">
<style>
.select-top{
	background-color: #59ACFF;
    display: block;
    text-decoration: none;
}
</style>
</head>
<body>
	<sf:form method="get" modelAttribute="id" enctype="multipart/form-data">
		<table width="100%" border="0" height="25" cellpadding="0" cellspacing="0" 
			background="${ctxStatic}/images/b-info.gif">
			<tr height="22">
				<td>	
					<div class="topnav">
						<ul>
						   <li><a target="treeFrame" class='select-top'
								href="${ctx}/tree/content?id=${id}">
								<font>概 况</font></a></li>
						   <li><a target="treeFrame"
								href="${ctx}/alarm/content?treeId=${id}">
								<font>告 警</font></a></li>
							<li><a target="treeFrame"
								href="${ctx}/menu/edit">
								<font>添加基站</font></a></li>
				  			<li><a target="treeFrame"
								href="${ctx}/tree/save?id=${id}">
								<font>添加节点</font></a></li>
						</ul>
					</div>
	    		</td>
			</tr>
	    </table>
	</sf:form>
	<script>
		$('.topnav li a').click(function() {
			$('.select-top').removeClass('select-top');
			$(this).addClass('select-top');
		});
	</script>
</body>
</html>