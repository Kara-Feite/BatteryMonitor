package com.gzyw.monitor.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @ClassName: DateUtils
 * @Description: 时间工具类
 * @author LW
 * @date 2018年8月21日 上午10:05:03
 */
public class DateUtil {
	
	/**
	 * 一秒
	 */
	public static final long MILLIS_PER_SECOND = 1000;
    
	/**
	 * 一分钟
	 */
    public static final long MILLIS_PER_MINUTE = 60 * MILLIS_PER_SECOND;
    
    /**
     * 一小时
     */
    public static final long MILLIS_PER_HOUR = 60 * MILLIS_PER_MINUTE;
   
    /**
     * 一天
     */
    public static final long MILLIS_PER_DAY = 24 * MILLIS_PER_HOUR;

	
	/**
	 * DateTime : 精确到秒(yyyy-MM-dd HH:mm:ss)
	 */
	public static final String DEFAULT_DATETIME_FORMAT_SEC = "yyyy-MM-dd HH:mm:ss";

	/**
	 * DateTime : 精确到分钟(yyyy-MM-dd HH-mm)
	 */
	public static final String DEFAULT_DATETIME_FORMAT_MIN = "yyyy-MM-dd HH-mm";

	/**
	 * Date : 年月日(yyyy-MM-dd)
	 */
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * Time : 时分秒(HH:mm:ss)
	 */
	public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

	
	/**
	 * WEEKS : 星期数组
	 */
	public static final String[] WEEKS = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
	
	/**
	 * 如果参数格式为空，格式化的时间如例：18-8-21 上午10:55
	 * @Title: getSimpleDateFormat
	 * @Description: 获取SimpleDateFormat对象
	 * @param strFormat
	 * @return
	 */
	public static SimpleDateFormat getSimpleDateFormat(final String strFormat) {
		if (StringUtil.isNotEmpty(strFormat)) {
			return new SimpleDateFormat(strFormat);
		}
		return new SimpleDateFormat();
	}
	
	/**
	 * 
	 * @Title: format
	 * @Description: 格式日期为字符串
	 * @param date 日期
	 * @param pattern 日期格式
	 * @return
	 */
	public static String format(final Date date, final String pattern) {
		SimpleDateFormat dateFormat = getSimpleDateFormat(pattern);
		return dateFormat.format(date);
	}
	
	/**
	 * yyyy-MM-dd HH:mm:ss
	 * @Title: formatDateTime
	 * @Description: 格式DateTime
	 * @param date
	 * @return
	 */
	public static String formatDateTime(final Date date) {
		return format(date, DEFAULT_DATETIME_FORMAT_SEC);
	}
	
	/**
	 * 默认格式yyyy-MM-dd HH:mm:ss
	 * @Title: now
	 * @Description: 获取当前时间
	 * @return
	 */
	public static String now() {
		return formatDateTime(new Date());
	}
	
	/**
	 * 
	 * @Title: now
	 * @Description: 获取指定格式当前时间
	 * @param pattern
	 * @return
	 */
	public static String now(final String pattern) {
		return format(new Date(), pattern);
	}
	
	/**
	 * 
	 * @Title: parseDate
	 * @Description: 字符串转指定格式日期
	 * @param str
	 * @param strFormat
	 * @return
	 */
	public static Date parseDate(final String str, final String strFormat) {
		SimpleDateFormat format = new SimpleDateFormat(strFormat);
		try {
			return format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @Title: parseDate
	 * @Description: 字符串转日期
	 * @param str
	 * @return
	 */
	public static Date parseDate(final String str) {
		return parseDate(str, DEFAULT_DATETIME_FORMAT_SEC);
	}
	
	/**
	 * 
	 * @Title: isSameDay
	 * @Description: 判断两日期是不是同一天
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameDay(final Date date1, final Date date2) {
        if (date1 == null || date2 == null) {
           return false;
        }
        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
	
	/**
	 * 加负数为减
	 * @Title: addYears
	 * @Description: 添加年份
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addYears(final Date date, final int amount) {
        return add(date, Calendar.YEAR, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: addMonths
	 * @Description: 添加月份
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addMonths(final Date date, final int amount) {
        return add(date, Calendar.MONTH, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: addDays
	 * @Description: 添加天数
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addDays(final Date date, final int amount) {
        return add(date, Calendar.DAY_OF_MONTH, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: addHours
	 * @Description: 添加小时
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addHours(final Date date, final int amount) {
        return add(date, Calendar.HOUR_OF_DAY, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: addMinutes
	 * @Description: 添加分钟
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addMinutes(final Date date, final int amount) {
        return add(date, Calendar.MINUTE, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: addSeconds
	 * @Description: 添加秒
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addSeconds(final Date date, final int amount) {
        return add(date, Calendar.SECOND, amount);
    }
	
	/**
	 * 加负数为减
	 * @Title: add
	 * @Description: 加指定时间字段的值
	 * @param date
	 * @param calendarField 时间字段（年、月、日、时、分、秒）
	 * @param amount
	 * @return
	 */
	private static Date add(final Date date, final int calendarField, final int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }
	
	/**
	 * 
	 * @Title: setYears
	 * @Description: 设置年份
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date setYears(final Date date, final int amount) {
        return set(date, Calendar.YEAR, amount);
    }

    /**
     * 
     * @Title: setMonths
     * @Description: 设置月份
     * @param date
     * @param amount
     * @return
     */
    public static Date setMonths(final Date date, final int amount) {
        return set(date, Calendar.MONTH, amount);
    }

   /**
    * 
    * @Title: setDays
    * @Description: 设置天
    * @param date
    * @param amount
    * @return
    */
    public static Date setDays(final Date date, final int amount) {
        return set(date, Calendar.DAY_OF_MONTH, amount);
    }

    /**
     * 
     * @Title: setHours
     * @Description: 设置小时
     * @param date
     * @param amount
     * @return
     */
    public static Date setHours(final Date date, final int amount) {
        return set(date, Calendar.HOUR_OF_DAY, amount);
    }

    /**
     * 
     * @Title: setMinutes
     * @Description: 设置分钟
     * @param date
     * @param amount
     * @return
     */
    public static Date setMinutes(final Date date, final int amount) {
        return set(date, Calendar.MINUTE, amount);
    }
    
    /**
     * 
     * @Title: setSeconds
     * @Description: 设置秒
     * @param date
     * @param amount
     * @return
     */
    public static Date setSeconds(final Date date, final int amount) {
        return set(date, Calendar.SECOND, amount);
    }

    /**
     * 
     * @Title: set
     * @Description: 设置指定时间字段的值
     * @param date
     * @param calendarField 时间字段（年、月、日、时、分、秒）
     * @param amount
     * @return
     */
    private static Date set(final Date date, final int calendarField, final int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        final Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(calendarField, amount);
        return c.getTime();
    }
    
    /**
     * 
     * @Title: timeDiffToDay
     * @Description: 两时间相差天数
     * @param date1
     * @param date2
     * @return
     */
    public static long timeDiffToDay(final Date date1, final Date date2) {
    	return timeDiff(date1, date2) / MILLIS_PER_DAY;
    }
    
    /**
     * 
     * @Title: timeDiffToHour
     * @Description: 两时间相差小时
     * @param date1
     * @param date2
     * @return
     */
    public static long timeDiffToHour(final Date date1, final Date date2) {
    	return timeDiff(date1, date2) / MILLIS_PER_HOUR;
    }
    
    /**
     * 
     * @Title: timeDiffToMinute
     * @Description: 两时间相差分钟
     * @param date1
     * @param date2
     * @return
     */
    public static long timeDiffToMinute(final Date date1, final Date date2) {
    	return timeDiff(date1, date2) / MILLIS_PER_MINUTE;
    }
    
    /**
     * 
     * @Title: timeDiffToSecond
     * @Description: 两时间相差秒数
     * @param date1
     * @param date2
     * @return
     */
    public static long timeDiffToSecond(final Date date1, final Date date2) {
    	return timeDiff(date1, date2) / MILLIS_PER_SECOND;
    }
    
    /**
     * 例如："2018-08-21 14:10:30"，"1999-11-27 13:10:20"
     * 返回：6842天1小时10秒875毫秒
     * @Title: timeDiffToDate
     * @Description: 两时间相差天、时、分、秒、毫秒
     * @param date1
     * @param date2
     * @return
     */
    public static String timeDiffToDate(final Date date1, final Date date2) {
    	long ms = timeDiff(date1, date2);
    	Long day = ms / MILLIS_PER_DAY;  
        Long hour = (ms - day * MILLIS_PER_DAY) / MILLIS_PER_HOUR;  
        Long minute = (ms - day * MILLIS_PER_DAY - hour * MILLIS_PER_HOUR) / MILLIS_PER_MINUTE;  
        Long second = (ms - day * MILLIS_PER_DAY - hour * MILLIS_PER_HOUR - minute * MILLIS_PER_MINUTE) / MILLIS_PER_SECOND;  
        Long milliSecond = ms - day * MILLIS_PER_DAY - hour * MILLIS_PER_HOUR - minute * MILLIS_PER_MINUTE - second * MILLIS_PER_SECOND;
        StringBuffer sb = new StringBuffer();  
        if(day > 0) {
            sb.append(day+"天");
        }
        if(hour > 0) {
            sb.append(hour+"小时");
        }
        if(minute > 0) {
            sb.append(minute+"分");
        }
        if(second > 0) {
            sb.append(second+"秒");
        }
        if(milliSecond > 0) {
            sb.append(milliSecond+"毫秒"); 
        }
        return sb.toString();
    }
    
    /**
     * 
     * @Title: timeDiff
     * @Description: 计算时间差
     * @param date1
     * @param date2
     * @return
     */
    public static long timeDiff(final Date date1, final Date date2) {
    	long longtime = date1.getTime() - date2.getTime();
    	return longtime > 0 ? longtime : -longtime;
    }
    
    /**
     * 
     * @Title: compareTo
     * @Description: 比较时间
     * @param date1
     * @param date2
     * @return
     */
    public static int compareTo(final Date date1, final Date date2) {
		long num = date1.getTime() - date2.getTime();
		if (num != 0) {
			return num > 0 ? 1 : -1;
		}
		return 0;
	}

}
