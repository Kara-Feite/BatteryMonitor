package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Limit;

/**
 * 
 * @ClassName: LimitDao
 * @Description: 警告门限持久层接口
 * @author LW
 * @date 2018年7月11日 下午3:44:08
 */
public interface LimitDao extends BaseDao<Limit> {

	/**
	 * 
	 * @Title: countLimit
	 * @Description: 统计门限总数
	 * @param limit
	 * @param groupId
	 * @return
	 */
	Long countLimit(Limit limit, Integer groupId);

	/**
	 * 
	 * @Title: listLimitByPage
	 * @Description: 获取门限分页数据
	 * @return
	 */
	List<Limit> listLimitByPage(Integer page, Integer rows, String sort, String order, Limit limit, Integer groupId);

	/**
	 * 
	 * @Title: getLimitByTypeAndGroup
	 * @Description: 根据类型和电池组获取门限
	 * @param type
	 * @param groupId
	 * @return
	 */
	Limit getLimitByTypeAndGroup(String type, Integer groupId);

	/**
	 * 
	 * @Title: listLimitByGroup
	 * @Description: 根据电池组获取警告门限
	 * @param groupId
	 * @return
	 */
	List<Limit> listLimitByGroup(Integer groupId);

}
