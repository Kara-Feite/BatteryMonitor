package com.gzyw.monitor.common.util;

/**
 * 
 * @ClassName: CRCUtil
 * @Description: CRC校验工具类
 * @author LW
 * @date 2018年5月31日 上午8:33:18
 */
public class CRCUtil {

	/**
	 * 业务需要：前后两位字符倒换位置
	 * @Title: getCrc16
	 * @Description: 获取crc校验码
	 * @param bytes
	 * @return
	 */
	public static String getCrc16(byte[] bytes) {
		int len = bytes.length;
		// 预置 1 个 16 位的寄存器为十六进制FFFF, 称此寄存器为 CRC寄存器。
		int crc = 0xFFFF;
		int i, j;
		for (i = 0; i < len; i++) {
			// 把第一个 8 位二进制数据 与 16 位的 CRC寄存器的低 8 位相异或, 把结果放于 CRC寄存器
			crc = ((crc & 0xFF00) | (crc & 0x00FF) ^ (bytes[i] & 0xFF));
			for (j = 0; j < 8; j++) {
				// 把 CRC 寄存器的内容右移一位( 朝低位)用 0 填补最高位, 并检查右移后的移出位
				if ((crc & 0x0001) > 0) {
					// 如果移出位为 1, CRC寄存器与多项式A001进行异或
					crc = crc >> 1;
					crc = crc ^ 0xA001;
				} else
					// 如果移出位为 0,再次右移一位
					crc = crc >> 1;
			}
		}
		//不足4位前面补0
		String hex = NumberUtil.decToHex(crc);
		//前后两位字符倒换位置
		char[] chars = hex.toCharArray();
		return String.valueOf(chars[2] + StringUtil.EMPTY + chars[3] + StringUtil.EMPTY + chars[0] + StringUtil.EMPTY + chars[1]);
	}
	
	public static void main(String[] args) {
		String crc16 = getCrc16(StringUtil.toBytes("010303e80019"));
		System.out.println(crc16);
	}

}
