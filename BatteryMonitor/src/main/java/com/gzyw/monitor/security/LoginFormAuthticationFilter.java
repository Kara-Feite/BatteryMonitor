package com.gzyw.monitor.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.common.util.JsonUtil;
import com.gzyw.monitor.dao.UserDao;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Log;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.LogService;

/**
 * 
 * @ClassName: LoginFormAuthticationFilter
 * @Description: 登录身份验证过滤器
 * @author LW
 * @date 2018年7月4日 上午9:36:54
 */
public class LoginFormAuthticationFilter extends FormAuthenticationFilter {
	
	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger(LoginFormAuthticationFilter.class);
	
	@Autowired
	private UserDao userDao;
	
	/**
	 * 日志服务层接口
	 */
	@Autowired
	private LogService logService;

	/**
	 * @param token
	 * @param subject
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @see org.apache.shiro.web.filter.authc.FormAuthenticationFilter#onLoginSuccess(org.apache.shiro.authc.AuthenticationToken,
	 *      org.apache.shiro.subject.Subject, javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse)
	 */
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
			ServletResponse response) throws Exception {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		UsernamePasswordToken authtoken = (UsernamePasswordToken) token;
		String userName = authtoken.getUsername();
		User user = userDao.getUserByName(userName);
		Date date = new Date();
		// 添加登录日志
		Log log = new Log();
		log.setCreateTime(date);
		log.setIpAddr(httpServletRequest.getRemoteAddr());
		log.setType("用户登录");
		log.setUser(user);
		@SuppressWarnings("unchecked")
		Map<String,String[]> params = request.getParameterMap();
		log.setMapToParams(params);
		log.setDescription("id为:" + user.getId() + ",用户名为:" + user.getNickName() + "的用户登录系统");
		logService.saveLog(log);
		logger.info("id为:" + user.getId() + ",用户名为:" + user.getNickName() + "的用户于" + DateUtil.formatDateTime(date) + "登录系统");
		HttpSession session = httpServletRequest.getSession();
		user.setPassword(null);
        //把用户信息保存到session
        session.setAttribute("user", user);
		// 不是ajax请求
		if (!"XMLHttpRequest".equalsIgnoreCase(httpServletRequest.getHeader("X-Requested-With"))) {
			issueSuccessRedirect(request, response);
		} else {
			httpServletResponse.setCharacterEncoding("UTF-8");
			PrintWriter out = httpServletResponse.getWriter();
			// 判断是否选择了记住密码
			if (!authtoken.isRememberMe()) {
				out.println(JsonUtil.objectToJson(ResultDto.ok()));
				return false;
			}
			user.setPassword(String.valueOf(authtoken.getPassword()));
			out.println(JsonUtil.objectToJson(ResultDto.ok(user)));
			out.flush();
			out.close();
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param mappedValue
	 * @return
	 * @see org.apache.shiro.web.filter.authc.AuthenticatingFilter#isAccessAllowed(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, java.lang.Object)
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		if (isLoginRequest(request, response) && isLoginSubmission(request, response)) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String uri = httpServletRequest.getRequestURI();
			if (uri.indexOf("user/login") != -1 && super.isAccessAllowed(request, response, mappedValue)) {
				Subject subject = this.getSubject(request, response);
				subject.logout();
			}
		}
		return super.isAccessAllowed(request, response, mappedValue);
	}

	/**
	 * @param token
	 * @param e
	 * @param request
	 * @param response
	 * @return
	 * @see org.apache.shiro.web.filter.authc.FormAuthenticationFilter#onLoginFailure(org.apache.shiro.authc.AuthenticationToken,
	 *      org.apache.shiro.authc.AuthenticationException,
	 *      javax.servlet.ServletRequest, javax.servlet.ServletResponse)
	 */
	@Override
	protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		// 不是ajax请求
		if (!"XMLHttpRequest".equalsIgnoreCase(((HttpServletRequest) request).getHeader("X-Requested-With"))) {
			setFailureAttribute(request, e);
			return true;
		}
		try {
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			if (e instanceof UnknownAccountException || e instanceof IncorrectCredentialsException) {
				out.println(JsonUtil.objectToJson(ResultDto.build(500, "账号或密码错误")));
			} else {
				out.println(JsonUtil.objectToJson(ResultDto.build(500, "出现未知错误，请联系管理员!")));
			}
			out.flush();
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return false;
	}

}
