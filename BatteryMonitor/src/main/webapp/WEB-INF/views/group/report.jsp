<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<style>
select,input{width:120px;}
</style>
<script>
$(function(){
	$('#report_lists').datagrid({
		url:'${ctx}/group/report/datagrid',
		pageList:[999],
		pagination:true,
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[
			{
				field:'id',
				width:30,
				checkbox:true
			},{
				field:'testTime',
				title:'测试时间',
				width: 80,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			}
		]],
		onBeforeLoad: function (param) {
            var firstLoad = $(this).attr("firstLoad");
            if (firstLoad == "false" || typeof (firstLoad) == "undefined")
            {
                $(this).attr("firstLoad", "true");
                return false;
            }
            return true;
        },
		toolbar:[
			{text:'导出',iconCls:'icon-print',handler:function(){exportExcel();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});
function query() {
	var myForm = document.myForm,
		treeId = myForm.id.value.trim(),
		startTime = myForm.startTime.value,
		endTime = myForm.endTime.value;
	$.ajax({
		url : '${ctx}/group/report/query',
		type : 'get',
		data: {'treeId':treeId, 'startTime':startTime, 'endTime':endTime},
		success: function(result) {
			if (result.status === 200) {
				$('#report_lists').datagrid('load',serializeObject($('#search_form').form()));
			} else {
				$.messager.alert({title:'提示',msg: result.msg});
			}
		}
	});
}
function unselect(){
	$('#report_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>	
<body>  
	<div class="easyui-panel" title="节点：${tree.name} 导出报告" style="width:100%;height:400px;padding:30px 40px;">
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'west',split:false" style="width:250px;padding:35px 0 0 25px">
				<sf:form name="myForm" method="get" modelAttribute="temp" enctype="multipart/form-data">
					<sf:hidden path="treeId" value="${tree.id}"/>
					 <div style="margin-bottom:20px">	   			   	 											
						<strong>&nbsp;电&nbsp;池&nbsp;组：</strong>
						<c:if test="${!empty listTree}">
							<sf:select path="id" items="${listTree}" 
								itemLabel="name" itemValue="id" /><br><br>
						</c:if>
						<c:if test="${empty listTree}">
							<sf:select path="id" editable="false">  
							    	<sf:option label="${tree.name}" value="${tree.id}"/>
							 </sf:select><br><br>
						</c:if>
						<strong>开始时间：</strong>	
							<sf:input path="startTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/><br><br>
						<strong>结束时间：</strong>	
							<sf:input path="endTime" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/><br><br>
						<strong>&nbsp;操 &nbsp;&nbsp;&nbsp;&nbsp;作：</strong>				
							<!-- <a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="exportExcel();" href="javascript:void(0);">柱状图</a>	 -->
							&nbsp;&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查询</a>						   			
					</div>
				</sf:form>
			</div>
			<div data-options="region:'center'" class='data-right'>
				<table id="report_lists"></table>
			</div>
		</div>
	</div>
	<iframe id='graph' src='' style='display:none'></iframe>
	<script>
		function exportExcel() {
			var rows = $('#report_lists').datagrid('getChecked');
			if(rows.length < 1){
				$.messager.alert({title:'提示',msg:'请选择要导出的记录！'});
				return;
			}
			for(var i = 0, row; row = rows[i++];){
				var time = ((i-1)*2000) + 50;
				setTimeout('report(' + row.testTime + ')', time);
			}
		}
		
		function report(testTime) {
			var date = new Date(testTime),
				myForm = document.myForm,
				treeId = myForm.id.value.trim(),
				startTime = date.format("yyyy-MM-dd hh:mm:ss");
			var exportForm = document.createElement("form"); 
				exportForm.id = "exportForm"; 
				exportForm.name = "exportForm"; 
				document.body.appendChild(exportForm); 
			var input1 = document.createElement("input"),
				input2 = document.createElement("input");
			
		  	input1.type = "text";
		  	input1.name = "startTime";
		  	input1.value = startTime;
		  	input2.type = "text";
		  	input2.name = "treeId";
		  	input2.value = treeId;
		  	
		  	exportForm.appendChild(input1);
		  	exportForm.appendChild(input2);
		  	exportForm.method = "POST"; 
		  	exportForm.action = "${ctx}/group/export/excel?dd=" + new Date(); 
		  	exportForm.submit(); 
		  	document.body.removeChild(exportForm);
		}
		
		// 导出方法（之前的方法，不适用批量导出）
		/* function report(testTime) {
			var g = document.getElementById('graph'),
				date = new Date(testTime),
				myForm = document.myForm,
				treeId = myForm.id.value.trim(),
				startTime = date.format("yyyy-MM-dd hh:mm:ss");
			
			setTimeout(function() {
				g.src = '${ctx}/group/graph?treeId=' + treeId + '&type=单体电压/V&startTime='+startTime+'&endTime=' + endTime;
			}, 500);
			setTimeout(function() {
				g.src = '${ctx}/group/graph?treeId=' + treeId + '&type=单体内阻/mΩ&startTime='+startTime+'&endTime=' + endTime;
			}, 1000);
			setTimeout(function() {
				// g.src = '${ctx}/group/graph?treeId=' + treeId + '&type=单体内阻/mΩ&startTime='+startTime+'&endTime=' + endTime;
				var voltageGraph = sessionStorage.getItem('voltage');
				var interGraph = sessionStorage.getItem('inter');
				var exportForm = document.createElement("form"); 
				sessionStorage.removeItem('voltage');
				sessionStorage.removeItem('inter');
				exportForm.id = "exportForm"; 
				exportForm.name = "exportForm"; 
				document.body.appendChild(exportForm); 
				var input1 = document.createElement("input"),
					input2 = document.createElement("input"),
					input3 = document.createElement("input"),
					input4 = document.createElement("input");
				// 0 500 1000 2000 2000 2500 3000 4000
			  	input1.type = "text";
			  	input1.name = "voltageGraph";
			  	input1.value = voltageGraph;
			  	input2.type = "text";
			  	input2.name = "interGraph";
			  	input2.value = interGraph;
			  	input3.type = "text";
			  	input3.name = "startTime";
			  	input3.value = startTime;
			  	input4.type = "text";
			  	input4.name = "treeId";
			  	input4.value = treeId;
			  	
			  	exportForm.appendChild(input1);
			  	exportForm.appendChild(input2);
			  	exportForm.appendChild(input3);
			  	exportForm.appendChild(input4);
			  	exportForm.method = "POST"; 
			  	exportForm.action = "${ctx}/group/export/excel?dd=" + new Date(); 
			  	exportForm.submit(); 
			  	document.body.removeChild(exportForm);
			}, 2000);
		} */
		// 获取任意区间的随机数
		function randDomNum (lowValue,highValue){
			var choice=highValue-lowValue+1;
			return Math.floor(Math.random()*choice+lowValue);
		}
	</script>
</body>
</html>