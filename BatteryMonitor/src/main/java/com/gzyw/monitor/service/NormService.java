package com.gzyw.monitor.service;

import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Norm;

/**
 * 
 * @ClassName: NormService
 * @Description: 电池规格服务层
 * @author LW
 * @date 2018年6月13日 上午9:25:49
 */
public interface NormService {

	/**
	 * 
	 * @Title: getDataGrid
	 * @Description: 获取电池规格列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param norm 电池规格
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Norm norm);

	/**
	 * 
	 * @Title: delNormByBat
	 * @Description: 批量删除规格
	 * @param ids
	 * @return
	 */
	ResultDto delNormByBat(int[] ids);

	/**
	 * 
	 * @Title: getNormById
	 * @Description: 根据id获取电池规格
	 * @param id
	 * @return
	 */
	Norm getNormById(Integer id);

	/**
	 * 
	 * @Title: listNormByAll
	 * @Description: 获取所有电池规格
	 * @return
	 */
	List<Norm> listNormByAll();

	/**
	 * 
	 * @Title: saveNorm
	 * @Description: 添加规格
	 * @param norm
	 * @return
	 */
	ResultDto saveNorm(Norm norm);

	/**
	 * 
	 * @Title: updateNorm
	 * @Description: 更新用户
	 * @param norm
	 * @return
	 */
	ResultDto updateNorm(Norm norm);

	/**
	 * 
	 * @Title: listTypeAll
	 * @Description: 获取输入的所有电池类型
	 * @return
	 */
	List<String> listTypeAll();

	/**
	 * 
	 * @Title: listMakerAll
	 * @Description: 获取输入的所有制造商
	 * @return
	 */
	List<String> listMakerAll();

	/**
	 * 
	 * @Title: listModelAll
	 * @Description: 获取输入的所有型号
	 * @return
	 */
	List<String> listModelAll();

	/**
	 * 
	 * @Title: getNormByGroupId
	 * @Description: 根据电池组获取电池规格
	 * @param treeId
	 * @return
	 */
	Norm getNormByGroupId(Integer treeId);

}
