package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Norm;

/**
 * 
 * @ClassName: NormDao
 * @Description: 电池规格持久层
 * @author LW
 * @date 2018年6月13日 上午9:32:56
 */
public interface NormDao extends BaseDao<Norm>{

	/**
	 * 
	 * @Title: countNorm
	 * @Description: 获取规格总数
	 * @param norm 电池规格
	 * @return
	 */
	Long countNorm(Norm norm);

	/**
	 * 
	 * @Title: listNormByPage
	  * @Description: 分页获取规格数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param norm 电池规格
	 * @return
	 */
	List<Norm> listNormByPage(Integer page, Integer rows, String sort, String order, Norm norm);

	/**
	 * 
	 * @Title: updateState
	 * @Description: 更新规格状态
	 * @param id
	 * @param state
	 * @return
	 */
	Integer updateState(int id, boolean state);

	/**
	 * 
	 * @Title: listNormByAll
	 * @Description: 获取所有电池规格
	 * @return
	 */
	List<Norm> listNormByAll();

	/**
	 * 
	 * @Title: listTypeAll
	 * @Description: 获取输入的所有电池类型
	 * @return
	 */
	List<String> listTypeAll();

	/**
	 * 
	 * @Title: listMakerAll
	 * @Description: 获取输入的所有制造商
	 * @return
	 */
	List<String> listMakerAll();

	/**
	 * 
	 * @Title: listModelAll
	 * @Description: 获取输入的所有型号
	 * @return
	 */
	List<String> listModelAll();

}
