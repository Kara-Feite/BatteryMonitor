package com.gzyw.monitor.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: WebUtil
 * @Description: web工具类
 * @author LW
 * @date 2018年7月18日 下午4:40:54
 */
public class WebUtil {
	
	/**
	 * 授权信息
	 */
	private static final List<String> PERMISSIONS = new ArrayList<String>();
	
	/**
	 * 
	 * @Title: setPermissions
	 * @Description: 设置授权信息
	 * @param permissions
	 */
	public static void setPermissions(List<String> permissions) {
		if (PERMISSIONS.size() <= 0 && CollectionUtil.isNotEmpty(permissions)) {
			permissions.addAll(permissions);
		}
	}
	
	/**
	 * 
	 * @Title: hasPower
	 * @Description: 判断是否拥有权限
	 * @param permission
	 * @return
	 */
	public static boolean hasPower(String permission) {
		for (String str : PERMISSIONS) {
			if (str.equals(permission)) {
				return true;
			}
		}
		return false;
	}

}
