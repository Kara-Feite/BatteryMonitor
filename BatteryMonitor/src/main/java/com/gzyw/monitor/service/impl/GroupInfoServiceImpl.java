package com.gzyw.monitor.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.ArrayUtil;
import com.gzyw.monitor.common.util.DataUtil;
import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.GroupInfoDao;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.MonomerInfo;
import com.gzyw.monitor.entity.MonomerStatus;
import com.gzyw.monitor.entity.SystemStatus;
import com.gzyw.monitor.service.GroupInfoService;

/**
 * 
 * @ClassName: GroupInfoServiceImpl
 * @Description: 组信息服务层接口实现类
 * @author LW
 * @date 2018年6月26日 下午4:39:17
 */
@Service
public class GroupInfoServiceImpl implements GroupInfoService {
	
	@Autowired
	private GroupInfoDao groupInfoDao;

	@Override
	public boolean saveGroupInfo(String sysInfo, Group group, Date date) {
		if (StringUtil.isEmpty(sysInfo)) {
			return false;
		}
		// 进行crc校验并获取对应数组格式数据
		String[] arr = DataUtil.toDataArr(sysInfo);
		if (ArrayUtil.isEmpty(arr)) {
			return false;
		}
		GroupInfo groupInfo = new GroupInfo();
		MonomerInfo monoInfo = new MonomerInfo();
		
		// 获取对应16进制数据，因为固定所以直接按下标获取
		String goupVoltage = arr[3] + arr[4];
		String elecCurrent = arr[5] + arr[6];
		String maxVoltage = arr[7] + arr[8];
		String maxVoltageNo = arr[9] + arr[10];
		String minVoltage = arr[11] + arr[12];
		String minVoltageNo = arr[13] + arr[14];
		String maxInter = arr[15] + arr[16];
		String maxInterNo = arr[17] + arr[18];
		String avgVoltage = arr[19] + arr[20];
		String countNode = arr[21] + arr[22];
		String totalChargVolume = arr[23] + arr[24];
		String totalDischargVolume = arr[25] + arr[26];
		String tempera = arr[27] + arr[28];
		String systemStatus = arr[31] + arr[32];
		String monomerStatus = arr[33] + arr[34];
		
		// 获取相关状态
		MonomerStatus monoStatus = getMonomerStatus(monomerStatus);
		monoStatus.setCreateTime(date);
		SystemStatus sysStatus = getSystemStatus(systemStatus);
		sysStatus.setCreateTime(date);
		
		// 设置单体信息
		monoInfo.setMaxVoltage(NumberUtil.hexToDouble(maxVoltage));
		monoInfo.setMaxVoltageNo("电池_" + NumberUtil.hexToDec(maxVoltageNo));
		monoInfo.setMinVoltage(NumberUtil.hexToDouble(minVoltage));
		monoInfo.setMinVoltageNo("电池_" + NumberUtil.hexToDec(minVoltageNo));
		monoInfo.setMaxInter(NumberUtil.hexToDouble(maxInter));
		monoInfo.setMaxInterNo("电池_" + NumberUtil.hexToDec(maxInterNo));
		monoInfo.setAvgVoltage(NumberUtil.hexToDouble(avgVoltage));
		monoInfo.setCreateTime(date);
		
		// 设置系统信息（组信息）
		groupInfo.setGoupVoltage(NumberUtil.hexToDouble(goupVoltage));
		groupInfo.setElecCurrent(NumberUtil.hexToDouble(elecCurrent));
		groupInfo.setCountNode(NumberUtil.hexToDec(countNode));
		groupInfo.setTotalChargVolume(NumberUtil.hexToDouble(totalChargVolume));
		groupInfo.setTotalDischargVolume(NumberUtil.hexToDouble(totalDischargVolume));
		groupInfo.setTempera(NumberUtil.hexToDouble(tempera));
		groupInfo.setMonomerInfo(monoInfo);
		groupInfo.setSystemStatus(sysStatus);
		groupInfo.setMonomerStatus(monoStatus);
		groupInfo.setGroup(group);
		groupInfo.setCreateTime(date);
		
		groupInfoDao.save(groupInfo);
		return !"0000".equals(monomerStatus);
	}

	/**
	 * 
	 * @Title: getSystemStatus
	 * @Description: 获取系统状态
	 * @param systemStatus
	 * @return
	 */
	private SystemStatus getSystemStatus(String systemStatus) {
		systemStatus = NumberUtil.hexToBin(systemStatus);
		SystemStatus sysStatus = new SystemStatus();
		String[] arr = systemStatus.split("");
		sysStatus.setIsHighVoltage(arr[7].equals("1") ? true : false);
		sysStatus.setIsLowVoltage(arr[6].equals("1") ? true : false);
		sysStatus.setIsOffline(arr[5].equals("1") ? true : false);
		sysStatus.setIsOpenCircuit(arr[4].equals("1") ? true : false);
		sysStatus.setIsOvercurrent(arr[3].equals("1") ? true : false);
		sysStatus.setIsOverDischarge(arr[2].equals("1") ? true : false);
		sysStatus.setIsHighTempera(arr[1].equals("1") ? true : false);
		sysStatus.setIsLowTempera(arr[0].equals("1") ? true : false);
		return sysStatus;
	}

	/**
	 * 
	 * @Title: getMonomerStatus
	 * @Description: 获取单体状态
	 * @param monomerStatus
	 * @return
	 */
	private MonomerStatus getMonomerStatus(String monomerStatus) {
		monomerStatus = NumberUtil.hexToBin(monomerStatus);
		MonomerStatus monoStatus = new MonomerStatus();
		String[] arr = monomerStatus.split("");
		monoStatus.setIsHighVoltage(arr[0].equals("1") ? true : false);
		monoStatus.setIsLowVoltage(arr[1].equals("1") ? true : false);
		monoStatus.setIsHighInter(arr[2].equals("1") ? true : false);
		monoStatus.setIsHighTempera(arr[3].equals("1") ? true : false);
		return monoStatus;
	}
	
	@Override
	public GroupInfo getGroupInfoByNow(Integer id) {
		return groupInfoDao.getGroupInfoByNow(id);
	}

}
