package com.gzyw.monitor.test;

import io.netty.channel.Channel;

public class Control {
	
	private Channel server;
	
	private Channel client;

	public Channel getServer() {
		return server;
	}

	public void setServer(Channel server) {
		this.server = server;
	}

	public Channel getClient() {
		return client;
	}

	public void setClient(Channel client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Control [server=" + server + ", client=" + client + "]";
	}
   
}
