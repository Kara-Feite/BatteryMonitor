package com.gzyw.monitor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @ClassName: LogRequestParam
 * @Description: 日志请求参数拦截
 * @author LW
 * @date 2018年7月9日 下午5:12:13
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LogRequestParam {
	
}