package com.gzyw.monitor.control;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;

/**
 * <p>
 * 	这里是根据字符串长度为11的字符判断电池组标识<br>
 * 	后面推荐用(地区+递增数字)<br>
 * 	例如，黑龙江：黑龙江>>>0000001，湖南：湖南>>>0000001
 * </p>
 * <p>
 * 	这里MonitorServer接收所有的请求做处理
 * 	后面推荐一个地区配置一个服务器，然后根据如上标识，做请求区分
 * </p>
 * <p>
 * 	这里使用nio来做请求接收，会有断包、粘包问题，
 * 	目前已采用二次读的方式解决，如果另有问题，可以参考此博客
 * 	<a href='https://blog.csdn.net/yh_zeng2/article/details/80885867'>https://blog.csdn.net/yh_zeng2/article/details/80885867</a>
 * </p>
 * @ClassName: MonitorServer
 * @Description: 定时监测服务器
 * @author LW
 * @date 2018年6月25日 下午3:38:45
 */
public class MonitorServer {
	
	private static final Logger LOG = LoggerFactory.getLogger(MonitorServer.class);
	
	private Selector selector;
	
	private static final Charset charset = Charset.forName("UTF-8");

    private static final Map<String, Control> controls = new ConcurrentHashMap<String, Control>();

	public MonitorServer(int port) throws IOException {
		ServerSocketChannel server = ServerSocketChannel.open();
		// 绑定端口
		server.bind(new InetSocketAddress(port));
		// 设置非阻塞
		server.configureBlocking(false);
		selector = Selector.open();
		server.register(selector, SelectionKey.OP_ACCEPT);
		LOG.info("MonitorServer start listen at " + port);
	}
	
	public void listener() throws IOException {
		new Thread(()->{
			while (true) {
				// 获取队列任务数，如果为0则continue
	            int wait = 0;
				try {
					wait = selector.select();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
	            if (wait == 0) continue;
	            Set<SelectionKey> selectionKeys = selector.selectedKeys();
	            Iterator<SelectionKey> it = selectionKeys.iterator();
	            while (it.hasNext()) {
	                SelectionKey key = it.next();
	                // 去除队列任务
	                it.remove();
	                // 业务逻辑
	                try {
						process(key);
					} catch (IOException e) {
						e.printStackTrace();
					}
	            }
	        }
		}).start();
	}
	
	private void process(SelectionKey key) throws IOException {
		// 接收客户端连接
        if (key.isAcceptable()) {
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel client = server.accept();
            client.configureBlocking(false);
            // 设置读模式
            client.register(selector, SelectionKey.OP_READ);
            key.interestOps(SelectionKey.OP_ACCEPT);
        }
        // 读取客户端消息
        if (key.isReadable()) {
            SocketChannel client = (SocketChannel) key.channel();
            ByteBuffer buff = ByteBuffer.allocate(1024);
            StringBuilder content = new StringBuilder();
            int c = 0;
            try {
            	if ((c = client.read(buff)) > 0) {
            		buff.flip();
                    content.append(charset.decode(buff));
            	}
            	key.interestOps(SelectionKey.OP_READ);
            	// 如果没有接受到消息，说明客户端连接已关闭
                if (c == -1) {
                	client.close();
                	return;
                }
                if (content.length() < 11) {
                	return;
                }
                String str = content.toString();
                // 判断是否是第一次连接，如果是就注册客户端到controls
                // 这里根据字符长度来判断电池组标识
                if (content.length() == 11) {
                    Control control = new Control();
                    control.setServer(client);
                    controls.put(str, control);
                    LOG.info("电池组注册，标识为：" + str);
                } else {	// 获取命令请求，判断发送的命令还是电池组返回的数据
                    String[] cons = StringUtils.split(str, "---->");
                    // 发送命令
                    if (ArrayUtils.getLength(cons) == 2) {
                    	Control control = controls.get(cons[0]);
                        if (control == null) return;
                        send(control, cons[1]);
                        // 将命令发送给对应电池组
                        control.setClient(client);
                        LOG.info("发送命令到：" + cons[0] + "，命令为：" +cons[1]);
                    // 接收电池组返回的数据
                    } else if (ArrayUtils.getLength(cons) == 1) {
                    	String address = str.substring(0, 11);
                        Control control = controls.get(address);
                    	if (control == null) return;
                        int limit = buff.limit();
                        byte[] bytes = new byte[limit];
                        for (int i = 11; i < limit; i++) {
                            bytes[i] = buff.get(i);
                        }
                        String hex = StringUtil.bytesToHex(bytes).substring(22);
                        int len = NumberUtil.hexToDec(hex.substring(4, 6)) * 2 + 6;
                    	if (len > hex.length()) {
                    		String giveData = giveData();
                    		hex = hex + giveData;
                        }
                        // 往对应客户端发送接收到的数据
                        receive(control, hex);
                        LOG.info("接收" + address + "电池组数据，内容为：" + hex);
                    }
                }
            } catch (IOException e) {
                key.cancel();
                if (key.channel() != null) {
                    key.channel().close();
                }
            }
        }
    }
	
	private String giveData() throws IOException {
		while (true) {
			// 获取队列任务数，如果为0则continue
            int wait = 0;
			wait = selector.select();
            if (wait == 0) continue;
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> it = selectionKeys.iterator();
            while (it.hasNext()) {
                SelectionKey key = it.next();
                // 去除队列任务
                it.remove();
                // 业务逻辑
                if (key.isReadable()) {
                	SocketChannel client = (SocketChannel) key.channel();
                    ByteBuffer buff = ByteBuffer.allocate(512);
                    int c = 0;
                    try {
                    	if ((c = client.read(buff)) > 0) {
                    		buff.flip();
                    	}
                    	key.interestOps(SelectionKey.OP_READ);
                    	// 如果没有接受到消息，说明客户端连接已关闭
                        if (c == -1) {
                        	client.close();
                        	return "";
                        }
                        int limit = buff.limit();
                        byte[] bytes = new byte[limit];
                        buff.get(bytes);
                        return StringUtil.bytesToHex(bytes);
                    } catch (IOException e) {
                        key.cancel();
                        if (key.channel() != null) {
                            key.channel().close();
                        }
                    }
                }
            }
        }
	}

    private void receive(Control control, String hex) {
        SocketChannel client = control.getClient();
        try {
        	ByteBuffer buff = charset.encode(hex);
        	System.out.println("缓存长度为：" + buff.limit());
        	buff = charset.encode(buff.limit() + "<----" + hex);
            client.write(buff);
            buff.clear();
        } catch (Exception e) {
            System.out.println("出错了！");
        }
    }

    private void send(Control control, String content) {
        SocketChannel server = control.getServer();
        try {
            byte[] bytes = StringUtil.toBytes(content);
            ByteBuffer buff = ByteBuffer.wrap(bytes);
            server.write(buff);
        } catch (Exception e) {
            System.out.println("出错了！");
        }
    }
    
}