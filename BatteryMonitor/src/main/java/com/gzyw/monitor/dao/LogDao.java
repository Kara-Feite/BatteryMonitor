package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.Log;

/**
 * 
 * @ClassName: LogDao
 * @Description: 日志持久层
 * @author LW
 * @date 2018年7月9日 下午2:58:39
 */
public interface LogDao extends BaseDao<Log> {

	/**
	 * 
	 * @Title: countLog
	 * @Description: 日志总数
	 * @param log
	 * @return
	 */
	long countLog(Log log, String userName);

	/**
	 * 
	 * @Title: listLogByPage
	 * @Description: 日志分页数据
	 * @return
	 */
	List<Log> listLogByPage(Integer page, Integer rows, String sort, String order, Log log, String userName);

}
