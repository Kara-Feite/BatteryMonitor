package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.gzyw.monitor.common.util.StringUtil;

/**
 * 
 * @ClassName: Log
 * @Description: 日志
 * @author LW
 * @date 2018年6月4日 上午11:24:14
 */
@Entity
@Table(name = "bm_log")
public class Log implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 3679061101072718617L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 日志类型
	 */
	@Column(name = "type", length = 32, nullable = false)
	private String type;

	/**
	 * 操作人id
	 */
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * IP地址
	 */
	@Column(name = "ip_addr", length = 32, nullable = false)
	private String ipAddr;

	/**
	 * 详情
	 */
	@Column(name = "description", length = 255)
	private String description;

	/**
	 * 参数
	 */
	@Column(name = "params", length = 2048)
	private String params;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * @Title: Log
	 * @Description: Log构造函数
	 */
	public Log() {
	}

	/**
	 * @Title: Log
	 * @Description: Log构造函数
	 * @param type
	 * @param userName
	 * @param ipAddr
	 * @param description
	 * @param createTime
	 */
	public Log(String type, String ipAddr, String description, Date createTime) {
		this.type = type;
		this.ipAddr = ipAddr;
		this.description = description;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getUser
	 * @Description: 获取user
	 * @return: User user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @Title: setUser
	 * @Description: 设置user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @Title: getIpAddr
	 * @Description: 获取ipAddr
	 * @return: String ipAddr
	 */
	public String getIpAddr() {
		return ipAddr;
	}

	/**
	 * @Title: setIpAddr
	 * @Description: 设置ipAddr
	 */
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	/**
	 * @Title: getDescription
	 * @Description: 获取description
	 * @return: String description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @Title: setDetails
	 * @Description: 设置description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @Title: getParams
	 * @Description: 获取params
	 * @return: String params
	 */
	public String getParams() {
		return params;
	}

	/**
	 * @Title: setParams
	 * @Description: 设置params
	 */
	public void setParams(String params) {
		this.params = params;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 
	 * @Title: setMapToParams
	 * @Description: 设置请求参数
	 * @param paramMap
	 */
	public void setMapToParams(Map<String, String[]> mapParam) {
		if (mapParam == null) {
			return;
		}
		StringBuilder params = new StringBuilder();
		for (Map.Entry<String, String[]> param : ((Map<String, String[]>) mapParam).entrySet()) {
			params.append((StringUtil.EMPTY.equals(params.toString()) ? StringUtil.EMPTY : "&") + param.getKey() + "=");
			String paramValue = (param.getValue() != null && param.getValue().length > 0 ? param.getValue()[0] : StringUtil.EMPTY);
			params.append(StringUtils.endsWithIgnoreCase(param.getKey(), "password") ? StringUtil.EMPTY : paramValue);
		}
		this.params = params.toString();
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Log [id=" + id + ", type=" + type + ", user=" + user + ", ipAddr=" + ipAddr + ", description="
				+ description + ", params=" + params + ", createTime=" + createTime + "]";
	}

}
