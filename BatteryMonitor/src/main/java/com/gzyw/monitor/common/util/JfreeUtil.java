package com.gzyw.monitor.common.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.Year;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: JfreeUtil
 * @Description: JFreeChart工具类
 * @author LW
 * @date 2018年9月18日 下午1:29:33
 */
public class JfreeUtil {

	/**
	 * 
	 * @Title: createLineDataset
	 * @Description: 创建折线图数据
	 * @param list
	 * @param name
	 * @return
	 */
	private static XYDataset createLineDataset(List<TestData> list, String name) {
		XYSeries localXYSeries = new XYSeries(name);
		if (list != null) {
			double x = 1;
			for (TestData testData : list) {
				localXYSeries.add(x++, testData.getVoltage());
			}
		}
		XYSeriesCollection localXYSeriesCollection = new XYSeriesCollection();
		localXYSeriesCollection.addSeries(localXYSeries);
		return localXYSeriesCollection;
	}

	/**
	 * 
	 * @Title: createDataset
	 * @Description: 创建柱状图数据
	 * @param list
	 * @return
	 */
	private static IntervalXYDataset createBarDataset(List<TestData> list, String name) {
		TimeSeries timeSeries = new TimeSeries(name, "序号", "Count");
		if (list != null) {
			int i = 1;
			for (TestData testData : list) {
				timeSeries.add(new Year(i), testData.getVoltage());
				i++;
			}
		}
		TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection(timeSeries);
		return timeSeriesCollection;
	}
	
	/**
	 * 
	 * @Title: createLineChart
	 * @Description: 创建折线图
	 * @param paramXYDataset
	 * @return
	 */
	public static JFreeChart createLineChart(List<TestData> list, String name){
		// 处理中文乱码
		StandardChartTheme chartTheme = new StandardChartTheme("CN");
		chartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 16)); // 标题
		chartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 16)); // 图例
		chartTheme.setLargeFont(new Font("宋体", Font.PLAIN, 16)); // 轴向字体
		ChartFactory.setChartTheme(chartTheme);
		String unit = "电压".equals(name) ? "V" : "mΩ";
	    JFreeChart localJFreeChart = ChartFactory.createXYLineChart(name + "走势图", "最近记录", "单位(/"+unit+")", 
	    		createLineDataset(list, name), PlotOrientation.VERTICAL, true, true, false);
	    XYPlot localXYPlot = (XYPlot)localJFreeChart.getPlot();
	    localXYPlot.setDomainPannable(true);
	    localXYPlot.setRangePannable(true);
	    XYLineAndShapeRenderer localXYLineAndShapeRenderer = (XYLineAndShapeRenderer) localXYPlot.getRenderer();
	    localXYLineAndShapeRenderer.setBaseShapesVisible(true);
	    localXYLineAndShapeRenderer.setBaseShapesFilled(true);
	    NumberAxis numberAxis = (NumberAxis)localXYPlot.getRangeAxis();
	    // 设置y轴刻度
 		numberAxis.setAutoTickUnitSelection(false);
 		// 设置刻度范围
 		numberAxis.setUpperBound(3);
 		numberAxis.setLowerBound(0D);
 		// 格式化刻度
 		numberAxis.setNumberFormatOverride(new DecimalFormat("0.0"));
 		// 设置刻度间隔
 		numberAxis.setTickUnit(new NumberTickUnit(0.3));
	    return localJFreeChart;
	  }

	/**
	 * 
	 * @Title: createChart
	 * @Description: 创建统计图
	 * @param list
	 * @param limit
	 * @return
	 */
	public static JFreeChart createXYBarChart(List<TestData> list, Double limit, String name) {
		// 处理中文乱码
		StandardChartTheme chartTheme = new StandardChartTheme("CN");
		chartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 16)); // 标题
		chartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 16)); // 图例
		chartTheme.setLargeFont(new Font("宋体", Font.PLAIN, 16)); // 轴向字体
		ChartFactory.setChartTheme(chartTheme);
		String unit = "电压".equals(name) ? "V" : "mΩ";
		JFreeChart localJFreeChart = ChartFactory.createXYBarChart("单体" + name, "序号", true, "单位(/" + unit + ")",
				createBarDataset(list, name), PlotOrientation.VERTICAL, true, false, false);
		XYPlot localXYPlot = (XYPlot) localJFreeChart.getPlot();
		// 设置上面出现数据值
		/*
		 * XYBarRenderer localXYBarRenderer = (XYBarRenderer) localXYPlot.getRenderer();
		 * localXYBarRenderer.setBaseItemLabelsVisible(true);
		 * localXYBarRenderer.setBaseItemLabelGenerator(new
		 * StandardXYItemLabelGenerator());
		 * localXYBarRenderer.setBasePositiveItemLabelPosition(new
		 * ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER));
		 */
		// 设置日期显示格式
		XYPlot plot = localJFreeChart.getXYPlot();
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		if (list.size() >= 100) {
			axis.setDateFormatOverride(new SimpleDateFormat("yyy"));
		} else {
			axis.setDateFormatOverride(new SimpleDateFormat("yy"));
		}
		// 设置水平线(告警线)
		if (limit != null) {
			ValueMarker valuemarker = new ValueMarker(limit); // 水平线的值
			valuemarker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
			valuemarker.setPaint(Color.red); // 线条颜色
			valuemarker.setStroke(new BasicStroke(2.0F)); // 粗细
			valuemarker.setLabelFont(new Font("SansSerif", 0, 11)); // 文本格式
			valuemarker.setLabelPaint(Color.red);
			valuemarker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
			valuemarker.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);
			plot.addRangeMarker(valuemarker);
		}
		/*XYBarRenderer reader = (XYBarRenderer) plot.getRenderer();
		reader.setSeriesPaint(0, Color.decode("#24F4DB"));
		reader.setSeriesPaint(0, Color.decode("#7979FF"));
		reader.setSeriesPaint(0, Color.decode("#FF5555"));*/
		// 设置y轴刻度
		NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
		numberAxis.setAutoTickUnitSelection(false);
		// 设置刻度范围
		numberAxis.setUpperBound(limit + 0.2);
		numberAxis.setLowerBound(0D);
		// 格式化刻度
		numberAxis.setNumberFormatOverride(new DecimalFormat("0.0"));
		// 设置刻度间隔
		numberAxis.setTickUnit(new NumberTickUnit(0.1));
		// 设置时间间隔
		DateAxis localDateAxis = (DateAxis) localXYPlot.getDomainAxis();
		localDateAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		localDateAxis.setLowerMargin(0.01D);
		localDateAxis.setUpperMargin(0.01D);
		ChartUtilities.applyCurrentTheme(localJFreeChart);
		return localJFreeChart;
	}

}
