<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>编辑节点</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
input{width:220px;}
a{width: 80px;}
.div-group, .div-group *{vertical-align:middle;}
</style>
<script>
function edit(){
	$('#myForm').form('submit',{
		url : '${ctx}/tree/update',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				window.parent.parent.tree.updateNode(result.data);
				window.location.href = '${ctx}/tree/content?id=${tree.parentId}';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="tree" enctype="multipart/form-data">
		<sf:hidden path="id"/>
	    <div class="easyui-panel" title="操作：编辑节点信息" style="padding:30px 60px;">
			<div style="margin-bottom:20px">			
			 	<sf:input path="name" class="easyui-textbox" 
			 	data-options="label:'名 称:',required:true"/><br><br>
			 	<sf:input path="remark" class="easyui-textbox" 
			 		data-options="label:'备 注:'"/><br><br>
			 	<div class='div-group'><label for='isGroup'>是否为电池组节点</label>
			 		<input type="checkbox" style='width:20px;' id='isGroup' name='isGroup' value='true' <c:if test="${readOnly}">disabled</c:if>></div><br>
            	<a href="javascript:void(0)" class="easyui-linkbutton" 
            		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a> 
            	&nbsp;&nbsp;&nbsp;&nbsp;
        		<a href="${ctx}/tree/content?id=${tree.parentId}" class="easyui-linkbutton" 
        			data-options="iconCls:'icon-reload'">返 回</a>      		
	 		</div>
		</div>    						
	</sf:form>
	<script>
		if ('${tree.url}'.indexOf('group') > 0) {
			document.getElementById('isGroup').checked = true;
		}
	</script>
</body>
</html>