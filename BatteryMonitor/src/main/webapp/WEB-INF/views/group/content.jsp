<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池组</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<c:set var="id" value="${tree.id}"/>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>
#denglu *{vertical-align:middle;}
td{font-size:12px;}
</style>
<script>
$(function(){
	var IsCheckFlag = true;
	$('#group_lists').datagrid({
		url:'${ctx}/group/datagrid?parentId=${id}',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		sortName : 'name',
		sortOrder : 'ASC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[
			{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'groupName',
		        title:'名 称',
		        width:50,
		        align:'center'
			},{
		        field:'address',
		        title:'电池组标识',
		        width:60,
		        align:'center'
			},{
		        field:'number',
		        title:'电 池 数',
		        width:50,
		        align:'center',
		        sortable:true
			},{
		        field:'intervals',
		        title:'测试间隔',
		        width:50,
		        align:'center'
			},{
		        field:'unit',
		        title:'间隔单位',
		        width:50,
		        align:'center',
	        	formatter : function (val, row) {
	    			if (val === 1) {
	    				return '秒';
	    			} else if (val === 2) {
	    				return '分钟';
	    			} else if (val === 3) {
	    				return '小时';
	    			} else if (val === 4) {
	    				return '天';
	    			} else if (val === 5) {
	    				return '固定小时';
	    			}
	    		}
			},{
		        field:'warn',
		        title:'当前告警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
		        field:'plan',
		        title:'当前预警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
				field : 'remark',
				title : '备注',
				align:'center',
				width : 50,
				formatter : formatNull
			},{
				field:'createTime',
				title:'创建时间',
				width: 80,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			},{
		        field:'isRun',
		        title:'监测状态',
		        width:50,
		        align:'center',
				formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">已运行</font>';
					}else{
						return '<font style="color:red">未运行</font>';
					}
				}
			},{
				field : 'state',
				title : '状 态',
				width:50,
				align:'center',
				formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">正 常</font>';
					}else{
						return '<font style="color:red">冻 结</font>';
					}
				}
			}
		]],
		toolbar:[		
			{text:'编 辑',iconCls:'icon-edit',handler:function(){update();}},'-',					
			/* {text:'图 形',iconCls:'icon-search',handler:function(){graph();}},'-', */
			/* {text:'测 试',iconCls:'icon-filter',handler:function(){test();}},'-', */
			{text:'冻 结',iconCls:'icon-remove',handler:function(){congeal();}},'-',
			{text:'解 冻',iconCls:'icon-reload',handler:function(){thaw();}},'-',
			{text:'开启监测',iconCls:'icon-remove',handler:function(){start();}},'-',
			{text:'关闭监测',iconCls:'icon-reload',handler:function(){stop();}},'-',
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		],
		onDblClickRow : clickRow,
		onClickCell: function (rowIndex, field, value) {
		    IsCheckFlag = false;
		},
		onSelect: function (rowIndex, rowData) {
		    if (!IsCheckFlag) {
		        IsCheckFlag = true;
		        $("#group_lists").datagrid("unselectRow", rowIndex);
		    }
		},                    
		onUnselect: function (rowIndex, rowData) {
		    if (!IsCheckFlag) {
		        IsCheckFlag = true;
		        $("#group_lists").datagrid("selectRow", rowIndex);
		    }
		}
	});
});
function clickRow(index, row) {
	window.parent.location.href="${ctx}/group/frame?id="+row.id;
}
function query(){
	$('#group_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function clean(){
	$('#group_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}
/* function test() {
	var rows = $('#group_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/group/test?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要测试的数据'});
	}
} */
function update(){
	<permission:notPermission name="tree:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#group_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/group/edit?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}

function start(){
	var rows=$('#group_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要开启监测选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}
				$.ajax({
					type:'post',
					url:'${ctx}/group/start',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#group_lists').datagrid('load',{});
						$('#group_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要开启监测的记录'});
	}
}

function stop(){
	var rows=$('#group_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要关闭监测选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}
				$.ajax({
					type:'post',
					url:'${ctx}/group/stop',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#group_lists').datagrid('load',{});
						$('#group_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要关闭监测的记录'});
	}
}

function congeal(){
	<permission:notPermission name="tree:congeal">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#group_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要冻结选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/tree/congeal',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#group_lists').datagrid('load',{});
						$('#group_lists').datagrid('unselectAll');
						window.parent.parent.tree.hideNodes(r.data);
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要冻结的记录'});
	}
}
function thaw(){
	<permission:notPermission name="tree:thaw">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#group_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要解冻选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/menu/thaw',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#group_lists').datagrid('load',{});
						$('#group_lists').datagrid('unselectAll');
						window.parent.parent.tree.showNodes(r.data);
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要解冻的记录'});
	}
}
function unselect(){
	$('#group_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}

/* function graph(){
	var rows = $('#group_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/group/frame?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择查看的对象'});
	}
} */
</script>
</head>

<body>
<div class="easyui-layout" fit="true">
	<div data-options="region:'north',title:'节 点：${tree.name }'" style="height:100px;">
		<form id="search_form">
			<table cellSpacing="1" cellPadding="1" width="100%" bgColor="#eeeeee" 
	    		style="border:0px solid #8ba7e3" border="0">						    
			    <tr bgColor="#f5fafe" class="ta_01">
			    	<td width="25%">
			    	 	<strong>&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;称&nbsp;&nbsp;</strong>
			    	 	<font color="#FF0000">*</font></td>
			         <td width="25%" bgColor="#ffffff">${tree.name}</td>
			    	<td width="25%">
			    		<strong>&nbsp;&nbsp;描&nbsp;&nbsp;&nbsp;&nbsp;述&nbsp;&nbsp;</strong>
			    		<font color="#FF0000">*</font></td>
			         <td width="25%" bgColor="#ffffff">${empty tree.remark ? '无' : tree.remark}</td>
			    </tr>	    	   				       		    
			     <tr bgColor="#f5fafe" class="ta_01">
			    	 <td>
			    	 	<strong>当前告警&nbsp;&nbsp;</strong>
			    	 	<font color="#FF0000">*</font></td>
			         <td bgColor="#ffffff">${tree.warn}</td>		   
			    	 <td>
			    	 	<strong>当前预警&nbsp;&nbsp;</strong>
			    	 	<font color="#FF0000">*</font></td>
			         <td bgColor="#ffffff">${tree.plan}</td>
			    </tr>	 	   	 			    		      		    											
				<tr height="25" bgcolor="#f5fafe">
					<td id="denglu" colSpan="4">
						<img src="${ctxStatic}/images/yin.gif" width="22">							
						<strong>名 称：</strong><input name="name" size="15"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
							onclick="query();" href="javascript:void(0);">查  询</a>						
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
							onclick="clean();" href="javascript:void(0);">清  空</a>
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-add'" 
							target="groupFrame" href="${ctx}/group/save?treeId=${tree.id}">添 加</a>															
					</td>					
				</tr>					
			</table>
		</form> 
	</div>
	<!-- ------------------------------------------ -->
	<div data-options="region:'center'">
		<table id="group_lists"></table>
	</div>
</div>
</body>
</html>