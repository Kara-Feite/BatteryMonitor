package com.gzyw.monitor.service;

import java.util.Date;
import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.TestData;

/**
 * 
 * @ClassName: TestDataService
 * @Description: 测试数据服务层
 * @author LW
 * @date 2018年6月28日 下午4:42:36
 */
public interface TestDataService {

	/**
	 * 
	 * @Title: saveData
	 * @Description: 添加测试数据
	 * @param voltage
	 * @param inter
	 * @param tempera
	 * @param group
	 */
	void saveData(String voltage, String inter, String tempera, Group group, Date date);

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取测试数据分页信息
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或者降序
	 * @param temp 临时变量
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, TempDto temp);

	/**
	 * 
	 * @Title: delTestDataByBat
	 * @Description: 批量删除测试数据
	 * @param ids
	 * @return
	 */
	ResultDto delTestDataByBat(String ids);

	/**
	 * 
	 * @Title: getMaxMinDto
	 * @Description: 获取相关最大值和最小值
	 * @param temp
	 * @return
	 */
	MaxMinDto getMaxMinDto(TempDto temp);

	/**
	 * 
	 * @Title: getGraphDto
	 * @Description: 获取图形数据
	 * @param temp
	 * @return
	 */
	GraphDto getGraphDto(TempDto temp);
	
	/**
	 * 
	 * @Title: saveTestData
	 * @Description: 添加手动测试数据
	 * @param voltage
	 * @param inter
	 * @param tempera
	 * @param group
	 * @return
	 */
	GraphDto saveTestData(String voltage, String inter, String tempera, Group group, Date date);

	/**
	 * 
	 * @Title: listTestDataByTime
	 * @Description: 根据时间获取测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByTime(TempDto temp);

	/**
	 * 
	 * @Title: listTestDataBetTime
	 * @Description: 查询时间段内的测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataBetTime(TempDto temp);

	/**
	 * 
	 * @Title: getGraphDtoByBat
	 * @Description: 获取电池统计图
	 * @param temp
	 * @return
	 */
	GraphDto getGraphDtoByBat(TempDto temp);
	
	/**
	 * 
	 * @Title: listTestDataByExport
	 * @Description: 获取电池组导出数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByExport(TempDto temp);

	/**
	 * 
	 * @Title: delTestDataByGroup
	 * @Description: 根据电池组删除测试数据
	 * @param groupId
	 */
	void delTestDataByGroup(Integer groupId);

	/**
	 * 
	 * @Title: listTestDataByGroup
	 * @Description: 更加测试数据和电池组获取测试数据
	 * @param temp
	 * @return
	 */
	List<TestData> listTestDataByGroup(TempDto temp);

	/**
	 * 
	 * @Title: getChartByGroup
	 * @Description: 获取电池组统计图
	 * @param temp
	 * @param name
	 * @return
	 */
	byte[] getChartByGroup(TempDto temp, String name);

	/**
	 * 
	 * @Title: getChartByBat
	 * @Description: 获取电池测试信息走势图
	 * @param temp
	 * @param name
	 * @return
	 */
	byte[] getChartByBat(TempDto temp, String name);

}
