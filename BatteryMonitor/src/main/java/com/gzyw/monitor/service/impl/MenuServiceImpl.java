package com.gzyw.monitor.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.dao.MenuDao;
import com.gzyw.monitor.entity.Menu;
import com.gzyw.monitor.service.MenuService;

/**
 * 
 * @ClassName: MenuServiceImpl
 * @Description: 菜单服务层接口实现类
 * @author LW
 * @date 2018年7月4日 上午10:32:50
 */
@Service
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private MenuDao menuDao;

	@Override
	public List<Menu> listMenuByUserId(Integer id, boolean isRoot) {
		List<Menu> listMenu = menuDao.listMenuByUserId(id, isRoot);
		listMenu = new ArrayList<Menu>(new LinkedHashSet<Menu>(listMenu));
		return listMenu;
	}

	@Override
	public List<Menu> listMenuByAll() {
		return menuDao.listMenuByAll();
	}

}
