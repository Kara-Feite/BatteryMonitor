package com.gzyw.monitor.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @ClassName: MapUtil
 * @Description: Map工具类
 * @author LW
 * @date 2018年6月13日 下午1:44:09
 */
public class MapUtil {
	
	/**
	 * 
	 * @Title: isEmpty
	 * @Description: 判断Map是否为空
	 * @param map
	 * @return
	 */
	public static boolean isEmpty(final Map<?,?> map) {
        return map == null || map.isEmpty();
    }
	
	/**
	 * 
	 * @Title: isNotEmpty
	 * @Description: 判断Map不为空
	 * @param map
	 * @return
	 */
	public static boolean isNotEmpty(final Map<?,?> map) {
        return !isEmpty(map);
    }
	
	/**
	 * 
	 * @Title: getMap
	 * @Description: 获取初始化一个HashMap
	 * @param k 键
	 * @param v 值
	 * @return HashMap
	 */
	public static <K, V> Map<K, V> getMap(K k, V v) {
		Map<K, V> map = new HashMap<K, V>();
		map.put(k, v);
		return map;
	}
	
	/**
	 * 格式：{字段名 : 字段值}
	 * @Title: getMapData
	 * @Description: 将对象obj所有字段值转化为HashMap
	 * @param obj
	 * @return
	 */
	public static Map<String, Object> getMapData(final Object obj) {
		if (obj == null) {
			throw new IllegalArgumentException("The object must not be null");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Class<? extends Object> clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		Method[] methods = clazz.getDeclaredMethods();
		if (ArrayUtil.isEmpty(fields) || ArrayUtil.isEmpty(methods)) {
			return map;
		}
		for (Field field : fields) {
			String name = field.getName();
			for (Method method : methods) {
				if (("get" + StringUtil.upperFirstCase(name)).equals(method.getName())) {
					try {
						Object val = method.invoke(obj);
						if (val != null && StringUtil.isNotEmpty(val.toString())) {
							map.put(name, val);
						}
						continue;
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return map;
	}

}
