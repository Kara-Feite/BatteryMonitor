package com.gzyw.monitor.service;

import java.util.Date;

import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;

/**
 * 
 * @ClassName: GroupInfoService
 * @Description: 电池组信息服务层
 * @author LW
 * @date 2018年6月26日 下午4:36:11
 */
public interface GroupInfoService {

	/**
	 * 
	 * @Title: saveGroupInfo
	 * @Description: 添加组信息(系统信息)
	 * @param sysInfo
	 * @param group
	 * @param date
	 * @return
	 */
	boolean saveGroupInfo(String sysInfo, Group group, Date date);

	/**
	 * 
	 * @Title: getGroupInfoByNow
	 * @Description: 获取最新电池组信息
	 * @param id
	 * @return
	 */
	GroupInfo getGroupInfoByNow(Integer id);


}
