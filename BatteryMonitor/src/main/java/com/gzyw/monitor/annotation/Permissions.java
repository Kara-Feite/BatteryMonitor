package com.gzyw.monitor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @ClassName: Permissions
 * @Description: 权限标识
 * @author LW
 * @date 2018年7月18日 下午4:18:45
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Permissions {
	
	/**
	 * 
	 * @Title: value
	 * @Description: 权限
	 * @return
	 */
	String value() default "";

}
