package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.UserDao;
import com.gzyw.monitor.entity.User;

/**
 * 
 * @ClassName: UserDaoImpl
 * @Description: 用户Dao实现类
 * @author LW
 * @date 2018年6月6日 下午8:31:10
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	@Override
	public User getUserByName(String name) {
		String hql = "FROM User WHERE userName = ?";
		return get(hql, name);
	}

	@Override
	public List<User> listUserByPage(Integer page, Integer rows, String sort, String order, User user) {
		String hql = "FROM User";
		String name = user.getNickName();
		if (StringUtil.isNotEmpty(name)) {
			hql += " WHERE nickName like '%" + name + "%'";
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			hql += " ORDER BY " + sort + " " + order;
		}
		return queryByPage(hql, page, rows);
	}

	@Override
	public Long countUser() {
		String hql = "SELECT COUNT(*) FROM User";
		return count(hql);
	}

	@Override
	public Integer updateUser(User user) {
		String hql = "UPDATE User SET nickName = ?, userName = ?, phone = ?,"
				+ " email = ? WHERE id = ?";
		return update(hql, user.getNickName(), user.getUserName(), user.getPhone(),
				user.getEmail(), user.getId());
	}

	@Override
	public Integer updateState(int id, boolean state) {
		String hql = "UPDATE User SET state = ? WHERE id = ?";
		return update(hql, state, id);
	}

	@Override
	public Integer changePwd(String password, Integer id) {
		String hql = "UPDATE User SET password = ? WHERE id = ?";
		return update(hql, password, id);
	}

	@Override
	public List<User> listUserByAll() {
		String hql = "FROM User";
		return queryForList(hql);
	}

}
