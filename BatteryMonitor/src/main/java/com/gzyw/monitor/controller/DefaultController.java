package com.gzyw.monitor.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 
 * @ClassName: DefaultController
 * @Description: 404错误拦截
 * @author LW
 * @date 2018年7月17日 下午5:10:45
 */
public class DefaultController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("error/404");
	}
	
}