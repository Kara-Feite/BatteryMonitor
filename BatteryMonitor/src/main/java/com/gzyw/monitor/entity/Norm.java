package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @ClassName: Norm
 * @Description: 电池规格
 * @author LW
 * @date 2018年6月4日 下午1:36:05
 */
@Entity
@Table(name = "bm_norm")
public class Norm implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -3572052030780325385L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 生产厂家
	 */
	@Column(name = "maker", length = 64, nullable = false)
	@NotEmpty(message = "制造商不能为空")
	private String maker;

	/**
	 * 型号
	 */
	@Column(name = "model", length = 64, nullable = false)
	@NotEmpty(message = "型号不能为空")
	private String model;

	/**
	 * 电池类型
	 */
	@Column(name = "type", length = 32, nullable = false)
	@NotEmpty(message = "电池类型不能为空")
	private String type;

	/**
	 * 参考电压
	 */
	@Column(name = "voltage", nullable = false)
	private Double voltage;

	/**
	 * 参考电阻
	 */
	@Column(name = "inter", nullable = false)
	private Double inter;

	/**
	 * 参考容量
	 */
	@Column(name = "volume", nullable = false)
	private Double volume;

	/**
	 * 投运时间
	 */
	@Column(name = "begin_time", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date beginTime;
	
	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 对应组
	 */
	@OneToOne(mappedBy = "norm", fetch= FetchType.EAGER)
	@JsonIgnore
	private Group group;

	/**
	 * @Title: Norm
	 * @Description: Norm构造函数
	 */
	public Norm() {}

	/**
	 * @Title: Norm
	 * @Description: Norm构造函数
	 * @param maker
	 * @param model
	 * @param type
	 * @param voltage
	 * @param inter
	 * @param volume
	 * @param remark
	 * @param beginTime
	 */
	public Norm(String maker, String model, String type, Double voltage, Double inter, Double volume, Date beginTime) {
		this.maker = maker;
		this.model = model;
		this.type = type;
		this.voltage = voltage;
		this.inter = inter;
		this.volume = volume;
		this.beginTime = beginTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getMaker
	 * @Description: 获取maker
	 * @return: String maker
	 */
	public String getMaker() {
		return maker;
	}

	/**
	 * @Title: setMaker
	 * @Description: 设置maker
	 */
	public void setMaker(String maker) {
		this.maker = maker;
	}

	/**
	 * @Title: getModel
	 * @Description: 获取model
	 * @return: String model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @Title: setModel
	 * @Description: 设置model
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: Double voltage
	 */
	public Double getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getInter
	 * @Description: 获取inter
	 * @return: Double inter
	 */
	public Double getInter() {
		return inter;
	}

	/**
	 * @Title: setInter
	 * @Description: 设置inter
	 */
	public void setInter(Double inter) {
		this.inter = inter;
	}

	/**
	 * @Title: getVolume
	 * @Description: 获取volume
	 * @return: Double volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @Title: setVolume
	 * @Description: 设置volume
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @Title: getBeginTime
	 * @Description: 获取beginTime
	 * @return: Date beginTime
	 */
	public Date getBeginTime() {
		return beginTime;
	}

	/**
	 * @Title: setBeginTime
	 * @Description: 设置beginTime
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	
	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getGroup
	 * @Description: 获取group
	 * @return: Group group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @Title: setGroup
	 * @Description: 设置group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "Norm [id=" + id + ", maker=" + maker + ", model=" + model + ", type=" + type + ", voltage=" + voltage
				+ ", inter=" + inter + ", volume=" + volume + ", beginTime="+ beginTime + ", updateTime=" + 
					updateTime + ", group=" + group + "]";
	}

}
