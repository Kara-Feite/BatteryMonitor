package com.gzyw.monitor.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @ClassName: StringUtil
 * @Description: 字符串处理工具类
 * @author LW
 * @date 2018年5月28日 下午3:00:11
 */
public class StringUtil {

	/**
	 * 空字符串
	 */
	public static final String EMPTY = "";
	
	/**
	 * 下划线
	 */
	private static final String UNDERLINE = "_";
	
	/**
	 * 中线
	 */
	private static final String MIDLINE = "-";
	
	/**
	 * utf-8字符长
	 */
	private static final String UTF8 = "UTF-8";

	/**
	 * HEX_CHAR
	 */
	private static final char[] HEX_CHAR = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
			'f' };

	/**
	 * 
	 * @Title: isEmpty
	 * @Description: 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(final String str) {
		return null == str || str.length() == 0 || EMPTY.equals(str) || str.matches("\\s*");
	}

	/**
	 * 
	 * @Title: isNotEmpty
	 * @Description: 判断字符串是否为非空
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(final String str) {
		return !isEmpty(str);
	}
	
	/**
	 * 
	 * @Title: matches
	 * @Description: 正则匹配
	 * @param str
	 * @param regex
	 * @return
	 */
	public static boolean matches(final String str, final String regex) {
		if (StringUtil.isEmpty(str)) {
			return false;
		}
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(str).matches();		
	}

	/**
	 * 
	 * @Title: getStringRamdom
	 * @Description: 生成任意长度随机字符
	 * @param length
	 * @return
	 */
	public static String getStringRamdom(final int length) {
		StringBuilder val = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
			val.append(random.nextInt(26) + temp);
		}
		return val.toString();
	}

	/**
	 * 
	 * @Title: getUUID
	 * @Description: 生成UUID
	 * @return
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replace(MIDLINE, EMPTY);
	}

	/**
	 * 
	 * @Title: bytesToHex
	 * @Description: byte[]转hex(16进制)字符串
	 * @param bytes
	 * @return
	 */
	public static String bytesToHex(byte[] bytes) {
		char[] buf = new char[bytes.length * 2];
		int index = 0;
		// 利用位运算进行转换
		for (byte b : bytes) {
			buf[index++] = HEX_CHAR[b >>> 4 & 0xf];
			buf[index++] = HEX_CHAR[b & 0xf];
		}
		return new String(buf);
	}
	
	/**
	 * 
	 * @Title: toUtf8String
	 * @Description: 转换为utf-8字符串
	 * @param s
	 * @return
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes(UTF8);
				} catch (Exception ex) {
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @Title: toUnderScoreCase
	 * @Description: 下划线转驼峰命名
	 * @param s
	 * @return
	 */
	public static String toUnderScoreCase(String s) {
		if (isEmpty(s)) {
			return EMPTY;
		}
		StringBuilder sb = new StringBuilder();
		boolean flag = false;
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (UNDERLINE.charAt(0) == ch) {
				flag = true;
			} else {
				if (flag) {
					sb.append(Character.toUpperCase(ch));
					flag = false;
				} else {
					sb.append(ch);
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @Title: camelToUnderline
	 * @Description: 驼峰转下划线命名
	 * @param str
	 * @return
	 */
	public static String camelToUnderline(String str) {
        if (isEmpty(str)){  
            return EMPTY;  
        }
        int len = str.length();  
        StringBuilder sb = new StringBuilder(len);  
        for (int i = 0; i < len; i++) {  
            char c = str.charAt(i);  
            if (Character.isUpperCase(c)){  
                sb.append(UNDERLINE);  
                sb.append(Character.toLowerCase(c));  
            }else{  
                sb.append(c);  
            }  
        }  
        return sb.toString();
    }
	
	/**
	 * 
	 * @Title: toBytes
	 * @Description: 将16进制字符串转换为byte[]
	 * @param str
	 * @return
	 */
	public static byte[] toBytes(String str) {
		if (isEmpty(str)) {
			return new byte[0];
		}
		int len = str.length();
		byte[] bytes = new byte[len / 2];
		for (int i = 0; i < len / 2; i++) {
			String subStr = str.substring(i * 2, i * 2 + 2);
			bytes[i] = (byte) Integer.parseInt(subStr, 16);
		}
		return bytes;
	}

	/**
	 * 
	 * @Title: toTwoSpaceStr
	 * @Description: 每隔两个字符加个空格
	 * @param str
	 * @return
	 */
	public static String toTwoSpaceStr(String str) {
		String regex = "(.{2})";
		str = str.replaceAll(regex, "$1 ");
		return str.trim();
	}

	/**
	 * 
	 * @Title: trimAll
	 * @Description: 去除所有空格
	 * @param str
	 * @return
	 */
	public static String trimAll(String str) {
		return str.replaceAll("\\s+", EMPTY);
	}

	/**
	 * 
	 * @Title: upperFirstCase
	 * @Description: 首字母大写
	 * @param str
	 * @return
	 */
	public static String upperFirstCase(String str) {
		if (isEmpty(str)) {
			throw new IllegalArgumentException("The string must not be null");
		}
		char[] ch = str.toCharArray();
		// 如果首字母为大写就不转
		if (Character.isUpperCase(ch[0])) {
			return str;
		} else {
			ch[0] -= 32;
		}
		return String.valueOf(ch);
	}

	/**
	 * 
	 * @Title: lowerFirstCase
	 * @Description: 将首字母小写
	 * @param str
	 * @return
	 */
	public static String lowerFirstCase(String str) {
		if (isEmpty(str)) {
			throw new IllegalArgumentException("The string must not be null");
		}
		char[] ch = str.toCharArray();
		// 如果首字母为小写就不转
		if (Character.isLowerCase(ch[0])) {
			return str;
		} else {
			ch[0] += 32;
		}
		return String.valueOf(ch);
	}

	/**
	 * 
	 * @Title: isEquals
	 * @Description: 判断两字符串中每个字符在双方都存在
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean isEquals(final String str1, final String str2) {
		if (isEmpty(str1) || isEmpty(str2)) {
			return false;
		}
		if (str1.length() != str2.length()) {
			return false;
		}
		String[] strs = str1.split(EMPTY);
		for (int i = 0, len = str1.length(); i < len; i++) {
			if (str2.indexOf(strs[i]) == -1) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 例如: len = 4, str = "1", toStr = "0";
	 * return str = "0001";
	 * @Title: notEnoughToStr
	 * @Description: 不足len位的str字符，前面补toStr
	 * @param str
	 * @param toStr
	 * @param len
	 * @return
	 */
	public static String notEnoughToStr(final String str, final String toStr, final int len) {
		StringBuilder sb = new StringBuilder(str);
		for (int i = sb.length(); i < len; i++) {
			sb.insert(0, toStr);
		}
		return sb.toString();
	}
	
	/**
	 * 例如：
	 * String str = "aaa || bbb && ccc"
	 * String regex = "\\s*[&|]+\\s*"
	 * return ["aaa", "||", "bbb", "&&", "ccc"]
	 * @Title: getSplit
	 * @Description: 字符以指定字符或正则分割成数组，数组包括匹配的字符
	 * @param str
	 * @return
	 */
	public static String[] getSplit(final String str, final String regex) {
		String[] arr = str.split(regex);
		Pattern pattern = Pattern.compile (regex);
		Matcher matcher = pattern.matcher(str);
		List<String> list = new ArrayList<String>();
		int i = 0;
		int len = arr.length;
		boolean flag = false;
		while ((flag = matcher.find()) || i < len) {
			list.add(arr[i++]);
			if (flag) {
				list.add(matcher.group());
			}
		}
		return list.toArray(arr);
	}
	
}
