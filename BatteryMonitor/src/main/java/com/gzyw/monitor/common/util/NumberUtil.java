package com.gzyw.monitor.common.util;

/**
 * 
 * @ClassName: NumberUtil
 * @Description: 数字工具类
 * @author LW
 * @date 2018年6月27日 下午3:39:40
 */
public class NumberUtil {

	/**
	 * 
	 * @Title: hexToDec
	 * @Description: 16进制转10进制
	 * @param hex
	 * @return
	 */
	public static int hexToDec(String hex) {
		hex = hex.toUpperCase();
		int max = hex.length();
		int result = 0;
		for (int i = max; i > 0; i--) {
			char c = hex.charAt(i - 1);
			int dec = 0;
			if (c >= '0' && c <= '9') {
				dec = c - '0';
			} else {
				dec = c - 55;
			}
			result += Math.pow(16, max - i) * dec;
		}
		return result;
	}
	
	/**
	 * 不足4位前面补0
	 * @Title: decToHex
	 * @Description: 十进制转十六进制
	 * @param dec
	 * @return
	 */
	public static String decToHex(int dec) {
		String hex = Integer.toHexString(dec);
		return StringUtil.notEnoughToStr(hex, "0", 4);
	}
	
	/**
	 * 转为十进制的hex转double
	 * 默认小数点3位数
	 * @Title: hexToDouble
	 * @Description: hex转double
	 * @param hex
	 * @return
	 */
	public static double hexToDouble(String hex) {
		return hexToDouble(hex, 3);
	}
	
	/**
	 * 转为十进制的hex转double
	 * @Title: hexToDouble
	 * @Description: TODO(描述)
	 * @param hex
	 * @return
	 */
	public static double hexToDouble(String hex, int len) {
		int hexToDec = hexToDec(hex);
		String val = String.valueOf(hexToDec);
		return toDouble(val, len);
	}
	
	/**
	 * 不足八位前面补0
	 * @Title: hexToBin
	 * @Description: 16进制hex转2进制字符串
	 * @param hex
	 * @return
	 */
	public static String hexToBin(String hex) {
		int bin = Integer.parseInt(hex, 16);
		String str = Integer.toBinaryString(bin);
		return StringUtil.notEnoughToStr(str, "0", 8);
	}

	/**
	 * 
	 * @Title: isNumber
	 * @Description: 判断是否是数字
	 * @param str
	 * @return
	 */
	public static boolean isNumber(final String str) {
		return StringUtil.matches(str, "^-?[0-9]+(.[0-9]+)?$");
	}
	
	/**
	 * 
	 * @Title: isNotNumber
	 * @Description: 判断不是数字
	 * @param str
	 * @return
	 */
	public static boolean isNotNumber(final String str) {
		return !isNumber(str);
	}
	
	/**
	 * 
	 * @Title: isInteger
	 * @Description: 判断是否是整数
	 * @param str
	 * @return
	 */
	public static boolean isInteger(final String str) {
		return StringUtil.matches(str, "^[-\\+]?[\\d]*$");
	}

	/**
	 * 
	 * @Title: isNotInteger
	 * @Description: 判断不是整数
	 * @param str
	 * @return
	 */
	public static boolean isNotInteger(final String str) {
		return !isInteger(str);
	}
	
	/**
	 * 
	 * @Title: isDouble
	 * @Description: 判断是否是小数
	 * @param str
	 * @return
	 */
	public static boolean isDouble(final String str) {
		return StringUtil.matches(str, "\\d+\\.\\d+$|-\\d+\\.\\d+$");
	}
	
	/**
	 * 
	 * @Title: isNotDouble
	 * @Description: 判断不是小数
	 * @param str
	 * @return
	 */
	public static boolean isNotDouble(final String str) {
		return !isDouble(str);
	}

	/**
	 * 
	 * @Title: toDouble
	 * @Description: 将超千位的字符串数字转小数（正数）
	 * @param str
	 * @return
	 */
	public static double toDouble(String str, int length) {
		if (!isNumber(str)) {
			throw new RuntimeException("字符串内容非数字");
		}
		if (Integer.parseInt(str) < 0) {
			throw new RuntimeException("数字必须为正数");
		}
		if (length <= 0) {
			return Double.parseDouble(str);
		}
		StringBuilder sb = new StringBuilder(str);
		int len = str.length();
		if (len == length) {
			sb.insert(0, "0.");
		} else if (len < length) {
			sb.insert(0, "0.");
			for (int i = len; i <= length - 1; i++) {
				sb.insert(sb.length() - 2, "0");
			}
		} else {
			sb.insert(len - length, ".");
		}
		return Double.parseDouble(sb.toString());
	}

}
