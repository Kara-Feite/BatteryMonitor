package com.gzyw.monitor.service;

import java.util.Date;

import com.gzyw.monitor.entity.Group;

/**
 * 
 * @ClassName: GroupLimitService
 * @Description: 组限制服务层接口（参数设置）
 * @author LW
 * @date 2018年6月29日 上午9:57:30
 */
public interface GroupLimitService {

	/**
	 * 
	 * @Title: saveLimit
	 * @Description: 添加设置
	 * @param param
	 * @param group
	 * @param date
	 */
	void saveLimit(String param, Group group, Date date);

}
