package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.BatteryDao;
import com.gzyw.monitor.entity.Battery;

/**
 * 
 * @ClassName: BatteryDaoImpl
 * @Description: 电池持久层实现类
 * @author LW
 * @date 2018年6月14日 下午4:41:31
 */
@Repository
public class BatteryDaoImpl extends BaseDaoImpl<Battery> implements BatteryDao {

	@Override
	public Long countBattery(Battery battery, Integer groupId) {
		String hql = "SELECT COUNT(*) FROM Battery";
		hql = setHQLParam(hql, null, null, battery, groupId);
		return count(hql);
	}

	@Override
	public List<Battery> listBatteryByPage(Integer page, Integer rows, String sort,
			String order, Battery battery, Integer groupId) {
		String hql = "FROM Battery";
		hql = setHQLParam(hql, sort, order, battery, groupId);
		return queryByPage(hql, page, rows);
	}
	
	@Override
	public List<Battery> listBatteryByGroup(Integer groupId) {
		String hql = "FROM Battery WHERE group_id = ? ORDER BY id ASC";
		return queryForList(hql, groupId);
	}
	
	@Override
	public void deleteBatteryByGroup(Integer groupId) {
		String hql = "DELETE FROM Battery WHERE group_id = ?";
		delete(hql, groupId);
	}
	
	@Override
	public Battery getBatteryByAlarm(int groupId, String batteryName) {
		String hql = "FROM Battery WHERE group_id = ? AND name = ?";
		return get(hql, groupId, batteryName);
	}

	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置HQL参数
	 * @param hql HQL语句
	 * @param sort 排序字段
	 * @param order 升序或降序
	 * @param battery 电池对象
	 * @param groupId 电池组id
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, Battery battery, Integer groupId) {
		StringBuilder sb = new StringBuilder(hql);
		sb.append(" WHERE group_id = " + groupId);
		String name = battery.getName();
		if (StringUtil.isNotEmpty(name)) {
			sb.append(" AND name LIKE '%" + name + "%'");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY " + sort + " " + order);
		}
		return sb.toString();
	}

}
