<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>编辑用户</title>
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<style>
select,input{width:220px;}
a{width: 80px;}
</style>
<script>
function change(){
	$('#myForm').form('submit',{
		url : '${ctx}/user/password/change',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				$.messager.alert({title:'提示',msg:'修改密码成功！'});
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<form name="myForm" id='myForm' method="post">
  		<div class="easyui-panel" title="操作：编辑用户信息" style="padding:30px 60px;">
           <div style="margin-bottom:20px">
				<input type = 'text' class="easyui-textbox" name = 'oldPwd' 
					data-options="label:'旧 密 码:', required:true"><br><br>
				<input type = 'text' class="easyui-textbox" name = 'newPwd'
					data-options="label:'新 密 码:', required:true"><br><br>
            	<a href="javascript:void(0)" class="easyui-linkbutton" 
            		data-options="iconCls:'icon-save'" onclick="change()">提 交</a>
          		&nbsp;&nbsp;&nbsp;&nbsp;
        		<a href="javascript:history.go(-1)" class="easyui-linkbutton"
        			data-options="iconCls:'icon-reload'">返 回</a>
        	</div>
   		</div>
	</form>
</body>
</html>