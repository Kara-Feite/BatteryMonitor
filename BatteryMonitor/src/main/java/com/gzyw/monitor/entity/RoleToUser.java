package com.gzyw.monitor.entity;

import java.io.Serializable;

/**
 * 
 * @ClassName: RoleToUser
 * @Description: 角色用户实体类
 * @author LW
 * @date 2018年7月16日 下午3:05:24
 */
public class RoleToUser implements Serializable {
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 3296437851247626341L;

	/**
	 * 角色id
	 */
	private Integer roleId;
	
	/**
	 * 用户id
	 */
	private Integer userId;

	/**
	 * 
	 * @Title: RoleToUser
	 * @Description: RoleToUser构造函数
	 */
	public RoleToUser() {}

	/**
	 * @Title: RoleToUser
	 * @Description: RoleToUser构造函数
	 * @param roleId
	 * @param userId
	 */
	public RoleToUser(Integer roleId, Integer userId) {
		super();
		this.roleId = roleId;
		this.userId = userId;
	}

	/**
	 * @Title: getRoleId
	 * @Description: 获取roleId
	 * @return: Integer roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @Title: setRoleId
	 * @Description: 设置roleId
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @Title: getUserId
	 * @Description: 获取userId
	 * @return: Integer userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @Title: setUserId
	 * @Description: 设置userId
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "RoleToUser [roleId=" + roleId + ", userId=" + userId + "]";
	}
	
}
