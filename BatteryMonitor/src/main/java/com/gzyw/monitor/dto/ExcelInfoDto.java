package com.gzyw.monitor.dto;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: ExcelInfoDto
 * @Description: Excel基本信息
 * @author LW
 * @date 2018年8月15日 上午9:59:03
 */
public class ExcelInfoDto {
	
	/**
	 * 生产厂家
	 */
	@ExcelColumn(row = 1, column = 0, value = "生产厂家")
	private String maker;
	
	/**
	 * 电压等级
	 */
	@ExcelColumn(row = 1, column = 2, value = "电压等级")
	private Double voltage;
	
	/**
	 * 电池容量
	 */
	@ExcelColumn(row = 1, column = 4, value = "电池容量/Ah")
	private Double volume;
	
	/**
	 * 电池组编号
	 */
	@ExcelColumn(row = 2, column = 0, value = "电池组编号")
	private String groupName;
	
	/**
	 * 测试地点
	 */
	@ExcelColumn(row = 3, column = 0, value = "测试地点")
	private String address;
	
	/**
	 * 操作人员
	 */
	@ExcelColumn(row = 3, column = 4, value = "操作人员")
	private String operator;

	/**
	 * @Title: getMaker
	 * @Description: 获取maker
	 * @return: String maker
	 */
	public String getMaker() {
		return maker;
	}

	/**
	 * @Title: setMaker
	 * @Description: 设置maker
	 */
	public void setMaker(String maker) {
		this.maker = maker;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: String voltage
	 */
	public Double getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getVolume
	 * @Description: 获取volume
	 * @return: Double volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @Title: setVolume
	 * @Description: 设置volume
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @Title: getGroupName
	 * @Description: 获取groupName
	 * @return: String groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @Title: setGroupName
	 * @Description: 设置groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @Title: getAddress
	 * @Description: 获取address
	 * @return: String address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @Title: setAddress
	 * @Description: 设置address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @Title: getOperator
	 * @Description: 获取operator
	 * @return: String operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @Title: setOperator
	 * @Description: 设置operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
}
