package com.gzyw.monitor.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.DataUtil;
import com.gzyw.monitor.common.util.ExcelUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ExcelGroupDto;
import com.gzyw.monitor.dto.ExcelInfoDto;
import com.gzyw.monitor.dto.ExcelLimitDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.GroupDto;
import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.dto.TestDataDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.MonomerStatus;
import com.gzyw.monitor.entity.Norm;
import com.gzyw.monitor.entity.SystemStatus;
import com.gzyw.monitor.entity.TestData;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.GroupInfoService;
import com.gzyw.monitor.service.GroupService;
import com.gzyw.monitor.service.NormService;
import com.gzyw.monitor.service.TestDataService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: GroupController
 * @Description: 电池组控制层
 * @author LW
 * @date 2018年6月14日 下午3:30:15
 */
@Controller
@RequestMapping("/group")
public class GroupController {

	/**
	 * 电池组服务层接口
	 */
	@Autowired
	private GroupService groupService;

	/**
	 * 基站节点服务层接口
	 */
	@Autowired
	private TreeService treeService;

	/**
	 * 电池规格服务层接口
	 */
	@Autowired
	private NormService normService;
	
	/**
	 * 测试数据服务层接口
	 */
	@Autowired
	private TestDataService testDataService;
	
	/**
	 * 系统信息服务层接口
	 */
	@Autowired
	private GroupInfoService groupInfoService;

	/**
	 * 电池组id
	 */
	private Integer groupId;
	
	/**
	 * 导出测试数据
	 */
	private List<TestData> listReportData;
	
	/**
	 * 导出电池组excel格式
	 */
	private ExcelGroupDto excelGroupDto;

	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: home
	 * @Description: 主页面
	 * @param model
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	@Permissions("tree:query")
	String home(Model model, Integer groupId) {
		this.groupId = groupId;
		model.addAttribute("groupId", groupId);
		return "group/home";
	}

	/**
	 * 
	 * @Title: title
	 * @Description: 头部页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/title", method = RequestMethod.GET)
	String title(Model model) {
		model.addAttribute("groupId", groupId);
		return "group/title";
	}

	/**
	 * 
	 * @Title: content
	 * @Description: content页面
	 * @param model
	 * @param ztreeId
	 * @return
	 */
	@RequestMapping(value = "/content", method = RequestMethod.GET)
	@Permissions("tree:query")
	String content(Model model, Integer groupId) {
		Tree tree = treeService.getTreeById(groupId);
		model.addAttribute("tree", tree);
		return "group/content";
	}
	
	/**
	 * 
	 * @Title: report
	 * @Description: 导出报告页面
	 * @param groupId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	String report(Integer groupId, Model model) {
		List<Tree> listTree = treeService.listTreeByPid(groupId);
		Tree tree = treeService.getTreeById(groupId);
		model.addAttribute("listTree", listTree);
		model.addAttribute("tree", tree);
		model.addAttribute("temp", new TempDto());
		return "group/report";
	}
	
	/**
	 * 
	 * @Title: exportExcel
	 * @Description: 导出Excel
	 * @param voltageGraph
	 * @param interGraph
	 * @param temp
	 * @param request
	 */
	@RequestMapping(value = "/export/excel", method = RequestMethod.POST)
	void exportExcel(/*String voltageGraph, String interGraph,*/ TempDto temp, HttpSession session, HttpServletResponse response) {
		LinkedHashMap<String, List<Map<Integer, ?>>> map = new LinkedHashMap<String, List<Map<Integer, ?>>>();
		Integer treeId = temp.getTreeId();
		// 基本信息
		User user = (User) session.getAttribute("user");
		Tree tree = treeService.getTreeById(treeId);
		Tree treeParent = treeService.getTreeById(tree.getParentId());
		Norm norm = normService.getNormByGroupId(treeId);
		ExcelInfoDto excelInfoDto = new ExcelInfoDto();
		excelInfoDto.setAddress(treeParent.getName());
		excelInfoDto.setGroupName(tree.getName());
		excelInfoDto.setMaker(norm.getMaker());
		excelInfoDto.setVoltage(norm.getVoltage());
		excelInfoDto.setVolume(norm.getVolume());
		excelInfoDto.setOperator(user.getNickName());
		Object[] objs1 = {excelInfoDto};
		
		// 系统信息
		Object[] objs2 = new Object[2];
		GroupInfo groupInfo = groupService.getSystemInfo(temp);
		objs2[0] = groupInfo;
		objs2[1] = groupInfo.getMonomerInfo();
		
		// 门限设置
		ExcelLimitDto excelLimitDto = groupService.getExcelLimitDto(temp.getTreeId());
		MonomerStatus monomerStatus = groupInfo.getMonomerStatus();
		SystemStatus systemStatus = groupInfo.getSystemStatus();
		if ("正常".equals(monomerStatus.toString()) && "正常".equals(systemStatus.toString())) {
			excelLimitDto.setIsNormal("正常");
		}
		Object[] objs3 = {excelLimitDto};
		
		// 单体信息
		List<TestData> listTestData = testDataService.listTestDataByTime(temp);
		
		try {
			DataUtil.toDataMap("0", objs1, map);
			DataUtil.toDataMap("系统信息", objs2, map);
			DataUtil.toDataMap("门限设置", objs3, map);
			DataUtil.toDataMap("单体信息", listTestData, map);
			// 下面为图片信息
			temp.setType("voltage");
			byte[] buf1 = testDataService.getChartByGroup(temp, "电压");
			temp.setType("inter");
			byte[] buf2 = testDataService.getChartByGroup(temp, "内阻");
			DataUtil.toDataMap("1", buf1, map);
			DataUtil.toDataMap("2", buf2, map);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		// 设置每列宽度
		Map<Integer, Integer> columnWidths = new HashMap<>();
		columnWidths.put(0, 24 * 256);
		columnWidths.put(1, 12 * 256);
		columnWidths.put(2, 23 * 256);
		columnWidths.put(3, 14 * 256);
		columnWidths.put(4, 20 * 256);
		columnWidths.put(5, 10 * 256);
		columnWidths.put(6, 18 * 256);
		columnWidths.put(7, 10 * 256);
		Workbook workbook = ExcelUtil.exportHSExcel("蓄电池在线监测数据分析报表", map, columnWidths, 8);
		try {
			OutputStream output = response.getOutputStream();
			//清空缓存
			response.reset();
			//定义浏览器响应表头，顺带定义下载名
			response.setHeader("Content-disposition", "attachment;filename="+ URLEncoder.encode("蓄电池在线监测数据分析报表", "UTF-8") + "（" + temp.getStartTime() + "）.xls");
			//定义下载的类型，标明是excel文件
			response.setContentType("application/vnd.ms-excel");
			//这时候把创建好的excel写入到输出流
			workbook.write(output);
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @Title: exportExportByGroup
	 * @Description: 导出电池组数据
	 */
	@RequestMapping(value = "/export/excel/group", method = RequestMethod.POST)
	void exportExportByGroup(HttpServletResponse response, TempDto temp) {
		if (this.excelGroupDto == null) {
			return;
		}
		List<TestData> listTestData = testDataService.listTestDataByExport(temp);
		List<TestDataDto> listTestDataDto = new ArrayList<TestDataDto>();
		for (TestData testData : listTestData) {
			listTestDataDto.add(new TestDataDto(testData));
		}
		LinkedHashMap<String, List<Map<Integer, ?>>> map = new LinkedHashMap<String, List<Map<Integer, ?>>>();
		Object[] objs = {this.excelGroupDto};
		try {
			DataUtil.toDataMap("电池组信息", objs, map);
			DataUtil.toDataMap("单体信息", listTestDataDto, map);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		// 设置每列宽度
		Map<Integer, Integer> columnWidths = new HashMap<>();
		columnWidths.put(0, 20 * 256);
		columnWidths.put(1, 20 * 256);
		columnWidths.put(2, 20 * 256);
		columnWidths.put(3, 21 * 256);
		columnWidths.put(4, 24 * 256);
		columnWidths.put(5, 18 * 256);
		columnWidths.put(6, 0);
		Workbook workbook = ExcelUtil.exportHSExcel("电池组信息报表", map, columnWidths, 6);
		try {
			OutputStream output = response.getOutputStream();
			//清空缓存
			response.reset();
			//定义浏览器响应表头，顺带定义下载名
			response.setHeader("Content-disposition", "attachment;filename="+URLEncoder.encode("电池组信息报表", "UTF-8")+".xls");
			//定义下载的类型，标明是excel文件
			response.setContentType("application/vnd.ms-excel");
			//这时候把创建好的excel写入到输出流
			workbook.write(output);
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @Title: queryTest
	 * @Description: 根据条件查询测试时间
	 * @return
	 */
	@RequestMapping(value = "/report/query", method = RequestMethod.GET)
	@ResponseBody
	ResultDto queryTest(TempDto temp) {
		if (null == temp) {
			return ResultDto.build(500, "参数不能为空！");
		}
		if (StringUtil.isEmpty(temp.getStartTime()) || StringUtil.isEmpty(temp.getEndTime())) {
			return ResultDto.build(500, "开始或结束时间不能为空！");
		}
		try {
			List<TestData> listTestData = testDataService.listTestDataBetTime(temp);
			this.listReportData = listTestData;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，请联系管理员！");
		}
		return ResultDto.ok();
	}
	
	/**
	 * 
	 * @Title: getExportDataGrid
	 * @Description: 展示查询到的时间
	 * @return
	 */
	@RequestMapping(value = "/report/datagrid")
	@ResponseBody
	EUDataGridDto getExportDataGrid() {
		EUDataGridDto datagrid = new EUDataGridDto();
		if (listReportData != null) {
			datagrid.setRows(listReportData);
			Integer size = listReportData.size();
			Long count = Long.valueOf(size.toString());
			datagrid.setTotal(count);
		}
		return datagrid;
	}

	/**
	 * 
	 * @Title: showEdit
	 * @Description: 跳转到编辑页面
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/edit")
	String showEdit(@RequestParam(value = "id", required = false) Integer id, Model model,
			@RequestParam(value = "treeId", required = false) Integer treeId) {
		if (id != null) {
			GroupDto groupDto = groupService.getGroupDtoById(id);
			if (groupDto.getTree() != null) {
				model.addAttribute("groupDto", groupDto);
			} else {
				return "redirect:/error";
			}
		} else if (treeId != null) {
			GroupDto groupDto = new GroupDto();
			Tree tree = new Tree();
			tree.setParentId(treeId);
			groupDto.setTree(tree);
			model.addAttribute("groupDto", groupDto);
		} else {
			return "redirect:/error";
		}
		List<String> listMaker = normService.listMakerAll();
		List<String> listModel = normService.listModelAll();
		List<String> listType = normService.listTypeAll();
		model.addAttribute("listMaker", listMaker);
		model.addAttribute("listModel", listModel);
		model.addAttribute("listType", listType);
		return "group/edit";
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 添加电池组
	 * @param model
	 * @param groupDto
	 * @param errors
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "添加电池组")
	@Permissions("tree:add")
	public ResultDto save(@Validated GroupDto groupDto, Errors errors) {
		if (errors.hasErrors()) {
			List<ObjectError> allErrors = errors.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage() + ",");
			}
			return ResultDto.build(500, sb.substring(0, sb.length() - 1));
		}
		return groupService.saveGroup(groupDto);
	}
	
	/**
	 * 
	 * @Title: update
	 * @Description: 更新电池组
	 * @param user
	 * @param br
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "更新电池组")
	@Permissions("tree:update")
	public ResultDto update(@Validated GroupDto groupDto , BindingResult br){
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage());
			}
			return ResultDto.build(500, sb.toString());
		}
		return groupService.updateGroup(groupDto);
	}

	/**
	 * 
	 * @Title: datagrid
	 * @Description: 获取节点分页数据
	 * @param page 页码
	 * @param norm 基站节点
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Tree tree,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return groupService.getDatagrid(page, rows, sort, order, tree);
	}
	
	/**
	 * 
	 * @Title: showGraph
	 * @Description: 测试数据主页
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/frame")
	String showGraph(Model model,Integer id){
		model.addAttribute("id",id);
		return "group/map/frame";
	}
	
	/**
	 * 
	 * @Title: top
	 * @Description: 测试数据头部页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/top")
	String top(Model model, Integer id){
		GroupDto groupDto = groupService.getGroupDtoById(id);
		GroupInfo groupInfo = groupInfoService.getGroupInfoByNow(id);
		TempDto temp = new TempDto();
		temp.setTreeId(id);
		MaxMinDto maxMinDto = testDataService.getMaxMinDto(temp);
		model.addAttribute("groupDto", groupDto);
		model.addAttribute("maxMinDto", maxMinDto);
		model.addAttribute("groupInfo", groupInfo);
		this.excelGroupDto = new ExcelGroupDto(groupDto, maxMinDto, groupInfo); 
		return "group/map/top";
	}
	
	/**
	 * 
	 * @Title: graph
	 * @Description: 统计图
	 * @param model
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value="/graph")
	String graph(Model model, TempDto temp){
		model.addAttribute("temp", temp);
		GraphDto graphDto = testDataService.getGraphDto(temp);
		model.addAttribute("graphDto", graphDto);
		return "group/map/graph";
	}
	
	/**
	 * 
	 * @Title: test
	 * @Description: 测试页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/test")
	String test(Model model, Integer id){
		Group group = groupService.getGroupById(id);
		Tree tree = treeService.getTreeById(id);
		model.addAttribute("tree", tree);
		model.addAttribute("group", group);
		TempDto temp = new TempDto();
		temp.setId(id);
		model.addAttribute("temp", temp);
		return "group/test";
	}
	
	/**
	 * 
	 * @Title: testResult
	 * @Description: 测试结果
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/test/graph")
	String testResult(Model model, TempDto temp){
		if (temp != null && temp.getId() != null) {
			GraphDto graphDto = groupService.getTestGraphDto(temp);
			if (graphDto == null) {
				model.addAttribute("graphError", "未查询到任何数据，请检查电池组连接是否正常!");
			}
			model.addAttribute("graphDto", graphDto);
		}
		return "group/graph";
	}
	
	/**
	 * 
	 * @Title: startGroup
	 * @Description: 开启监测
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "/start", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "开启电池组监测")
	public ResultDto startGroup(int[] ids) {
		try {
			return groupService.updateIsRun(ids, Boolean.TRUE);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，开启失败！");
		}
	}
	
	/**
	 * 
	 * @Title: StopGroup
	 * @Description: 关闭监测
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "关闭电池组监测")
	public ResultDto StopGroup(int[] ids) {
		try {
			return groupService.updateIsRun(ids, Boolean.FALSE);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，关闭失败！");
		}
	}

}
