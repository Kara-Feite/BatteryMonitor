<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<meta name='Description' content='404页面找不到...'>
<title>04页面找不到...</title>
<link href='${ctxStatic}/css/public.css' rel='stylesheet'>
<style>
	body{
		background-color: #007BC1;
	}
	#main{
		position: relative;
		margin: 0 auto;
		width: 1200px;
		height: 641px;
		background: url('${ctxStatic}/images/404.png') no-repeat;
		background-size: cover;
	}
	#btn-index {
		position: absolute;
		bottom: 20px;
		left: 50%;
		margin-left: -180px;
		width: 360px;
		height: 65px;
		line-height: 65px;
		text-align: center;
		color: #fff;
		font-size: 30px;
		border: 2px solid #fff;
	}
	#btn-index:hover {
		background-color: #0c83c7;
	}
</style>
</head>
<body>
	<div id='main'>
		<a id='btn-index' href='${ctx}/main/index'>返回首页</a>
	</div>
</body>
</html>