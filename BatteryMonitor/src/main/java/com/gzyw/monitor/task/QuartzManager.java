package com.gzyw.monitor.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;

import com.gzyw.monitor.common.util.CronUtil;
import com.gzyw.monitor.common.util.JsonUtil;
import com.gzyw.monitor.control.MonitorServer;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.GroupInfoService;
import com.gzyw.monitor.service.GroupLimitService;
import com.gzyw.monitor.service.GroupService;
import com.gzyw.monitor.service.TestDataService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: QuartzManager
 * @Description: Quartz管理类
 * @author LW
 * @date 2018年7月25日 下午2:31:34
 */
public class QuartzManager {

	private Scheduler scheduler;
	
	@Autowired
	private MonitorServer monitorServer;
	
	/**
	 * 组信息服务层
	 */
	@Autowired
	public GroupInfoService groupInfoService;
	
	/**
	 * 测试数据服务层
	 */
	@Autowired
	public TestDataService testDataService;
	
	/**
	 * 组限制服务层
	 */
	@Autowired
	public GroupLimitService groupLimitService;
	
	/**
	 * 电池组服务层
	 */
	@Autowired
	private GroupService groupService;
	
	/**
	 * 基站节点服务层
	 */
	@Autowired
	public TreeService treeService;

	/**
	 * @Title: addJob
	 * @Description: 添加一个定时任务
	 * @param jobName 任务名
	 * @param jobGroupName 任务组名
	 * @param triggerName 触发器名
	 * @param triggerGroupName 触发器组名 Scheduler
	 * @param jobClass 任务
	 * @param cron 时间设置，参考quartz说明文档
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName, Class jobClass,
			String cron, Map<String, String> params) {
		try {
			// 任务名，任务组，任务执行类
			JobBuilder jobBuilder = JobBuilder.newJob(jobClass).withIdentity(jobName, jobGroupName);
			if (params != null) {
				for(Entry<String, String> param : params.entrySet()) {
					jobBuilder.usingJobData(param.getKey(), param.getValue());
				}
			}
			JobDetail jobDetail = jobBuilder.build();
			// 触发器
			TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
			// 触发器名,触发器组
			triggerBuilder.withIdentity(triggerName, triggerGroupName);
			triggerBuilder.startNow();
			// 触发器时间设定
			triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
			// 创建Trigger对象
			CronTrigger trigger = (CronTrigger) triggerBuilder.build();
			// 调度容器设置JobDetail和Trigger
			scheduler.scheduleJob(jobDetail, trigger);
			// 启动
			if (!scheduler.isShutdown()) {
				scheduler.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @Title: addJob
	 * @Description: 添加电池组任务
	 * @param group
	 * @param tree
	 */
	public void addJob(Group group, Tree tree) {
		String groupId = group.getGroupId().toString();
		if (isExists(groupId, "group")) {
			return;
		}
		String cron = CronUtil.getCron(group.getInterval(), group.getUnit());
		Map<String, String> params = new HashMap<String, String>();
		params.put("group", JsonUtil.objectToJson(group));
		params.put("tree", JsonUtil.objectToJson(tree));
		addJob(groupId, "group", groupId, "group", MyJob.class, cron, params);
	}

	/**
	 * @Title: modifyJobTime
	 * @Description: 修改一个任务的触发时间
	 * @param jobName
	 * @param jobGroupName
	 * @param triggerName 触发器名
	 * @param triggerGroupName 触发器组名
	 * @param cron 时间设置，参考quartz说明文档
	 */
	public void modifyJobTime(String jobName, String jobGroupName, String triggerName, String triggerGroupName,
			String cron) {
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			if (trigger == null) {
				return;
			}
			String oldTime = trigger.getCronExpression();
			if (!oldTime.equalsIgnoreCase(cron)) {
				/** 方式一 ：调用 rescheduleJob 开始 */
				// 触发器
				TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
				// 触发器名,触发器组
				triggerBuilder.withIdentity(triggerName, triggerGroupName);
				triggerBuilder.startNow();
				// 触发器时间设定
				triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
				// 创建Trigger对象
				trigger = (CronTrigger) triggerBuilder.build();
				// 方式一 ：修改一个任务的触发时间
				scheduler.rescheduleJob(triggerKey, trigger);
				/** 方式一 ：调用 rescheduleJob 结束 */

				/** 方式二：先删除，然后在创建一个新的Job */
				// JobDetail jobDetail = scheduler.getJobDetail(JobKey.jobKey(jobName,
				// jobGroupName));
				// Class<? extends Job> jobClass = jobDetail.getJobClass();
				// removeJob(jobName, jobGroupName, triggerName, triggerGroupName);
				// addJob(jobName, jobGroupName, triggerName, triggerGroupName, jobClass, cron);
				/** 方式二 ：先删除，然后在创建一个新的Job */
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @Title: removeJob
	 * @Description: 移除一个任务
	 * @param group
	 */
	public void removeJob(Group group) {
		String groupId = group.getGroupId().toString();
		if (isExists(groupId, "group")) {
			removeJob(groupId, "group", groupId, "group");
		}
	}

	/**
	 * 
	 * @Title: removeJob
	 * @Description: 移除一个任务
	 * @param jobName
	 * @param jobGroupName
	 * @param triggerName
	 * @param triggerGroupName
	 */
	public void removeJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName) {
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
			scheduler.pauseTrigger(triggerKey);// 停止触发器
			scheduler.unscheduleJob(triggerKey);// 移除触发器
			scheduler.deleteJob(JobKey.jobKey(jobName, jobGroupName));// 删除任务
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @Title: isExists
	 * @Description: 判断任务是否存在
	 * @param jobName
	 * @param jobGroupName
	 * @return
	 */
	public boolean isExists(String jobName, String jobGroupName) {
		try {
            JobKey jobKey = JobKey.jobKey(jobName, jobGroupName);
            if (scheduler.checkExists(jobKey)) {
                return true;
            }
		} catch (SchedulerException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * 
	 * @Title: startJobs
	 * @Description: 启动所有定时任务
	 */
	public void startJobs() {
		try {
			monitorServer.listener();
			List<Group> listGroup = groupService.listGroupByRun();
			for (Group group : listGroup) {
				Tree tree = treeService.getTreeById(group.getGroupId());
				addJob(group, tree);
			}
			scheduler.start();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @Title: shutdownJobs
	 * @Description: 关闭所有定时任务
	 */
	public void shutdownJobs() {
		try {
			if (!scheduler.isShutdown()) {
				scheduler.shutdown();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * @Title: getScheduler
	 * @Description: 获取scheduler
	 * @return: Scheduler scheduler
	 */
	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * @Title: setScheduler
	 * @Description: 设置scheduler
	 */
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
	
}
