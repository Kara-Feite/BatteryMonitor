<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>

<script src='${ctxStatic}/js/easyui/jquery.min.js'></script>
<script src='${ctxStatic}/js/easyui/jquery.easyui.min.js'></script>
<script src='${ctxStatic}/js/easyui/locale/easyui-lang-zh_CN.js'></script>
<script src='${ctxStatic}/js/easyui/easyloader.js'></script>
<script src='${ctxStatic}/js/script/syUtil.js'></script>

<link href='${ctxStatic}/js/easyui/demo/demo.css' rel='stylesheet'>
<link href='${ctxStatic}/js/easyui/themes/default/easyui.css' rel='stylesheet'>
<link href='${ctxStatic}/js/easyui/themes/icon.css' rel='stylesheet'>

<script>
	// 时间格式化
	Date.prototype.format = function(format){ 
	    var o =  { 
	    "M+" : this.getMonth()+1,
	    "d+" : this.getDate(),
	    "h+" : this.getHours(),
	    "m+" : this.getMinutes(),
	    "s+" : this.getSeconds(),
	    "q+" : Math.floor((this.getMonth()+3)/3),
	    "S" : this.getMilliseconds()
	    };
	    if(/(y+)/.test(format)){ 
	    	format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
	    }
	    for(var k in o)  { 
		    if(new RegExp("("+ k +")").test(format)){ 
		    	format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
		    } 
	    } 
	    return format; 
	};
	function formatDateTime(val,row){
		if (!val) {
			return '无';
		}
		var now = new Date(val);
    	return now.format("yyyy-MM-dd hh:mm:ss");
	}
	function formatNull(val, row) {
		return val ? val : '无';
	}
</script>


