package com.gzyw.monitor.dto;

import java.util.Date;

import com.gzyw.monitor.entity.Norm;

/**
 * 
 * @ClassName: NormDto
 * @Description: 电池规格包装类
 * @author LW
 * @date 2018年9月25日 下午1:21:33
 */
public class NormDto {
	
	/**
	 * id
	 */
	private Integer id;

	/**
	 * 生产厂家
	 */
	private String maker;

	/**
	 * 型号
	 */
	private String model;

	/**
	 * 电池类型
	 */
	private String type;

	/**
	 * 参考电压
	 */
	private Double voltage;

	/**
	 * 参考电阻
	 */
	private Double inter;

	/**
	 * 参考容量
	 */
	private Double volume;

	/**
	 * 投运时间
	 */
	private Date beginTime;
	
	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 对应组
	 */
	private String groupName;
	
	/**
	 * 
	 * @Title: NormDto
	 * @Description: NormDto构造函数
	 */
	public NormDto() {}

	/**
	 * 
	 * @Title: NormDto
	 * @Description: NormDto构造函数
	 * @param norm
	 * @param groupName
	 */
	public NormDto(Norm norm, String groupName) {
		this.id = norm.getId();
		this.maker = norm.getMaker();
		this.model = norm.getModel();
		this.type = norm.getType();
		this.voltage = norm.getVoltage();
		this.inter = norm.getInter();
		this.volume = norm.getVolume();
		this.beginTime = norm.getBeginTime();
		this.updateTime = norm.getUpdateTime();
		this.groupName = groupName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getVoltage() {
		return voltage;
	}

	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	public Double getInter() {
		return inter;
	}

	public void setInter(Double inter) {
		this.inter = inter;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "NormDto [id=" + id + ", maker=" + maker + ", model=" + model + ", type=" + type + ", voltage=" + voltage
				+ ", inter=" + inter + ", volume=" + volume + ", beginTime=" + beginTime + ", updateTime=" + updateTime
				+ ", groupName=" + groupName + "]";
	}
	
}
