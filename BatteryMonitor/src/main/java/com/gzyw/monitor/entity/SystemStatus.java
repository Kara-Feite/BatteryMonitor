package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @ClassName: SystemStatus
 * @Description: 系统状态
 * @author LW
 * @date 2018年6月4日 下午1:49:10
 */
@Entity
@Table(name = "bm_system_status")
public class SystemStatus implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 5345846557528074300L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电压高
	 */
	@Column(name = "is_high_voltage", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isHighVoltage;

	/**
	 * 电压低
	 */
	@Column(name = "is_low_voltage", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isLowVoltage;

	/**
	 * 离线
	 */
	@Column(name = "is_offline", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isOffline;

	/**
	 * 开路
	 */
	@Column(name = "is_open_circuit", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isOpenCircuit;

	/**
	 * 放电过流
	 */
	@Column(name = "is_overcurrent", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isOvercurrent;

	/**
	 * 过放
	 */
	@Column(name = "is_over_discharge", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isOverDischarge;

	/**
	 * 环境温度高
	 */
	@Column(name = "is_high_tempera", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isHighTempera;

	/**
	 * 环境温度低
	 */
	@Column(name = "is_low_tempera", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isLowTempera;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 电池组信息
	 */
	@OneToOne(mappedBy = "systemStatus")
	private GroupInfo groupInfo;

	/**
	 * @Title: SystemStatus
	 * @Description: SystemStatus构造函数
	 */
	public SystemStatus() {}

	/**
	 * @Title: SystemStatus
	 * @Description: SystemStatus构造函数
	 * @param isHighVoltage
	 * @param isLowVoltage
	 * @param isOffline
	 * @param isOpenCircuit
	 * @param isOvercurrent
	 * @param isOverDischarge
	 * @param isHighTempera
	 * @param isLowTempera
	 * @param createTime
	 */
	public SystemStatus(Boolean isHighVoltage, Boolean isLowVoltage, Boolean isOffline, Boolean isOpenCircuit,
			Boolean isOvercurrent, Boolean isOverDischarge, Boolean isHighTempera, Boolean isLowTempera,
			Date createTime) {
		this.isHighVoltage = isHighVoltage;
		this.isLowVoltage = isLowVoltage;
		this.isOffline = isOffline;
		this.isOpenCircuit = isOpenCircuit;
		this.isOvercurrent = isOvercurrent;
		this.isOverDischarge = isOverDischarge;
		this.isHighTempera = isHighTempera;
		this.isLowTempera = isLowTempera;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getIsHighVoltage
	 * @Description: 获取isHighVoltage
	 * @return: Boolean isHighVoltage
	 */
	public Boolean getIsHighVoltage() {
		return isHighVoltage;
	}

	/**
	 * @Title: setIsHighVoltage
	 * @Description: 设置isHighVoltage
	 */
	public void setIsHighVoltage(Boolean isHighVoltage) {
		this.isHighVoltage = isHighVoltage;
	}

	/**
	 * @Title: getIsLowVoltage
	 * @Description: 获取isLowVoltage
	 * @return: Boolean isLowVoltage
	 */
	public Boolean getIsLowVoltage() {
		return isLowVoltage;
	}

	/**
	 * @Title: setIsLowVoltage
	 * @Description: 设置isLowVoltage
	 */
	public void setIsLowVoltage(Boolean isLowVoltage) {
		this.isLowVoltage = isLowVoltage;
	}

	/**
	 * @Title: getIsOffline
	 * @Description: 获取isOffline
	 * @return: Boolean isOffline
	 */
	public Boolean getIsOffline() {
		return isOffline;
	}

	/**
	 * @Title: setIsOffline
	 * @Description: 设置isOffline
	 */
	public void setIsOffline(Boolean isOffline) {
		this.isOffline = isOffline;
	}

	/**
	 * @Title: getIsOpenCircuit
	 * @Description: 获取isOpenCircuit
	 * @return: Boolean isOpenCircuit
	 */
	public Boolean getIsOpenCircuit() {
		return isOpenCircuit;
	}

	/**
	 * @Title: setIsOpenCircuit
	 * @Description: 设置isOpenCircuit
	 */
	public void setIsOpenCircuit(Boolean isOpenCircuit) {
		this.isOpenCircuit = isOpenCircuit;
	}

	/**
	 * @Title: getIsOvercurrent
	 * @Description: 获取isOvercurrent
	 * @return: Boolean isOvercurrent
	 */
	public Boolean getIsOvercurrent() {
		return isOvercurrent;
	}

	/**
	 * @Title: setIsOvercurrent
	 * @Description: 设置isOvercurrent
	 */
	public void setIsOvercurrent(Boolean isOvercurrent) {
		this.isOvercurrent = isOvercurrent;
	}

	/**
	 * @Title: getIsOverDischarge
	 * @Description: 获取isOverDischarge
	 * @return: Boolean isOverDischarge
	 */
	public Boolean getIsOverDischarge() {
		return isOverDischarge;
	}

	/**
	 * @Title: setIsOverDischarge
	 * @Description: 设置isOverDischarge
	 */
	public void setIsOverDischarge(Boolean isOverDischarge) {
		this.isOverDischarge = isOverDischarge;
	}

	/**
	 * @Title: getIsHighTempera
	 * @Description: 获取isHighTempera
	 * @return: Boolean isHighTempera
	 */
	public Boolean getIsHighTempera() {
		return isHighTempera;
	}

	/**
	 * @Title: setIsHighTempera
	 * @Description: 设置isHighTempera
	 */
	public void setIsHighTempera(Boolean isHighTempera) {
		this.isHighTempera = isHighTempera;
	}

	/**
	 * @Title: getIsLowTempera
	 * @Description: 获取isLowTempera
	 * @return: Boolean isLowTempera
	 */
	public Boolean getIsLowTempera() {
		return isLowTempera;
	}

	/**
	 * @Title: setIsLowTempera
	 * @Description: 设置isLowTempera
	 */
	public void setIsLowTempera(Boolean isLowTempera) {
		this.isLowTempera = isLowTempera;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getGroupInfo
	 * @Description: 获取groupInfo
	 * @return: GroupInfo groupInfo
	 */
	public GroupInfo getGroupInfo() {
		return groupInfo;
	}

	/**
	 * @Title: setGroupInfo
	 * @Description: 设置groupInfo
	 */
	public void setGroupInfo(GroupInfo groupInfo) {
		this.groupInfo = groupInfo;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (isHighVoltage) {
			return "电压高";
		}
		if (isLowVoltage) {
			return "电压低";
		}
		if (isOffline) {
			return "离线";
		}
		if (isOpenCircuit) {
			return "开路";
		}
		if (isOvercurrent) {
			return "放电过流";
		}
		if (isOverDischarge) {
			return "过放";
		}
		if (isHighTempera) {
			return "环境温度高";
		}
		if (isLowTempera) {
			return "环境温度低";
		}
		return "正常";
	}

}
