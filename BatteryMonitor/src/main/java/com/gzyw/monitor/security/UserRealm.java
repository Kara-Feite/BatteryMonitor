package com.gzyw.monitor.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.common.util.EncryptUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.UserDao;
import com.gzyw.monitor.entity.Menu;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.MenuService;

/**
 * 
 * @ClassName: UserRealm
 * @Description: 用户域,进行认证和授权
 * @author LW
 * @date 2018年7月17日 上午9:54:48
 */
public class UserRealm extends AuthorizingRealm {

	@Autowired
	private UserDao userDao;

	@Autowired
	private MenuService menuService;

	/**
	 * 用户身份认证
	 * @param token
	 * @return
	 * @throws AuthenticationException 
	 * @see org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// 获取页面传入的用户名和密码 token
		UsernamePasswordToken authtoken = (UsernamePasswordToken) token;
		String userName = authtoken.getUsername();
		// 根据userName去数据库查询用户对象
		User user = userDao.getUserByName(userName);
		if (user == null) {
			return null;
		}
		// 身份信息已经确认，接下来进行凭证信息匹配
		if (userName.equals(user.getUserName())) {
			// 获取salt
			byte[] salt = EncryptUtil.decodeHex(user.getPassword().substring(0, 64));
			String password = user.getPassword().substring(64);
			// 告诉shiro用什么加密算法(sha1,md5) 和迭代次数
			SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user,
					password, ByteSource.Util.bytes(salt), getName());
			//simpleAuthenticationInfo
			return simpleAuthenticationInfo;
		}
		return null;
	}

	/**
	 * 用户授权
	 * @param principles
	 * @return 
	 * @see org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principles) {
		User user = (User) principles.getPrimaryPrincipal();
		List<Menu> listMenu = menuService.listMenuByUserId(user.getId(), false);
		List<String> permissions = new ArrayList<String>();
		if (CollectionUtil.isNotEmpty(listMenu)) {
			for (Menu menu : listMenu) {
				if (StringUtil.isNotEmpty(menu.getPermission())) {
					permissions.add(menu.getPermission());
				}
			}
		}
		// 将得到的权限信息放入simpleAuthorizationInfo对象保存
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.addStringPermissions(permissions);
		return info;
	}

	/**
	 * 
	 * @Title: clearCachedAuthorizationInfo
	 * @Description: 清除授权信息缓存
	 */
	public void clearCachedAuthorizationInfo() {
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCachedAuthorizationInfo(principals);
    }

}
