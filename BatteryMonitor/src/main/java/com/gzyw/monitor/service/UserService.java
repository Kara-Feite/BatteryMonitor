package com.gzyw.monitor.service;

import java.util.List;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.User;

/**
 * 
 * @ClassName: UserService
 * @Description: 用户服务层
 * @author LW
 * @date 2018年6月6日 下午8:44:24
 */
public interface UserService {

	/**
	 * 
	 * @Title: loginUser
	 * @Description: 用户登录
	 * @param userName
	 * @param password
	 * @return
	 */
	User loginUser(String userName, String password);

	/**
	 * 
	 * @Title: getDataGrid
	 * @Description: 获取用户列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param user 用户
	 * @return
	 */
	EUDataGridDto getDataGrid(Integer page, Integer rows, String sort, String order, User user);

	/**
	 * 
	 * @Title: getUserById
	 * @Description: 根据id查询用户
	 * @param id
	 * @return
	 */
	User getUserById(Integer id);

	/**
	 * 
	 * @Title: saveUser
	 * @Description: 添加用户
	 * @param user
	 * @param listRole 
	 * @return
	 */
	ResultDto saveUser(User user, int[] listRole);

	/**
	 * 
	 * @Title: updateUser
	 * @Description: 更新用户
	 * @param user
	 * @param listRole 
	 * @return
	 */
	ResultDto updateUser(User user, int[] listRole);

	/**
	 * 
	 * @Title: delUserByBat
	 * @Description: 批量删除用户
	 * @param ids
	 * @return
	 */
	ResultDto delUserByBat(int[] ids);

	/**
	 * 
	 * @Title: updateState
	 * @Description: 更新用户状态
	 * @param ids
	 * @param state
	 * @return
	 */
	ResultDto updateState(int[] ids, boolean state);

	/**
	 * 
	 * @Title: changePwd
	 * @Description: 修改密码
	 * @param oldPwd
	 * @param newPwd
	 * @param userId
	 * @return
	 */
	ResultDto changePwd(String oldPwd, String newPwd, Integer userId);

	/**
	 * 
	 * @Title: listUserByAll
	 * @Description: 获取所有用户
	 * @return
	 */
	List<User> listUserByAll();

}
