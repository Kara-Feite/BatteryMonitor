<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>权限编辑</title>
<link href='${ctxStatic}/css/zTreeStyle/zTreeStyle.css' rel='stylesheet'>
<link href="${ctxStatic}/js/bootstrap/2.3.1/css/bootstrap.min.css" rel="stylesheet" />
<link href="${ctxStatic}/js/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
<link href="${ctxStatic}/js/jquery-validation/1.11.0/jquery.validate.min.css" rel="stylesheet" />
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<script src='${ctxStatic}/js/jquery-1.8.3.min.js'></script>
<script src="${ctxStatic}/js/jquery-validation/1.11.0/jquery.validate.min.js"></script>
<script src="${ctxStatic}/js/bootstrap/2.3.1/js/bootstrap.min.js"></script>
<script src="${ctxStatic}/js/jquery-jbox/2.3/jquery.jBox-2.3.min.js"></script>
<script src="${ctxStatic}/js/jquery.ztree.all.min.js"></script>
<style>
	.auth{margin-top:3px;float:left;}
	.auth p{font-size:14px;margin:3px 13px 2px 0;text-align:center;}
	.control-group{padding-left:60px;}
</style>
<script>
$(document).ready(function(){
	$(function() {
		$("#name").focus();
		$("#roleEditForm").validate({
			submitHandler: function(form){
				var formObject = {};
				var formArray =$("#roleEditForm").serializeArray();
				$.each(formArray, function(i, item){
					formObject[item.name]=item.value;
				 });
			 	var jsonObj = JSON.stringify(formObject);
				console.info(jsonObj);
				
			 	//获取菜单树被选中的节点
			 	var menuArr = [];
			 	var menuNodes = menuTree.getCheckedNodes(true);
			 	$.each(menuNodes, function(i, item){
			 		menuArr.push(item.id);
				 });
			 	var menuIds = menuArr.join(',');
			 	//获取区域树被选中的节点
			 	var treeArr = [];
			 	var treeNodes = areaTree.getCheckedNodes(true);
			 	$.each(treeNodes, function(i, item){
			 		treeArr.push(item.id);
				 });
			 	var treeIds = treeArr.join(',');
			 	console.info(treeIds);
				$.ajax({
					type:'post',
					url:'${ctx}/role/${empty role.id ? "save" : "update"}',
					dataType:'json', //返回数据的几种格式 xml html json text 等常用
					//data传值的另外一种方式 form的序列化
					data: {"roleJson":jsonObj,"menuIds":menuIds,"treeIds":treeIds},//传递给服务器的参数					
					success:function(result){
						if (result.status === 200) {
							alert('${empty role.id ? "添加成功！" : "更新成功！"}');
							//$.messager.alert({title:'提示',msg:'sfdsdfsdf'});
							window.history.go(-1);
						} else {
							$.messager.alert({title:'提示',msg:result.msg});
						}
					}
				});
			},
			errorContainer: "#messageBox",
			errorPlacement: function(error, element) {
				$("#messageBox").text("输入有误，请先更正。");
				if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			}
		});
	});
	
	//菜单资源树
	var menuSetting = {check:{enable:true,nocheckInherit:true},view:{selectedMulti:false},
			data:{simpleData:{enable:true}},callback:{beforeClick:function(id, node){
				menuTree.checkNode(node, !node.checked, true, true);
				return false;
		}}};
	
	// 用户-菜单
	var menuZNodes=[
			<c:forEach items="${listMenu}" var="menu">
			{
				id: "${menu.id}",
				pId: "${menu.parentId}", 
				name: "${menu.name}"
			},
			</c:forEach>
        ];
	// 初始化树结构
	var menuTree = $.fn.zTree.init($("#menuTree"), menuSetting, menuZNodes);
	//进入修改页面的时候 ，选中已经拥有资源的节点	
	<c:forEach items="${listRoleToMenu}" var="roleToMenu">
		var node = menuTree.getNodeByParam("id","${roleToMenu.menuId}");
		menuTree.checkNode(node,true,false);
	</c:forEach>
	//默认展开全部节点
	menuTree.expandAll(true);
	
	//区域资源树
	var treeSetting = {check:{enable:true,nocheckInherit:true},view:{selectedMulti:false},
			data:{simpleData:{enable:true}},callback:{beforeClick:function(id, node){
				areaTree.checkNode(node, !node.checked, true, true);
				return false;
		}}};
	
	// 用户-区域
	var treeZNodes=[
			<c:forEach items="${listTree}" var="tree">
			{
				id: "${tree.id}",
				pId: "${tree.parentId}", 
				name: "${tree.name}"
			},
			</c:forEach>
        ];
	// 初始化树结构
	var areaTree = $.fn.zTree.init($("#areaTree"), treeSetting, treeZNodes);
	//进入修改页面的时候 ，选中已经拥有资源的节点	
	<c:forEach items="${listRoleToTree}" var="roleToTree">
		var node = areaTree.getNodeByParam("id", "${roleToTree.treeId}");
		areaTree.checkNode(node,true,false);
	</c:forEach>
	//默认展开全部节点
	areaTree.expandAll(true);
});	
</script>
</head>
<body>
	<form id="roleEditForm" class="form-horizontal" action="#" method="post">
		<div class="easyui-panel" title="操作：编辑角色信息" style="padding-top:30px;">
			<c:if test="${!empty role.id}">
				<input id="id" name="id" type="hidden" value="${role.id}"/>
			</c:if>
			<div class="control-group">
				<label class="control-label">名称:</label>
				<div class="controls">
					<input id="name" name="name" class="required input-xlarge" type="text" value="${role.name}" maxlength="50"/>
					<span class="help-inline"><span style="color:red">*</span> </span>
				</div>
			</div>
				 
			<div class="control-group">
				<label class="control-label">备注:</label>
				<div class="controls">
					<textarea id="remarks" name="remark" maxlength="200" class="input-xxlarge" rows="3">${role.remark}</textarea>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">角色授权:</label>
				<div class="controls">
					<div class='auth' style="margin-top:3px;float:left;">
						<p>菜单授权</p>
						<div id="menuTree" class="ztree"></div>
					</div>
					<div class='auth' style="margin-left:150px;">
						<p>基站授权</p>
	 					<div id="areaTree" class="ztree"></div>
					</div>
	 			</div>
			</div>
			<div class="form-actions" style='margin-bottom:0;padding-left:400px;background:none;'>
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
				<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
			</div>
		</div>
	</form>
</body>
</html>