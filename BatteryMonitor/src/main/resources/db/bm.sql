/*
 Navicat Premium Data Transfer

 Source Server         : mydb
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : 127.0.0.1:3306
 Source Schema         : bm

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 19/09/2018 09:06:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bm_alarm
-- ----------------------------
DROP TABLE IF EXISTS `bm_alarm`;
CREATE TABLE `bm_alarm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alarm_time` datetime(0) NOT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `dealer` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_deal` tinyint(1) NOT NULL DEFAULT 0,
  `mold` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKhms6yb1uh7giab8p324rs2b92`(`group_id`) USING BTREE,
  INDEX `mold_index`(`mold`) USING BTREE,
  INDEX `type_index`(`type`) USING BTREE,
  INDEX `name_index`(`name`) USING BTREE,
  CONSTRAINT `FKhms6yb1uh7giab8p324rs2b92` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_battery
-- ----------------------------
DROP TABLE IF EXISTS `bm_battery`;
CREATE TABLE `bm_battery`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `plan` int(11) NOT NULL DEFAULT 0,
  `update_time` datetime(0) NOT NULL,
  `warn` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKp9rq0cbl3a6os62aysc66gw6s`(`group_id`) USING BTREE,
  INDEX `name_index`(`name`) USING BTREE,
  CONSTRAINT `FKp9rq0cbl3a6os62aysc66gw6s` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_group
-- ----------------------------
DROP TABLE IF EXISTS `bm_group`;
CREATE TABLE `bm_group`  (
  `group_id` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `intervals` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ip_addr` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_run` tinyint(1) NOT NULL DEFAULT 1,
  `port` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `norm_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE,
  INDEX `FKh74vd9n07o9wj17dv7ge4fodt`(`norm_id`) USING BTREE,
  INDEX `crtime_index`(`create_time`) USING BTREE,
  CONSTRAINT `FKh74vd9n07o9wj17dv7ge4fodt` FOREIGN KEY (`norm_id`) REFERENCES `bm_norm` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_group_info
-- ----------------------------
DROP TABLE IF EXISTS `bm_group_info`;
CREATE TABLE `bm_group_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `count_node` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `elec_current` double NOT NULL,
  `goup_voltage` double NOT NULL,
  `tempera` double NOT NULL,
  `total_charg_volume` double NOT NULL,
  `total_discharg_volume` double NOT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `mon_info_id` int(11) NULL DEFAULT NULL,
  `mon_status_id` int(11) NULL DEFAULT NULL,
  `sys_status_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKfhgn3ih7g7yiay3h29ssirf6d`(`group_id`) USING BTREE,
  INDEX `FKkto0jd0sa4f19uhmfq10hwik`(`mon_info_id`) USING BTREE,
  INDEX `FKphf0k08vklnkpsjdocd7fi1ve`(`mon_status_id`) USING BTREE,
  INDEX `FKfq9fl5k2sdwbfq6igxlbwhxp4`(`sys_status_id`) USING BTREE,
  CONSTRAINT `FKfhgn3ih7g7yiay3h29ssirf6d` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKfq9fl5k2sdwbfq6igxlbwhxp4` FOREIGN KEY (`sys_status_id`) REFERENCES `bm_system_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKkto0jd0sa4f19uhmfq10hwik` FOREIGN KEY (`mon_info_id`) REFERENCES `bm_monomer_info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKphf0k08vklnkpsjdocd7fi1ve` FOREIGN KEY (`mon_status_id`) REFERENCES `bm_monomer_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_group_limit
-- ----------------------------
DROP TABLE IF EXISTS `bm_group_limit`;
CREATE TABLE `bm_group_limit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `battery_num` int(11) NOT NULL DEFAULT 0,
  `baud_rate` double NOT NULL,
  `charge_judge` double NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `discharge` double NOT NULL,
  `high_inter` double NOT NULL,
  `high_tempera` double NOT NULL,
  `high_voltage` double NOT NULL,
  `inter_test_interval` int(11) NOT NULL,
  `low_tempera` double NOT NULL,
  `low_voltage` double NOT NULL,
  `open_circuit` double NOT NULL,
  `open_circuit_judge` double NOT NULL,
  `socket_addr` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `storage_interval` int(11) NOT NULL,
  `voltage_equaliza` double NOT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `mon_limit_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK3yqiah2loonjvffe2biwo7qnb`(`group_id`) USING BTREE,
  INDEX `FKb7ijquo8ie1rlqkv7wxjethoq`(`mon_limit_id`) USING BTREE,
  CONSTRAINT `FK3yqiah2loonjvffe2biwo7qnb` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKb7ijquo8ie1rlqkv7wxjethoq` FOREIGN KEY (`mon_limit_id`) REFERENCES `bm_monomer_limit` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_limit
-- ----------------------------
DROP TABLE IF EXISTS `bm_limit`;
CREATE TABLE `bm_limit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `group_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `high_plan` double NOT NULL DEFAULT 0,
  `high_warn` double NOT NULL DEFAULT 0,
  `low_plan` double NULL DEFAULT 0,
  `low_warn` double NULL DEFAULT 0,
  `mold` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `val` double NOT NULL DEFAULT 0,
  `group_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK2prdy2rhccq40020j5e00vmiv`(`group_id`) USING BTREE,
  INDEX `type_index`(`type`) USING BTREE,
  CONSTRAINT `FK2prdy2rhccq40020j5e00vmiv` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_log
-- ----------------------------
DROP TABLE IF EXISTS `bm_log`;
CREATE TABLE `bm_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ip_addr` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `params` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKi7xk3b605cji673tfex92c0wd`(`user_id`) USING BTREE,
  CONSTRAINT `FKi7xk3b605cji673tfex92c0wd` FOREIGN KEY (`user_id`) REFERENCES `bm_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_menu
-- ----------------------------
DROP TABLE IF EXISTS `bm_menu`;
CREATE TABLE `bm_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_by` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `href` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `permission` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` tinyint(1) NULL DEFAULT 1,
  `target` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bm_menu
-- ----------------------------
INSERT INTO `bm_menu` VALUES (1, 1, '2018-07-05 14:16:35', '/menu/list', NULL, '基站管理', 0, NULL, NULL, 1, 'mainFrame', 1, '2018-07-05 14:16:38');
INSERT INTO `bm_menu` VALUES (2, 1, '2018-07-05 14:24:07', NULL, NULL, '查询', 1, 'tree:query', NULL, 1, NULL, 1, '2018-07-05 14:24:10');
INSERT INTO `bm_menu` VALUES (3, 1, '2018-07-05 14:24:07', NULL, NULL, '添加', 1, 'tree:add', NULL, 1, NULL, 1, '2018-07-05 14:24:10');
INSERT INTO `bm_menu` VALUES (4, 1, '2018-07-05 14:29:52', NULL, NULL, '编辑', 1, 'tree:update', NULL, 1, NULL, 1, '2018-07-05 14:29:55');
INSERT INTO `bm_menu` VALUES (5, 1, '2018-07-05 14:33:47', NULL, NULL, '冻结', 1, 'tree:congeal', NULL, 1, NULL, 1, '2018-07-05 14:33:51');
INSERT INTO `bm_menu` VALUES (6, 1, '2018-07-05 14:36:02', NULL, NULL, '解冻', 1, 'tree:thaw', NULL, 1, NULL, 1, '2018-07-05 14:36:05');
INSERT INTO `bm_menu` VALUES (7, 1, '2018-07-05 14:44:57', '/norm/list', NULL, '电池规格', 0, NULL, NULL, 1, 'mainFrame', 1, '2018-07-05 14:45:00');
INSERT INTO `bm_menu` VALUES (8, 1, '2018-07-05 14:47:09', NULL, NULL, '查询', 7, 'norm:query', NULL, 1, NULL, 1, '2018-07-05 14:47:14');
INSERT INTO `bm_menu` VALUES (9, 1, '2018-07-05 14:47:09', NULL, NULL, '添加', 7, 'norm:add', NULL, 1, NULL, 1, '2018-07-05 14:47:14');
INSERT INTO `bm_menu` VALUES (10, 1, '2018-07-05 14:47:44', NULL, NULL, '编辑', 7, 'norm:update', NULL, 1, NULL, 1, '2018-07-05 14:47:48');
INSERT INTO `bm_menu` VALUES (11, 1, '2018-07-05 14:48:39', NULL, NULL, '删除', 7, 'norm:delete', NULL, 1, NULL, 1, '2018-07-05 14:48:43');
INSERT INTO `bm_menu` VALUES (12, 1, '2018-07-05 14:49:08', NULL, NULL, '冻结', 7, 'norm:congeal', NULL, 1, NULL, 1, '2018-07-05 14:49:11');
INSERT INTO `bm_menu` VALUES (13, 1, '2018-07-05 14:49:40', NULL, NULL, '解冻', 7, 'norm:thaw', NULL, 1, NULL, 1, '2018-07-05 14:49:42');
INSERT INTO `bm_menu` VALUES (14, 1, '2018-07-05 14:50:52', '/user/list', NULL, '用户管理', 0, NULL, NULL, 1, 'mainFrame', 1, '2018-07-05 14:50:54');
INSERT INTO `bm_menu` VALUES (15, 1, '2018-07-05 14:51:52', NULL, NULL, '查询', 14, 'user:query', NULL, 1, NULL, 1, '2018-07-05 14:51:54');
INSERT INTO `bm_menu` VALUES (16, 1, '2018-07-05 14:51:52', NULL, NULL, '添加', 14, 'user:add', NULL, 1, NULL, 1, '2018-07-05 14:51:54');
INSERT INTO `bm_menu` VALUES (17, 1, '2018-07-05 14:52:25', NULL, NULL, '编辑', 14, 'user:update', NULL, 1, NULL, 1, '2018-07-05 14:52:28');
INSERT INTO `bm_menu` VALUES (18, 1, '2018-07-05 14:53:30', NULL, NULL, '激活', 14, 'user:thaw', NULL, 1, NULL, 1, '2018-07-05 14:53:33');
INSERT INTO `bm_menu` VALUES (19, 1, '2018-07-05 14:53:55', NULL, NULL, '禁用', 14, 'user:congeal', NULL, 1, NULL, 1, '2018-07-05 14:53:58');
INSERT INTO `bm_menu` VALUES (20, 1, '2018-07-05 14:54:33', NULL, NULL, '删除', 14, 'user:delete', NULL, 1, NULL, 1, '2018-07-05 14:54:36');
INSERT INTO `bm_menu` VALUES (21, 1, '2018-07-17 10:23:53', '/role/list', NULL, '权限管理', 0, NULL, NULL, 1, 'mainFrame', 1, '2018-07-17 10:23:56');
INSERT INTO `bm_menu` VALUES (22, 1, '2018-07-05 14:51:52', NULL, NULL, '查询', 21, 'role:query', NULL, 1, NULL, 1, '2018-07-05 14:51:54');
INSERT INTO `bm_menu` VALUES (23, 1, '2018-07-05 14:51:52', NULL, NULL, '添加', 21, 'role:add', NULL, 1, NULL, 1, '2018-07-05 14:51:54');
INSERT INTO `bm_menu` VALUES (24, 1, '2018-07-05 14:52:25', NULL, NULL, '编辑', 21, 'role:update', NULL, 1, NULL, 1, '2018-07-05 14:52:28');
INSERT INTO `bm_menu` VALUES (25, 1, '2018-07-05 14:53:30', NULL, NULL, '激活', 21, 'role:thaw', NULL, 1, NULL, 1, '2018-07-05 14:53:33');
INSERT INTO `bm_menu` VALUES (26, 1, '2018-07-05 14:53:55', NULL, NULL, '禁用', 21, 'role:congeal', NULL, 1, NULL, 1, '2018-07-05 14:53:58');
INSERT INTO `bm_menu` VALUES (27, 1, '2018-07-05 14:54:33', NULL, NULL, '删除', 21, 'role:delete', NULL, 1, NULL, 1, '2018-07-05 14:54:36');
INSERT INTO `bm_menu` VALUES (28, 1, '2018-07-17 10:23:53', '/log/list', NULL, '日志管理', 0, NULL, NULL, 1, 'mainFrame', 1, '2018-07-17 10:23:56');
INSERT INTO `bm_menu` VALUES (29, 1, '2018-07-05 14:54:33', NULL, NULL, '查询', 28, 'log:query', NULL, 1, NULL, 1, '2018-07-05 14:54:36');
INSERT INTO `bm_menu` VALUES (30, 1, '2018-07-05 14:54:33', NULL, NULL, '删除', 28, 'log:delete', NULL, 1, NULL, 1, '2018-07-05 14:54:36');

-- ----------------------------
-- Table structure for bm_monomer_info
-- ----------------------------
DROP TABLE IF EXISTS `bm_monomer_info`;
CREATE TABLE `bm_monomer_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avg_voltage` double NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `max_inter` double NOT NULL,
  `max_inter_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `max_voltage` double NOT NULL,
  `max_voltage_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `min_voltage` double NOT NULL,
  `min_voltage_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_monomer_limit
-- ----------------------------
DROP TABLE IF EXISTS `bm_monomer_limit`;
CREATE TABLE `bm_monomer_limit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_voltage` double NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `high_tempera` double NOT NULL,
  `high_voltage` double NOT NULL,
  `inter_test_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `low_voltage` double NOT NULL,
  `open_circuit` double NOT NULL,
  `stop_voltage` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_monomer_status
-- ----------------------------
DROP TABLE IF EXISTS `bm_monomer_status`;
CREATE TABLE `bm_monomer_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `is_high_inter` tinyint(1) NOT NULL DEFAULT 0,
  `is_high_tempera` tinyint(1) NOT NULL DEFAULT 0,
  `is_high_voltage` tinyint(1) NOT NULL DEFAULT 0,
  `is_low_voltage` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_norm
-- ----------------------------
DROP TABLE IF EXISTS `bm_norm`;
CREATE TABLE `bm_norm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_time` datetime(0) NOT NULL,
  `inter` double NOT NULL,
  `maker` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `voltage` double NOT NULL,
  `volume` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `maker_index`(`maker`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_role
-- ----------------------------
DROP TABLE IF EXISTS `bm_role`;
CREATE TABLE `bm_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_by` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `update_by` int(11) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bm_role
-- ----------------------------
INSERT INTO `bm_role` VALUES (1, 1, '2018-07-05 13:12:33', '超级管理员', '', 1, 1, '2018-08-24 14:09:52');
INSERT INTO `bm_role` VALUES (2, 1, '2018-07-17 10:40:15', '广东省管理员', '', 1, 1, '2018-07-24 15:44:07');

-- ----------------------------
-- Table structure for bm_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `bm_role_menu`;
CREATE TABLE `bm_role_menu`  (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
  INDEX `FKeddhks4wxkh2js4q0kn4a3oxs`(`menu_id`) USING BTREE,
  CONSTRAINT `FKc2n73wyxxqy9vr4m2oif19p66` FOREIGN KEY (`role_id`) REFERENCES `bm_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKeddhks4wxkh2js4q0kn4a3oxs` FOREIGN KEY (`menu_id`) REFERENCES `bm_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bm_role_menu
-- ----------------------------
INSERT INTO `bm_role_menu` VALUES (1, 1);
INSERT INTO `bm_role_menu` VALUES (1, 2);
INSERT INTO `bm_role_menu` VALUES (1, 3);
INSERT INTO `bm_role_menu` VALUES (1, 4);
INSERT INTO `bm_role_menu` VALUES (1, 5);
INSERT INTO `bm_role_menu` VALUES (1, 6);
INSERT INTO `bm_role_menu` VALUES (1, 7);
INSERT INTO `bm_role_menu` VALUES (1, 8);
INSERT INTO `bm_role_menu` VALUES (1, 9);
INSERT INTO `bm_role_menu` VALUES (1, 10);
INSERT INTO `bm_role_menu` VALUES (1, 11);
INSERT INTO `bm_role_menu` VALUES (1, 12);
INSERT INTO `bm_role_menu` VALUES (1, 13);
INSERT INTO `bm_role_menu` VALUES (1, 14);
INSERT INTO `bm_role_menu` VALUES (1, 15);
INSERT INTO `bm_role_menu` VALUES (1, 16);
INSERT INTO `bm_role_menu` VALUES (1, 17);
INSERT INTO `bm_role_menu` VALUES (1, 18);
INSERT INTO `bm_role_menu` VALUES (1, 19);
INSERT INTO `bm_role_menu` VALUES (1, 20);
INSERT INTO `bm_role_menu` VALUES (1, 21);
INSERT INTO `bm_role_menu` VALUES (1, 22);
INSERT INTO `bm_role_menu` VALUES (1, 23);
INSERT INTO `bm_role_menu` VALUES (1, 24);
INSERT INTO `bm_role_menu` VALUES (1, 25);
INSERT INTO `bm_role_menu` VALUES (1, 26);
INSERT INTO `bm_role_menu` VALUES (1, 27);
INSERT INTO `bm_role_menu` VALUES (1, 28);
INSERT INTO `bm_role_menu` VALUES (1, 29);
INSERT INTO `bm_role_menu` VALUES (1, 30);

-- ----------------------------
-- Table structure for bm_role_tree
-- ----------------------------
DROP TABLE IF EXISTS `bm_role_tree`;
CREATE TABLE `bm_role_tree`  (
  `role_id` int(11) NOT NULL,
  `tree_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `tree_id`) USING BTREE,
  INDEX `FK1opm85o1irqb1v62c5xl55crn`(`tree_id`) USING BTREE,
  CONSTRAINT `FK1opm85o1irqb1v62c5xl55crn` FOREIGN KEY (`tree_id`) REFERENCES `bm_tree` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK5f3a5wisvusauhblt20xcy5wp` FOREIGN KEY (`role_id`) REFERENCES `bm_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_role_user
-- ----------------------------
DROP TABLE IF EXISTS `bm_role_user`;
CREATE TABLE `bm_role_user`  (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`) USING BTREE,
  INDEX `FKcvd31i9f1qve5aucwk7ecnqwl`(`user_id`) USING BTREE,
  CONSTRAINT `FKcvd31i9f1qve5aucwk7ecnqwl` FOREIGN KEY (`user_id`) REFERENCES `bm_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKjdc52sew6txtkgvy2hkpbtvfx` FOREIGN KEY (`role_id`) REFERENCES `bm_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bm_role_user
-- ----------------------------
INSERT INTO `bm_role_user` VALUES (1, 1);

-- ----------------------------
-- Table structure for bm_system_status
-- ----------------------------
DROP TABLE IF EXISTS `bm_system_status`;
CREATE TABLE `bm_system_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `is_high_tempera` tinyint(1) NOT NULL DEFAULT 0,
  `is_high_voltage` tinyint(1) NOT NULL DEFAULT 0,
  `is_low_tempera` tinyint(1) NOT NULL DEFAULT 0,
  `is_low_voltage` tinyint(1) NOT NULL DEFAULT 0,
  `is_offline` tinyint(1) NOT NULL DEFAULT 0,
  `is_open_circuit` tinyint(1) NOT NULL DEFAULT 0,
  `is_over_discharge` tinyint(1) NOT NULL DEFAULT 0,
  `is_overcurrent` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_test_data
-- ----------------------------
DROP TABLE IF EXISTS `bm_test_data`;
CREATE TABLE `bm_test_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `battery_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `inter` double NOT NULL DEFAULT 0,
  `tempera` double NOT NULL DEFAULT 0,
  `test_time` datetime(0) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 1,
  `voltage` double NOT NULL DEFAULT 0,
  `battery_id` int(11) NULL DEFAULT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKljb5x9106gdwwblq2sba5ra7r`(`battery_id`) USING BTREE,
  INDEX `FKbygd959lhy2h7e7qyeiam151u`(`group_id`) USING BTREE,
  INDEX `test_time_index`(`test_time`) USING BTREE,
  INDEX `voltage_index`(`voltage`) USING BTREE,
  INDEX `inter_index`(`inter`) USING BTREE,
  INDEX `temper_index`(`tempera`) USING BTREE,
  CONSTRAINT `FKbygd959lhy2h7e7qyeiam151u` FOREIGN KEY (`group_id`) REFERENCES `bm_group` (`group_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKljb5x9106gdwwblq2sba5ra7r` FOREIGN KEY (`battery_id`) REFERENCES `bm_battery` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_tree
-- ----------------------------
DROP TABLE IF EXISTS `bm_tree`;
CREATE TABLE `bm_tree`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_id` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `number` int(11) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL,
  `plan` int(11) NOT NULL DEFAULT 0,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `update_time` datetime(0) NOT NULL,
  `url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `warn` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid_index`(`parent_id`) USING BTREE,
  INDEX `name_index`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bm_user
-- ----------------------------
DROP TABLE IF EXISTS `bm_user`;
CREATE TABLE `bm_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nick_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `update_time` datetime(0) NOT NULL,
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bm_user
-- ----------------------------
INSERT INTO `bm_user` VALUES (1, '2018-06-07 09:37:13', '315646125@qq.com', 'admin', '1d324ddc14bebb518b63d8627944c4df003457e76a3d57aa2d888df7424585767e1f2c2f0be786a4d802c1db56139cd8d1fdfd13', '12345678', 1, '2018-06-07 09:37:21', 'admin');

-- ----------------------------
-- Procedure structure for pro_save_battery
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_save_battery`;
delimiter ;;
CREATE PROCEDURE `pro_save_battery`(groupId INT, num INT)
BEGIN
	DECLARE cu_time DATETIME DEFAULT (SELECT NOW() FROM DUAL);
	DECLARE i INT DEFAULT 1;
	while i <= num DO
		INSERT INTO 
		bm_battery(`name`, create_time, update_time, group_id)
		VALUES(CONCAT('电池_',i), cu_time, cu_time, groupId);
		SET i := i + 1;
	END WHILE;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
