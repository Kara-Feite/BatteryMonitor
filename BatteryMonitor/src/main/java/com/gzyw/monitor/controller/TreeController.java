package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: TreeController
 * @Description: 基站树形节点控制层
 * @author LW
 * @date 2018年6月14日 下午2:24:25
 */
@Controller
@RequestMapping("/tree")
public class TreeController {

	/**
	 * 节点服务层接口
	 */
	@Autowired
	private TreeService treeService;

	/**
	 * 父节点
	 */
	private Integer parentId;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: home
	 * @Description: home
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	String home(Model model, Integer id) {
		parentId = id;
		model.addAttribute("id", id);
		return "ztree/home";
	}

	/**
	 * 
	 * @Title: frame
	 * @Description: frame页码
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/frame", method = RequestMethod.GET)
	String frame(Model model, Integer id) {
		parentId = id;
		model.addAttribute("id", id);
		return "ztree/frame";
	}

	/**
	 * frame顶部
	 * 
	 * @Title: title
	 * @Description: 头部页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/title", method = RequestMethod.GET)
	String title(Model model) {
		model.addAttribute("id", parentId);
		return "ztree/title";
	}

	/**
	 * home顶部
	 * 
	 * @Title: top
	 * @Description: 顶部页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/top", method = RequestMethod.GET)
	String top(Model model) {
		model.addAttribute("id", parentId);
		return "ztree/top";
	}

	/**
	 * 
	 * @Title: content
	 * @Description: content页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/content", method = RequestMethod.GET)
	String content(Model model, Integer id) {
		Tree tree = treeService.getTreeById(id);
		model.addAttribute("tree", tree);
		return "ztree/content";
	}
	
	/**
	 * 
	 * @Title: showSave
	 * @Description: 跳转到节点添加页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.GET)
	String showSave(Model model, Integer id) {
		Tree tree = new Tree();
		tree.setParentId(id);
		model.addAttribute("tree", tree);
		return "ztree/save";
	}
	
	/**
	 * 
	 * @Title: saveTree
	 * @Description: 添加节点
	 * @param tree
	 * @param isGroup
	 * @return
	 */
	@RequestMapping(value="/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "添加基站节点")
	@Permissions("tree:add")
	public ResultDto saveTree(Tree tree, Boolean isGroup){
		if (tree == null || StringUtil.isEmpty(tree.getName())) {
			return ResultDto.build(500, "名称不能为空");
		}
		if (isGroup != null && isGroup) {
			tree.setUrl("../group/home?groupId=");
		}
		return treeService.saveTree(tree);
	}	
	
	/**
	 * 
	 * @Title: showEdit
	 * @Description: 跳转到编辑页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	String showEdit(Model model, Integer id, HttpSession session) {
		Tree tree = treeService.getTreeById(id);
		List<Tree> listTree = treeService.listTreeByPid(id);
		model.addAttribute("readOnly", false);
		if (CollectionUtil.isNotEmpty(listTree)) {
			model.addAttribute("readOnly", true);
		}
		model.addAttribute("tree", tree);
		return "ztree/edit";
	}
	
	/**
	 * 
	 * @Title: update
	 * @Description: 更新节点
	 * @param tree
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "更新基站节点")
	@Permissions("tree:update")
	public ResultDto update(Tree tree, Boolean isGroup){
		if (tree == null || StringUtil.isEmpty(tree.getName())) {
			return ResultDto.build(500, "名称不能为空");
		}
		if (isGroup != null && isGroup) {
			tree.setUrl("../group/home?groupId=");
		} else {
			tree.setUrl("../tree/home?id=");
		}
		return treeService.updateTree(tree);
	}

	/**
	 * 
	 * @Title: datagrid
	 * @Description: 获取节点分页数据
	 * @param page 页码
	 * @param norm 基站节点
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Tree tree,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return treeService.getDatagrid(page, rows, sort, order, tree);
	}
	
	/**
	 * 
	 * @Title: congeal
	 * @Description: 冻结
	 * @param ids 选中的id数组
	 * @return
	 */
	@RequestMapping(value = "/congeal")
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "禁用基站节点")
	@Permissions("tree:congeal")
	public ResultDto congeal(int[] ids) {
		try {
			return treeService.updateTreeByState(ids, Boolean.FALSE);
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，冻结失败！");
		}
	}
	
	/**
	 * 
	 * @Title: thaw
	 * @Description: 解冻
	 * @param ids 选中的id数组
	 * @return
	 */
	@RequestMapping(value = "/thaw")
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "激活基站节点")
	@Permissions("tree:thaw")
	public ResultDto thaw(int[] ids) {
		try {
			return treeService.updateTreeByState(ids, Boolean.TRUE);
		} catch(Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，解冻失败！");
		}
	}
	
	/**
	 * 
	 * @Title: get
	 * @Description: 根据id获取基站节点
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/get/{id}")
	@ResponseBody
	ResultDto get(@PathVariable("id") Integer id) {
		Tree tree = treeService.getTreeById(id);
		if (tree == null) {
			return ResultDto.build(400, "未找到该基站节点");
		}
		return ResultDto.ok(tree);
	}

}
