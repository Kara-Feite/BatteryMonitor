package com.gzyw.monitor.dto;

/**
 * 
 * @ClassName: TempDto
 * @Description: 临时变量类
 * @author LW
 * @date 2018年7月11日 下午2:16:42
 */
public class TempDto {
	
	/**
	 * id
	 */
	private Integer id;
	
	/**
	 * ids
	 */
	private String ids;
	
	/**
	 * 节点ID
	 */
	private Integer treeId;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 编号
	 */
	private String number;
	
	/**
	 * 开始时间
	 */
	private String startTime;
	
	/**
	 * 时间
	 */
	private String toTime;
	
	/**
	 * 结束时间
	 */
	private String endTime;
	
	/**
	 * 类型
	 */
	private String type;
	
	/**
	 * 是否处理
	 */
	private Boolean state;
	
	/**
	 * 告警类型
	 */
	private String mold;

	/**
	 * 参考值
	 */
	private Double val;

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getIds
	 * @Description: 获取ids
	 * @return: String ids
	 */
	public String getIds() {
		return ids;
	}

	/**
	 * @Title: setIds
	 * @Description: 设置ids
	 */
	public void setIds(String ids) {
		this.ids = ids;
	}

	/**
	 * @Title: getTreeId
	 * @Description: 获取treeId
	 * @return: Integer treeId
	 */
	public Integer getTreeId() {
		return treeId;
	}

	/**
	 * @Title: setTreeId
	 * @Description: 设置treeId
	 */
	public void setTreeId(Integer treeId) {
		this.treeId = treeId;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getNumber
	 * @Description: 获取number
	 * @return: String number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @Title: setNumber
	 * @Description: 设置number
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @Title: getStartTime
	 * @Description: 获取startTime
	 * @return: String startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @Title: setStartTime
	 * @Description: 设置startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @Title: getToTime
	 * @Description: 获取toTime
	 * @return: String toTime
	 */
	public String getToTime() {
		return toTime;
	}

	/**
	 * @Title: setToTime
	 * @Description: 设置toTime
	 */
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	/**
	 * @Title: getEndTime
	 * @Description: 获取endTime
	 * @return: String endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @Title: setEndTime
	 * @Description: 设置endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getMold
	 * @Description: 获取mold
	 * @return: String mold
	 */
	public String getMold() {
		return mold;
	}

	/**
	 * @Title: setMold
	 * @Description: 设置mold
	 */
	public void setMold(String mold) {
		this.mold = mold;
	}

	/**
	 * @Title: getVal
	 * @Description: 获取val
	 * @return: Double val
	 */
	public Double getVal() {
		return val;
	}

	/**
	 * @Title: setVal
	 * @Description: 设置val
	 */
	public void setVal(Double val) {
		this.val = val;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "TempDto [id=" + id + ", ids=" + ids + ", treeId=" + treeId + ", name=" + name + ", number=" + number
				+ ", startTime=" + startTime + ", toTime=" + toTime + ", endTime=" + endTime + ", type=" + type
				+ ", state=" + state + ", mold=" + mold + ", val=" + val + "]";
	}
	
}
