package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.RoleDao;
import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToMenu;
import com.gzyw.monitor.entity.RoleToTree;
import com.gzyw.monitor.entity.RoleToUser;

/**
 * 
 * @ClassName: RoleDaoImpl
 * @Description: 角色持久层实现类
 * @author LW
 * @date 2018年7月8日 下午2:45:58
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

	@Override
	public Long countRole(Role role) {
		String hql = "SELECT COUNT(*) FROM Role WHERE 1 = 1";
		hql = setHQLParam(hql, null, null, role);
		return count(hql);
	}

	@Override
	public List<Role> listRoleByPage(Integer page, Integer rows, String sort, String order, Role role) {
		String hql = "FROM Role WHERE 1 = 1";
		hql = setHQLParam(hql, sort, order, role);
		return queryByPage(hql, page, rows);
	}
	
	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置HQL语句参数
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, Role role) {
		String name = role.getName();
		if (StringUtil.isNotEmpty(name)) {
			hql += " AND maker like '%" + name + "%'";
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			hql += " ORDER BY " + sort + " " + order;
		}
		return hql;
	}

	@Override
	public void saveRoleToMenu(Integer roleId, String[] ids) {
		String sql = "INSERT INTO bm_role_menu VALUES(?, ?)";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		for (String id : ids) {
			q.setParameter(1, roleId).setParameter(2, id).executeUpdate();
		}
	}

	@Override
	public void saveRoleToTree(Integer roleId, String[] ids) {
		String sql = "INSERT INTO bm_role_tree VALUES(?, ?)";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		for (String id : ids) {
			q.setParameter(1, roleId).setParameter(2, id).executeUpdate();
		}
	}
	
	@Override
	public void saveRoleToUser(Integer userId, int[] listRole) {
		String sql = "INSERT INTO bm_role_user VALUES(?, ?)";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		for (int id : listRole) {
			q.setParameter(1, id).setParameter(2, userId).executeUpdate();
		}
	}

	@Override
	public List<RoleToMenu> listMenuById(Integer id) {
		String sql = "SELECT role_id roleId, menu_id menuId FROM bm_role_menu WHERE role_id = ?";
		@SuppressWarnings({ "unchecked", "deprecation" })
		Query<RoleToMenu> q = getSession().createNativeQuery(sql)
				.setResultTransformer(Transformers.aliasToBean(RoleToMenu.class));
		return q.setParameter(1, id).list();
	}

	@Override
	public List<RoleToTree> listTreeById(Integer id) {
		String sql = "SELECT role_id roleId, tree_id treeId FROM bm_role_tree WHERE role_id = ?";
		@SuppressWarnings({ "unchecked", "deprecation" })
		Query<RoleToTree> q = getSession().createNativeQuery(sql)
				.setResultTransformer(Transformers.aliasToBean(RoleToTree.class));
		return q.setParameter(1, id).list();
	}
	
	@Override
	public List<RoleToUser> listUserById(Integer id) {
		String sql = "SELECT role_id roleId, user_id userId FROM bm_role_user WHERE role_id = ?";
		@SuppressWarnings({ "unchecked", "deprecation" })
		Query<RoleToUser> q = getSession().createNativeQuery(sql)
				.setResultTransformer(Transformers.aliasToBean(RoleToUser.class));
		return q.setParameter(1, id).list();
	}


	@Override
	public void updateRole(Role role) {
		String hql = "UPDATE Role SET name = ?, remark = ?, updateBy = ?, updateTime = ? WHERE id = ?";
		update(hql, role.getName(), role.getRemark(), role.getUpdateBy(), role.getUpdateTime(), role.getId());
	}

	@Override
	public void deleteRoleToMenu(Integer id) {
		String sql = "DELETE FROM bm_role_menu WHERE role_id = ?";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		q.setParameter(1, id).executeUpdate();
	}

	@Override
	public void deleteRoleToTree(Integer id) {
		String sql = "DELETE FROM bm_role_tree WHERE role_id = ?";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		q.setParameter(1, id).executeUpdate();
	}

	@Override
	public void deleteRoleToUser(Integer userId) {
		String sql = "DELETE FROM bm_role_user WHERE user_id = ?";
		NativeQuery<?> q = getSession().createNativeQuery(sql);
		q.setParameter(1, userId).executeUpdate();
	}

	@Override
	public List<Role> listRoleByAll() {
		String hql = "FROM Role";
		return queryForList(hql);
	}
	
	@Override
	public List<RoleToUser> listRoleToUser(Integer userId) {
		String sql = "SELECT role_id roleId, user_id userId FROM bm_role_user WHERE user_id = ?";
		@SuppressWarnings({ "unchecked", "deprecation" })
		Query<RoleToUser> q = getSession().createNativeQuery(sql)
				.setResultTransformer(Transformers.aliasToBean(RoleToUser.class));
		return q.setParameter(1, userId).list();
	}
	
}
