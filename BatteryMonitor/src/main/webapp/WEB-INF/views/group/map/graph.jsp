<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池组统计图</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src="${ctxStatic}/js/echarts/echarts.min.js"></script>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<style>
body{background-color:#fff;}
#denglu *{vertical-align:middle;}
table {
    cellspacing:0;
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;
}
.bordered tr:hover {
    background: #fbf8e9;
    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;
    transition: all 0.1s ease-in-out;
}
.bordered th {
    padding: 7px;
    text-align: center;
    cellspacing:0;
}
.bordered td{
    padding: 7px;
    text-align: center;
    cellspacing:0;
}
.bordered th {
     background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
     background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
     background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
     background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
     background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
     background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
}
.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}
.bordered  tr:nth-of-type(2n){background:#FFFFFF;cursor: pointer;}
.bordered  tr:nth-of-type(2n+1){background:#F7FAFC;cursor: pointer;}
.bordered  tbody tr:hover{
    background: #fbf8e9;
    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;
    transition: all 0.1s ease-in-out;
}
</style>
<script>
function query(){
   	document.myForm.action="${ctx}/group/graph";
  	document.myForm.submit();
}
</script>
</head>
<body>
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north'" style="height:34px;">
			<sf:form name="myForm" method="get" modelAttribute="temp" enctype="multipart/form-data">
				<sf:hidden path="treeId"/>
				<table width="100%" bgcolor="#f5fafe" id="denglu"> 		 		   																																			   
					<tr>
						<td>
							<strong>测量类型：</strong>
								<sf:select path="type">
								 	<sf:option value="单体电压/V"/> 		           
						            <sf:option value="单体内阻/mΩ"/> 
						            <sf:option value="单体温度/℃"/>  
								</sf:select>
							&nbsp;&nbsp;
							<strong>参 考 值：</strong><sf:input path="val" size="10"/>
							&nbsp;&nbsp;
							<strong>测试日期：</strong>
							<sf:input path="startTime" size="20" editable="false" autocomplete='off'
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查  询</a>
							&nbsp;&nbsp; 
							<!-- <a class="easyui-linkbutton" data-options="iconCls:'icon-print'" 
								onclick="checkExecl();" href="javascript:void(0);" style="width:130px;">导出Excel报告</a>	 -->
						</td>									
					</tr>											
				</table>		
			</sf:form>		
		</div>
		<!-- ------------------------------------------ -->
		<div data-options="region:'center'">
			<div id='view' style="width:800px;height:420px;margin:5px 0 0 200px;"></div>
		</div>
		<c:set var="name" value="${graphDto.name}"></c:set>
		<c:set var="limitVal" value="${graphDto.limitVal}"></c:set>
		<script>
			function getGroupNum(num) {
				var arr = [];
				for (var i = 1; i <= num; i++) {
					arr.push(i);
				}
				return arr;
			}
			var myChart = echarts.init(document.getElementById('view'));
	        // 指定图表的配置项和数据
	        var option = {
	            title : {
	                text: '${graphDto.type}'
	            },
	            tooltip : {
	                trigger: 'axis',
	                formatter: '{b0}<br />${name} : {c0}${graphDto.unit}'
	            },
	            animation : false,
	            legend: {
	                selectedMode : false,
	                data:[
	                    /*icon : 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'*/
	                    {
	                        name : '未超出${name}警告线'
	                    }
	                ]
	            },
	            toolbox: {
	                show : true,
	                orient: 'horizontal',
	                showTitle: true,
	                feature : {
	                    dataView:{
	                        show: true,
	                        readOnly: true,
	                        optionToContent: function(opt) {
	                            var colName = "序号";
	                            var typeName = "${name}值";
	                            var dataview = opt.toolbox[0].feature.dataView;  //获取dataview
	                            var table = '<div style="position:absolute;top: 5px;left: 0;right: 0;line-height: 1.4em;text-align:center;font-size:14px;"></div>';
	                            table += getTable(opt, colName, typeName);
	                            return table;
	                        }
	                    },
	                    magicType : {show: true, type: ['line', 'bar']},
	                    restore : {show: true},
	                    saveAsImage : {show: true}
	                }
	            },
	            /*['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3']*/
	            color : ['#2284c7'],
	            calculable : true,
	            xAxis : [
	                {
	                    gridIndex : 0,
	                    type : 'category',
	                    data : getGroupNum('${graphDto.num}')
	                }
	            ],
	            yAxis : [
	                {
	                    type : 'value'
	                }
	            ],
	            series : [
	                {
	                    name:'未超出${name}警告线',
	                    type:'bar',
	                    data:[
	                    	<c:forEach items="${graphDto.listData}" var="data">
	                    		<c:set var="val" value="${data.voltage}"></c:set>
	                    		{
	                    			name : '${data.batteryName}',
	                    			value : '${val}'
	                    			<c:if test="${val >= limitVal}">
		                    			,
			                            symbolSize : 10,
			                            itemStyle:{color : '#c23531'}
	                    			</c:if>
	                    		},
	                    	</c:forEach>
	                    ],
	                    markPoint : {
	                        data : [
	                            {type : 'max', name: '最大值', itemStyle:{color : '#c23531'}},
	                            {type : 'min', name: '最小值', itemStyle:{color : '#2284c7'}}
	                        ]
	                    },
	                    markLine: {
	                        itemStyle: {
	                            normal: {lineStyle: {type: 'dashed'},label: {show: true}}
	                        },
	                        data: [
	                            {
	                                name: '平均线',
	                                // 支持 'average', 'min', 'max'
	                                type: 'average'
	                            },
	                            {
	                                itemStyle:{color : '#c23531'},
	                                name : '${name}警告门限',
	                                yAxis: '${limitVal}'
	                            }
	                        ]
	                    }
	                }
	            ]
	        };
			// 获取数据表格	        
	        function getTable(opt,colName,typeName){
	            var axisData = opt.xAxis[0].data;//获取图形的data数组
	            var series = opt.series;//获取series
	            var num = 0;//记录序号
	            var sum = new Array();//获取合计数组（根据对应的系数生成相应数量的sum）
	            var len = series.length;
	            for(var i=0; i<len; i++){
	                sum[i] = 0;
	            }
	            var table = '<table class="bordered"><thead><tr>'
	                + '<th>'+colName+'</th>'
	                + '<th>'+typeName+'</th>';
	            table += '</tr></thead><tbody>';
	            for (var i = 0, l = axisData.length; i < l; i++) {
	                num += 1;
	                table += '<tr>'
	                    + '<td>电池_' + axisData[i] + '</td>';
	                for(var j=0; j<len;j++){
	                    if(series[j].data[i]){
	                        var val = parseFloat(series[j].data[i].value).toFixed(3);
	                        table += '<td>' + val + '</td>';
	                    }else{
	                        table += '<td>' + 0.0 + '</td>';
	                    }
	
	                }
	                table += '</tr>';
	            }
	            table += '</tr></tbody></table>';
	            return table;
	        }
	        myChart.setOption(option);
        	/* 将图片信息添加到sessionStorage */
        	console.log('${name}');
	        if ('电压' === '${name}') {
	        	sessionStorage.setItem('voltage', myChart.getDataURL()); 
	        } else if ('内阻' === '${name}') {
	        	sessionStorage.setItem('inter', myChart.getDataURL()); 
	        }
		</script>
	</div>	
</body>
</html>