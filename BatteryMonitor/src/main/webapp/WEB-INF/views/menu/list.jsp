<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>基站</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
#denglu *{vertical-align:middle;}
a{width:80px;}
</style>
<script>
$(function(){
	$('#menu_lists').datagrid({
		url:'${ctx}/menu/datagrid',
		pagination:true,
		pageSize:20,
		pageList:[10,20,30,40,50],
		sortName : 'create_time',
		sortOrder : 'ASC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[
			{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'name',
		        title:'名 称',
		        width:50,
		        align:'center'
			},{
		        field:'warn',
		        title:'当前告警',
		        width:30,
		        align:'center'
			},{
		        field:'plan',
		        title:'当前预警',
		        width:30,
		        align:'center'
			},{
				field:'remark',
				title:'备 注',
				width:50,
				align:'center'
			},{
				field:'createTime',
				title:'创建时间',
				width:50,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			},{
				field : 'state',
				title : '状 态',
				width:30,
				align:'center',
				formatter : function(val, row) {
					if (val == true) {
						return '<font style="color:green">正常</font>';
					}else{
						return '<font style="color:red">冻结</font>';
					}
				}
			}
		]],
		toolbar:[				
			{text:'编 辑',iconCls:'icon-edit',handler:function(){edit();}},'-',					
			{text:'冻 结',iconCls:'icon-remove',handler:function(){congeal();}},'-',	
			{text:'解 冻',iconCls:'icon-reload',handler:function(){thaw();}},'-',			
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});

function query(){
	$('#menu_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function edit(){
	<permission:notPermission name="tree:update">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows = $('#menu_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href='${ctx}/menu/edit?id='+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择要编辑的数据'});
	}
}
function add() {
	<permission:notPermission name="tree:add">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	window.location.href='${ctx}/menu/edit';
}
function congeal(){
	<permission:notPermission name="tree:congeal">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#menu_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要冻结选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/menu/congeal',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#menu_lists').datagrid('load',{});
						$('#menu_lists').datagrid('unselectAll');
						window.parent.tree.hideNodes(r.data);
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要冻结的记录'});
	}
}
function thaw(){
	<permission:notPermission name="tree:thaw">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#menu_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要解冻选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}			
				$.ajax({
					type:'get',
					url:'${ctx}/menu/thaw',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#menu_lists').datagrid('load',{});
						$('#menu_lists').datagrid('unselectAll');
						var data = r.data;
						var len = window.parent.tree.showNodes(data);
						if (!len) {
							for (var d in data) {
								var id = data[d];
								if (window.parent.tree.getNodesByIds([id]).length > 0) {
									continue;
								}
								$.get('${ctx}/tree/get/' + id, function(result) {
									if (result.status === 200) {
										window.parent.tree.addNodes([result.data]);
									}
								});
							}
						}
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要解冻的记录'});
	}
}
function unselect(){
	$('#menu_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>
<body>
	<div class='easyui-layout' fit='true' border='false'>
		<div data-options='region:"north",title:"查询条件"' style='height:60px;'>
			<form id='search_form'>
				<table width='100%' bgcolor='#f5fafe'>
					<tr height='25'>
						<td id='denglu'><img src='${ctxStatic}/images/yin.gif' width='22'>							
							<strong>名 称：</strong><input name='name' size='15'/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class='easyui-linkbutton' data-options='iconCls:"icon-search"' 
								onclick='query();' href='javascript:void(0);'>查  询</a>						
							&nbsp;&nbsp;
							<a class='easyui-linkbutton' data-options='iconCls:"icon-remove"' 
								onclick='clean();' href='javascript:void(0);'>清  空</a>
							&nbsp;&nbsp;
							<a class='easyui-linkbutton' data-options='iconCls:"icon-add"' 
								target='mainFrame' href='javascript:add()'>添 加</a>
						</td>
					</tr>					
				</table>
			</form>
		</div>
		<!-- ------------------------------------------ -->
		<div data-options='region:"center"'>
			<table id='menu_lists'></table>
		</div>
	</div>
</body>
</html>
