package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.entity.User;

/**
 * 
 * @ClassName: UserDao
 * @Description: 用户持久层
 * @author LW
 * @date 2018年6月6日 下午8:12:23
 */
public interface UserDao extends BaseDao<User> {
	
	/**
	 * 
	 * @Title: getUserByName
	 * @Description: 根据姓名获取用户
	 * @param name
	 * @return
	 */
	User getUserByName(String name);

	/**
	 * 
	 * @Title: listUserByPage
	 * @Description: 分页获取用户数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @param user 用户
	 * @return
	 */
	List<User> listUserByPage(Integer page, Integer rows, String sort, String order, User user);

	/**
	 * 
	 * @Title: countUser
	 * @Description: 获取用户总数
	 * @return
	 */
	Long countUser();

	/**
	 * 
	 * @Title: updateUser
	 * @Description: 更新用户
	 * @param user
	 * @return
	 */
	Integer updateUser(User user);

	/**
	 * 
	 * @Title: updateState
	 * @Description: 更新用户状态
	 * @param id
	 * @param state
	 * @return
	 */
	Integer updateState(int id, boolean state);

	/**
	 * 
	 * @Title: changePwd
	 * @Description: 修改密码
	 * @param newPwd
	 * @param userId
	 * @return
	 */
	Integer changePwd(String newPwd, Integer userId);

	/**
	 * 
	 * @Title: listUserByAll
	 * @Description: 获取所有用户
	 * @return
	 */
	List<User> listUserByAll();

}
