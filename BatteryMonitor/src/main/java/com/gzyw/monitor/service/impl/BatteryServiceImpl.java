package com.gzyw.monitor.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.dao.BatteryDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Battery;
import com.gzyw.monitor.service.BatteryService;

/**
 * 
 * @ClassName: BatteryServiceImpl
 * @Description: 电池服务层实现类
 * @author LW
 * @date 2018年6月14日 下午4:36:35
 */
@Service
public class BatteryServiceImpl implements BatteryService {
	
	@Autowired
	private BatteryDao batteryDao;

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Battery battery, Integer groupId) {
		Long count = batteryDao.countBattery(battery, groupId);
		List<Battery> listBattery = batteryDao.listBatteryByPage(page, rows, sort, order, battery, groupId);
		return new EUDataGridDto(count, listBattery);
	}

	@Override
	public Battery getBatteryById(Integer id) {
		return batteryDao.get(id);
	}

	@Override
	public ResultDto deleteBattery(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的电池！");
		}
		for (int id : ids) {
			Battery battery = batteryDao.get(id);
			if (battery != null) {
				batteryDao.delete(battery);
			}
		}
		return ResultDto.build(200, "删除电池成功！");
	}
	
}
