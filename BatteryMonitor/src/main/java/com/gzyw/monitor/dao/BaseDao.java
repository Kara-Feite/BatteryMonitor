package com.gzyw.monitor.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.jdbc.Work;

/**
 * 
 * @ClassName: BaseDao
 * @Description: 基础CRUD持久层封装
 * @author LW
 * @date 2018年6月6日 下午2:31:00
 */
public interface BaseDao<T> {

	/**
	 * 
	 * @Title: save
	 * @Description: 添加
	 * @param t
	 * @return
	 */
	Serializable save(T t);

	/**
	 * 
	 * @Title: delete
	 * @Description: 根据id删除
	 * @param id
	 */
	void delete(Serializable id);

	/**
	 * 
	 * @Title: delete
	 * @Description: 根据对象删除
	 * @param t
	 */
	void delete(T t);
	
	/**
	 * 
	 * @Title: delete
	 * @Description: 根据条件删除
	 * @param hql
	 * @param params
	 */
	int delete(String hql, Object... params);
	
	/**
	 * 
	 * @Title: delete
	 * @Description: 根据条件删除
	 * @param hql
	 * @param params
	 */
	int delete(String hql, Map<String, Object> params);

	/**
	 * 
	 * @Title: update
	 * @Description: 更新
	 * @param t
	 */
	void update(T t);
	
	/**
	 * 
	 * @Title: update
	 * @Description: 根据条件更新
	 * @param hql
	 * @param params
	 * @return
	 */
	int update(String hql, Object... params);
	
	/**
	 * 
	 * @Title: update
	 * @Description: 根据条件更新
	 * @param hql
	 * @param params
	 * @return
	 */
	int update(String hql, Map<String, Object> params);

	/**
	 * 
	 * @Title: saveOrUpdate
	 * @Description: 添加或删除
	 * @param t
	 */
	void saveOrUpdate(T t);

	/**
	 * 
	 * @Title: get
	 * @Description: 根据id获取对象
	 * @param id
	 * @return
	 */
	T get(Serializable id);
	
	/**
	 * 
	 * @Title: get
	 * @Description: 传参获取对象
	 * @param hql
	 * @param params
	 * @return
	 */
	T get(String hql, Object... params);
	
	/**
	 * 
	 * @Title: get
	 * @Description: 传参获取对象
	 * @param hql
	 * @param params
	 * @return
	 */
	T get(String hql, Map<String, Object> params);

	/**
	 * 
	 * @Title: queryForList
	 * @Description: 传参获取集合数据
	 * @param hql HQL查询语句
	 * @param params 参数
	 * @return
	 */
	List<T> queryForList(String hql, Object... params);

	/**
	 * 
	 * @Title: queryForList
	 * @Description: 传参获取数据
	 * @param hql HQL查询语句
	 * @param params 参数
	 * @return
	 */
	List<T> queryForList(String hql, Map<String, Object> params);

	/**
	 * 
	 * @Title: queryByPage
	 * @Description: 分页获取数据
	 * @param hql HQL查询语句
	 * @param pageNo 页码
	 * @param pageSize 页大小
	 * @param params 参数
	 * @return
	 */
	List<T> queryByPage(String hql, int pageNo, int pageSize, Object... params);

	/**
	 * 
	 * @Title: queryByPage
	 * @Description: 传参分页获取数据
	 * @param hql HQL查询语句
	 * @param pageNo 页码
	 * @param pageSize 页大小
	 * @param params 参数
	 * @return
	 */
	List<T> queryByPage(String hql, int pageNo, int pageSize, Map<String, Object> params);

	/**
	 * 
	 * @Title: count
	 * @Description: 获取记录数
	 * @param hql HQL查询语句
	 * @param params 参数
	 * @return
	 */
	Long count(String hql, Object... params);

	/**
	 * 
	 * @Title: count
	 * @Description: 传参获取记录数
	 * @param hql HQL查询语句
	 * @param params 参数
	 * @return
	 */
	Long count(String hql, Map<String, Object> params);
	
	/**
	 * 
	 * @Title: uniqueResult
	 * @Description: 获取唯一值
	 * @param hql
	 * @param params
	 * @return
	 */
	Object uniqueResult(String hql, Object... params);
	
	/**
	 * 
	 * @Title: uniqueResult
	 * @Description: 获取唯一值
	 * @param hql
	 * @param params
	 * @return
	 */
	Object uniqueResult(String hql, Map<String, Object> params);
	
	/**
	 * 
	 * @Title: getAutoId
	 * @Description: 获取自增长id值
	 * @param tableName
	 * @return
	 */
	Integer getAutoId(String tableName);
	
	/**
	 * 
	 * @Title: executeProcedure
	 * @Description: 调用无返回值得存储过程
	 * @param work
	 */
	void executeProcedure(Work work);

}