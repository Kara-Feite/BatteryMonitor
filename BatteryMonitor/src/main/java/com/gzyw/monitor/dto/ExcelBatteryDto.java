package com.gzyw.monitor.dto;

import com.gzyw.monitor.annotation.ExcelColumn;
import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.Norm;

/**
 * 
 * @ClassName: ExcelBatteryDto
 * @Description: 电池excel导出实体类
 * @author LW
 * @date 2018年9月10日 上午11:22:50
 */
public class ExcelBatteryDto {
	
	/**
	 * 生产厂家
	 */
	@ExcelColumn(row = 1, column = 0, value = "生产厂家")
	private String maker;
	
	/**
	 * 电池组电压
	 */
	@ExcelColumn(row = 1, column = 2, value = "参考电压/V")
	private Double groupVoltage;
	
	/**
	 * 投运时间
	 */
	@ExcelColumn(row = 1, column = 4, value = "投运日期")
	private String beginTime;
	
	/**
	 * 型号
	 */
	@ExcelColumn(row = 2, column = 0, value = "型号")
	private String model;
	
	/**
	 * 内阻
	 */
	@ExcelColumn(row = 2, column = 2, value = "参考内阻/mΩ ")
	private Double groupInter;
	
	/**
	 * 电池组电压
	 */
	@ExcelColumn(row = 2, column = 4, value = "电压/V")
	private String voltage;
	
	/**
	 * 电池类型
	 */
	@ExcelColumn(row = 3, column = 0, value = "电池类型")
	private String type;
	
	/**
	 * 容量
	 */
	@ExcelColumn(row = 3, column = 2, value = "电池容量/Ah")
	private Double volume;
	
	/**
	 * 电池组电压
	 */
	@ExcelColumn(row = 3, column = 4, value = "内阻/mΩ")
	private String inter;
	
	/**
	 * @Title: ExcelBatteryDto
	 * @Description: ExcelBatteryDto构造函数
	 * @param maker
	 * @param groupVoltage
	 * @param beginTime
	 * @param model
	 * @param groupInter
	 * @param voltage
	 * @param type
	 * @param volume
	 * @param inter
	 */
	public ExcelBatteryDto(Group group, MaxMinDto maxMinDto) {
		Norm norm = group.getNorm();
		this.maker = norm.getMaker();
		this.groupVoltage = norm.getVoltage();
		this.beginTime = DateUtil.format(norm.getBeginTime(), "yyyy年MM月dd日");
		this.model = norm.getModel();
		this.groupInter = norm.getInter();
		this.voltage = "最大：" + maxMinDto.getMaxVoltage() + "  最小：" + maxMinDto.getMinVoltage();
		this.type = norm.getType();
		this.volume = norm.getVolume();
		this.inter = "最大：" + maxMinDto.getMaxInter() + "  最小：" + maxMinDto.getMinInter();
	}

	/**
	 * @Title: getMaker
	 * @Description: 获取maker
	 * @return: String maker
	 */
	public String getMaker() {
		return maker;
	}

	/**
	 * @Title: setMaker
	 * @Description: 设置maker
	 */
	public void setMaker(String maker) {
		this.maker = maker;
	}

	/**
	 * @Title: getGroupVoltage
	 * @Description: 获取groupVoltage
	 * @return: Double groupVoltage
	 */
	public Double getGroupVoltage() {
		return groupVoltage;
	}

	/**
	 * @Title: setGroupVoltage
	 * @Description: 设置groupVoltage
	 */
	public void setGroupVoltage(Double groupVoltage) {
		this.groupVoltage = groupVoltage;
	}

	/**
	 * @Title: getBeginTime
	 * @Description: 获取beginTime
	 * @return: String beginTime
	 */
	public String getBeginTime() {
		return beginTime;
	}

	/**
	 * @Title: setBeginTime
	 * @Description: 设置beginTime
	 */
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * @Title: getModel
	 * @Description: 获取model
	 * @return: String model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @Title: setModel
	 * @Description: 设置model
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @Title: getGroupInter
	 * @Description: 获取groupInter
	 * @return: Double groupInter
	 */
	public Double getGroupInter() {
		return groupInter;
	}

	/**
	 * @Title: setGroupInter
	 * @Description: 设置groupInter
	 */
	public void setGroupInter(Double groupInter) {
		this.groupInter = groupInter;
	}

	/**
	 * @Title: getVoltage
	 * @Description: 获取voltage
	 * @return: String voltage
	 */
	public String getVoltage() {
		return voltage;
	}

	/**
	 * @Title: setVoltage
	 * @Description: 设置voltage
	 */
	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}

	/**
	 * @Title: getType
	 * @Description: 获取type
	 * @return: String type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Title: setType
	 * @Description: 设置type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Title: getVolume
	 * @Description: 获取volume
	 * @return: Double volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @Title: setVolume
	 * @Description: 设置volume
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @Title: getInter
	 * @Description: 获取inter
	 * @return: String inter
	 */
	public String getInter() {
		return inter;
	}

	/**
	 * @Title: setInter
	 * @Description: 设置inter
	 */
	public void setInter(String inter) {
		this.inter = inter;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "ExcelBatteryDto [maker=" + maker + ", groupVoltage=" + groupVoltage + ", beginTime=" + beginTime
				+ ", model=" + model + ", groupInter=" + groupInter + ", voltage=" + voltage + ", type=" + type
				+ ", volume=" + volume + ", inter=" + inter + "]";
	}
	
}
