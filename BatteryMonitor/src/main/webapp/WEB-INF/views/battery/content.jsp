<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>站点管理</title>	
<c:set var="groupId" value="${group.groupId}"/>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<script src='${ctxStatic}/js/My97DatePicker/WdatePicker.js'></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>	
#denglu *{vertical-align:middle;}
</style>
<script>
$(function(){
	$('#battery_lists').datagrid({
		url:'${ctx}/battery/datagrid?groupId=${groupId}',
		pagination:true,
		pageSize:23,
		pageList:[10,23,30,40,50],
		pageNumber : 1,
		sortName : 'id',
		sortOrder : 'ASC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[{
		        field:'name',
		        title:'名 称',
		        width:50,
		        align:'center'
			},{
		        field:'warn',
		        title:'当前告警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
		        field:'plan',
		        title:'当前预警',
		        width:50,
		        align:'center',
		        sortable:true
			},{
				field:'createTime',
				title:'创建时间',
				width:50,
				align:'center',
				sortable:true,
				formatter : formatDateTime
			}]],
		onDblClickRow : clickRow
	});
});
function clickRow(index, row) {
	window.parent.location.href="${ctx}/battery/frame?id=" + row.id;
}
function graph(){
	var rows = $('#battery_lists').datagrid('getChecked');
	if(rows.length==1){
		window.location.href="${ctx}/battery/frame?id="+rows[0].id;
	}else{
		$.messager.alert({title:'提示',msg:'请选择查看的对象'});
	}
}
function query(){
	$('#battery_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function clean(){
	$('#battery_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
}
function del(){
	// 权限校验
	<permission:notPermission name="tree:delete">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#battery_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}
				$.ajax({
					type:'get',
					url:'${ctx}/battery/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#battery_lists').datagrid('load',{});
						$('#battery_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}
function unselect(){
	$('#battery_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
} 
</script>
<body>	
	<div class="easyui-layout" fit="true">
		<div data-options="region:'north',title:'节点：${tree.name}'" style="height:140px;">
			<form id="search_form">
				<input type="hidden" name="groupId" value="${group.groupId}">
				<table cellSpacing="1" cellPadding="1" width="100%" bgColor="#eeeeee" 
			    	style="border:0px solid #8ba7e3" border="0">	   
				    <tr bgColor="#f5fafe" class="ta_01">
				    	<td width="15%">
				    	 	<strong>电池组标识：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff" width="20%">${group.address}</td>
				         <td width="15%">
				          	<strong>制 造 商：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff" width="20%">${group.norm.maker}</td>
				         <td width="10%">
				          	<strong>电池电压/V：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff" width="20%">${group.norm.voltage}</td>	
				    </tr>	    	   
				     <tr bgColor="#f5fafe" class="ta_01">
				    	<%-- <td>
				          	<strong>端 口 号：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.port}</td> --%>
				          <td>
				          	<strong>电池类型：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.norm.type}</td>
				          <td>
				          	<strong>参考内阻/mΩ：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.norm.inter}</td>
				         <td>
				          	<strong>电池容量/Ah：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.norm.volume}</td>
				    </tr>
				    <tr bgColor="#f5fafe" class="ta_01">
				    	 <td>
				          	<strong>电 池 数：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${tree.number}</td>
				         <td>
				          	<strong>&nbsp;&nbsp;型&nbsp;&nbsp;&nbsp;号：</strong>
				          	<font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.norm.model}</td>
				         <td>
				          	<strong>投运日期：</strong><font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${group.norm.beginTime}</td>
				    </tr>
				    <tr bgColor="#f5fafe" class="ta_01">	    	 		         
				         <td>
				    	 	<strong>&nbsp;&nbsp;备&nbsp;&nbsp;&nbsp;注：</strong>
				    	 	<font color="#FF0000">*</font></td>
				         <td bgColor="#ffffff">${empty tree.remark ? '无' : tree.remark}</td> 
				         <td></td>
				         <td bgColor="#ffffff"></td>
				         <td></td>
				         <td bgColor="#ffffff"></td>
				    </tr>
					<tr height="25" bgcolor="#f5fafe">
						<td id="denglu" colSpan="6">
							<img src="${ctxStatic}/images/yin.gif" width="22">
							<strong>名 称：</strong><input name="name" size="15"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
								onclick="query();" href="javascript:void(0);">查  询</a>
							&nbsp;&nbsp;
							<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
								onclick="clean();" href="javascript:void(0);">清  空</a>
						</td>					
					</tr>					
				</table>
			</form>
		</div>
		<!-- ----------------数据层------------- -->
		<div data-options="region:'center'">
			<table id="battery_lists"></table>
		</div>
	</div>
</body>
</html>