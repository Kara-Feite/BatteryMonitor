package com.gzyw.monitor.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.ArrayUtil;
import com.gzyw.monitor.common.util.DataUtil;
import com.gzyw.monitor.common.util.NumberUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.GroupLimitDao;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupLimit;
import com.gzyw.monitor.entity.MonomerLimit;
import com.gzyw.monitor.service.GroupLimitService;

/**
 * 
 * @ClassName: GroupLimitServiceImpl
 * @Description: 组限制服务层接口实现类
 * @author LW
 * @date 2018年6月29日 上午9:59:29
 */
@Service
public class GroupLimitServiceImpl implements GroupLimitService {

	@Autowired
	private GroupLimitDao groupLimitDao;
	
	@Override
	public void saveLimit(String param, Group group, Date date) {
		if (StringUtil.isEmpty(param)) {
			return;
		}
		String[] arr = DataUtil.toDataArr(param);
		if (ArrayUtil.isEmpty(arr)) {
			return;
		}
		// 获取对应16进制数据，因为固定所以直接按下标获取
		String batteryNum = arr[3] + arr[4]; // 电池节数
		String highVoltage = arr[5] + arr[6]; // 电池组电压高越限
		String lowVoltage = arr[7] + arr[8]; // 电池组电压低越限
		String discharge = arr[9] + arr[10]; // 电池组放电过流门限
		String openCircuit = arr[11] + arr[12]; // 开路电压门限
		String highInter = arr[13] + arr[14]; // 内阻高越限
		String highVoltage2 = arr[15] + arr[16]; // 单体电压高越限
		String lowVoltage2 = arr[17] + arr[18]; // 单体电压低越限
		String stopVoltage = arr[19] + arr[20]; // 单体放电终止电压
		String balanceVoltage = arr[21] + arr[22]; // 单体均衡电压
		String openCircuit2 = arr[23] + arr[24]; // 单体电压开路门限
		String chargeJudge = arr[25] + arr[26]; // 均充电流判定门限
		String openCircuitJudge = arr[27] + arr[28]; // 开路电流判定门限
		String highTempera2 = arr[29] + arr[30]; // 单体温度高
		String highTempera = arr[31] + arr[32]; // 环境温度高
		String lowTempera = arr[33] + arr[34]; // 环境温度低越限
		String interTestInterval = arr[35] + arr[36]; // 内阻测试时间间隔
		String storageInterval = arr[37] + arr[38]; // 历史存储时间间隔
		String interTestNo = arr[39] + arr[40]; // 单体内阻测试序号
		String voltageEqualiza = arr[41] + arr[42]; // 电压均衡始能
		String socketAddr = arr[43] + arr[44]; // 本机通信地址号
		String baudRate = arr[45] + arr[46]; // 本机通信波特率
		
		// 设置单体数据
		MonomerLimit monoLimit = new MonomerLimit();
		monoLimit.setHighVoltage(NumberUtil.hexToDouble(highVoltage2));
		monoLimit.setLowVoltage(NumberUtil.hexToDouble(lowVoltage2));
		monoLimit.setStopVoltage(NumberUtil.hexToDouble(stopVoltage));
		monoLimit.setBalanceVoltage(NumberUtil.hexToDouble(balanceVoltage));
		monoLimit.setOpenCircuit(NumberUtil.hexToDouble(openCircuit2));
		monoLimit.setHighTempera(NumberUtil.hexToDouble(highTempera2));
		monoLimit.setInterTestNo("" + NumberUtil.hexToDec(interTestNo));
		monoLimit.setCreateTime(date);
		
		// 设置电池组数据
		GroupLimit groupLimit = new GroupLimit();
		groupLimit.setBatteryNum(NumberUtil.hexToDec(batteryNum));
		groupLimit.setHighVoltage(NumberUtil.hexToDouble(highVoltage));
		groupLimit.setLowVoltage(NumberUtil.hexToDouble(lowVoltage));
		groupLimit.setDischarge(NumberUtil.hexToDouble(discharge));
		groupLimit.setOpenCircuit(NumberUtil.hexToDouble(openCircuit));
		groupLimit.setHighInter(NumberUtil.hexToDouble(highInter));
		groupLimit.setChargeJudge(NumberUtil.hexToDouble(chargeJudge));
		groupLimit.setOpenCircuitJudge(NumberUtil.hexToDouble(openCircuitJudge));
		groupLimit.setHighTempera(NumberUtil.hexToDouble(highTempera));
		groupLimit.setLowTempera(NumberUtil.hexToDouble(lowTempera));
		groupLimit.setInterTestInterval(NumberUtil.hexToDec(interTestInterval));
		groupLimit.setStorageInterval(NumberUtil.hexToDec(storageInterval));
		groupLimit.setVoltageEqualiza(NumberUtil.hexToDouble(voltageEqualiza));
		groupLimit.setSocketAddr("" + NumberUtil.hexToDec(socketAddr));
		groupLimit.setBaudRate(NumberUtil.hexToDouble(baudRate));
		groupLimit.setCreateTime(date);
		groupLimit.setMonomerLimit(monoLimit);
		groupLimit.setGroup(group);
		
		groupLimitDao.save(groupLimit);
	}

}
