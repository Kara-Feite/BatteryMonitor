package com.gzyw.monitor.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.jdbc.Work;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.DateUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.GroupDao;
import com.gzyw.monitor.dto.GroupAndTreeDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: GroupDaoImpl
 * @Description: 电池组持久层实现类
 * @author LW
 * @date 2018年6月14日 下午4:30:39
 */
@Repository
public class GroupDaoImpl extends BaseDaoImpl<Group> implements GroupDao {

	@Override
	public void saveBattery(int groupId, int num) {
		executeProcedure(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				CallableStatement call = conn.prepareCall("{Call pro_save_battery(?,?)}");
				call.setInt(1, groupId);
				call.setInt(2, num);
				call.execute();
			}
		});
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Group> listGroupByRun() {
		String sql = "SELECT bg.* FROM bm_group bg INNER JOIN bm_tree bt ON bg.group_id = bt.id WHERE bt.state = true AND bg.is_run = true";
		return getSession().createNativeQuery(sql).addEntity(Group.class).list();
	}

	@Override
	public void updateIsRun(int groupId, boolean isRun) {
		String hql = "UPDATE Group SET isRun = ? WHERE groupId = ?";
		update(hql, isRun, groupId);
	}

	@Override
	public List<Group> listGroupByAll() {
		String hql = "FROM Group";
		return queryForList(hql);
	}
	
	@Override
	public List<GroupAndTreeDto> listGroupByPage(Integer page, Integer rows, String sort, String order, Tree tree) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT bg.group_id id, bt.name groupName, bg.address address,");
		sb.append(" bt.number number, bg.intervals intervals, bg.unit unit, bg.is_run isRun, bt.state state,");
		sb.append(" bt.warn warn, bt.plan plan, bt.remark remark, bg.create_time createTime, bg.update_time updateTime");
		sb.append(" FROM bm_group bg INNER JOIN bm_tree bt ON bg.group_id = bt.id WHERE bt.parent_id = ?");
		String name = tree.getName();
		if (StringUtil.isNotEmpty(name)) {
			sb.append(" AND bt.name LIKE '%" + name + "%'");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			sb.append(" ORDER BY bt." + sort + " " + order);
		}
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<GroupAndTreeDto> list = getSession().createNativeQuery(sb.toString()).setParameter(1, tree.getParentId())
				.setFirstResult((page - 1)*rows).setMaxResults(rows).setResultTransformer(Transformers.aliasToBean(GroupAndTreeDto.class)).list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public GroupInfo getSystemInfo(TempDto temp) {
		String hql = "FROM GroupInfo WHERE group_id = ? AND createTime = ?";
		Query<GroupInfo> q = getSession().createQuery(hql);
		q.setParameter(0, temp.getTreeId());
		q.setParameter(1, DateUtil.parseDate(temp.getStartTime()));
		return q.uniqueResult();
	}

	@Override
	public Group getGroupByNorm(Integer id) {
		String hql = "FROM Group WHERE norm_id = ?";
		return get(hql, id);
	}
	
}
