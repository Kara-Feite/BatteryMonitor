package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Limit;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.LimitService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: LimitController
 * @Description: 警告门限控制层
 * @author LW
 * @date 2018年7月11日 下午3:01:45
 */
@Controller
@RequestMapping("/limit")
public class LimitController {
	
	@Autowired
	private LimitService limitService;
	
	@Autowired
	private TreeService treeServie;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}
	
	/**
	 * 
	 * @Title: showContent
	 * @Description: 跳转到content页面
	 * @param groupId
	 * @return
	 */
	@RequestMapping("/content")
	@Permissions("tree:query")
	String showContent(Integer groupId, Model model) {
		Tree tree = treeServie.getTreeById(groupId);
		model.addAttribute("tree", tree);
		model.addAttribute("groupId", groupId);	
		return "limit/content";
	}
	
	/**
	 * 
	 * @Title: showEdit
	 * @Description: 跳转到编辑页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	String showEdit(Model model, Integer groupId, @RequestParam(required = false) Integer id) {
		if (id == null) {
			model.addAttribute("limit", new Limit());
		} else {
			Limit limit = limitService.getLimitById(id);
			limit.getGroup();
			model.addAttribute("limit", limit);
		}
		List<Tree> listGroup = treeServie.listTreeByPid(groupId);
		model.addAttribute("listGroup", listGroup);
		model.addAttribute("groupId", groupId);
		return "limit/edit";
	}
	
	/**
	 * 
	 * @Title: datagrid
	 * @Description: 获取门限分组信息
	 * @param page 页码
	 * @param limit 门限实体
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 升序或降序
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Limit limit,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order, Integer groupId) {
		return limitService.getDatagrid(page, rows, sort, order, limit, groupId);
	}
	
	/**
	 * 
	 * @Title: update
	 * @Description: 更新警告门限
	 * @param limit
	 * @param br
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "门限管理", description = "更新门限")
	@Permissions("tree:update")
	public ResultDto update(@Validated Limit limit , BindingResult br){
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage());
			}
			return ResultDto.build(500, sb.toString());
		}
		return limitService.updateLimit(limit);
	}
	
	/**
	 * 
	 * @Title: saveLimit
	 * @Description: 添加警告门限
	 * @param limit
	 * @param br
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "门限管理", description = "添加门限")
	@Permissions("tree:add")
	public ResultDto saveLimit(Boolean isExist, @Validated Limit limit , BindingResult br){
		if (isExist != null && isExist) {
			Limit limitd = limitService.getLimitByTypeAndGroup(limit.getType(), limit.getGroup().getGroupId());
			limit.setId(limitd.getId());
			limit.setGroupName(limitd.getGroupName());
			limit.setCreateTime(limitd.getCreateTime());
			return limitService.updateLimit(limit);
		}
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage());
			}
			return ResultDto.build(500, sb.toString());
		}
		return limitService.saveLimit(limit);
	}
	
	/**
	 * 
	 * @Title: delLimitByBat
	 * @Description: 删除告警门限
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	@SystemControllerLog(type = "门限管理", description = "删除告警门限")
	public ResultDto delLimitByBat(int[] ids) {
		try {
			return limitService.delLimitByBat(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: checkLimit
	 * @Description: 验证是否存在该门限
	 * @param groupName
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "/check", method = RequestMethod.GET)
	@ResponseBody
	ResultDto checkLimit(String type, Integer groupId) {
		Limit limit = limitService.getLimitByTypeAndGroup(type, groupId);
		if (limit != null) {
			return ResultDto.ok();
		}
		return ResultDto.build(500, "不存在的");
	}

}
