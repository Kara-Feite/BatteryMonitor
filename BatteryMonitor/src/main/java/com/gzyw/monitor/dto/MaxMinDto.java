package com.gzyw.monitor.dto;

import java.io.Serializable;

/**
 * 
 * @ClassName: MaxMinDto
 * @Description: 电压电阻最大值最小值信息
 * @author LW
 * @date 2018年7月2日 下午2:38:23
 */
public class MaxMinDto implements Serializable{
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 5800934163556147603L;

	/**
	 * 最大电压
	 */
	private Double maxVoltage;
	
	/**
	 * 最大电压编号
	 */
	private String maxVoltageNo;
	
	/**
	 * 最小电压
	 */
	private Double minVoltage;
	
	/**
	 * 最小电压编号
	 */
	private String minVoltageNo;
	
	/**
	 * 最大电阻
	 */
	private Double maxInter;
	
	/**
	 * 最大电阻编号
	 */
	private String maxInterNo;
	
	/**
	 * 最小电阻
	 */
	private Double minInter;
	
	/**
	 * 最小电阻编号
	 */
	private String minInterNo;
	
	/**
	 * 平均电压
	 */
	private Double avgVoltage;
	
	/**
	 * @Title: MaxMinDto
	 * @Description: MaxMinDto构造函数
	 */
	public MaxMinDto() {}

	/**
	 * @Title: MaxMinDto
	 * @Description: MaxMinDto构造函数
	 * @param maxVoltage
	 * @param maxVoltageNo
	 * @param minVoltage
	 * @param minVoltageNo
	 * @param maxInter
	 * @param maxInterNo
	 * @param minInter
	 * @param minInterNo
	 * @param avgVoltage
	 */
	public MaxMinDto(Double maxVoltage, String maxVoltageNo, Double minVoltage, String minVoltageNo, Double maxInter,
			String maxInterNo, Double minInter, String minInterNo, Double avgVoltage) {
		super();
		this.maxVoltage = maxVoltage;
		this.maxVoltageNo = maxVoltageNo;
		this.minVoltage = minVoltage;
		this.minVoltageNo = minVoltageNo;
		this.maxInter = maxInter;
		this.maxInterNo = maxInterNo;
		this.minInter = minInter;
		this.minInterNo = minInterNo;
		this.avgVoltage = avgVoltage;
	}

	/**
	 * @Title: getMaxVoltage
	 * @Description: 获取maxVoltage
	 * @return: Double maxVoltage
	 */
	public Double getMaxVoltage() {
		return maxVoltage;
	}

	/**
	 * @Title: setMaxVoltage
	 * @Description: 设置maxVoltage
	 */
	public void setMaxVoltage(Double maxVoltage) {
		this.maxVoltage = maxVoltage;
	}

	/**
	 * @Title: getMaxVoltageNo
	 * @Description: 获取maxVoltageNo
	 * @return: String maxVoltageNo
	 */
	public String getMaxVoltageNo() {
		return maxVoltageNo;
	}

	/**
	 * @Title: setMaxVoltageNo
	 * @Description: 设置maxVoltageNo
	 */
	public void setMaxVoltageNo(String maxVoltageNo) {
		this.maxVoltageNo = maxVoltageNo;
	}

	/**
	 * @Title: getMinVoltage
	 * @Description: 获取minVoltage
	 * @return: Double minVoltage
	 */
	public Double getMinVoltage() {
		return minVoltage;
	}

	/**
	 * @Title: setMinVoltage
	 * @Description: 设置minVoltage
	 */
	public void setMinVoltage(Double minVoltage) {
		this.minVoltage = minVoltage;
	}

	/**
	 * @Title: getMinVoltageNo
	 * @Description: 获取minVoltageNo
	 * @return: String minVoltageNo
	 */
	public String getMinVoltageNo() {
		return minVoltageNo;
	}

	/**
	 * @Title: setMinVoltageNo
	 * @Description: 设置minVoltageNo
	 */
	public void setMinVoltageNo(String minVoltageNo) {
		this.minVoltageNo = minVoltageNo;
	}

	/**
	 * @Title: getMaxInter
	 * @Description: 获取maxInter
	 * @return: Double maxInter
	 */
	public Double getMaxInter() {
		return maxInter;
	}

	/**
	 * @Title: setMaxInter
	 * @Description: 设置maxInter
	 */
	public void setMaxInter(Double maxInter) {
		this.maxInter = maxInter;
	}

	/**
	 * @Title: getMaxInterNo
	 * @Description: 获取maxInterNo
	 * @return: String maxInterNo
	 */
	public String getMaxInterNo() {
		return maxInterNo;
	}

	/**
	 * @Title: setMaxInterNo
	 * @Description: 设置maxInterNo
	 */
	public void setMaxInterNo(String maxInterNo) {
		this.maxInterNo = maxInterNo;
	}

	/**
	 * @Title: getMinInter
	 * @Description: 获取minInter
	 * @return: Double minInter
	 */
	public Double getMinInter() {
		return minInter;
	}

	/**
	 * @Title: setMinInter
	 * @Description: 设置minInter
	 */
	public void setMinInter(Double minInter) {
		this.minInter = minInter;
	}

	/**
	 * @Title: getMinInterNo
	 * @Description: 获取minInterNo
	 * @return: String minInterNo
	 */
	public String getMinInterNo() {
		return minInterNo;
	}

	/**
	 * @Title: setMinInterNo
	 * @Description: 设置minInterNo
	 */
	public void setMinInterNo(String minInterNo) {
		this.minInterNo = minInterNo;
	}
	
	/**
	 * @Title: getAvgVoltage
	 * @Description: 获取avgVoltage
	 * @return: Double avgVoltage
	 */
	public Double getAvgVoltage() {
		return avgVoltage;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setAvgVoltage(Double avgVoltage) {
		this.avgVoltage = avgVoltage;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "MaxMinDto [maxVoltage=" + maxVoltage + ", maxVoltageNo=" + maxVoltageNo + ", minVoltage=" + minVoltage
				+ ", minVoltageNo=" + minVoltageNo + ", maxInter=" + maxInter + ", maxInterNo=" + maxInterNo
				+ ", minInter=" + minInter + ", minInterNo=" + minInterNo + ", avgVoltage=" + avgVoltage + "]";
	}

}
