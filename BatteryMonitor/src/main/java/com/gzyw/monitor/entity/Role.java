package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @ClassName: Role
 * @Description: 角色
 * @author LW
 * @date 2018年6月4日 下午1:41:57
 */
@Entity
@Table(name = "bm_role")
public class Role implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -9203673160801973607L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 名称
	 */
	@Column(name = "name", length = 64, nullable = false)
	private String name;

	/**
	 * 状态
	 */
	@Column(name = "state", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean state;

	/**
	 * 创建者
	 */
	@Column(name = "create_by", nullable = false)
	private Integer createBy;

	/**
	 * 更新者
	 */
	@Column(name = "update_by", nullable = false)
	private Integer updateBy;

	/**
	 * 备注
	 */
	@Column(name = "remark", length = 255, nullable = false)
	private String remark;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 菜单资源
	 */
	@JsonIgnore
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "bm_role_menu", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "menu_id"))
	private Set<Menu> setMenu;

	/**
	 * 树形菜单
	 */
	@JsonIgnore
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "bm_role_tree", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "tree_id"))
	private Set<Tree> setTree;

	/**
	 * 用户
	 */
	@JsonIgnore
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "bm_role_user", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> setUser;

	/**
	 * @Title: Role
	 * @Description: Role构造函数
	 */
	public Role() {}

	/**
	 * @Title: Role
	 * @Description: Role构造函数
	 * @param name
	 * @param state
	 * @param createBy
	 * @param updateBy
	 * @param remark
	 * @param createTime
	 * @param updateTime
	 */
	public Role(String name, Boolean state, Integer createBy, Integer updateBy, String remark, Date createTime,
			Date updateTime) {
		this.name = name;
		this.state = state;
		this.createBy = createBy;
		this.updateBy = updateBy;
		this.remark = remark;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setName
	 * @Description: 设置name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setState
	 * @Description: 设置state
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getCreateBy
	 * @Description: 获取createBy
	 * @return: Integer createBy
	 */
	public Integer getCreateBy() {
		return createBy;
	}

	/**
	 * @Title: setCreateBy
	 * @Description: 设置createBy
	 */
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	/**
	 * @Title: getUpdateBy
	 * @Description: 获取updateBy
	 * @return: Integer updateBy
	 */
	public Integer getUpdateBy() {
		return updateBy;
	}

	/**
	 * @Title: setUpdateBy
	 * @Description: 设置updateBy
	 */
	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setRemark
	 * @Description: 设置remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUpdateTime
	 * @Description: 设置updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetMenu
	 * @Description: 获取setMenu
	 * @return: Set<Menu> setMenu
	 */
	public Set<Menu> getSetMenu() {
		return setMenu;
	}

	/**
	 * @Title: setSetMenu
	 * @Description: 设置setMenu
	 */
	public void setSetMenu(Set<Menu> setMenu) {
		this.setMenu = setMenu;
	}

	/**
	 * @Title: getSetTree
	 * @Description: 获取setTree
	 * @return: Set<Tree> setTree
	 */
	public Set<Tree> getSetTree() {
		return setTree;
	}

	/**
	 * @Title: setSetTree
	 * @Description: 设置setTree
	 */
	public void setSetTree(Set<Tree> setTree) {
		this.setTree = setTree;
	}

	/**
	 * @Title: getSetUser
	 * @Description: 获取setUser
	 * @return: Set<User> setUser
	 */
	public Set<User> getSetUser() {
		return setUser;
	}

	/**
	 * @Title: setSetUser
	 * @Description: 设置setUser
	 */
	public void setSetUser(Set<User> setUser) {
		this.setUser = setUser;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", state=" + state + ", createBy=" + createBy + ", updateBy="
				+ updateBy + ", remark=" + remark + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
