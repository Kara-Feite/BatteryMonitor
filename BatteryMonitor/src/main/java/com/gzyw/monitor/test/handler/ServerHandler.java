package com.gzyw.monitor.test.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.test.Control;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ServerHandler extends ChannelInboundHandlerAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(ServerHandler.class);

	private static final Map<String, Control> controls = new ConcurrentHashMap<String, Control>();

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// 第一次确认连接，
		ctx.channel().writeAndFlush("欢迎连接!");
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf buf = (ByteBuf) msg;
		final int len = buf.readableBytes();
		final byte[] arr = new byte[len];
		final int readerIndex = buf.readerIndex();
		buf.getBytes(readerIndex, arr, 0, len);
		String content = new String(arr, readerIndex, len);
		if (StringUtils.isBlank(content)) {
			return;
		}
		// 判断是否是第一次连接，如果是就注册客户端到controls
		if (content.length() == 11) {
			Control control = new Control();
			control.setServer(ctx.channel());
			controls.put(content, control);
			LOG.info("电池组注册，标识为：" + content);
		} else { // 获取命令请求，判断发送的命令还是电池组返回的数据
			String[] cons = StringUtils.split(content, ",");
			// 发送命令
			if (ArrayUtils.getLength(cons) == 2) {
				Control control = controls.get(cons[0]);
				if (control == null)
					return;
				send(control, cons[1]);
				// 将命令发送给对应电池组
				control.setClient(ctx.channel());
				LOG.info("发送命令到：" + cons[0] + "，命令为：" + cons[1]);
				// 接收电池组返回的数据
			} else if (ArrayUtils.getLength(cons) == 1) {
				String address = content.substring(0, 11);
				Control control = controls.get(address);
				if (control == null) {
					return;
				}
				final byte[] bytes = new byte[len - 11];
				buf.getBytes(readerIndex, bytes, 11, len);
				String hex = StringUtil.bytesToHex(bytes).substring(22);
				// 往对应客户端发送接收到的数据
				receive(control, hex);
				LOG.info("接收" + address + "电池组数据，内容为：" + hex);
			}
		}
	}

	private void receive(Control control, String hex) {
		Channel client = control.getClient();
		client.writeAndFlush(hex);
	}

	private void send(Control control, String content) {
		Channel server = control.getServer();
		byte[] bytes = StringUtil.toBytes(content);
		server.writeAndFlush(bytes);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		LOG.info("Socket Server: 与电池组断开连接:" + cause.getMessage());
		cause.printStackTrace();
		ctx.close();
	}

}
