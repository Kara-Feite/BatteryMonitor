package com.gzyw.monitor.entity;

import java.io.Serializable;

/**
 * 
 * @ClassName: RoleToMenu
 * @Description: 角色菜单实体类
 * @author LW
 * @date 2018年7月16日 下午1:09:41
 */
public class RoleToMenu implements Serializable {
	
	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 4623404120182657493L;

	/**
	 * 角色id
	 */
	private Integer roleId;
	
	/**
	 * 菜单id
	 */
	private Integer menuId;
	
	/**
	 * 
	 * @Title: RoleToMenu
	 * @Description: RoleToMenu构造函数
	 */
	public RoleToMenu() {}

	/**
	 * 
	 * @Title: RoleToMenu
	 * @Description: RoleToMenu构造函数
	 * @param roleId
	 * @param menuId
	 */
	public RoleToMenu(Integer roleId, Integer menuId) {
		this.roleId = roleId;
		this.menuId = menuId;
	}

	/**
	 * @Title: getRoleId
	 * @Description: 获取roleId
	 * @return: Integer roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @Title: setRoleId
	 * @Description: 设置roleId
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @Title: getMenuId
	 * @Description: 获取menuId
	 * @return: Integer menuId
	 */
	public Integer getMenuId() {
		return menuId;
	}

	/**
	 * @Title: setMenuId
	 * @Description: 设置menuId
	 */
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "RoleToMenu [roleId=" + roleId + ", menuId=" + menuId + "]";
	}
	
}
