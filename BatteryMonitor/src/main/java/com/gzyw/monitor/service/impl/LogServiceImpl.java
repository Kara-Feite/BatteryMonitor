package com.gzyw.monitor.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.dao.LogDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Log;
import com.gzyw.monitor.service.LogService;

/**
 * 
 * @ClassName: LogServiceImpl
 * @Description: 日志服务层接口实现类
 * @author LW
 * @date 2018年7月9日 下午2:46:39
 */
@Service
public class LogServiceImpl implements LogService {
	
	@Autowired
	private LogDao logDao;

	@Override
	public void saveLog(Log log) {
		logDao.save(log);
	}

	@Override
	public void updateLog(Log log) {
		logDao.update(log);
	}

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Log log, String userName) {
		long count = logDao.countLog(log, userName);
		List<Log> listLog = logDao.listLogByPage(page, rows, sort, order, log, userName);
		return new EUDataGridDto(count, listLog);
	}

	@Override
	public ResultDto deleteLog(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的日志");
		}
		for (int id : ids) {
			Log log = logDao.get(id);
			if (log != null) {
				logDao.delete(log);
			}
		}
		return ResultDto.build(200, "删除日志成功！");
	}

}
