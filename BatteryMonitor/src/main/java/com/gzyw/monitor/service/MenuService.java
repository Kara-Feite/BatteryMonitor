package com.gzyw.monitor.service;

import java.util.List;

import com.gzyw.monitor.entity.Menu;

/**
 * 
 * @ClassName: MenuService
 * @Description: 菜单资源服务层
 * @author LW
 * @date 2018年7月4日 上午10:31:47
 */
public interface MenuService {

	/**
	 * 
	 * @Title: listMenuByUserId
	 * @Description: 根据用户id获取菜单
	 * @param id
	 * @return
	 */
	List<Menu> listMenuByUserId(Integer id, boolean isRoot);

	/**
	 * 
	 * @Title: listMenuByAll
	 * @Description: 获取所有菜单
	 * @return
	 */
	List<Menu> listMenuByAll();

}
