<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>日志管理</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
#denglu *{vertical-align:middle;}
a{width:80px;}
</style>
<script>
$(function(){
	$('#log_lists').datagrid({
		url:'${ctx}/log/datagrid',
		pagination:true,
		pageSize:30,
		pageList:[10,20,30,40,50],
		sortName : 'createTime',
		sortOrder : 'DESC',
		fit:true,
		fitColumns:true,
		striped:true,
		nowrap:false,
		border:false,
		checkOnSelect:true,
		selectOnCheck:true,
		idField:'id',
		columns:[[
			{
				field:'id',
				width:30,
				checkbox:true
			},{
		        field:'type',
		        title:'名称',
		        width:30,
		        align:'center'
			},{
				field:'user',
				title:'操作人',
				width:30,
				align:'center',
				sortable:true,
				formatter : function(val, row) {
					return val ? val['nickName'] : '';
				}
			},{
				field:'ipAddr',
				title:'IP地址',
				width:30,
				align:'center'
			}/* ,{
				field:'params',
				title:'请求参数',
				width:100,
				align:'center',
				formatter : formatNull
			} */,{
				field:'description',
				title:'详情',
				width:60,
				align:'center'
			},{
				field:'createTime',
				title:'操作时间',
				width:50,
				align:'center',
				formatter : formatDateTime
			}
		]],
		toolbar:[			
			{text:'删除',iconCls:'icon-cancel',handler:function(){del();}},'-',			
			{text:'取消选中',iconCls:'icon-redo',handler:function(){unselect();}}
		]
	});
});
function query(){
	$('#log_lists').datagrid('load',serializeObject($('#search_form').form()));
}
function clean(){
	$('#log_lists').datagrid('load',{});
	$('#search_form').form().find('input').val('');
	$('#search_form').form().find('select').val('');
}

function del(){
	<permission:notPermission name="log:delete">
		$.messager.alert({title:'提示',msg:'您没有此操作权限！'});
		return;
	</permission:notPermission>
	var rows=$('#log_lists').datagrid('getChecked');
	if(rows.length>0){
		$.messager.confirm('确认','你确认要删除选择的记录吗？',function(r){
			if(r){
				var ids=[];
				for(i=0;i<rows.length;i++){
					ids.push(rows[i].id);
				}
				$.ajax({
					type:'get',
					url:'${ctx}/log/delete',
					data:{ids:ids.join(',')},
					dataType:'json',
					success:function(r){
						$('#log_lists').datagrid('load',{});
						$('#log_lists').datagrid('unselectAll');
						$.messager.alert({title:'提示',msg:r.msg});
					}
				});
			}
		});
	}else{
		$.messager.alert({title:'提示',msg:'请选择要删除的记录'});
	}
}

function unselect(){
	$('#log_lists').datagrid('uncheckAll').datagrid('unselectAll').datagrid('clearSelections');
}
</script>
</head>
<body>
<div class="easyui-layout" fit="true" border="false">
	<div data-options="region:'north',title:'查询条件'" style="height:61px;">
		<form id="search_form">
			<table width="100%" bgcolor="#f5fafe">
				<tr height="25">
				<td id="denglu"><img src="${ctxStatic}/images/yin.gif" width="22">
					<strong>事件类型：</strong>			
					<select name="type" style="width:120px;height=22;"> 
						<option value=""></option>
			            <option value="用户登录">用户登录</option>
						<option value="用户管理">用户管理</option>
			            <option value="基站管理">基站管理</option>
			            <option value="权限管理">权限管理</option>					           
			            <option value="警告管理">警告管理</option>
			            <option value="门限管理">门限管理</option>
			            <option value="规格管理">规格管理</option>
	        		</select>
		        	&nbsp;&nbsp;			
					<strong>操作人：</strong><input name="userName" size="15"/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
							onclick="query();" href="javascript:void(0);">查  询</a>						
						&nbsp;&nbsp;
						<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" 
						onclick="clean();" href="javascript:void(0);">清  空</a>
					</td>			
				</tr>
			</table>
		</form>
	</div>
	<!-- ------------显示数据------------------- -->
	<div data-options="region:'center'">
		<table id="log_lists"></table>
	</div>
</div>
</body>
</html>