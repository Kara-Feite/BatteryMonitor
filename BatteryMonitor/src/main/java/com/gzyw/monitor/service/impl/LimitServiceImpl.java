package com.gzyw.monitor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.dao.LimitDao;
import com.gzyw.monitor.dao.TreeDao;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Limit;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.LimitService;

/**
 * 
 * @ClassName: LimitServiceImpl
 * @Description: 门限服务层接口实现类
 * @author LW
 * @date 2018年7月11日 下午3:43:18
 */
@Service
public class LimitServiceImpl implements LimitService {
	
	@Autowired
	private LimitDao limitDao;
	
	@Autowired
	private TreeDao treeDao;

	@Override
	public EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Limit limit, Integer groupId) {
		Long count = limitDao.countLimit(limit, groupId);
		List<Limit> listLimit = new ArrayList<Limit>();
		List<Limit> list1 = limitDao.listLimitByPage(page, rows, sort, order, limit, groupId);
		if (CollectionUtil.isNotEmpty(list1)) {
			listLimit.addAll(list1);
		}
		listLimitAll(listLimit, page, rows, sort, order, limit, groupId);
		return new EUDataGridDto(count, listLimit);
	}

	@Override
	public Limit getLimitById(Serializable id) {
		return limitDao.get(id);
	}
	
	@Override
	public ResultDto updateLimit(Limit limit) {
		limit.setUpdateTime(new Date());
		try {
			limitDao.update(limit);
		} catch (Exception e) {
			return ResultDto.build(500, "出现未知错误，更新失败");
		}
		return ResultDto.ok();
	}
	
	@Override
	public ResultDto saveLimit(Limit limit) {
		Date date = new Date();
		limit.setCreateTime(date);
		limit.setUpdateTime(date);
		Tree tree = treeDao.get(limit.getGroup().getGroupId());
		limit.setGroupName(tree.getName());
		try {
			limitDao.save(limit);
		} catch (Exception e) {
			return ResultDto.build(500, "出现未知错误，添加失败");
		}
		return ResultDto.ok();
	}
	
	@Override
	public Limit getLimitByTypeAndGroup(String type, Integer groupId) {
		return limitDao.getLimitByTypeAndGroup(type, groupId);
	}
	
	@Override
	public ResultDto delLimitByBat(int[] ids) {
		if (ids == null) {
			return ResultDto.build(500, "请选择要删除的门限");
		}
		for (int id : ids) {
			Limit limit = limitDao.get(id);
			if (limit != null) {			
				limitDao.delete(limit);														
			}
		}
		return ResultDto.build(200, "删除门限成功！");
	}

	/**
	 * 
	 * @Title: addAlarmAll
	 * @Description: 递归找到子节点所有警告门限
	 * @param listAlarm
	 */
	private void listLimitAll(List<Limit> listLimit, Integer page, Integer rows, String sort, String order, Limit limit, Integer groupId) {
		List<Tree> listTree = treeDao.listTreeByPid(groupId);
		if (CollectionUtil.isNotEmpty(listTree)) {
			for (Tree tree : listTree) {
				List<Limit> list = limitDao.listLimitByPage(page, rows, sort, order, limit, tree.getId());
				if (CollectionUtil.isNotEmpty(list)) {
					listLimit.addAll(list);
				}
				listLimitAll(listLimit, page, rows, sort, order, limit, tree.getId());
			}
		}
	}

}
