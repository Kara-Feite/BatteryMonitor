package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * 
 * @ClassName: Menu
 * @Description: 菜单资源
 * @author LW
 * @date 2018年6月4日 上午11:29:16
 */
@Entity
@Table(name = "bm_menu")
public class Menu implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 2329084368455724018L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 父id
	 */
	@Column(name = "parent_id", nullable = false)
	private Integer parentId;

	/**
	 * 菜单名称
	 */
	@Column(name = "name", length = 64, nullable = false)
	private String name;

	/**
	 * 对应链接
	 */
	@Column(name = "href", length = 128)
	private String href;

	/**
	 * 链接目标
	 */
	@Column(name = "target", length = 32)
	private String target;

	/**
	 * 图标
	 */
	@Column(name = "icon", length = 64)
	private String icon;

	/**
	 * 备注
	 */
	@Column(name = "remark", length = 255)
	private String remark;

	/**
	 * 状态
	 */
	@Column(name = "state", columnDefinition = "TINYINT(1) DEFAULT 1")
	private Boolean state;
	
	/**
	 * 权限标识
	 */
	@Column(name = "permission", length = 32)
	private String permission;

	/**
	 * 创建人
	 */
	@Column(name = "create_by", nullable = false)
	private Integer createBy;
	
	/**
	 * 更新人
	 */
	@Column(name = "update_by", nullable = false)
	private Integer updateBy;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time", nullable = false)
	private Date updateTime;

	/**
	 * 角色菜单对应表
	 */
	@ManyToMany(mappedBy = "setMenu", fetch = FetchType.EAGER)
	private Set<Role> setRole;

	/**
	 * @Title: Menu
	 * @Description: Menu构造函数
	 */
	public Menu() {}

	/**
	 * @Title: Menu
	 * @Description: Menu构造函数
	 * @param id
	 * @param parentId
	 * @param name
	 * @param href
	 * @param target
	 * @param icon
	 * @param remark
	 * @param state
	 * @param permission
	 * @param createBy
	 * @param updateBy
	 * @param createTime
	 * @param updateTime
	 */
	public Menu(Integer id, Integer parentId, String name, String href, String target, String icon, String remark,
			Boolean state, String permission, Integer createBy, Integer updateBy, Date createTime, Date updateTime) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.href = href;
		this.target = target;
		this.icon = icon;
		this.remark = remark;
		this.state = state;
		this.permission = permission;
		this.createBy = createBy;
		this.updateBy = updateBy;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getParentId
	 * @Description: 获取parentId
	 * @return: Integer parentId
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @Title: getName
	 * @Description: 获取name
	 * @return: String name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Title: getHref
	 * @Description: 获取href
	 * @return: String href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @Title: getTarget
	 * @Description: 获取target
	 * @return: String target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @Title: getIcon
	 * @Description: 获取icon
	 * @return: String icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @Title: getRemark
	 * @Description: 获取remark
	 * @return: String remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @Title: getState
	 * @Description: 获取state
	 * @return: Boolean state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @Title: getPermission
	 * @Description: 获取permission
	 * @return: String permission
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setPermission(String permission) {
		this.permission = permission;
	}

	/**
	 * @Title: getCreateBy
	 * @Description: 获取createBy
	 * @return: Integer createBy
	 */
	public Integer getCreateBy() {
		return createBy;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	/**
	 * @Title: getUpdateBy
	 * @Description: 获取updateBy
	 * @return: Integer updateBy
	 */
	public Integer getUpdateBy() {
		return updateBy;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getUpdateTime
	 * @Description: 获取updateTime
	 * @return: Date updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @Title: getSetRole
	 * @Description: 获取setRole
	 * @return: Set<Role> setRole
	 */
	public Set<Role> getSetRole() {
		return setRole;
	}

	/**
	 * @Title: setUsername
	 * @Description: 设置username
	 */
	public void setSetRole(Set<Role> setRole) {
		this.setRole = setRole;
	}
	
	/**
	 * 
	 * @return 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createBy == null) ? 0 : createBy.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		result = prime * result + ((icon == null) ? 0 : icon.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((permission == null) ? 0 : permission.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((setRole == null) ? 0 : setRole.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + ((updateBy == null) ? 0 : updateBy.hashCode());
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		return result;
	}

	/**
	 * 
	 * @param obj
	 * @return 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menu other = (Menu) obj;
		if (createBy == null) {
			if (other.createBy != null)
				return false;
		} else if (!createBy.equals(other.createBy))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		if (icon == null) {
			if (other.icon != null)
				return false;
		} else if (!icon.equals(other.icon))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (permission == null) {
			if (other.permission != null)
				return false;
		} else if (!permission.equals(other.permission))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (setRole == null) {
			if (other.setRole != null)
				return false;
		} else if (!setRole.equals(other.setRole))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		if (updateBy == null) {
			if (other.updateBy != null)
				return false;
		} else if (!updateBy.equals(other.updateBy))
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		return true;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "Menu [id=" + id + ", parentId=" + parentId + ", name=" + name + ", href=" + href + ", target=" + target
				+ ", icon=" + icon + ", remark=" + remark + ", state=" + state + ", permission=" + permission
				+ ", createBy=" + createBy + ", updateBy=" + updateBy + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}

}
