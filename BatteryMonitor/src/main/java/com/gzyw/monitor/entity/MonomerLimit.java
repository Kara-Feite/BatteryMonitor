package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @ClassName: MonomerLimit
 * @Description: 单体限制
 * @author LW
 * @date 2018年6月4日 下午1:25:14
 */
@Entity
@Table(name = "bm_monomer_limit")
public class MonomerLimit implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = -3449470510378748972L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 单体电压高越限
	 */
	@Column(name = "high_voltage", nullable = false)
	private Double highVoltage;

	/**
	 * 单体电压低越限
	 */
	@Column(name = "low_voltage", nullable = false)
	private Double lowVoltage;

	/**
	 * 单体放电终止电压
	 */
	@Column(name = "stop_voltage", nullable = false)
	private Double stopVoltage;

	/**
	 * 单体均衡电压
	 */
	@Column(name = "balance_voltage", nullable = false)
	private Double balanceVoltage;

	/**
	 * 单体电压开路门限
	 */
	@Column(name = "open_circuit", nullable = false)
	private Double openCircuit;

	/**
	 * 单体温度高越限
	 */
	@Column(name = "high_tempera", nullable = false)
	private Double highTempera;

	/**
	 * 单体内阻测试序号
	 */
	@Column(name = "inter_test_no", length = 32, nullable = false)
	private String interTestNo;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 组限制
	 */
	@OneToOne(mappedBy = "monomerLimit")
	private GroupLimit groupLimit;

	/**
	 * @Title: MonomerLimit
	 * @Description: MonomerLimit构造函数
	 */
	public MonomerLimit() {
	}

	/**
	 * @Title: MonomerLimit
	 * @Description: MonomerLimit构造函数
	 * @param highVoltage
	 * @param lowVoltage
	 * @param stopVoltage
	 * @param balanceVoltage
	 * @param openCircuit
	 * @param highTempera
	 * @param interTestNo
	 * @param createTime
	 */
	public MonomerLimit(Double highVoltage, Double lowVoltage, Double stopVoltage, Double balanceVoltage,
			Double openCircuit, Double highTempera, String interTestNo, Date createTime) {
		this.highVoltage = highVoltage;
		this.lowVoltage = lowVoltage;
		this.stopVoltage = stopVoltage;
		this.balanceVoltage = balanceVoltage;
		this.openCircuit = openCircuit;
		this.highTempera = highTempera;
		this.interTestNo = interTestNo;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getHighVoltage
	 * @Description: 获取highVoltage
	 * @return: Double highVoltage
	 */
	public Double getHighVoltage() {
		return highVoltage;
	}

	/**
	 * @Title: setHighVoltage
	 * @Description: 设置highVoltage
	 */
	public void setHighVoltage(Double highVoltage) {
		this.highVoltage = highVoltage;
	}

	/**
	 * @Title: getLowVoltage
	 * @Description: 获取lowVoltage
	 * @return: Double lowVoltage
	 */
	public Double getLowVoltage() {
		return lowVoltage;
	}

	/**
	 * @Title: setLowVoltage
	 * @Description: 设置lowVoltage
	 */
	public void setLowVoltage(Double lowVoltage) {
		this.lowVoltage = lowVoltage;
	}

	/**
	 * @Title: getStopVoltage
	 * @Description: 获取stopVoltage
	 * @return: Double stopVoltage
	 */
	public Double getStopVoltage() {
		return stopVoltage;
	}

	/**
	 * @Title: setStopVoltage
	 * @Description: 设置stopVoltage
	 */
	public void setStopVoltage(Double stopVoltage) {
		this.stopVoltage = stopVoltage;
	}

	/**
	 * @Title: getBalanceVoltage
	 * @Description: 获取balanceVoltage
	 * @return: Double balanceVoltage
	 */
	public Double getBalanceVoltage() {
		return balanceVoltage;
	}

	/**
	 * @Title: setBalanceVoltage
	 * @Description: 设置balanceVoltage
	 */
	public void setBalanceVoltage(Double balanceVoltage) {
		this.balanceVoltage = balanceVoltage;
	}

	/**
	 * @Title: getOpenCircuit
	 * @Description: 获取openCircuit
	 * @return: Double openCircuit
	 */
	public Double getOpenCircuit() {
		return openCircuit;
	}

	/**
	 * @Title: setOpenCircuit
	 * @Description: 设置openCircuit
	 */
	public void setOpenCircuit(Double openCircuit) {
		this.openCircuit = openCircuit;
	}

	/**
	 * @Title: getHighTempera
	 * @Description: 获取highTempera
	 * @return: Double highTempera
	 */
	public Double getHighTempera() {
		return highTempera;
	}

	/**
	 * @Title: setHighTempera
	 * @Description: 设置highTempera
	 */
	public void setHighTempera(Double highTempera) {
		this.highTempera = highTempera;
	}

	/**
	 * @Title: getInterTestNo
	 * @Description: 获取interTestNo
	 * @return: String interTestNo
	 */
	public String getInterTestNo() {
		return interTestNo;
	}

	/**
	 * @Title: setInterTestNo
	 * @Description: 设置interTestNo
	 */
	public void setInterTestNo(String interTestNo) {
		this.interTestNo = interTestNo;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getGroupLimit
	 * @Description: 获取groupLimit
	 * @return: GroupLimit groupLimit
	 */
	public GroupLimit getGroupLimit() {
		return groupLimit;
	}

	/**
	 * @Title: setGroupLimit
	 * @Description: 设置groupLimit
	 */
	public void setGroupLimit(GroupLimit groupLimit) {
		this.groupLimit = groupLimit;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonomerLimit [id=" + id + ", highVoltage=" + highVoltage + ", lowVoltage=" + lowVoltage
				+ ", stopVoltage=" + stopVoltage + ", balanceVoltage=" + balanceVoltage + ", openCircuit=" + openCircuit
				+ ", highTempera=" + highTempera + ", interTestNo=" + interTestNo + ", createTime=" + createTime
				+ ", groupLimit=" + groupLimit + "]";
	}

}
