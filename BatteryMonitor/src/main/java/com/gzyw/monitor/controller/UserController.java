package com.gzyw.monitor.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.CodeUtil;
import com.gzyw.monitor.common.util.CollectionUtil;
import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Role;
import com.gzyw.monitor.entity.RoleToUser;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.RoleService;
import com.gzyw.monitor.service.UserService;

/**
 * 
 * @ClassName: UserController
 * @Description: 用户控制层
 * @author LW
 * @date 2018年6月6日 下午5:03:24
 */
@Controller
@RequestMapping("/user")
public class UserController {

	/**
	 * 用户服务层
	 */
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}
	
	/**
	 * 
	 * @Title: showList
	 * @Description: 用户列表页面
	 * @return
	 */
	@RequestMapping("/list")
	@Permissions("user:query")
	String showList() {
		return "user/list";
	}
	
	/**
	 * id为空就添加，否则更新
	 * @Title: showEdit
	 * @Description: 跳转到编辑页码
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/edit")
	String showEdit(@RequestParam(value = "id", required = false) Integer id, Model model) {
		if (id != null) {
			User user = userService.getUserById(id);
			if (user != null) {
				model.addAttribute("user", user);
				List<RoleToUser> listRoleToUser = roleService.listRoleToUser(user.getId());
				if (CollectionUtil.isNotEmpty(listRoleToUser)) {
					Map<Integer, Integer> mapUserRole = new HashMap<Integer, Integer>();
					for (RoleToUser roleToUser : listRoleToUser) {
						Integer roleId = roleToUser.getRoleId();
						mapUserRole.put(roleId, roleId);
					}
					model.addAttribute("mapUserRole", mapUserRole);
				}
			} else {
				return "redirect:/error";
			}
		} else {
			model.addAttribute("user", new User());
		}
		List<Role> listRole = roleService.listRoleByAll();
		model.addAttribute("listRole", listRole);
		return "user/edit";
	}
	
	/**
	 * 
	 * @Title: showInfo
	 * @Description: 跳转到用户信息页面
	 * @return
	 */
	@RequestMapping("/password")
	String showInfo(Model model) {
		return "user/changePwd";
	}
	
	/**
	 * 
	 * @Title: changeInfo
	 * @Description: 修改密码
	 * @param oldPwd 旧密码
	 * @param newPwd 新密码
	 * @param session
	 * @return
	 */
	@RequestMapping("/password/change")
	@ResponseBody
	@SystemControllerLog(type = "用户管理", description = "修改密码")
	public ResultDto changeInfo(String oldPwd, String newPwd, HttpSession session) {
		if (StringUtil.isEmpty(oldPwd) || StringUtil.isEmpty(newPwd)) {
			return ResultDto.build(500, "密码不能为空");
		}
		User user = (User) session.getAttribute("user");
		return userService.changePwd(oldPwd, newPwd, user.getId());
	}
	
	/**
	 * 
	 * @Title: save
	 * @Description: 添加用户
	 * @param model
	 * @param user
	 * @param errors
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "用户管理", description = "添加用户")
	@Permissions("user:add")
	public ResultDto saveUser(Model model, @Validated User user, Errors errors, int[] listRole){
		if(errors.hasErrors()){
			List<ObjectError> allErrors = errors.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage() + ",");
			}
			return ResultDto.build(500, sb.substring(0, sb.length() - 1));
		}
		if (listRole == null || listRole.length <= 0) {
			return ResultDto.build(500, "请选择角色!");
		}
		return userService.saveUser(user, listRole);
	}
	
	/**
	 * 
	 * @Title: update
	 * @Description: 更新用户
	 * @param user
	 * @param br
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@SystemControllerLog(type = "用户管理", description = "更新用户")
	@Permissions("user:update")
	public ResultDto update(@Validated User user , BindingResult br, int[] listRole, HttpServletRequest request){
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage());
			}
			return ResultDto.build(500, sb.toString());
		}
		if (listRole == null || listRole.length <= 0) {
			return ResultDto.build(500, "请选择角色!");
		}
		return userService.updateUser(user, listRole);
	}
	
	/**
	 * 
	 * @Title: listUserData
	 * @Description: 用户列表数据
	 * @param page 页码
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或者升序
	 * @return
	 */
	@RequestMapping("/datagrid")
	@ResponseBody
	EUDataGridDto listUserData(@RequestParam(value = "page", defaultValue = "1") Integer page, User user,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return userService.getDataGrid(page, rows, sort, order, user);
	}
	
	/**
	 * 
	 * @Title: delUserByBat
	 * @Description: 批量删除用户
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	@SystemControllerLog(type = "用户管理", description = "删除用户")
	@Permissions("user:delete")
	public ResultDto delUserByBat(int[] ids) {
		try {
			return userService.delUserByBat(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: congeal
	 * @Description: 冻结
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/congeal",method=RequestMethod.GET)
	@ResponseBody
	@SystemControllerLog(type = "用户管理", description = "禁用用户")
	@Permissions("user:congeal")
	public ResultDto congeal(int[] ids) {
		try {
			return userService.updateState(ids, false);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，冻结失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: thaw
	 * @Description: 激活
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/thaw",method=RequestMethod.GET)
	@ResponseBody
	@SystemControllerLog(type = "用户管理", description = "激活用户")
	@Permissions("user:thaw")
	public ResultDto thaw(int[] ids) {
		try {
			return userService.updateState(ids, true);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，激活失败，请联系管理员");
		}
	}

	/**
	 * 
	 * @Title: showCode
	 * @Description: 生成验证码
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "/code", method = RequestMethod.GET)
	void showCode(HttpSession session, HttpServletResponse response) {
		// 设置页面不缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		// 获取验证码
		String code = CodeUtil.getCode();
		session.setAttribute("code", code);
		BufferedImage image = CodeUtil.createCodeImage(code);
		try {
			ImageIO.write(image, "JPEG", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: checkCode
	 * @Description: 验证验证码
	 * @param code
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/check/code")
	@ResponseBody
	ResultDto checkCode(String code, HttpSession session) {
		return checkCode(code, session.getAttribute("code").toString());
	}

	/**
	 * 
	 * @Title: checkCode
	 * @Description: 校验验证码
	 * @param code
	 * @param checkCode
	 * @return
	 */
	private ResultDto checkCode(String code, String checkCode) {
		if (StringUtil.isEmpty(code)) {
			return ResultDto.build(500, "验证码不能为空！");
		}
		if (!code.equals(checkCode)) {
			return ResultDto.build(500, "验证码错误！");
		}
		return ResultDto.ok();
	}

	/**
	 * 
	 * @Title: loginUser
	 * @Description: 登录页面
	 * @return
	 */
	/*@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	ResultDto loginUser(String userName, String password, String code, Boolean remeber,
			HttpServletRequest request, HttpSession session, HttpServletResponse response) {
		ResultDto checkCode = checkCode(code, session.getAttribute("code").toString());
		if (checkCode.getStatus() != 200) {
			return checkCode;
		}
		if (StringUtil.isEmpty(userName)) {
			return ResultDto.build(500, "账号不能为空");
		}
		if (StringUtil.isEmpty(password)) {
			return ResultDto.build(500, "密码不能为空");
		}
		User user = userService.loginUser(userName, password);
		if (user == null) {
			return ResultDto.build(500, "账号或密码错误");
		}
		// 将用户存入session
		user.setPassword("");
		session.setAttribute("user", user);
		// 判断是否选择了记住密码
		if (remeber == null || !remeber) {
			return ResultDto.ok();
		}
		// 判断cookie是否存在
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("userName")) {
				return ResultDto.ok();
			}
		}
		// 将用户名存入cookie
		String encodeName = null;
		try {
			encodeName = URLEncoder.encode(userName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Cookie cookieUser = new Cookie("userName", encodeName);
		cookieUser.setMaxAge(60 * 60 * 24 * 30);
		cookieUser.setPath("/");
		response.addCookie(cookieUser);
		// 将密码存入cookie
		String encodePwd = null;
		try {
			encodePwd = java.net.URLEncoder.encode(password, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Cookie cookiePwd = new Cookie("password", encodePwd);
		cookiePwd.setMaxAge(60 * 60 * 24 * 30);
		cookiePwd.setPath("/");
		response.addCookie(cookiePwd);
		return ResultDto.ok();
	}*/
	
	/**
	 * 
	 * @Title: showLogin
	 * @Description: 登录页面
	 * @return
	 */
	@RequestMapping("/login")
	String showLogin() {
		return "login";
	}
	
	/**
	 * 
	 * @Title: logout
	 * @Description: 用户退出
	 * @param session
	 * @return
	 */
	/*@RequestMapping("/exit")
	String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}*/

}
