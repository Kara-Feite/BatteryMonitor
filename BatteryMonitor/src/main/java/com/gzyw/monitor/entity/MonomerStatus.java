package com.gzyw.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @ClassName: MonomerStatus
 * @Description: 单体状态
 * @author LW
 * @date 2018年6月4日 下午1:30:46
 */
@Entity
@Table(name = "bm_monomer_status")
public class MonomerStatus implements Serializable {

	/**
	 * @Fields serialVersionUID : 序列化标识
	 */
	private static final long serialVersionUID = 7924368843303915306L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 电压高
	 */
	@Column(name = "is_high_voltage", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isHighVoltage;

	/**
	 * 电压低
	 */
	@Column(name = "is_low_voltage", length = 1, nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isLowVoltage;

	/**
	 * 内阻高
	 */
	@Column(name = "is_high_inter", length = 1, nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isHighInter;

	/**
	 * 温度高
	 */
	@Column(name = "is_high_tempera", length = 1, nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
	private Boolean isHighTempera;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	/**
	 * 电池组信息
	 */
	@OneToOne(mappedBy = "monomerStatus")
	private GroupInfo groupInfo;

	/**
	 * @Title: MonomerStatus
	 * @Description: MonomerStatus构造函数
	 */
	public MonomerStatus() {}

	/**
	 * @Title: MonomerStatus
	 * @Description: MonomerStatus构造函数
	 * @param isHighVoltage
	 * @param isLowVoltage
	 * @param isHighInter
	 * @param isHighTempera
	 * @param createTime
	 */
	public MonomerStatus(Boolean isHighVoltage, Boolean isLowVoltage, Boolean isHighInter, Boolean isHighTempera,
			Date createTime) {
		this.isHighVoltage = isHighVoltage;
		this.isLowVoltage = isLowVoltage;
		this.isHighInter = isHighInter;
		this.isHighTempera = isHighTempera;
		this.createTime = createTime;
	}

	/**
	 * @Title: getId
	 * @Description: 获取id
	 * @return: Integer id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @Title: setId
	 * @Description: 设置id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @Title: getIsHighVoltage
	 * @Description: 获取isHighVoltage
	 * @return: Boolean isHighVoltage
	 */
	public Boolean getIsHighVoltage() {
		return isHighVoltage;
	}

	/**
	 * @Title: setIsHighVoltage
	 * @Description: 设置isHighVoltage
	 */
	public void setIsHighVoltage(Boolean isHighVoltage) {
		this.isHighVoltage = isHighVoltage;
	}

	/**
	 * @Title: getIsLowVoltage
	 * @Description: 获取isLowVoltage
	 * @return: Boolean isLowVoltage
	 */
	public Boolean getIsLowVoltage() {
		return isLowVoltage;
	}

	/**
	 * @Title: setIsLowVoltage
	 * @Description: 设置isLowVoltage
	 */
	public void setIsLowVoltage(Boolean isLowVoltage) {
		this.isLowVoltage = isLowVoltage;
	}

	/**
	 * @Title: getIsHighInter
	 * @Description: 获取isHighInter
	 * @return: Boolean isHighInter
	 */
	public Boolean getIsHighInter() {
		return isHighInter;
	}

	/**
	 * @Title: setIsHighInter
	 * @Description: 设置isHighInter
	 */
	public void setIsHighInter(Boolean isHighInter) {
		this.isHighInter = isHighInter;
	}

	/**
	 * @Title: getIsHighTempera
	 * @Description: 获取isHighTempera
	 * @return: Boolean isHighTempera
	 */
	public Boolean getIsHighTempera() {
		return isHighTempera;
	}

	/**
	 * @Title: setIsHighTempera
	 * @Description: 设置isHighTempera
	 */
	public void setIsHighTempera(Boolean isHighTempera) {
		this.isHighTempera = isHighTempera;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取createTime
	 * @return: Date createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @Title: setCreateTime
	 * @Description: 设置createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @Title: getGroupInfo
	 * @Description: 获取groupInfo
	 * @return: GroupInfo groupInfo
	 */
	public GroupInfo getGroupInfo() {
		return groupInfo;
	}

	/**
	 * @Title: setGroupInfo
	 * @Description: 设置groupInfo
	 */
	public void setGroupInfo(GroupInfo groupInfo) {
		this.groupInfo = groupInfo;
	}

	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (isHighVoltage) {
			return "电压高";
		}
		if (isHighVoltage) {
			return "电压高";
		}
		if (isLowVoltage) {
			return "电压低";
		}
		if (isHighInter) {
			return "内阻高";
		}
		if (isHighTempera) {
			return "温度高";
		}
		return "正常";
	}

}
