<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>电池管理</title>		
<script src="${ctxStatic}/js/jquery-1.4.4.min.js"></script>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>	
a:active {color: #cc0000; text-decoration: underline;}
a:hover {color: #cc0000; text-decoration: underline;position:relative;left:1px;top:1px}
#denglu *{ vertical-align:middle;}
</style>
<script>
function checkExecl(id){
   	document.myForm.action="${ctx}/battery/exportExecl?groupId=" + id;
  	document.myForm.submit();
 }
</script>
</head>	
<body>
<sf:form name="myForm" method="post" modelAttribute="battery" enctype="multipart/form-data">  
     <table cellSpacing="1" cellPadding="1" width="100%" bgColor="#eeeeee" 
			style="border:0px solid #8ba7e3" border="0">
	    <tr>
			<td colSpan="10" id="denglu">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" bgColor="#ffffff">  	
				  	<tr height="23">
				  		<td id="denglu">	  			
				    		<img src="${ctxStatic}/images/yin.gif" width="22">
			 				<strong>查看：${tree.name}&nbsp;&nbsp;${battery.name}</strong>	    				    					    	
				    	</td>
				    	<td align="right">
				    		<font><b>&nbsp;|&nbsp;</b></font>	  
				    		<a target="mapFrame"
								href="${ctx}/test/data/battery?id=${battery.id}">
								<b><font>表 格</font></b></a>			    		
							<font><b>&nbsp;|&nbsp;</b></font>		
				    		<%-- <a target="mapFrame"
								href="${ctx}/battery/graph?id=${battery.id}">
								<b><font>混合图</font></b></a>						
							<font><b>&nbsp;|&nbsp;</b></font> --%>
							<a target="mapFrame"
								href="${ctx}/battery/graph?id=${battery.id}&treeId=${group.groupId}">
								<b><font>走势图</font></b></a>
							<font><b>&nbsp;|&nbsp;</b></font>
							<a target="batFrame"
								href="javascript:history.go(-1)">
								<b><font>返 回</font></b></a>
							<font><b>&nbsp;|&nbsp;</b></font>
				    	</td> 	    	    			    	
				  	</tr>  	
				</table>					
			</td>
	    </tr>	
	    <tr class="ta_01" bgColor="#f5fafe">    	
	         <td align="center" width="15%">
	          	<strong>制造商</strong><font color="#FF0000">*</font></td>
	         <td bgColor="#ffffff" width="15%">${group.norm.maker}</td>
	         <td align="center" width="20%">
	          	<strong>电池电压/V</strong><font color="#FF0000">*</font></td>
	         <td bgColor="#ffffff" width="10%">${group.norm.volume}</td>         
	         <td align="center" width="20%">
	          	<strong>投运日期</strong><font color="#FF0000">*</font></td>
	         <td bgColor="#ffffff" width="20%">${group.norm.beginTime}</td>       
	    </tr>	    	   
	     <tr class="ta_01" bgColor="#f5fafe">    	
	        <td align="center" bgColor="#f5fafe" >
	          	<strong>型 &nbsp;&nbsp; &nbsp;&nbsp;号</strong><font color="#FF0000">*</font></td>
	        <td bgColor="#ffffff">${group.norm.model}</td>	   
	        <td align="center">
	          	<strong>参考内阻/mΩ</strong><font color="#FF0000">*</font></td>  
	        <td bgColor="#ffffff">${group.norm.inter}</td>      
	        <td align="center">
	          	<strong>电 压/V</strong><font color="#FF0000">*</font></td>         
	        <td bgColor="#ffffff">
	        	最大：${maxMinDto.maxVoltage}
         	    &nbsp;&nbsp;&nbsp;&nbsp;
				最小：${maxMinDto.minVoltage}</td>        
	    </tr>
	    <tr class="ta_01" bgColor="#f5fafe">    	
	         <td align="center">
	          	<strong>电池类型</strong><font color="#FF0000">*</font></td>
	         <td bgColor="#ffffff">${group.norm.type}</td>
	         <td align="center">
	          	<strong>电池容量/Ah</strong><font color="#FF0000">*</font></td>
	         <td bgColor="#ffffff">${group.norm.volume}</td>	       
	         <td align="center">
	          	<strong>内阻/mΩ</strong><font color="#FF0000">*</font></td>    
	         <td bgColor="#ffffff">
	         	最大：${maxMinDto.maxInter}
	          	&nbsp;&nbsp;&nbsp;&nbsp;
	          	最小：${maxMinDto.minInter}</td>               	        
	    </tr>	 	    	   	    						
	</table>					
	</sf:form>
</body>
</html>