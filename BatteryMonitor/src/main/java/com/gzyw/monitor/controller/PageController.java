package com.gzyw.monitor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 * @ClassName: ErrorController
 * @Description: 错误Controller是
 * @author LW
 * @date 2018年7月17日 下午4:06:29
 */
@Controller
public class PageController {

	/**
	 * 
	 * @Title: pageNotFind
	 * @Description: 404
	 * @return
	 */
	@RequestMapping("/404")
	String pageNotFind() {
		return "error/404";
	}

	/**
	 * 
	 * @Title: showError
	 * @Description: 错误页面
	 * @return
	 */
	@RequestMapping("/error")
	String showError() {
		return "error/error";
	}

	/**
	 * 
	 * @Title: showRefuse
	 * @Description: 无权限页面
	 * @return
	 */
	@RequestMapping("/refuse")
	String showRefuse() {
		return "error/refuse";
	}

}
