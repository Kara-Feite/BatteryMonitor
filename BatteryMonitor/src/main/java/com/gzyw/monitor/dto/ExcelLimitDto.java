package com.gzyw.monitor.dto;

import com.gzyw.monitor.annotation.ExcelColumn;

/**
 * 
 * @ClassName: ExcelLimitDto
 * @Description: Excel电池组限制
 * @author LW
 * @date 2018年8月15日 上午10:10:40
 */
public class ExcelLimitDto {
	
	/**
	 * 组电压高越限/V
	 */
	@ExcelColumn(row = 1, column = 0, value = "组电压高越限/V")
	private Double groupHighVoltage;
	
	/**
	 * 组电压低越限/V
	 */
	@ExcelColumn(row = 1, column = 2, value = "组电压低越限/V")
	private Double groupLowVoltage;
	
	/**
	 * 环境温度高越限/℃
	 */
	@ExcelColumn(row = 1, column = 4, value = "环境温度高越限/℃")
	private Double groupHighTempera;
	
	/**
	 * 单体电压高越限/V
	 */
	@ExcelColumn(row = 2, column = 0, value = "单体电压高越限/V")
	private Double monHighVoltage;
	
	/**
	 * 单体电压低越限/V
	 */
	@ExcelColumn(row = 2, column = 2, value = "单体电压低越限/V")
	private Double monLowVoltage;
	
	/**
	 * 单体温度高越限/℃
	 */
	@ExcelColumn(row = 2, column = 4, value = "单体温度高越限/℃")
	private Double monHighTempera;
	
	/**
	 * 单体内阻高越限/mΩ
	 */
	@ExcelColumn(row = 3, column = 0, value = "单体内阻高越限/mΩ")
	private Double monHighInter;
	
	/**
	 * 是否正常
	 */
	@ExcelColumn(row = 3, column = 4, value = "是否正常")
	private String isNormal;
	
	/**
	 * @Title: LimitDto
	 * @Description: LimitDto构造函数
	 */
	public ExcelLimitDto() {}

	/**
	 * @Title: ExcelLimitDto
	 * @Description: ExcelLimitDto构造函数
	 * @param groupHighVoltage
	 * @param groupLowVoltage
	 * @param groupHighTempera
	 * @param monHighVoltage
	 * @param monLowVoltage
	 * @param monHighTempera
	 * @param monHighInter
	 */
	public ExcelLimitDto(Double groupHighVoltage, Double groupLowVoltage, Double groupHighTempera,
			Double monHighVoltage, Double monLowVoltage, Double monHighTempera, Double monHighInter) {
		this.groupHighVoltage = groupHighVoltage;
		this.groupLowVoltage = groupLowVoltage;
		this.groupHighTempera = groupHighTempera;
		this.monHighVoltage = monHighVoltage;
		this.monLowVoltage = monLowVoltage;
		this.monHighTempera = monHighTempera;
		this.monHighInter = monHighInter;
	}

	/**
	 * @Title: getGroupHighVoltage
	 * @Description: 获取groupHighVoltage
	 * @return: Double groupHighVoltage
	 */
	public Double getGroupHighVoltage() {
		return groupHighVoltage;
	}

	/**
	 * @Title: setGroupHighVoltage
	 * @Description: 设置groupHighVoltage
	 */
	public void setGroupHighVoltage(Double groupHighVoltage) {
		this.groupHighVoltage = groupHighVoltage;
	}

	/**
	 * @Title: getGroupLowVoltage
	 * @Description: 获取groupLowVoltage
	 * @return: Double groupLowVoltage
	 */
	public Double getGroupLowVoltage() {
		return groupLowVoltage;
	}

	/**
	 * @Title: setGroupLowVoltage
	 * @Description: 设置groupLowVoltage
	 */
	public void setGroupLowVoltage(Double groupLowVoltage) {
		this.groupLowVoltage = groupLowVoltage;
	}

	/**
	 * @Title: getGroupHighTempera
	 * @Description: 获取groupHighTempera
	 * @return: Double groupHighTempera
	 */
	public Double getGroupHighTempera() {
		return groupHighTempera;
	}

	/**
	 * @Title: setGroupHighTempera
	 * @Description: 设置groupHighTempera
	 */
	public void setGroupHighTempera(Double groupHighTempera) {
		this.groupHighTempera = groupHighTempera;
	}

	/**
	 * @Title: getMonHighVoltage
	 * @Description: 获取monHighVoltage
	 * @return: Double monHighVoltage
	 */
	public Double getMonHighVoltage() {
		return monHighVoltage;
	}

	/**
	 * @Title: setMonHighVoltage
	 * @Description: 设置monHighVoltage
	 */
	public void setMonHighVoltage(Double monHighVoltage) {
		this.monHighVoltage = monHighVoltage;
	}

	/**
	 * @Title: getMonLowVoltage
	 * @Description: 获取monLowVoltage
	 * @return: Double monLowVoltage
	 */
	public Double getMonLowVoltage() {
		return monLowVoltage;
	}

	/**
	 * @Title: setMonLowVoltage
	 * @Description: 设置monLowVoltage
	 */
	public void setMonLowVoltage(Double monLowVoltage) {
		this.monLowVoltage = monLowVoltage;
	}

	/**
	 * @Title: getMonHighTempera
	 * @Description: 获取monHighTempera
	 * @return: Double monHighTempera
	 */
	public Double getMonHighTempera() {
		return monHighTempera;
	}

	/**
	 * @Title: setMonHighTempera
	 * @Description: 设置monHighTempera
	 */
	public void setMonHighTempera(Double monHighTempera) {
		this.monHighTempera = monHighTempera;
	}

	/**
	 * @Title: getMonHighInter
	 * @Description: 获取monHighInter
	 * @return: Double monHighInter
	 */
	public Double getMonHighInter() {
		return monHighInter;
	}

	/**
	 * @Title: setMonHighInter
	 * @Description: 设置monHighInter
	 */
	public void setMonHighInter(Double monHighInter) {
		this.monHighInter = monHighInter;
	}

	/**
	 * @Title: getIsNormal
	 * @Description: 获取isNormal
	 * @return: String isNormal
	 */
	public String getIsNormal() {
		return isNormal;
	}

	/**
	 * @Title: setIsNormal
	 * @Description: 设置isNormal
	 */
	public void setIsNormal(String isNormal) {
		this.isNormal = isNormal;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "ExcelLimitDto [groupHighVoltage=" + groupHighVoltage + ", groupLowVoltage=" + groupLowVoltage
				+ ", groupHighTempera=" + groupHighTempera + ", monHighVoltage=" + monHighVoltage + ", monLowVoltage="
				+ monLowVoltage + ", monHighTempera=" + monHighTempera + ", monHighInter=" + monHighInter
				+ ", isNormal=" + isNormal + "]";
	}

}
