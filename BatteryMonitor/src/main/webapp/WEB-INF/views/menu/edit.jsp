<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>添加基站</title>
<jsp:include page="/WEB-INF/include/easyui.jsp"></jsp:include>
<style>
a{width: 80px;}
</style>
<script>
function edit(){
	$('#myForm').form('submit',{
		url : '${ctx}/menu/${empty tree.id ? "save" : "update"}',
		onSubmit:function(param){
			return $(this).form('enableValidation').form('validate');
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				<c:if test="${empty tree.id}">
					window.parent.parent.tree.addNodes([result.data]);
			 	</c:if>
			 	<c:if test="${!empty tree.id}">
			 		window.parent.parent.tree.updateNode(result.data);
			 	</c:if>
				window.location.href = '${ctx}/menu/list';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
	    }
	});
}
</script>
</head>
<body>
	<sf:form name="myForm" id='myForm' method="post" modelAttribute="tree" enctype="multipart/form-data">
		<sf:hidden path="id"/>
		<div class="easyui-panel" title="操作：添加基站" style="padding:30px 60px;">
			<div style="margin-bottom:20px">
				<sf:input path="name" size="35" class="easyui-textbox" 
					data-options="label:'名 称:',required:true"/><br><br>
				<sf:input path="remark" size="35" class="easyui-textbox" 
					data-options="label:'备 注:'"/><br><br>
	        	<a href="javascript:void(0)" class="easyui-linkbutton" 
	        		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a>
	      		&nbsp;&nbsp;&nbsp;&nbsp;
	    		<a href="${ctx}/menu/list" class="easyui-linkbutton" 
	      			data-options="iconCls:'icon-reload'">返 回</a>
			</div>
		</div>
	</sf:form>
</body>
</html>