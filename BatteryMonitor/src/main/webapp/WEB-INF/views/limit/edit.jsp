<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/jstl.jsp" %>
<!DOCTYPE html>
<html lang='zh-Hans-CN'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<title>警告门限编辑</title>
<jsp:include page="/WEB-INF/include/module.jsp"></jsp:include>
<link href="${ctxStatic}/css/Style.css" rel="stylesheet">
<style>	
select,input{width:220px;}
</style>
<script>
var isExist = false, isSave = false;
function checkLimit() {
	var type = $('#myForm').find('[name=type]').val(),
		groupId = $('#myForm > div > div.easyui-panel.panel-body > div > span:nth-child(3) > input.textbox-value').val();
	$.ajax({
		'url' : '${ctx}/limit/check',
		'type' : 'get',
		'async' : false,
		'data' : {'type' : type, 'groupId' : groupId},
		'dataType' : 'json',
		'success' : function(result) {
			if (result.status === 200) {
				var msg = "该门限已存在,是否更新？"; 
				if (confirm(msg) === true){ 
					isExist = true;
				}
			} else {
				isSave = true;
			}
		}
	});
}
function edit(){
	$('#myForm').form('submit',{
		url : '${ctx}/limit/${empty limit.id ? "save" : "update"}',
		onSubmit:function(param){
			var flag = $(this).form('enableValidation').form('validate');
			<c:if test="${empty limit.id}">
				if (!flag) {
					return flag;
				}
				checkLimit();
				param.isExist = isExist;
				flag = isExist || isSave;
			</c:if>
			return flag;
		},
		success : function(result) {
			result = JSON.parse(result);
			if (result.status === 200) {
				//$.messager.alert({title:'提示',msg:'${empty user.id ? "添加成功！" : "更新成功！"}'});
				window.location.href = '${ctx}/limit/content?groupId=${groupId}';
			} else {
				$.messager.alert({title:'提示',msg:result.msg});
			}
			isExist = false;
	    }
	});
}
</script>
</head>  
<body>  		
<sf:form name="myForm" id='myForm' method="post" modelAttribute="limit" enctype="multipart/form-data">
	<div class="easyui-panel" title="操作：设置门限告警参数" style="padding:30px 60px;">
		<c:if test="${!empty limit.id}">
			<%-- <input name='groupId' type='hidden' value='${limit.group.id}'> --%>
			<sf:hidden path="groupName"/>
			<sf:hidden path="createTime"/>
			<sf:hidden path="id"/>
		</c:if>
		<div style="margin-bottom:20px">
			<sf:select path="group.groupId" items="${listGroup}" class="easyui-combobox" 
				 data-options="label:'电 池 组：',required:true, editable:false"
				 itemLabel="name" itemValue="id"/><br><br>
			<sf:select path="type" class="easyui-combobox" 
				 data-options="label:'测量类型：',required:true,editable:false,onSelect:judgeWarn">
				 	<sf:option value="电池组电压/V" label="电池组电压/V"/>
				 	<sf:option value="环境温度/℃" label="环境温度/℃"/>
				    <sf:option value="单体电压/V" label="单体电压/V"/>
		            <sf:option value="单体内阻/mΩ" label="单体内阻/mΩ"/>
		            <sf:option value="单体温度/℃" label="单体温度/℃"/>
			</sf:select><br><br>
		
			<sf:select path="state" class="easyui-combobox" 
				 data-options="label:'&nbsp;激&nbsp;&nbsp;活：&nbsp;',required:true,editable:false">  
				     <sf:option value="true" label="激活"/>  
			         <sf:option value="false" label="禁用"/>   
			</sf:select><br><br>
		
			<sf:select path="mold" class="easyui-combobox" 
				 data-options="label:'门限类型：',required:true, editable:false">  
				     <sf:option value="相对" label="相对"/>  
			         <sf:option value="绝对" label="绝对"/>
			</sf:select><br><br>	
				
			<sf:input path="val" class="easyui-numberbox" 
				 data-options="label:'参 考 值：',required:true,min:0,precision:2"/><br><br> 
			<sf:input path="highWarn" class="easyui-numberbox" 
				 data-options="label:'过高告警：',required:true,min:0,precision:2"/><br><br>
			<sf:input path="highPlan" class="easyui-numberbox" 
				 data-options="label:'过高预警：',required:true,min:0,precision:2"/><br><br>
			<div id='low-input'>
				<sf:input path="lowWarn" class="easyui-numberbox" id='lowWarn'
					 data-options="label:'过低告警：',required:true,min:0,precision:2"/><br><br>
				<sf:input path="lowPlan" class="easyui-numberbox" id='lowPlan'
					 data-options="label:'过低预警：',required:true,min:0,precision:2"/><br><br>
			</div>
			<a href="javascript:void(0)" class="easyui-linkbutton" 
           		data-options="iconCls:'icon-save'" onclick="edit()">提 交</a>
         		&nbsp;&nbsp;&nbsp;&nbsp;
       		<a href="${ctx}/limit/content?groupId=${groupId}" class="easyui-linkbutton" 
       			data-options="iconCls:'icon-reload'">返 回</a>	
		</div>
	</div>	    	  		  				 				
</sf:form>
<script>
	var html;
	$(function() {
		html = document.getElementById('low-input').innerHTML;
	});
	function judgeWarn(rec) {
		var low = document.getElementById('low-input');
		if ('单体内阻/mΩ' === rec.value || '单体温度/℃' === rec.value) {
			/* $('#lowWarn').numberbox({ required: false });
			$('#lowPlan').numberbox({ required: false }); */
			low.innerHTML = "";
		} else {
			if (!$(low).children('input').length) {
				$(low).append(html);
				$.parser.parse();
			}
		}
	}
</script>
</body>
</html>