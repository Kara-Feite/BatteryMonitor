package com.gzyw.monitor.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @ClassName: CronUtil
 * @Description: cron工具类
 * @author LW
 * @date 2018年7月20日 下午3:38:12
 */
public class CronUtil {

	/**
	 * cron格式
	 */
	private static final String CRON_DATE_FORMAT = "ss mm HH dd MM ? yyyy";

	/**
	 * 
	 * @Title: getCron
	 * @Description: 根据日期获取Cron
	 * @param date
	 * @return
	 */
	public static String getCron(final Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(CRON_DATE_FORMAT);
		String formatTimeStr = "";
		if (date != null) {
			formatTimeStr = sdf.format(date);
		}
		return formatTimeStr;
	}

	/**
	 * 
	 * @Title: getDate
	 * @Description: 根据cron获取时间
	 * @param cron
	 * @return
	 */
	public static Date getDate(final String cron) {
		if (cron == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(CRON_DATE_FORMAT);
		Date date = null;
		try {
			date = sdf.parse(cron);
		} catch (ParseException e) {
			return null;// 此处缺少异常处理,自己根据需要添加
		}
		return date;
	}
	
	/**
	 * 
	 * @Title: getCron
	 * @Description: 根据unit获取时间字符串对应cron
	 * @param interval 间隔时间字符串
	 * @param unit 1:秒，2:分钟，3:小时，4:天，5:固定小时
	 * @return
	 */
	public static String getCron(final String interval, final int unit) {
		/*if (unit != 5 && Integer.parseInt(interval) >= 60) {
			throw new RuntimeException("interval格式不正确");
		}*/
		String cron = null;
		if (unit == 1) { //秒
			cron = "*/" + interval + " * * * * ?";
		} else if (unit == 2) { //分钟
			cron = "0 */" + interval + " * * * ?";
		} else if (unit == 3) { //小时
			//  */5 * * * * ?
			//  0 */1 * * * ?
			cron = "0 0 */" + interval + " * * ?";
		} else if (unit == 4) { //天
			cron = "0 0 0 */" + interval + " * ?";
		} else if (unit == 5) { //固定小时
			cron = "0 0 " + interval + " * * ?";
		}
		/* else {
			throw new RuntimeException("unit格式不正确");
		}*/
		return cron;
	}
	
	/**
	 * 
	 * @Title: getUnit
	 * @Description: 根据cron获取unit（间隔单位）
	 * @param cron
	 * @return
	 */
	public static int getUnit(final String cron) {
		String[] arr = cron.split("\\s+");
		for (int i = 1, len = arr.length; i <= len; i++) {
			if (arr[i - 1].indexOf("*/") != -1) {
				return i;
			} else if (i > 4) {
				return 5;
			}
		}
		return -1;
	}

}
