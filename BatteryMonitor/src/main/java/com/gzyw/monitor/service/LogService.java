package com.gzyw.monitor.service;

import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.entity.Log;

/**
 * 
 * @ClassName: LogService
 * @Description: 日志服务层
 * @author LW
 * @date 2018年7月9日 下午2:46:03
 */
public interface LogService {

	/**
	 * 
	 * @Title: saveLog
	 * @Description: 添加日志
	 * @param log
	 */
	void saveLog(Log log);

	/**
	 * 
	 * @Title: updateLog
	 * @Description: 更新日志
	 * @param log
	 */
	void updateLog(Log log);

	/**
	 * 
	 * @Title: getDatagrid
	 * @Description: 获取日志分页数据
	 * @return
	 */
	EUDataGridDto getDatagrid(Integer page, Integer rows, String sort, String order, Log log, String userName);

	/**
	 * 
	 * @Title: deleteLog
	 * @Description: 删除日志
	 * @param ids
	 * @return
	 */
	ResultDto deleteLog(int[] ids);
	
}
