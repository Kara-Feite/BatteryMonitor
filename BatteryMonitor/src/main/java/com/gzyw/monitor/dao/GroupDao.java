package com.gzyw.monitor.dao;

import java.util.List;

import com.gzyw.monitor.dto.GroupAndTreeDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.GroupInfo;
import com.gzyw.monitor.entity.Tree;

/**
 * 
 * @ClassName: GroupDao
 * @Description: 电池组持久层
 * @author LW
 * @date 2018年6月14日 下午4:29:15
 */
public interface GroupDao extends BaseDao<Group> {
	
	/**
	 * 
	 * @Title: saveBattery
	 * @Description: 添加num节电池
	 * @param num
	 */
	void saveBattery(int groupId, int num);

	/**
	 * 
	 * @Title: listGroupByRun
	 * @Description: 获取需要运行的电池组
	 * @return
	 */
	List<Group> listGroupByRun();
	
	/**
	 * 
	 * @Title: updateIsRun
	 * @Description: 更新运行状态
	 * @param groupId
	 * @param isRun
	 */
	void updateIsRun(int groupId, boolean isRun);

	/**
	 * 
	 * @Title: listGroupByAll
	 * @Description: 获取所有电池组
	 * @return
	 */
	List<Group> listGroupByAll();

	/**
	 * 
	 * @Title: listGroupByPage
	 * @Description: 获取电池组
	 * @return
	 */
	List<GroupAndTreeDto> listGroupByPage(Integer page, Integer rows, String sort, String order, Tree tree);

	/**
	 * 
	 * @Title: getSystemInfo
	 * @Description: 获取系统信息
	 * @param temp
	 * @return
	 */
	GroupInfo getSystemInfo(TempDto temp);

	/**
	 * 
	 * @Title: getGroupByNorm
	 * @Description: 更加电池规格获取电池
	 * @param id
	 * @return
	 */
	Group getGroupByNorm(Integer id);

}
