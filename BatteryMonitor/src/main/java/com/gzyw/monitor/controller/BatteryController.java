package com.gzyw.monitor.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.common.util.DataUtil;
import com.gzyw.monitor.common.util.ExcelUtil;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ExcelBatteryDto;
import com.gzyw.monitor.dto.GraphDto;
import com.gzyw.monitor.dto.MaxMinDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.dto.TestDataDto;
import com.gzyw.monitor.entity.Battery;
import com.gzyw.monitor.entity.Group;
import com.gzyw.monitor.entity.TestData;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.service.BatteryService;
import com.gzyw.monitor.service.GroupService;
import com.gzyw.monitor.service.TestDataService;
import com.gzyw.monitor.service.TreeService;

/**
 * 
 * @ClassName: BatteryController
 * @Description: 电池控制层
 * @author LW
 * @date 2018年6月14日 下午4:15:38
 */
@Controller
@RequestMapping("/battery")
public class BatteryController {

	@Autowired
	private GroupService groupService;

	@Autowired
	private TreeService treeService;

	@Autowired
	private BatteryService batteryService;
	
	@Autowired
	private TestDataService testDataService;

	/**
	 * 电池组id
	 */
	private Integer groupId;
	
	/**
	 * 电池导出excel格式
	 */
	private ExcelBatteryDto excelBatteryDto;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: home
	 * @Description: 主页面
	 * @param model
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	@Permissions("tree:query")
	public String home(Model model, Integer groupId) {
		this.groupId = groupId;
		model.addAttribute("groupId", groupId);
		return "battery/home";
	}

	/**
	 * 
	 * @Title: top
	 * @Description: 头部页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/title", method = RequestMethod.GET)
	public String top(Model model) {
		model.addAttribute("groupId", groupId);
		return "battery/title";
	}

	/**
	 * 告警
	 * 
	 * @param model
	 * @param moduleId
	 * @return
	 */
	@RequestMapping(value = "/content", method = RequestMethod.GET)
	@Permissions("tree:query")
	public String content(Model model, Integer groupId) {
		Group group = groupService.getGroupById(groupId);
		Tree tree = treeService.getTreeById(groupId);
		model.addAttribute("group", group);
		model.addAttribute("tree", tree);
		return "battery/content";
	}

	/**
	 * 
	 * @Title: datagrid
	 * @Description: 获取电池分页数据
	 * @param page 页码
	 * @param battery 电池
	 * @param groupId 电池组id
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, Battery battery,
			Integer groupId, @RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return batteryService.getDatagrid(page, rows, sort, order, battery, groupId);
	}
	
	/**
	 * 
	 * @Title: exportExportByGroup
	 * @Description: 导出电池组数据
	 */
	@RequestMapping(value = "/export/excel", method = RequestMethod.POST)
	void exportExport(HttpServletResponse response, TempDto temp) {
		if (this.excelBatteryDto == null) {
			return;
		}
		List<TestData> listTestData = testDataService.listTestDataByExport(temp);
		List<TestDataDto> listTestDataDto = new ArrayList<TestDataDto>();
		for (TestData testData : listTestData) {
			listTestDataDto.add(new TestDataDto(testData));
		}
		LinkedHashMap<String, List<Map<Integer, ?>>> map = new LinkedHashMap<String, List<Map<Integer, ?>>>();
		Object[] objs = {this.excelBatteryDto};
		try {
			DataUtil.toDataMap("电池信息", objs, map);
			DataUtil.toDataMap("历史测试信息", listTestDataDto, map);
			temp.setType("voltage");
			byte[] buf1 = testDataService.getChartByBat(temp, "电压");
			temp.setType("inter");
			byte[] buf2 = testDataService.getChartByBat(temp, "内阻");
			DataUtil.toDataMap("0", buf1, map);
			DataUtil.toDataMap("1", buf2, map);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		// 设置每列宽度
		Map<Integer, Integer> columnWidths = new HashMap<>();
		columnWidths.put(0, 20 * 256);
		columnWidths.put(1, 20 * 256);
		columnWidths.put(2, 20 * 256);
		columnWidths.put(3, 21 * 256);
		columnWidths.put(4, 24 * 256);
		columnWidths.put(5, 30 * 256);
		columnWidths.put(6, 0);
		Workbook workbook = ExcelUtil.exportHSExcel("电池信息报表", map, columnWidths, 6);
		try {
			OutputStream output = response.getOutputStream();
			//清空缓存
			response.reset();
			//定义浏览器响应表头，顺带定义下载名
			response.setHeader("Content-disposition", "attachment;filename="+URLEncoder.encode("电池信息报表", "UTF-8")+".xls");
			//定义下载的类型，标明是excel文件
			response.setContentType("application/vnd.ms-excel");
			//这时候把创建好的excel写入到输出流
			workbook.write(output);
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: frame
	 * @Description: frame页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/frame", method = RequestMethod.GET)
	@Permissions("tree:query")
	String frame(Model model, String id) {
		model.addAttribute("id", id);
		return "battery/map/frame";
	}
	
	/**
	 * 
	 * @Title: graph
	 * @Description: 统计图
	 * @param model
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value="/graph")
	String graph(Model model, TempDto temp){
		model.addAttribute("temp", temp);
		GraphDto graphDto = testDataService.getGraphDtoByBat(temp);
		model.addAttribute("graphDto", graphDto);
		return "battery/map/graph";
	}
	
	/**
	 * 
	 * @Title: deleteBattery
	 * @Description: 删除电池
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@SystemControllerLog(type = "基站管理", description = "删除电池")
	@Permissions("tree:delete")
	public ResultDto deleteBattery(int[] ids) {
		try {
			return batteryService.deleteBattery(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}
	
	/**
	 * 
	 * @Title: showTop
	 * @Description: 头部页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/top", method = RequestMethod.GET)
	String showTop(Model model, Integer id){
		Battery battery = batteryService.getBatteryById(id);
		Group group = battery.getGroup();
		Tree tree = treeService.getTreeById(group.getGroupId());
		TempDto temp = new TempDto();
		temp.setId(id);
		MaxMinDto maxMinDto = testDataService.getMaxMinDto(temp);
		model.addAttribute("battery",battery);
		model.addAttribute("group", group);
		model.addAttribute("tree", tree);
		model.addAttribute("maxMinDto", maxMinDto);
		if (maxMinDto != null) {
			this.excelBatteryDto = new ExcelBatteryDto(group, maxMinDto);
		}
		return "battery/map/top";
	}

}
