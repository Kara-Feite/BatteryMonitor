package com.gzyw.monitor.entity;

import java.util.Date;

public class QuartzJob {

	public static final int STATUS_RUNNING = 1;
	
	public static final int STATUS_NOT_RUNNING = 0;
	
	public static final int CONCURRENT_IS = 1;
	
	public static final int CONCURRENT_NOT = 0;

	/** 任务id */
	private Integer jobId;

	/** 任务名称 */
	private String jobName;

	/** 任务分组，任务名称+组名称应该是唯一的 */
	private String jobGroup;

	/** 任务初始状态 0禁用 1启用 2删除 */
	private String jobStatus;

	/** 任务是否有状态（并发） */
	private String isConcurrent = "1";

	/** 任务运行时间表达式 */
	private String cronExpression;

	/** 任务描述 */
	private String description;

	/** 任务调用类在spring中注册的bean id，如果spingId不为空，则按springId查找 */
	private String springId;

	/** 任务调用类名，包名+类名，通过类反射调用 ，如果spingId为空，则按jobClass查找 */
	private String jobClass;

	/** 任务调用的方法名 */
	private String methodName;

	/** 启动时间 */
	private Date startTime;

	/** 前一次运行时间 */
	private Date previousTime;

	/** 下次运行时间 */
	private Date nextTime;

	/**
	 * @Title: getJobId
	 * @Description: 获取jobId
	 * @return: Integer jobId
	 */
	public Integer getJobId() {
		return jobId;
	}

	/**
	 * @Title: setJobId
	 * @Description: 设置jobId
	 */
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	/**
	 * @Title: getJobName
	 * @Description: 获取jobName
	 * @return: String jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @Title: setJobName
	 * @Description: 设置jobName
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @Title: getJobGroup
	 * @Description: 获取jobGroup
	 * @return: String jobGroup
	 */
	public String getJobGroup() {
		return jobGroup;
	}

	/**
	 * @Title: setJobGroup
	 * @Description: 设置jobGroup
	 */
	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	/**
	 * @Title: getJobStatus
	 * @Description: 获取jobStatus
	 * @return: String jobStatus
	 */
	public String getJobStatus() {
		return jobStatus;
	}

	/**
	 * @Title: setJobStatus
	 * @Description: 设置jobStatus
	 */
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * @Title: getIsConcurrent
	 * @Description: 获取isConcurrent
	 * @return: String isConcurrent
	 */
	public String getIsConcurrent() {
		return isConcurrent;
	}

	/**
	 * @Title: setIsConcurrent
	 * @Description: 设置isConcurrent
	 */
	public void setIsConcurrent(String isConcurrent) {
		this.isConcurrent = isConcurrent;
	}

	/**
	 * @Title: getCronExpression
	 * @Description: 获取cronExpression
	 * @return: String cronExpression
	 */
	public String getCronExpression() {
		return cronExpression;
	}

	/**
	 * @Title: setCronExpression
	 * @Description: 设置cronExpression
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	/**
	 * @Title: getDescription
	 * @Description: 获取description
	 * @return: String description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @Title: setDescription
	 * @Description: 设置description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @Title: getSpringId
	 * @Description: 获取springId
	 * @return: String springId
	 */
	public String getSpringId() {
		return springId;
	}

	/**
	 * @Title: setSpringId
	 * @Description: 设置springId
	 */
	public void setSpringId(String springId) {
		this.springId = springId;
	}

	/**
	 * @Title: getJobClass
	 * @Description: 获取jobClass
	 * @return: String jobClass
	 */
	public String getJobClass() {
		return jobClass;
	}

	/**
	 * @Title: setJobClass
	 * @Description: 设置jobClass
	 */
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	/**
	 * @Title: getMethodName
	 * @Description: 获取methodName
	 * @return: String methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @Title: setMethodName
	 * @Description: 设置methodName
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @Title: getStartTime
	 * @Description: 获取startTime
	 * @return: Date startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @Title: setStartTime
	 * @Description: 设置startTime
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @Title: getPreviousTime
	 * @Description: 获取previousTime
	 * @return: Date previousTime
	 */
	public Date getPreviousTime() {
		return previousTime;
	}

	/**
	 * @Title: setPreviousTime
	 * @Description: 设置previousTime
	 */
	public void setPreviousTime(Date previousTime) {
		this.previousTime = previousTime;
	}

	/**
	 * @Title: getNextTime
	 * @Description: 获取nextTime
	 * @return: Date nextTime
	 */
	public Date getNextTime() {
		return nextTime;
	}

	/**
	 * @Title: setNextTime
	 * @Description: 设置nextTime
	 */
	public void setNextTime(Date nextTime) {
		this.nextTime = nextTime;
	}

	/**
	 * @return 
	 * @see java.lang.Object#toString() 
	 */
	@Override
	public String toString() {
		return "QuartzJob [jobId=" + jobId + ", jobName=" + jobName + ", jobGroup=" + jobGroup + ", jobStatus="
				+ jobStatus + ", isConcurrent=" + isConcurrent + ", cronExpression=" + cronExpression + ", description="
				+ description + ", springId=" + springId + ", jobClass=" + jobClass + ", methodName=" + methodName
				+ ", startTime=" + startTime + ", previousTime=" + previousTime + ", nextTime=" + nextTime + "]";
	}
	
}
