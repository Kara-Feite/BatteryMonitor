package com.gzyw.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzyw.monitor.annotation.LogRequestParam;
import com.gzyw.monitor.annotation.Permissions;
import com.gzyw.monitor.annotation.SystemControllerLog;
import com.gzyw.monitor.dto.EUDataGridDto;
import com.gzyw.monitor.dto.ResultDto;
import com.gzyw.monitor.dto.TempDto;
import com.gzyw.monitor.entity.Alarm;
import com.gzyw.monitor.entity.Tree;
import com.gzyw.monitor.entity.User;
import com.gzyw.monitor.service.AlarmService;
import com.gzyw.monitor.service.TreeService;
import com.gzyw.monitor.service.UserService;

/**
 * 
 * @ClassName: AlarmController
 * @Description: 警告控制层
 * @author LW
 * @date 2018年6月19日 上午10:23:06
 */
@Controller
@RequestMapping("/alarm")
public class AlarmController {
	
	/**
	 * 节点服务层接口
	 */
	@Autowired
	private TreeService treeService;

	/**
	 * 警告服务层接口
	 */
	@Autowired
	private AlarmService alarmService;
	
	/**
	 * 用户服务层接口
	 */
	@Autowired
	private UserService userService;
	
	/**
	 * 
	 * @Title: requestParam
	 * @Description: 请求参数
	 * @param request
	 */
	@ModelAttribute
	@LogRequestParam
	public void requestParam(HttpServletRequest request) {}

	/**
	 * 
	 * @Title: content
	 * @Description: content页面
	 * @param model
	 * @param treeId
	 * @return
	 */
	@RequestMapping(value = "/content", method = RequestMethod.GET)
	String content(Model model, Integer treeId) {
		Tree tree = treeService.getTreeById(treeId);
		model.addAttribute("tree", tree);
		model.addAttribute("treeId", treeId);
		return "alarm/content";
	}

	/**
	 * 
	 * @Title: datagrid
	 * @Description: 警告列表数据
	 * @param page 页码
	 * @param temp 临时对象
	 * @param rows 页大小
	 * @param sort 排序字段
	 * @param order 降序或升序
	 * @return
	 */
	@RequestMapping(value = "/datagrid")
	@ResponseBody
	EUDataGridDto datagrid(@RequestParam(value = "page", defaultValue = "1") Integer page, TempDto temp,
			@RequestParam(value = "rows", defaultValue = "20") Integer rows, String sort, String order) {
		return alarmService.getDatagrid(page, rows, sort, order, temp);
	}

	/**
	 * 
	 * @Title: showEdit
	 * @Description: 跳转到编辑页面
	 * @param model
	 * @param id
	 * @param treeId
	 * @return
	 */
	@RequestMapping(value = "/edit")
	String showEdit(Model model, Integer id, Integer treeId) {
		Alarm alarm = alarmService.getAlarmById(id);
		model.addAttribute("alarm", alarm);
		List<User> listUser = userService.listUserByAll();
		model.addAttribute("listUser", listUser);
		model.addAttribute("treeId", treeId);
		return "alarm/edit";
	}
	
	/**
	 * 
	 * @Title: update
	 * @Description: 更新警告
	 * @param user
	 * @param br
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(type = "警告管理", description = "更新警告")
	@Permissions("tree:update")
	public ResultDto update(@Validated Alarm alarm , BindingResult br){
		if(br.hasErrors()){
			List<ObjectError> allErrors = br.getAllErrors();
			StringBuilder sb = new StringBuilder();
			for (ObjectError error : allErrors) {
				sb.append(error.getDefaultMessage());
			}
			return ResultDto.build(500, sb.toString());
		}
		return alarmService.updateAlarm(alarm);
	}
	
	/**
	 * 
	 * @Title: delAlarmByBat
	 * @Description: 删除警告
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	@SystemControllerLog(type = "警告管理", description = "删除警告")
	@Permissions("tree:delete")
	public ResultDto delAlarmByBat(int[] ids) {
		try {
			return alarmService.delAlarmByBat(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDto.build(500, "出现未知错误，删除失败，请联系管理员");
		}
	}

}