package com.gzyw.monitor.security.tag;

import org.apache.shiro.web.tags.PermissionTag;

/**
 * 
 * @ClassName: NotPermissionTag
 * @Description: 没有权限
 * @author LW
 * @date 2018年7月19日 下午3:30:31
 */
public class NotPermissionTag extends PermissionTag {

	/**
	 * @Fields serialVersionUID : 序列化标示
	 */
	private static final long serialVersionUID = 5080473920678880924L;

	@Override
	protected boolean showTagBody(String p) {
		return !isPermitted(p);
	}

}
