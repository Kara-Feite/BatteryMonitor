package com.gzyw.monitor.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gzyw.monitor.common.util.StringUtil;
import com.gzyw.monitor.dao.LimitDao;
import com.gzyw.monitor.entity.Limit;

/**
 * 
 * @ClassName: LimitDaoImpl
 * @Description: 警告门限持久层接口实现类
 * @author LW
 * @date 2018年7月11日 下午3:45:31
 */
@Repository
public class LimitDaoImpl extends BaseDaoImpl<Limit> implements LimitDao {

	@Override
	public Long countLimit(Limit limit, Integer groupId) {
		String hql = "SELECT COUNT(*) FROM Limit";
		hql = setHQLParam(hql, null, null, limit, groupId);
		return count(hql);
	}

	@Override
	public List<Limit> listLimitByPage(Integer page, Integer rows, String sort, String order, Limit limit,
			Integer groupId) {
		String hql = "FROM Limit";
		hql = setHQLParam(hql, sort, order, limit, groupId);
		return queryByPage(hql, page, rows);
	}
	
	@Override
	public Limit getLimitByTypeAndGroup(String type, Integer groupId) {
		String hql = "FROM Limit WHERE type = ? AND group_id = ?";
		return (Limit) uniqueResult(hql, type, groupId);
	}
	
	@Override
	public List<Limit> listLimitByGroup(Integer groupId) {
		String hql = "FROM Limit WHERE group_id = ?";
		return queryForList(hql, groupId);
	}

	/**
	 * 
	 * @Title: setHQLParam
	 * @Description: 设置HQL语句参数
	 * @return
	 */
	private String setHQLParam(String hql, String sort, String order, Limit limit, Integer groupId) {
		StringBuilder sb = new StringBuilder(hql);
		sb.append(" WHERE group_id = " + groupId);
		String groupName = limit.getGroupName();
		String type = limit.getType();
		if (StringUtil.isNotEmpty(groupName)) {
			sb.append("AND groupName LIKE '%" + groupName +"%'");
		}
		if (StringUtil.isNotEmpty(type)) {
			sb.append("AND type LIKE '%" + type + "%'");
		}
		if (StringUtil.isNotEmpty(sort) && StringUtil.isNotEmpty(order)) {
			hql += " ORDER BY " + sort + " " + order;
		}
		return sb.toString();
	}

}
